#ifndef	_ALARM_H
#define	_ALARM_H

#include	"task.h"

#define	ALSTATE_NONE	0
#define	ALSTATE_INUSE	1
#define ALSTATE_HOLD	2

#if	SIM || IPDOOR
#define	TICKS_PER_SEC		18
#define	USEC_PER_TICK		55
#else

#define	TICKS_PER_SEC		100
#define	USEC_PER_TICK		10
#endif

#define	ALRC_WAKEUP	0x3456

typedef	struct	_alarm {
	int	al_state;
	long	al_ticks;		/* when ticks >ticks, wakeup */
	int	al_rcode;		/* return code to send when alarm goes off */
	Task	*al_task;		/* the task to wakeup */
} Alarm;

void	alarm_init(); 		/* trap int vector */
void	alarm_close();		/* reset int vector */

#define	MAX_ALARMS	10

#ifdef	__cplusplus
extern "C" {
#endif

int	alarm_sleep(struct timeval *when, int rc);	/* wake current task at when, with rc */
int	alarm_reset(int al);	/* reset this alarm */

unsigned long	Ticks();		/* get tick count */
unsigned long	Ticks100();		/* get time in 100ths */
unsigned long	Time(long *);		/* get time in seconds */
void	Settime(long t);	/* set the time in seconds */

#ifdef	__cplusplus
};
#endif

#endif

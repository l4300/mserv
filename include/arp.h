#ifndef	_ARP_H
#define	_ARP_H

#define	AR_HARDWARE	1	/* ethernet hardware type code */

#define	AR_REQUEST	1	/* arp request to resolve addr */
#define	AR_REPLY	2	/* arp reply to resolve request */

#define	RA_REQUEST	3	/* reverse arp request */
#define	RA_REPLY	4	/* rev arp reply */

typedef	struct	arp {
	unsigned	ar_hwtype;	/* hw type */
	unsigned	ar_prtype;	/* protocol type */
	char		ar_hwlen;	/* hardware address length */
	char		ar_prlen;	/* protocol address length */
	unsigned	ar_op;		/* arp operation AR_ RA_ */
	char		ar_addrs[1];	/* data */
} ARP_Packet;


#define	SHA(p)		(&p->ar_addrs[0])
#define	SPA(p)		(&p->ar_addrs[p->ar_hwlen])
#define	THA(p)		(&p->ar_addrs[p->ar_hwlen+p->ar_prlen])
#define	TPA(p)		(&p->ar_addrs[(p->ar_hwlen*2) + p->ar_prlen])

#define	MAXHWALEN	ETHER_HWA_LEN
#define	MAXPRALEN	sizeof(IPaddr)

#define	ARP_TSIZE	16		/* arp cache table size */
#define	ARP_QSIZE	10		/* arp port queue size */

#define	ARP_TIMEOUT	600		/* timeout in 1/100's */
#define	ARP_RESEND	1		/* resend if no reply */
#define	ARP_MAXRETRY	4

typedef struct	arpentry {
	int		ae_state;	/* state of this entry */
	int		ae_hwtype;	/* hardware type */
	int		ae_prtype;	/* protocol type */
	int		ae_hwlen;	/* hw len */
	int             ae_prlen;	/* protocol len */
	Net_Interface	*ae_pni;	/* net interface we got this on */
	Queue		ae_queue;	/* queue of packets waiting to go out */
	int		ae_attempts;	/* number of retries */
	int		ae_ttl;		/* time to live */
	unsigned char	ae_hwa[MAXHWALEN];
	unsigned char	ae_pra[MAXPRALEN];
} Arp_Entry;


#define	AS_FREE		0
#define	AS_PENDING	1
#define	AS_RESOLVED	2

Arp_Entry	*arpfind(void *pra, int prtype, Net_Interface *pni);
Arp_Entry	*arpalloc();
Arp_Entry	*arpadd(struct netif *pni, struct arp *parp);
void		arpqsend(Arp_Entry *pae);
int		arp_in(struct netif *pni, struct ep *pep);
void		arptime(int gran);
void		arpdq(Arp_Entry *pae);
int		arpsend(Arp_Entry	*pae);
int		arpAddProxy(struct netif *pni, IPaddr addr);
int		arpDeleteProxy(struct netif *pni, IPaddr addr);
#endif
#ifndef	_BUFS_H
#define	_BUFS_H

#include	"queue.h"

#ifdef	__cplusplus
extern "C" {
#endif

typedef	struct	_buffer {
	Queue_Element	QE;		/* priority = buffer size */
	unsigned char	data[1];	/* actually larger */
} Buffer;

int	BufferInitialize(int count, int size);	/* create this many bufs */
int	BufferCreate(Queue *Queue, int count, int size, int type); /* put em here */

#define	BUFFER_RESULT_OK	 0
#define	BUFFER_RESULT_NOMEM	-1

#if	BUFDEBUG
Buffer	*BufferAllocX(int size, char *file, unsigned lnum);
#define	BufferAlloc(size)	BufferAllocX(size,__FILE__,__LINE__)
#else
Buffer	*BufferAlloc(int size);		/* get a buffer */
#endif
void	BufferFree(Buffer *B);		/* free a buffer */
// void	BufferDump();

#define	DataToBuf(d)		((Buffer *) (((char *) (d)) - sizeof(Queue_Element)))
#define	DataToQE(d)		((Queue_Element *) DataToBuf((d)))
#define	BufferFreeData(m)	(BufferFree(DataToBuf(m)))
#define	MessageBoxReceiveBuf(M)	((Buffer *) MessageBoxReceive((M)))

extern	Queue	_Buffers;
extern	int	minbufs;
#ifdef	__cplusplus
};
#endif

#endif

#ifndef	_COMPAT_H
#define	_COMPAT_H

#ifdef	__cplusplus
extern "C" {
#endif

void	bzero(char *c, int len);
void	bcopy(char *c, char *d, int len);
unsigned long	inet_addr(char *c);
int	inet_addr_parse(char *c, IPaddr s);	/* returns non-zero on error */
void	panic(char *c, ...);
char *	inet_ntoa(struct in_addr in);

unsigned long	ntohl(unsigned long s);
unsigned long	htonl(unsigned long s);
unsigned long	lswap(unsigned long *s);
unsigned int	htons(unsigned int s);
unsigned int	ntohs(unsigned int s);

gettimeofday(struct timeval *t, struct timezone *m);

#ifdef	__cplusplus
};
#endif

#endif

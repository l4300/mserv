#ifndef	_CONF_H
#define	_CONF_H

#define	MAXLRGBUF	8192		/* max size frag packet we can handle */
#define	BPMAXB		MAXLRGBUF
#define	MAXNETBUF	1514

#if	IPDOOR
#define	Ntcp		5
#else
#define	Ntcp		30		/* max number of tcp connections */
#endif

#ifndef	DONEAR
#define	DONEAR		0
#endif
#endif

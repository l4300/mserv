#ifndef	_EP_H
#define	_EP_H

/* generic interface packet */

#define	EP_ALEN	6

typedef struct	ep {
	unsigned char	ep_dst[NI_MAXHWA];
	unsigned char	ep_src[NI_MAXHWA];
	unsigned	ep_type;		/* packet type */
	unsigned	ep_len;			/* length of data area */
	IPaddr		ep_nexthop;		/* next hop address */
	unsigned char	ep_data[1];
} EP;
#define	EP_CRC	0	/* size of crc section */

#define	EP_HLEN	(sizeof(EP))

#endif
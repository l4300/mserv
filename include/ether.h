#ifndef	_ETHER_H
#define	_ETHER_H

#define	ETHER_HWA_LEN	6

/* these defines are in host byte order */

#define	EPT_IP		0x0800
#define	EPT_RARP        0x8035
#define	EPT_ARP		0x0806
#define	EPT_PPP		0x8181	// type used by PPP driver when sending PPP control packets

#define	ETHERMTU	1500
#define	ETHERMIN	(60-14)

typedef struct ether {		/* ethernet packet */
	unsigned char	ep_dst[ETHER_HWA_LEN];
	unsigned char	ep_src[ETHER_HWA_LEN];
	unsigned int	ep_type;	/* ethernet type */
	unsigned char	ep_data[1];
} Ether_Packet;

#endif
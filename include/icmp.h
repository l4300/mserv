#ifndef	_ICMP_H
#define	_ICMP_H
/* ic_type field */

#define	ICT_ECHORP	0
#define	ICT_DESTUR	3
#define	ICT_SRCQ	4
#define	ICT_REDIRECT	5
#define	ICT_ECHORQ	8
#define	ICT_TIMEX	11
#define	ICT_PARAMP	12
#define	ICT_TIMERQ	13
#define	ICT_TIMERP	14
#define ICT_INFORQ	15
#define	ICT_INFORP	16
#define	ICT_MASKRQ	17
#define	ICT_MASKRP	18

/* ic_code field */
#define	ICC_NETUR	0
#define	ICC_HOSTUR	1
#define	ICC_PROTOUR	2
#define	ICC_PORTUR	3
#define	ICC_FNADF	4
#define	ICC_SRCRT	5

#define ICC_NETRD	0
#define	ICC_HOSTRD	1
#define IC_TOSNRD	2
#define	IC_TOSHRD	3

#define ICC_TIMEX	0
#define	ICC_FTIMEX	1

#define	IC_HLEN		8
#define	IC_PADLEN	3

#define	IC_RDTTL	300

/* ICMP packet format (following the IP header)			*/

struct		icmp	{
		char	ic_type;
		char	ic_code;
		short	ic_cksum;

		union	{
			struct	{
				short	ic1_id;
				short	ic1_seq;
			} ic1;
			IPaddr	ic2_gw;
			struct {
				char	ic3_ptr;
				char	ic3_pad[IC_PADLEN];
			} ic3;
			int	ic4_mbz;
		} icu;
		char	ic_data[1];
};

/* format 1 */
#define	ic_id	icu.ic1.ic1_id
#define ic_seq	icu.ic1.ic1_seq

/* format 2 */
#define	ic_gw	icu.ic2_gw

/* format 3 */
#define	ic_ptr	icu.ic3.ic3_ptr
#define	ic_pad	icu.ic3.ic3_pad

/* format 4 */
#define	ic_mbz	icu.ic4_mbz

int	icmp_in(struct netif *pni, struct ep *pep);
int	icredirect(struct ep *pep);
int	setmask(int inum, IPaddr mask);
void	icsetsrc(struct ip *pip);
int	icmp(int type, int code, IPaddr dst, char *pa1, char *pa2);
Bool	icerrok(struct ep *pep);
struct	ep * icsetbuf(int type, char *pa1, Bool *pisresp, Bool *piserr);
int	icsetdata(int type, struct ip *pip, char *pa2);
void	ipredirect(struct ep *pep, int ifnum, struct route *prt);


#endif

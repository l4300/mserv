#ifndef	_IOCONF_H
#define	_IOCONF_H

/* I/O configuration information and constants for all serial, par
   mio and xio operations

*/

#define	AF_IOSOCKET		5
#define	IOSOCKET_PARALLEL	0
#define	IOSOCKET_SERIAL		1
#define	IOSOCKET_MIO		2
#define	IOSOCKET_XIO		3

struct	sockaddr_io {
	unsigned int	sin_family;
	unsigned int	sin_port;  	/* randomly set by bind */
	unsigned int	sin_mode;	/* mode flags */
	char	sin_zero[10];
};

/* choose only one from each of the following */

#define	IOMODES_OUTPUT		0x0f	/* reserved for output modes */
#define	IOMODE_OUTPUT		0x01	/* output is allowed */

#define	IOMODES_INPUT		0xf0	/* input mode mask */
#define	IOMODE_CINPUT		0x10	/* cooked (line oriented) input */
#define	IOMODE_RINPUT		0x20	/* raw input (serial only) */
#define	IOMODE_EPP		0x40	/* EPP input (par bi-tronics) */

#define	IO_IPMODE_NONE		0x00	/* input processing mode */
#define	IO_OPMODE_NONE		0x00	/* output processing mode */

#define	IO_IFMODE_STOP1BIT	0x00
#define	IO_IFMODE_STOP2BIT	0x04
#define	IO_IFMODE_DATA7BIT	0x00
#define	IO_IFMODE_DATA8BIT	0x08
#define	IO_IFMODE_PARITY_NONE	0x00
#define	IO_IFMODE_PARITY_ZERO	0x10
#define	IO_IFMODE_PARITY_ODD	0x20
#define	IO_IFMODE_PARITY_EVEN	0x30
#define	IO_IFMODE_FLOW_XON	0x40
#define	IO_IFMODE_FLOW_RTS	0x80

/* set and get these socket options */

#define	IO_SOPT_IOMODE		1	/* lowest level input-output mode */
#define	IO_SOPT_IPMODE		2	/* input processing modes */
#define	IO_SOPT_OPMODE		3	/* output processing modes */
#define	IO_SOPT_IFMODE		4	/* flow, bits, etc */
#define	IO_SOPT_SPEED		5	/* baud rate */
#define	IO_SOPT_IBSIZE		6	/* set input buffer size (bytes) */
#define	IO_SOPT_OBSIZE		7	/* set output buffer size (bytes) */
#define	IO_SOPT_IMINCOUNT	8	/* set input min byte count for read post */
#define	IO_SOPT_ITIMEOUT	9	/* set input timeout (100ths sec) for read post */
#define	IO_SOPT_IBUFSIZE	10	/* set/get input buffer allocation size */
#define	IO_SOPT_DEVTYPE		11
#define	IO_SOPT_AIOMODES	12
#define	IO_SOPT_AIPMODES	13
#define	IO_SOPT_AOPMODES	14
#define	IO_SOPT_DEVFLAGS	15
#define	IO_SOPT_DEVTIMER	16	/* read timer only */
#define	IO_SOPT_DEVSTATUS	17	/* return device specific status string */

#define	DEFAULT_OBYTELIMIT	2048
#define	DEFAULT_IBYTELIMIT	1024
#define	DEFAULT_IBUFSIZE	128	/* buffers allocated using this size */
#define	MAX_INPUT_BUFFERS	8	/* won't allocate more than this many
					   input buffers */

#define	HOLDOFF_COUNT		64	/* how many remaining free chars before hold off? */

int	iosocket_init();
int	iosocket_close();

#endif
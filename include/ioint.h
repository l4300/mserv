#ifndef	_IOINT_H
#define	_IOINT_H
/* internal defines for I/O sockets */

#include	"ioconf.h"
#include	"semaphore.h"

typedef	int	(*_io_function)(struct _io_port *, int mode, void *arg);

#define	IOFUNCTION_PROBE	1	/* detect device (nonzero nosuch dev) */
#define	IOFUNCTION_INIT		2	/* initialize self */
#define	IOFUNCTION_OPEN		3	/* open device */
#define	IOFUNCTION_SETOPT	4	/* set operational option or mode */
#define	IOFUNCTION_CLOSE	5	/* close device */
#define	IOFUNCTION_SHUTDOWN	6	/* shutdown self */
#define	IOFUNCTION_STATUS	7	/* query status */
#define	IOFUNCTION_KICKSTART	8	/* kick start device */
#define	IOFUNCTION_TIMER	9	/* timer went off */
#define	IOFUNCTION_RESTART	10	/* restart client input from held off state */


#define	IODEVFLAG_NEEDTIMER	0x01	/* need timer service */
#define	IODEVFLAG_ACTIVE	0x02	/* device exists */
#define	IODEVFLAG_NEEDRESTART	0x04	/* input held off, call restart */
#define	IODEVFLAG_CLOSED	0x08	/* device is closed */
#define	IODEVFLAG_HELDOFF	0x10	/* output held off */
#define	IODEVFLAG_IOACTIVE	0x20	/* don't need kickstart */
#define	IODEVFLAG_MSRECV	0x40	/* macro service in recv is active */
#define	IODEVFLAG_MSXMIT	0x80	/* macro service in xmit is active */

typedef	struct	_io_port {	/* this struct holds physical device info */
	int		io_socket_type;	/* IOSOCKET_PARALLEL ... etc */
	int		io_unit_number;	/* which unit it is */
	int		io_base_address;/* base address of unit... serial, etc */
				/* for parallel microserve, its not used
				   for serial microserve, its either 0 or 1 for port
				*/
	int		io_interrupt;	/* which interrupt to use (int vector *) */

	/* begin variable mode information */
	unsigned int   	io_allowed_modes;	/* bitmask of allowed modes */
	unsigned int	io_allowed_ipost;	/* allowed input processing */
	unsigned int	io_allowed_opost;	/* allowed output processing */
	unsigned int	io_devflags;		/* internal device flags */

	_io_function	io_function;		/* func process to call */

	int		io_timeout;		/* 100's second counts down */
	unsigned int	io_device_type;		/* for use later, EPP dev type, etc RO */
	unsigned int	io_desired_modes;	/* only one of each class allowed  RW */
	unsigned int	io_ipost_process;	/* input processing to be applied  RW */
	unsigned int	io_opost_process;       /* output processing to be applied RW */
	unsigned int	io_speed;		/* output speed RW */
	unsigned int	io_intfmodes;		/* interface modes, parity, etc */

	Semaphore	io_owner_semaphore;	/* only the current owner is active on it */
	Semaphore	io_close_semaphore;	/* signal when last byte is output */
	struct _socket	*io_socket;		/* currently active socket */
	Queue		io_obuffers;		/* output buffer queue */
	unsigned char	*io_ochar;		/* current output character */
	unsigned int	io_occount;		/* current count of output chars in this buf */
	Queue		io_ibuffers;		/* input buffer queue */
	unsigned char	*io_ichar;		/* current input character */
	unsigned int	io_iccount;		/* remaining space in this buffer */
	unsigned int	io_ibytecount;		/* bytes used in current buffer */
	int		io_ibufsize;		/* input buffer size (individual) */
	Queue		io_fbuffers;		/* free input buffers */
	unsigned int	io_iminbytes;		/* min number of input bytes to post */
	unsigned int	io_imintimeout;		/* min timeout (100ths) to post data */

	void	* 	io_p1;			/* private ptr */
} IO_Port;

#define	IOTASK_NAME		"io"
#define	IOTASK_STACKSIZE        1024
#define	IOTASK_PRIORITY		100

#endif
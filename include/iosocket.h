#ifndef	_IOSOCKET_H
#define	_IOSOCKET_H

#define	IO_BUFFER_SIZE	512
#define	MAX_IO_SOCKETS	2


#define	AF_IOSOCKET	5
#define	IOSOCKET_PARALLEL	0
#define	IOSOCKET_SERIAL		1


#define	IOSOCKET_PAR_TASKNAME	"io_driver"
#define	IOSOCKET_PAR_PRIORITY	100
#define	IOSOCKET_PAR_STACK	512

#define	PAR_QUEUE_SIZE		2

#define	IOSINMODE_NORMAL	0
#define	IOSINMODE_READ		1	/* specified during connect phase */

struct	sockaddr_io {
	unsigned int	sin_family;
	unsigned int	sin_port;  	/* randomly set by bind */
	unsigned int	sin_mode;	/* mode flags */
	char	sin_zero[10];
};

struct	_io_socket {
	unsigned int	io_mode;
	unsigned char   io_type;
#define	IO_TYPE_SERIAL	1
#define	IO_TYPE_PARR	0
	unsigned	int	io_baudrate;		/* user level baud */
			/* 0 means 115KB */
	unsigned	char	io_databits;		/* 8 7 6 etc */
#define	IO_DATABITS_8	1
#define	IO_DATABITS_7	0
	unsigned	char	io_stopbits;
#define	IO_STOPBITS_1	0
#define	IO_STOPBITS_2	1
	unsigned 	char	io_parity;
#define	IO_PARITY_NONE	0
#define	IO_PARITY_ZERO	1
#define	IO_PARITY_ODD	2
#define	IO_PARITY_EVEN	3
	unsigned	char	io_flowcontrol;
#define	IO_FLOW_NONE	0
#define	IO_FLOW_DTR	1
#define	IO_FLOW_XON	2
	unsigned	char	io_postprocess;
#define	IO_POST_NLCR	1
	unsigned	char	io_outbuffer[IO_BUFFER_SIZE];
	unsigned	char	*io_outptr;
	unsigned	char	*io_inptr;
	Buffer			*io_buffer;	/* for serial only */
	Semaphore		io_semaphore;
	int			io_outlimit;
	int			io_inbase;
	int			io_incount;
	int			io_outcount;	/* number of bytes in the outgoing buffer */
	Semaphore		io_outsemaphore;    /* block waiting to write */
	struct	_socket		*io_socket;
	int			(*io_kickstart)(struct _io_socket * sock, char *data, int len);
	Queue			io_buffers;	/* outgoing buffers */
};

int	iosocket_init(void *stack, int stacksize);
int	iosocket_stacksize();
#endif


#ifndef	_IP_H
#define	_IP_H
#if	_IP_C && DONEAR
#define	NEAR	near
#else
#define	NEAR
#endif
#include	<nettypes.h>

#define	IP_ALEN	4
typedef	unsigned char	IPaddr[IP_ALEN];

#define	IP_CLASSA(x)	((x[0] & 0x80) == 0x00)
#define	IP_CLASSB(x)	((x[0] & 0xc0) == 0x80)
#define	IP_CLASSC(x)	((x[0] & 0xe0) == 0xc0)
#define	IP_CLASSD(x)	((x[0] & 0xf0) == 0xe0)
#define	IP_CLASSE(x)	((x[0] & 0xf8) == 0xf0)

#define	IPT_ICMP	1	/* icmp protocol */
#define	IPT_TCP		6
#define	IPT_EGP		8
#define	IPT_UDP		17

typedef struct ip {
	unsigned char	ip_verlen;
	unsigned char	ip_tos;
	unsigned	ip_len;
	unsigned	ip_id;
	unsigned	ip_fragoff;
	unsigned char	ip_ttl;
	unsigned char	ip_proto;
	unsigned	ip_cksum;
	IPaddr		ip_src;
	IPaddr		ip_dst;
	unsigned char	ip_data[1];
} IP;

#define	IP_VERSION	4
#define	IP_MINHLEN	5
#define	IP_TTL		250

#define	IP_MF		0x2000	/* more fragments */
#define	IP_DF		0x4000	/* don't fragment */
#define	IP_FRAGOFF	0x1fff	/* frag mask */
#define	IP_PREC		0xe0	/* precedence mask for tos */

#define	IP_HLEN(pip)	((pip->ip_verlen & 0xf) << 2)
#define	IPMHLEN	20	/* minimum header length */

#define	IPO_COPY	0x80	/* copy on fragment mask */
#define	IPO_CLASS	0x60	/* option class */
#define	IPO_NUM		0x17	/* option number */

#define	IPO_EOOP	0x00	/* end of options */
#define	IPO_NOP		0x01	/* no op */
#define	IPO_SEC		0x82	/* dod security */
#define	IPO_LSRCRT	0x83	/* loose source routing */
#define IPO_SSRCRT	0x89	/* strict source routing */
#define	IPO_RECRT	0x07	/* record route */
#define	IPO_STRID	0x88	/* stream ID */
#define	IPO_TIME	0x44	/* timestamp */

#define	IP_MAXLEN	BPMAXB-EP_HLEN	/* max packet size */

#if	!IPDOOR
#define	IPPROC_STACK	512
#else
#if	MBBS
#define	IPPROC_STACK	8192
#else
#define	IPPROC_STACK	3072
#endif
#endif
#define	IPPROC_PRIORITY	100	/* process priority */
#define	IPNAME	"ip"		/* process name */
#define	IPARGC	0		/* number of args */
#define	IP_QUEUE_SIZE	100	/* max number of messages allowed in IP queue */

extern 	IPaddr	ip_maskall, ip_anyaddr, ip_loopback;

extern	int		gateway;
extern 	struct _task	*iptask;
int			ipproc(void *parent, char *name, int nargs, char **args);

struct	ep   *	  NEAR	ipgetp();
int			cksum(unsigned int *buf, int nwords);
void		  NEAR	ipdbc(int ifnum, struct ep *pep, struct route *prt);
Bool		  	isbrc(IPaddr dest);
#if	!DOINLINE
struct	ip   *	iph2net(struct ip *pip);
struct  ip   *	ipnet2h(struct ip *pip);
#endif
int			ipsend(IPaddr faddr, struct ep *pep, int datalen);
int		  NEAR	ipputp(int ifnum, IPaddr nh, struct ep *pep);
int		  NEAR	ipfsend(struct netif *pni, IPaddr nexthop, struct ep *pep, int offset, int maxdlen, int offindg);
int		  NEAR	ipfhcopy(struct ep *pepto, struct ep *pepfrom, int offindg);
struct ep     *	  NEAR	ipreass(struct ep *pep);
Bool		  NEAR	ipfadd(struct ipfq *iq, struct ep *pep);
struct	ep    *	  NEAR	ipfjoin(struct ipfq *iq);
struct ep     *	  NEAR	ipfcons(struct ipfq *iq);
void                    ipftimer(int gran);
void			ipfinit();

void		  	ipredirect(struct ep *pep, int ifnum, struct route *prt);
int		  	local_out(struct ep *pep);
#endif

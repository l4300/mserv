#ifndef	_IPREASS_H
#define	_IPREASS_H

#define	IP_FQSIZE	10	/* number of frags in queue */
#define	IP_MAXNF	8	/* max frags per datagram */
#define	IP_FTTL		2	/* ttl in seconds for each frag */

#define	IPFF_VALID	1
#define	IPFF_BOGUS	2
#define	IPFF_FREE	0

struct ipfq {
	char	ipf_state;
	IPaddr	ipf_src;
	short	ipf_id;
	int	ipf_ttl;
	Queue	ipf_q;
};

extern	Semaphore	ipfmutex;
extern	struct	ipfq	ipfqt[];
#endif
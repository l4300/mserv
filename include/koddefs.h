#ifndef	_KODDEF_H
#define	_KODDEF_H
/*
 * 8005 buffer memory partitions.
 */

#define	FIFO_DEPTH	18

#define TX_BUFFER_START	0x0000
#define RX_BUFFER_START	0x8000
#define TX_BUFFER_END	0xffff
#define RX_BUFFER_END	0xffff

/* 8005 register addresses. */
#define COMMAND_PORT     (reg_base+0x00)
#define STATUS_PORT      (reg_base+0x00)
#define	FIFO_PORT	 (reg_base+0x01)
#define CONFIG_1_PORT    (reg_base+0x02)
#define CONFIG_2_PORT    (reg_base+0x04)
#define REA_PTR_PORT     (reg_base+0x06)
#define WINDOW_PORT      (reg_base+0x08)
#define RX_PTR_PORT      (reg_base+0x0a)
#define TX_PTR_PORT      (reg_base+0x0c)
#define DMA_ADDR_PORT    (reg_base+0x0e)

/* 8005 command register bits. */
#define DMA_INT_EN       0x0001
#define RX_INT_EN        0x0002
#define TX_INT_EN        0x0004
#define WINDOW_INT_EN    0x0008
#define DMA_INT_ACK      0x0010
#define RX_INT_ACK       0x0020
#define TX_INT_ACK       0x0040
#define WINDOW_INT_ACK   0x0080
#define SET_DMA_ON       0x0100
#define SET_RX_ON        0x0200
#define SET_TX_ON        0x0400
#define SET_DMA_OFF      0x0800
#define SET_RX_OFF       0x1000
#define SET_TX_OFF       0x2000
#define FIFO_READ        0x4000
#define FIFO_WRITE       0x8000

/* 8005 status register bits. */
#define DMA_INT          0x0010
#define RX_INT           0x0020
#define TX_INT           0x0040
#define WINDOW_INT       0x0080
#define DMA_ON           0x0100
#define RX_ON            0x0200
#define TX_ON            0x0400
#define FIFO_FULL        0x2000
#define FIFO_EMPTY       0x4000
#define FIFO_DIR         0x8000

/* 8005 configuration register 1 bits. */
#define MATCH_BITS       0xc000
#define MATCH_ONLY       0x0
#define MATCH_BROAD      0x4000
#define MATCH_MULTI      0x8000
#define MATCH_ALL        0xc000
#define ALL_STATIONS     0x3f00
#define STATION_0_EN     0x0100
#define STATION_1_EN     0x0200
#define STATION_2_EN     0x0400
#define STATION_3_EN     0x0800
#define STATION_4_EN     0x1000
#define STATION_5_EN     0x2000
#define DMA_LENGTH       0x00c0
#define NBYTES_1         0x0
#define NBYTES_2         0x0040
#define NBYTES_4         0x0080
#define NBYTES_8         0x00c0
#define DMA_INTERVAL     0x0030
#define CONTINUOUS       0x0
#define DELAY_800        0x0010
#define DELAY_1600       0x0020
#define DELAY_3200       0x0030
#define BUFFER_CODE_BITS 0x000f

#define STATION_0_SEL    0x0
#define STATION_1_SEL    0x0001
#define STATION_2_SEL    0x0002
#define STATION_3_SEL    0x0003
#define STATION_4_SEL    0x0004
#define STATION_5_SEL    0x0005
#define PROM_SEL         0x0006
#define TEA_SEL          0x0007
#define BUFFER_MEM_SEL   0x0008
#define INT_VECTOR_SEL   0x0009
#define	CONFIG3_REGISTER_SEL	0x000c	/* 80c04 */
#define	PRODUCT_ID_REGISTER	0x000d
#define	TEST_ENABLE_REGISTER	0x000e
#define	HASH_TABLE		0x000f

/* 8005 configuration register 2 bits. */
#define BYTE_SWAP        0x0001
#define AUTO_UPDATE_REA  0x0002
#define RECV_WHILE_XMIT_OFF	0x0004	/* 80c04 */
#define CRC_ERR_EN       0x0008
#define DRIBBLE_EN       0x0010
#define SHORT_FRAME_EN   0x0020
#define ALL_ERRORS	 0x0038
#define SLOT_TIME_SELECT 0x0040
#define XMIT_NO_PREAMBLE 0x0080
#define ADDR_LENGTH      0x0100
#define RECEIVE_CRC      0x0200
#define XMIT_NO_CRC      0x0400
#define LOOPBACK_EN      0x0800
#define KILL_WATCHDOG    0x1000
#define RESET            0x8000

/* 80C04 configuration register 3 bits */
#define	AUTO_PAD_TRANSMIT	0x0001
#define	NOT_USED		0x0002
#define	SQE_TEST_ENABLED	0x0004
#define	SLEEP_MODE_ON		0x0008
#define	READY_ADVANCED		0x0010
#define	NOT_USED_2		0x0020
#define	GROUP_ADDRESS_ENABLE	0x0040
#define	NPP_BYTE_SWAP		0x0080

/* 8005 transmit header command byte bits. */
#define CHAIN_CONTINUE  0x40
#define DATA_FOLLOWS    0x20
#define XMIT_OK_INT_EN  0x08
#define _16_COLL_INT_EN 0x04
#define COLL_INT_EN     0x02
#define BABBLE_INT_EN   0x01

/* 8005 transmit packet status byte bits. */
#define DONE            0x80
#define _16_COLL_ERROR  0x04
#define COLLISION       0x02
#define BABBLE_ERROR    0x01

/* 8005 receive packet status byte bits. */
#define SHORT_FRAME     0x08
#define DRIBBLE_ERROR   0x04
#define CRC_ERROR       0x02
#define OVERSIZE        0x01

/*
 * PROM Contents.
 */

/* Offsets */
#define	PRODUCT_CODE	4
#define	CHARACTERISTICS	5
/* To mask out characteristics byte */
#define CHAR_MASK	0x03
/* 8 or 16 bit characteristics */
#define DATA_WIDTH_8	1
#define DATA_WIDTH_16	2
/* Defined in Product code */
#define UTP_CODE	3

/*
 * Fast Access Macros to registers
 */
#define rx_data_ready(x)	((inpw(STATUS_PORT) & RX_INT) ? TRUE : FALSE)

#define TIMEOUT		5		/* max timeout in 1/100 sec */
#define NRETRIES	4		/* number of transmit retries */

struct reg_file {
	unsigned int command;
	unsigned int status;
	unsigned int config1;
	unsigned int config2;
	};

struct rx_packet
{
	unsigned int next_pkt_ptr;
	unsigned char hdr_stat;
	unsigned char pkt_stat;
	unsigned char dest_addr[6];
	unsigned char src_addr[6];
	unsigned int byte_count;
	char		data[1];
};

struct tx_packet {
	unsigned int next_pkt_ptr;
	unsigned char hdr_cmd;
	unsigned char pkt_stat;
	unsigned char dest_addr[6];
	unsigned char src_addr[6];
	unsigned int byte_count;
	char		data[1];
};

#endif

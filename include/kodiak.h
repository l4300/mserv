#ifndef	_KODIAK_H
#define	_KODIAK_H

int	kodiak_init(struct netif *pni, unsigned arg1, unsigned arg2, unsigned arg3);
int	kodiak_send(struct netif *pni, struct ep *pep, int len);
int	kodiak_close(struct netif *pni);
int	kodiak_watchdog(struct netif *pni, int delay);


#endif
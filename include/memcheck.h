
/* #*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

                  MemCheck (R) Runtime Allocation Debugger

                   Version 2.1c for ANSI- and K&R-compliant
                          development environments

              Copyright (c) 1990-1994, StratosWare Corporation.  
                            All rights reserved.
                              1-800-WE-DEBUG

        This file must be #included AFTER any other #includes in
        each source file which is to be memory checked, and BEFORE
        any code that performs any operations on allocated pointers.

        Compilation Control Variables

        _MCANSI_    Defined for ANSI standard compiles; 
                    depends on __STDC__ or __cplusplus
        _MCKR_      Defined for non-System V K&R compiles;
                    `NOTSYSV' must be #defined manually
                    per make instructions and SHOW.C
        _MCSYSV_    Defined if none of the above are defined;
                    for System V-compatible K&R compilations

    Modification History

    WHO     WHEN        WHAT
    CFA     06/08/93    Ported to ANSI, tested
    KWB     02/27/94    Code cleanup & review
    KWB     06/29/94    Added old GNU C note

*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*# */

/* Avoid multiple inclusions */
#ifndef _MEMCHECK_H_
#define _MEMCHECK_H_

/*  C++ new interception -- see note later and
    read the Release Notes, section
    "Integration With C++."  Uncommenting the next line
    and following simple instructions allows seamless
    transmission of the exact file and line location
    of new's in your source code.
*/
/*  #define NEW_OVERLOADED  */


/* Port1.3 */
/*  These compilers are `ANSI-compliant' but do not 
    follow strict ANSI format for vsprintf() and sprintf() calls,
    i.e. these routines return a `char *' instead of an `int':

        - Sparkworks C
        - GNU C

    If your compiler is `ANSI-compliant' as reported
    by SHOW.C, but sprintf() and vsprintf() return `char *'
    pointers, then you should add an #ifdef-#endif section
    below to define `_MCWRV_'.  (Wrong Return Value)
*/
#ifdef SPARCWORKS   /* Sparcworks C */
#define _MCWRV_
#endif
/* NOTE: older versions of GNU C, like version 1.3.7, 
         actually return the correct ANSI standard
         return value (an int) from sprintf().
         For those compilers, you will want to comment out
         the #define of _MCWRV_ below. 
*/
#ifdef __GNUC__     /* GNU C */
#define _MCWRV_
#endif

#ifdef	__BORLANDC__
#   define __STDC__	1
#endif

#ifdef __STDC__
#   define _MCANSI_
#else
#   ifdef __cplusplus
	/* If C++, must be ANSI-compliant */
#       define _MCANSI_
#   else
/*      If not ANSI-standard, can be either
	System V-compatible or K&R-compatible
*/
#       ifndef NOTSYSV
#           define _MCSYSV_
#       else
#           define _MCKR_
#       endif
#   endif   /* not C++ */
#endif

#ifndef NULL
#include <stdio.h>
#endif
#ifndef NULL
#include <stdlib.h>
#endif

/*  This may not be supported on all systems;
    comment out if necessary...
*/
#include <stddef.h>

/* For va_list ... */
#ifndef va_start
#if  defined(_MCANSI_) || defined(__BORLANDC__)
#include <stdarg.h>
#else
#include <varargs.h>
#endif
#endif


/* Compatibility with MemCheck V2.0 */
#ifdef NOMEMCHK 
#   define  NOMEMCHECK
#endif

/* Backwards compatibility - naming convention changes */
#define mc_isactive         mc_is_active
#define mc_getmode          mc_get_mode
#define mc_errorstatus      mc_error_status
#define mc_verify_memory    mc_check_buffers

#define MC_USEDISK      MC_USING_DISK
#define MC_USEMEM       MC_USING_MEMORY

/* 
    Error Flag Definitions

    Returned by mc_endcheck() and mc_error_status().
*/
#define EFLAG_NULL_PTR       0x01   /* NULL ptr was passed to a mem routine */
#define EFLAG_BAD_PTR        0x02   /* unallocated ptr "   "  "  "     "    */
#define EFLAG_FRONT_MAGIC    0x04   /* front overwritten on buffer(s) */
#define EFLAG_BACK_MAGIC     0x08   /* back  overwritten on buffer(s) */
#define EFLAG_PTRS_NOT_FREED 0x10   /* allocated ptr not freed */
#define EFLAG_TRACK_FILE     0x20   /* error opening disk tracking file */
#define EFLAG_NULL_ASSIGN    0x40   /* null runtime pointer assignment */

/*  MemCheck Tracking Mode 
    
    Returned by mc_get_mode().
    Indicates whether information on each allocation
    is being stored in memory or on disk.
*/
#define MC_USING_MEMORY     (0x01)
#define MC_USING_DISK       (0x02)

/*  MemCheck `Speed' Setting

    Returned by mc_get_speed().
    Pass as params to mc_set_speed().
*/
#define MC_RUN_NORMAL       1
#define MC_RUN_FAST         2

/*
    Maximum number of breakpoints set via mc_breakpoint().
*/
#define MC_MAX_BREAKS       50

#   define _MCAPI
#   define _MCCALLBACK

typedef char *          MCSF;       /* source file      */
typedef unsigned int    MCSL;       /* source line      */
typedef size_t          MCSIZE;     /* destination size */

/*  `MFUNC' is typedef for a var pointing
    to a message function, also called an
    `error reporting function'.
*/
#ifdef _MCANSI_
typedef void        _MCCALLBACK (*MFUNC) (char *);
#else
typedef void        _MCCALLBACK (*MFUNC) ();
#endif

/* ANSI Location Defines */

/* *** MemCheck Source File *** */
#ifdef __FILE__
#   define _MCSF_       (MCSF)__FILE__
#else
#   define _MCSF_       (MCSF)NULL
#endif

/* *** MemCheck Source Line *** */
#ifdef __LINE__
#   define _MCSL_        ((MCSL)__LINE__)
#else
#   define _MCSL_        ((MCSL)0)
#endif


#ifdef _MCANSI_ 

/*  Macros that sub out _MCPARAMS in favor of the proper ANSI format.
    Most simply allow a passthrough of parameters that were adjusted 
    if K&R C was used 
*/
#define _MCPARAMS0()    (void)
#define _MCPARAMS1(t1,p1)\
        (t1 p1)
#define _MCPARAMS2(t1,p1,t2,p2)\
        (t1 p1,t2 p2)
#define _MCPARAMS3(t1,p1,t2,p2,t3,p3)\
        (t1 p1,t2 p2,t3 p3)
#define _MCPARAMS4(t1,p1,t2,p2,t3,p3,t4,p4)\
        (t1 p1,t2 p2,t3 p3,t4 p4)
#define _MCPARAMS5(t1,p1,t2,p2,t3,p3,t4,p4,t5,p5)\
        (t1 p1,t2 p2,t3 p3,t4 p4,t5 p5)
#define _MCPARAMS6(t1,p1,t2,p2,t3,p3,t4,p4,t5,p5,t6,p6)\
        (t1 p1,t2 p2,t3 p3,t4 p4,t5 p5,t6 p6)

#define MCCONST const
#define TOUCH(var) if(var)

typedef void *          MCVOIDP;
typedef MCCONST void *  MCCVOIDP;   /* const void * */

#define _MCPROTO(args)                  args


#else   /* *****************  K  &  R  ********************** */

/*  K&R does not support having type declarations 
    in the formal parameter list.
*/
#define _MCPARAMS0()    ()
#define _MCPARAMS1(t1,p1)                           \
        (p1)                                        \
        t1 p1;
#define _MCPARAMS2(t1,p1,t2,p2)                     \
        (p1, p2)                                    \
        t1 p1;  \
        t2 p2;
#define _MCPARAMS3(t1,p1,t2,p2,t3,p3)               \
        (p1, p2, p3)                                \
        t1 p1;  \
        t2 p2;  \
        t3 p3;                  
#define _MCPARAMS4(t1,p1,t2,p2,t3,p3,t4,p4)         \
        (p1, p2, p3, p4)                            \
        t1 p1;  \
        t2 p2;  \
        t3 p3;  \
        t4 p4;  
#define _MCPARAMS5(t1,p1,t2,p2,t3,p3,t4,p4,t5,p5)   \
        (p1, p2, p3, p4, p5)                        \
        t1 p1;  \
        t2 p2;  \
        t3 p3;  \
        t4 p4;  \
        t5 p5; 
#define _MCPARAMS6(t1,p1,t2,p2,t3,p3,t4,p4,t5,p5,t6,p6) \
        (p1, p2, p3, p4, p5, p6)                \
        t1 p1;  \
        t2 p2;  \
        t3 p3;  \
        t4 p4;  \
        t5 p5;  \
        t6 p6; 

/* K&R does not support void* or const */
#define MCCONST
#define TOUCH(var) if(var)

typedef char *          MCVOIDP;
typedef MCCONST char *  MCCVOIDP;   /* const `void' * */

/* K&R does not support types & parameters in prototypes */
#define _MCPROTO(args)  ()

#endif
 
/*  Alias -- "NOMC" is shorthand for "NOMEMCHECK",
    which when defined compiles MemCheck completely
    out of code.
*/
#ifdef NOMC
#define NOMEMCHECK
#endif

/*
    If MemCheck is not being `compiled out' (via definition
    of the constant NOMEMCHECK), include this section...
*/
#ifndef NOMEMCHECK  

            /*  ***    M E M C H E C K   A C T I V E    ***  */
#define MEMCHECK

#ifndef MEMCHECK_MODULE

/*
    MC_NO_TRANSFER_SIZE is used to eliminate errors or warnings
    like "sizeof returns 0" or "Not allowed type in sizeof <expr>".
    These occur for unsized variables declared like

        extern unsigned char gHelpString[];     

    The optimal solution is to "size" the extern, e.g.

        extern unsigned char gHelpString[80];   

    but where this may not be practical, MC_NO_TRANSFER_SIZE may
    be defined on a module-by-module OR project-wide basis.
*/
/* Alias ... */
#ifdef NO_TRANSFER_SIZE
#   define MC_NO_TRANSFER_SIZE
#endif

#ifndef MC_NO_TRANSFER_SIZE
#   define _MCSIZEOF(d)  (MCSIZE)sizeof(d)
#else
#   define _MCSIZEOF(d)  (MCSIZE)0
#endif


/* 
        --------   Function Call Interception Definitions   ---------
*/

/*
    STANDARD C  - Allocation & data transfer routines
*/
#   define calloc(n,size)   mc_calloc   (n, size, _MCSF_, _MCSL_)
#   define free(p)          mc_free     (p, _MCSF_, _MCSL_)
#   define malloc(size)     mc_malloc   (size, _MCSF_, _MCSL_)
#   define memcpy(d,s,n)    mc_memcpy   (d,s,n, _MCSF_, _MCSL_, _MCSIZEOF(d))
#   define memset(d,c,n)    mc_memset   (d,c,n, _MCSF_, _MCSL_, _MCSIZEOF(d))
#   define realloc(p,s)     mc_realloc  (p,s, _MCSF_, _MCSL_)
#   define sprintf          mc_sprintf
#   define strcat(d,s)      mc_strcat   (d,s, _MCSF_, _MCSL_, _MCSIZEOF(d))
#   define strcpy(d,s)      mc_strcpy   (d,s, _MCSF_, _MCSL_, _MCSIZEOF(d))
#   define strncat(d,s,n)   mc_strncat  (d,s,n, _MCSF_, _MCSL_, _MCSIZEOF(d))
#   define strncpy(d,s,n)   mc_strncpy  (d,s,n, _MCSF_, _MCSL_, _MCSIZEOF(d))
#   define vsprintf(s,f,a)  mc_vsprintf (s,f,a, _MCSF_, _MCSL_, _MCSIZEOF(s))

/*
    NOT ALWAYS STANDARD C - Allocation & data transfer routines
*/
#   define memmove(d,s,n)   mc_memmove  (d,s,n, _MCSF_, _MCSL_,_MCSIZEOF(d))
#   define strdup(s)        mc_strdup   (s, _MCSF_, _MCSL_)

/*
    MemCheck's external-to-internal interceptions
*/
#define mc_check(p)                 mci_check(p,_MCSF_,_MCSL_)
#define mc_register(p,size)         mci_register(p,size,_MCSF_,_MCSL_)
#define mc_unregister(p)            mci_unregister(p,_MCSF_,_MCSL_)
#define mc_nullcheck()              mci_nullcheck(_MCSF_,_MCSL_)

#endif  /* ------------  Function Interceptions  ----------------- */


/*
    External and Library Prototype Section
*/
#ifndef NOPROTO  

/*
    C++ needs special prototype directions...
*/
#ifdef __cplusplus
extern "C" {
#endif

extern  MCVOIDP     _MCAPI mc_malloc  _MCPROTO((size_t, MCSF, MCSL));
extern  void        _MCAPI mc_free    _MCPROTO((MCVOIDP, MCSF, MCSL));
extern  MCVOIDP     _MCAPI mc_calloc  _MCPROTO((size_t, size_t, MCSF, MCSL));
extern  MCVOIDP     _MCAPI mc_realloc _MCPROTO((MCVOIDP,size_t,
                                                MCSF, MCSL));
extern  char *      _MCAPI mc_strcpy  _MCPROTO((char *, MCCONST char *,
                                                MCSF, MCSL, MCSIZE));
extern  char *      _MCAPI mc_strncpy _MCPROTO((char *, MCCONST char *, size_t,
                                                MCSF, MCSL, MCSIZE));
extern  char *      _MCAPI mc_strcat  _MCPROTO((char *, MCCONST char *,
                                                MCSF, MCSL, MCSIZE));
extern  char *      _MCAPI mc_strncat _MCPROTO((char *, MCCONST char *, size_t,
                                                MCSF, MCSL, MCSIZE));
extern  MCVOIDP     _MCAPI mc_memset  _MCPROTO((MCVOIDP, int, size_t,
                                                MCSF, MCSL, MCSIZE));
extern  MCVOIDP     _MCAPI mc_memcpy  _MCPROTO((MCVOIDP, MCCVOIDP, 
                                                size_t, MCSF, MCSL, MCSIZE));
extern  char *      _MCAPI mc_tempnam _MCPROTO((char *, char *, MCSF, MCSL));
        
#ifdef _MCWRV_
extern char *   _MCAPI mc_sprintf (char *, MCCONST char *, ...);
extern char *   _MCAPI mc_vsprintf (char *, MCCONST char *, va_list, MCSF, MCSL, MCSIZE);
#else
#   ifdef _MCANSI_
    extern int  _MCAPI mc_sprintf  (char *, MCCONST char *, ...);
    extern int  _MCAPI mc_vsprintf (char *, MCCONST char *, va_list, MCSF, MCSL, MCSIZE);
#   endif
#endif

#ifdef _MCSYSV_
extern int      _MCAPI mc_sprintf ();
extern int      _MCAPI mc_vsprintf ();
#endif

#ifdef _MCKR_
extern char *   _MCAPI mc_sprintf ();
extern char *   _MCAPI mc_vsprintf ();
#endif

extern  MCVOIDP _MCAPI mc_memmove _MCPROTO((MCVOIDP, MCCVOIDP, size_t,
                                                MCSF, MCSL, MCSIZE));
extern  char *  _MCAPI mc_strdup _MCPROTO((MCCONST char *,
                                                MCSF, MCSL));

/* MemCheck API Prototypes */

extern  void            _MCAPI mc_startcheck    _MCPROTO((void _MCCALLBACK (*mfunc)(char *)));
extern  int             _MCAPI mc_endcheck      _MCPROTO((void));
extern  int             _MCAPI mc_is_active     _MCPROTO((void));
extern  int             _MCAPI mc_get_mode      _MCPROTO((void));
extern  int             _MCAPI mc_get_speed     _MCPROTO((void));
extern  void            _MCAPI mc_set_speed     _MCPROTO((int));
extern  int             _MCAPI mc_error_status  _MCPROTO((void));
extern  void            _MCAPI mc_set_msgfunc   _MCPROTO((void _MCCALLBACK (*mfunc)(char *)));
extern  void            _MCCALLBACK (* _MCAPI mc_get_msgfunc _MCPROTO((void ))) _MCPROTO((char *));
extern  void            _MCAPI mc_set_alignsize _MCPROTO((unsigned int));
extern  void            _MCAPI mc_set_checkbytes _MCPROTO((unsigned int));
extern  int             _MCAPI mc_breakpoint    _MCPROTO((char *));
extern  void            _MCAPI mc_report        _MCPROTO(
                            (void (* _MCCALLBACK rptfunc) (char *)) );
extern  unsigned long   _MCAPI mc_alloc_count   _MCPROTO((void));

extern  int             _MCAPI mc_check_buffers _MCPROTO((void));
extern  int             _MCAPI mci_check        _MCPROTO((MCVOIDP, MCSF, MCSL));

extern  void            _MCAPI mci_register     _MCPROTO((MCVOIDP, long, 
                                                    MCSF, MCSL));
extern  void            _MCAPI mci_unregister   _MCPROTO((MCVOIDP, 
                                                    MCSF, MCSL));
extern  int             _MCAPI mci_nullcheck    _MCPROTO((MCSF, MCSL));
extern  void            _MCAPI _mc_clear_flags  _MCPROTO((void));
extern  void            _MCAPI _mc_set_delflag  _MCPROTO((void));
extern  void            _MCAPI _mc_set_newflag  _MCPROTO((void));
extern  void            _MCAPI _mcsl_new        _MCPROTO((char *,int));
extern  void            _MCAPI _mcsl_delete     _MCPROTO((char *,int));
extern  void            _MCAPI _mc_cpp_location _MCPROTO((MCSF *, MCSL *));

extern void             _MCCALLBACK erf_default  _MCPROTO((char *));
extern void             _MCCALLBACK erf_standard _MCPROTO((char *));
extern void             _MCCALLBACK erf_logfile  _MCPROTO((char *));
extern void             _MCCALLBACK erf_log_only _MCPROTO((char *));

/* ------------- End MEMCHECK Library Calls --------------- */

#ifdef __cplusplus
}

/*  C++ MemCheck Class

    This class can be used as an alternative to
    AutoInit, or to placing the mc_startcheck() and
    mc_endcheck() calls in your main() program.
    Just declaring an object of class 'MemCheck'
    will start MemCheck up;  usually you will place
    this 'above' any other global or statically declared
    C++ objects in your main module.

    Here are some examples of starting MemCheck up
    via object mechanics:

        MemCheck On;
        MemCheck Active;
        MemCheck CostsALotLessThanPurify;

    Use your imagination...
*/
#if !defined (NO_INTERCEPT) /* must not have this def'd */

/* This class def causes a warning under MSC if not used */
#ifndef _MSC_VER

class MemCheck {
public:
    MemCheck ()  { mc_startcheck (NULL); }
    ~MemCheck () { mc_endcheck ();       }
};

#endif

#endif


/* *** For use in new and delete modules only *** */
/*
    Replace 'malloc' with 'cpp_malloc', etc.
    In new and delete modules, NO_INTERCEPT should be #defined, e.g.

        #define NO_INTERCEPT
        #include <memcheck.h>
        :
        void * operator new ( size_t size )
        {
            if (!size)  size = 1;
            return (cpp_malloc (size));
        }
        etc.
*/
#define cpp_malloc(_s)      (_mc_set_newflag(), malloc(_s))
#define cpp_calloc(_n,_s)   (_mc_set_newflag(), calloc(_n,_s))
#define cpp_free(_p)        (_mc_set_delflag(), free(_p))


/* C++ */
#if !defined (NO_INTERCEPT)
#if !defined (NO_CPP)

/*
    This method is off by default, because it
    requires definition of a new operator like:

      void * new (size_t size, char *file, int lineno);

    Such a new operator is included in your SOURCE\CPP
    directory. To have this method used for all modules,
    #define NEW_OVERLOADED at the top of this header file
    or in your project #include file, BEFORE the MemCheck
    header file is #included.

    The substitutions for the new operator
    may not work in all situations.  To disable
    MemCheck's interception of new on a module-by-
    module basis by undefining NEW_OVERLOADED.
*/
#if defined (NEW_OVERLOADED)

/*  Method 1: Placement Operators

    Use placement operators to trap file and line location transparently
    on calls to new.  
    
    Thanks for this tip to Dan Saks,
    C and C++ writer, author, teacher, and columnist--buy his books.
    He came through when no one else had a clue!

    Please consult your manual, MemCheck technotes, 
    or StratosWare Technical Support (1-800-WE-DEBUG)
    for details on how to configure your project for
    use with an overloaded new placement operator.
*/

/* Declare overloaded new with placement operators */
void *operator new (size_t _sz, char *file, int lineno);

#define new     new((char *)__FILE__,(int)__LINE__)

/*  NOTE:
    This placement operator interception syntax has been
    known to cause syntax errors (in VC++) for no apparent reason
    on statements like

            Domain *d = new Domain ();

    Workaround is to change line (sorry!) to equivalent

            Domain *d = new Domain;
*/

/* Backwards compatibility with the original V2.1 C++ macros */
#define NEW(_object)         new _object
#define DELETE(_object)      delete _object
#define DELETE_ARR(_arr)     delete[] _arr

#else   /* !NEW_OVERLOADED - end of Placement Operator intercept */

/*  New and Delete Interception, Method 2: NEW() and DELETE()

    The NEW() and DELETE() macros may be used to transmit file
    and line of new and delete.   These macros, which require
    modification of source code, i.e. "NEW(object)" for "new object",
    should probably be used only if the above overloaded new does
    not work for your code base.
    
    Please consult your manual, MemCheck technotes, 
    or StratosWare Technical Support (1-800-WE-DEBUG)
    for details on how to configure your project for
    use with NEW() and DELETE().

    If calling, please have your MemCheck serial number handy.
*/
#define NEW(_object)        (_mcsl_new(_MCSF_,_MCSL_),    new _object)
#define DELETE(_object)     (_mcsl_delete(_MCSF_,_MCSL_), delete _object)
#define DELETE_ARR(_arr)    (_mcsl_delete(_MCSF_,_MCSL_), delete[] _arr)

#endif  /* !NEW_OVERLOADED */

#define delete  _mcsl_delete(_MCSF_,_MCSL_), delete

#endif  /* !NO_CPP */
#endif  /* NO_INTERCEPT */
#endif  /* cplusplus */
/******** End C++ ************/

#endif  /* Prototype Definition Section */


/* -------------------------------------------------------------------------- */

#else                   

/* -------------------------------------------------------------------------- */
/*  *****************************
     MemCheck Not Active Section
    *****************************

    This section completely removes or
    "evaporates" all MemCheck function references
    from your projects when you compile with
    NOMEMCHECK #defined.

    There's no need to remove any MemCheck
    headers or statements from your code
    to produce a full production version
    of your application.

                    o
                   ooo
                 ooooooo
                ooooooooo
               ooooooooooo
                   ooo
                   ooo
                   ooo
                   ooo
                                    */

#ifndef MEMCHECK_MODULE
#   define  mc_startcheck(msgfunc)
#   define  mc_endcheck()                   0
#   define  mc_is_active()                  0
#   define  mc_get_mode()                   0
#   define  mc_set_speed(speed)
#   define  mc_get_speed()                  0
#   define  mc_error_status()               0
#   define  mc_set_msgfunc(msgfunc)
#   define  mc_get_msgfunc()                NULL
#   define  mc_set_alignsize(alignsize)
#   define  mc_set_checkbytes(checkbytect)
#   define  mc_breakpoint(fi)               0
#   define  mc_report(rptfunc)
#   define  mc_alloc_count()                0L
#   define  mc_check_buffers()              0
#   define  mc_check(p)                     0
#   define  mc_register(p,s)    
#   define  mc_unregister(p)                
#   define  mc_nullcheck()                  0

/* *** C++ *** */
#ifdef __cplusplus

#define NEW(_object)        new _object
#define DELETE(_object)     delete _object
#define DELETE_ARR(_arr)    delete[] _arr

#define cpp_malloc(_s)      malloc(_s)
#define cpp_calloc(_n,_s)   calloc(_n,_s)
#define cpp_free(_p)        free(_p)

#endif  /* C++ */

#endif  /* not MEMCHECK_MODULE */

#endif      /* End of Section for NOMEMCHECK Defined */


#endif  /* not defined _MEMCHECK_H_ */

/********************************
 * End of MemCheck 2.1 Header *
 ********************************/

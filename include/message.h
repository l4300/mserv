#ifndef	_MESSAGE_H
#define	_MESSAGE_H

#include	"semaphore.h"

typedef struct	_message_box {	/* place to receive messages */
	Queue	mesg_MQueue;	/* queue of messages */
	Semaphore	mesg_readSem;	/* those blocked on read */
	Semaphore	mesg_writeSem;	/* those blocked on write */
	int		mesg_maxMessages;	/* max messages we can take */
	int		mesg_alarm;	/* set if we posted an alarm */
} MessageBox;

#ifdef	__cplusplus
extern "C" {
#endif

MessageBox	*MessageBoxAlloc(int maxmessages);
int		 MessageBoxInitialize(MessageBox *M, int max);	/* init preallocated structure */
int		 MessageBoxRelease(MessageBox *M);	/* signal everyone waiting on write Queue */

int		 MessageBoxSend(MessageBox *M, Queue_Element *Q);
int		 MessageBoxISend(MessageBox *M, Queue_Element *Q);	/* send from interrupt */

#define	MessageBoxSendBuf(M,B) (MessageBoxSend((M), ((Queue_Element *) B)))
#define	MessageBoxISendBuf(M,B) (MessageBoxISend((M), ((Queue_Element *) B)))

int		MessageBoxSignal(MessageBox *M, int signal);
int		MessageBoxISignal(MessageBox *M, int signal);

Queue_Element	*MessageBoxReceive(MessageBox *M);

Queue_Element	*MessageBoxReceiveAlarm(MessageBox *M, unsigned timeout);	/* timeout in 100's seconds */
int		 MessageBoxCount(MessageBox *M);	/* number of messages that can be read */
int		 MessageBoxFree(MessageBox *M);	/* number of messages that could be written w/o block */

#ifdef	__cplusplus
};
#endif

#define	MBOX_RESULT_BADMBOX	-1
#define	MBOX_RESULT_OK		 0
#define	MBOX_RESULT_EBLOCK	 -2	/* would block in interrupt */

#endif

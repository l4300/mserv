#ifndef	_NET_H
#define	_NET_H

#ifdef	__cplusplus
extern "C" {
#endif

typedef	struct	netnums {
	IPaddr	net_addr;
	IPaddr	net_subnetmask;
} NetNumbers;


int	net_setaddr(int inum, IPaddr net_addr, IPaddr subnmask);
int	netinit(void *stack, unsigned stacksize);
void	netclose();
int	netwatchdog(int delay);

unsigned netstacksize();	/* return total stack needed */
#ifdef	__cplusplus
};
#endif

#endif

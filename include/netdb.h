#ifndef	_NETDB_H
#define	_NETDB_H

#include	"ip.h"
struct	hostent {
	char	*h_name;
	char	**h_aliases;
	int	h_addrtype;		/* sin_family */
	int	h_length;		/* length of address */
	char	**h_addr_list;
#define	h_addr	h_addr_list[0]
};

#ifdef	__cplusplus
extern "C" {
#endif

struct	hostent	*gethostbyname(char *name);
struct	hostent *gethostbyaddr(IPaddr address);
struct  hostent *gethostent();

extern	int	h_errno;

#ifdef	__cplusplus
};
#endif

#define	HOST_NOT_FOUND	1
#define	TRY_AGAIN	2
#define	NO_RECOVERY	3
#define	NO_DATA		4
#define	NO_ADDRESS	NO_DATA

#define	MAXHOSTNAMELEN	256

#endif
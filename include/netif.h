#ifndef	_NETIF_H
#define	_NETIF_H

#include	"queue.h"

#define	NI_MAXHWA	14		/* max size of any hardware address */

#define	HWT_ETHER	1		/* hardware type ethernet, requires arp */
#define	HWT_SLIP	2		/* hardware type slip, no arp */
#define	HWT_PPP		3		/* hardware type ppp, no arp */

struct	hwa 	{
	int		ha_len;           	/* len of this address */
	unsigned char	ha_addr[NI_MAXHWA];	/* the address */
};

#define	NI_INQSZ	30		/* interface input queue size */
#define	NETNLEN		30		/* network name length	*/

#define	NI_LOCAL	0		/* index of local interface */
#define	NI_PRIMARY	1		/* indx of prim int */

#define	NIS_UP		0x01
#define	NIS_DOWN	0x02
#define	NIS_TESTING	0x04
#define	NIS_PRESENT	0x08

#define	NI_MAXPROXYARP	10		/* maximum number of proxy arp addresses */

typedef struct netif {
	char	ni_name[NETNLEN];
	char	ni_state;
	int	ni_ifnumber;	/* our IF number */
	IPaddr	ni_ip;		/* our ip address */
	IPaddr	ni_net;		/* network IP address */
	IPaddr	ni_subnet;	/* subnet number */
	IPaddr	ni_mask;	/* subnet mask */
	IPaddr	ni_brc;		/* broadcast address */
	IPaddr	ni_nbrc;
	IPaddr	ni_proxyip[NI_MAXPROXYARP];
	int	ni_mtu;
	int	ni_hwtype;	/* hardware type */
	struct hwa	ni_hwa;	/* hardware address of interface */
	struct hwa	ni_hwb;	/* broadcast hardware address */
	Bool	ni_ivalid;	/* ip valid */
	Bool	ni_nvalid;
	Bool	ni_svalid;	/* subnet valid */
	int	ni_dev;		/* farg! device id of this interface for devwrite call */
	Queue	*ni_ipinq;	/* ptr to IP input queue */
	Queue	ni_outq;	/* the output queue for this device */
	long	ni_pvtlong;
	void	*ni_pvtvoid1;
	void	*ni_pvtvoid2;
	void	*ni_pvtvoid3;
	unsigned short ni_pvtshort1;
	unsigned short ni_pvtshort2;
	unsigned short ni_pvtshort3;
	unsigned short ni_pvtshort4;
	long	ni_pvtuser1;
	long	ni_pvtuser2;	
	/* mib info */
	char	*ni_descr;
	int	ni_mtype;	/* mib type */
	long	ni_speed;
	char	ni_admstate;
	long	ni_lastchange;
	long	ni_ioctects;
	long	ni_iucast;
	long	ni_ibcast;
	long	ni_idiscard;
	long	ni_ierrors;
	long	ni_iunkproto;
	long	ni_ooctects;
	long	ni_oucast;
	long	ni_obcast;
	long	ni_odiscard;
	long	ni_oerrors;
	long	ni_qlen;	/* output q length ? */

	/* action interface */
	int	(*ni_init)(struct netif *pni, unsigned arg1, unsigned arg2, unsigned arg3);
			/* returns 0 if ok */
	int	(*ni_send)(struct netif *pni, struct ep *packet, int length);
			/* this function enqueues the packet and calls kick */
	int	(*ni_upcall)(struct netif *pni, struct ep *pep, int len);
	int	(*ni_close)(struct netif *pni);		/* shut down the interface */
	void	(*ni_watchdog)(struct netif *pni, int count);	/* periodic watchdog */

} Net_Interface;

typedef struct netinfo {
	int	(*ni_init)(struct netif *pni, unsigned arg1, unsigned arg2, unsigned arg3);
	int	(*ni_send)(struct netif *pni, struct ep *packet, int length);
	int	(*ni_close)(struct netif *pni);		/* shut down the interface */
	int	(*ni_watchdog)(struct netif *pni, int delay); /* periodic watchdog */
	unsigned	ni_mtu;
	long		ni_speed;	/* speed of interface */
	char		*ni_name;
	int		ni_hwtype;
	unsigned	ni_arg1;	/* int */
	unsigned	ni_arg2;	/* io addr */
	unsigned	ni_arg3;	/* mem addr */
} NetInfo;

int	ni_init(int inum, IPaddr addr, IPaddr netmask);

extern	Net_Interface	nif[];
extern	int	nifcount;
extern 	NetInfo	Nifs[];

extern	int	maxnifs;
#if	IPDOOR
#define	NIFCOUNT	4
#else
#define	NIFCOUNT	2
#endif

#endif
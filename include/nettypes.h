#ifndef	_NETTYPES_H
#define	_NETTYPES_H

typedef	int	Bool;

#define	OK		0
#define	SYSERR		-1

#define	isodd(x)	((x) & 1)

#ifndef	PACSOFT
struct	timeval	{
	long	tv_sec;
	long	tv_usec;
};

#endif

#endif
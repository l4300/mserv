#ifndef	_NETWORK_H
#define	_NETWORK_H

#include "conf.h"
#include	"bufs.h"
#include	"semaphore.h"

#include "nettypes.h"
#include "ip.h"
#include "ether.h"
#include "netif.h"
#include "arp.h"
#include "ep.h"
#include "icmp.h"
#include "route.h"
#include "ipreass.h"
#include "net.h"

#include	<alloc.h>
#pragma hdrstop

int	arp_in(Net_Interface *pni, EP *pep);
int	rarp_in(Net_Interface *pni, EP *pep);
int	ip_in(Net_Interface *pni, EP *pep);
int	local_out(EP *pep);
#endif
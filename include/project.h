
/* #*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

                  MemCheck (R) Runtime Allocation Debugger

                   Version 2.1c for ANSI- and K&R-compliant
                          development environments

              Copyright (c) 1990-1994, StratosWare Corporation.  
                            All rights reserved.

    PROJECT.H
    Project-wide declarations for compiling the MemCheck libraries.  
    Included by ALL MemCheck internal modules (NOT by MemCheck users...)

    ------  The header files that are included are mostly to 
     NOTE   prototype functions called, like memset() and strncat(), etc.
    ------  If your particular system does not support a named header
      ||    file, you may comment it out, as long as the code compiles
      ||    with no warnings.   

    Modification History

    WHO     WHEN        WHAT
    CFA     06/06/93    Added this header 
    CFA     06/06/93    ANSI/K & R port, testing
    KWB     02/27/94    General makeover

------------------------------------------------------------------------------*/


#define MEMCHECK_MODULE /* I, MemCheck */
#define LINT_ARGS

/* VER/REV */
#define MEMCHECK_VERSION    "2.1c"
#ifdef	__BORLANDC__
#define __STDC__ 1
#endif

#ifdef __STDC__
#   define _MCANSI_
#else
#   ifdef __cplusplus
        /* If C++, must be ANSI-compliant */
#       define _MCANSI_
#   else
        /* If not ANSI-standard, can be either 
        System V-compatible or K&R-compatible */
#       ifndef NOTSYSV
#           define _MCSYSV_
#       else
#           define _MCKR_
#       endif
#   endif   /* not C++ */
#endif


/* **************** HEADER FILE INCLUSION SECTION ************ */

/*  See note at the top of this file if you get
    errors or warnings...  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* memory.h -- available on some systems,
   not on others.  You can take it out
   if it causes errors...  it's simply
   included for function prototypes.
*/
#ifndef __VAXC__
#ifndef _MCANSI_
#include <memory.h>
#endif

#ifdef __GNUC__
#include <memory.h>
#endif
#endif /* not VAX C */
    

/*  Include the MemCheck header file.
    Contains macros for ANSI/KR 
*/
#include "memcheck.h"

/* Put in 9/23 to elim warnings for ANSI C under mc_breakpoint */
#if defined (_MCANSI_) 
#   include <errno.h>
#endif

/* ** END HEADER FILE INCLUSION SECTION ** */


#if !defined (min)
#   define min(a,b)     ( ((a) < (b)) ? (a) : (b) )
#endif
#if !defined (max)
#   define max(a,b)     ( ((a) > (b)) ? (a) : (b) )
#endif

#   ifdef   USE_FILES       
#       ifndef _MCANSI_
            typedef unsigned long fpos_t;
#       endif

        /* --------- FILE DECLARATIONS --------------*/
        /* Track by file positions */
        typedef fpos_t  BXPTR;      
        typedef fpos_t  BXHDL;      

#   else                    

        /* --------- CHIP DECLARATIONS -------------*/
        /* Track by memory pointers */
        typedef struct Node *   BXPTR;
        typedef BXPTR *         BXHDL;

#   endif

/*  Use of `register' keyword may cause errors in
    earlier compilers...
*/
#ifndef _MCANSI_
#define register
#endif

typedef struct Node 
    {
        BXPTR   LeftHandle;
        BXPTR   RightHandle;
    } MEMNODE; 

typedef MEMNODE *MEMNODEPTR;


/*
    User-defined node comparison function typedef.

    This is the type of function that will discriminate between
    the nodes of data the user stores in the tree.
*/
typedef int     (*COMPFUNC) _MCPROTO((void *, void *)); 

/* Debugging use... */
typedef char *  (*DATAPRT)  _MCPROTO((void *));

/* BX Error Codes */
#define BXE_NO_ERROR        0       /* all clear */
#define BXE_BTREE_FILE      1       /* error opening binary tree datafile */
#define BXE_DISK_SPACE      2       /* out of disk space */
#define BXE_OUT_OF_MEMORY   3       /* what it says */
#define BXE_DUPLICATE_NODE  4       /* node already in tree [AddNode()] */


/* 
    BX Operation Mode Values. 
    Using files or memory for tree?

    Returned by BXGetMode ();
*/
#define BXMODE_FILES        0
#define BXMODE_MEMORY       1

#ifndef NOPROTO      /* Skip if creating prototypes */

#ifdef USE_FILES

    void    BXSetDataPrint      _MCPROTO((char *(*)(void  *)));
    int     BXAddNode           _MCPROTO((void *,fpos_t *));
    int     BXDeleteNode        _MCPROTO((fpos_t));
    fpos_t  BXFindNode          _MCPROTO((void *));
    fpos_t  BXGetRootNode       _MCPROTO((void));
    fpos_t  BXGetRightHandle    _MCPROTO((fpos_t));
    fpos_t  BXGetLeftHandle     _MCPROTO((fpos_t));
    void    BXGetData           _MCPROTO((fpos_t,void *));
    int     BXGetMode           _MCPROTO((void));
    char  * BXErrText           _MCPROTO((unsigned));
    void    BXSetNodeCompareFunc _MCPROTO((int (*)(void *,void *)));
    void    BXSetTrackDir       _MCPROTO((char *));
    void    ExitBXTree          _MCPROTO((void));
    int     InitBXTree          _MCPROTO((int (*)(void *,void *),unsigned int));

#else
    
    void    BXSetDataPrint      _MCPROTO((char *(*)(void  *)));
    int     BXAddNode           _MCPROTO((void *,struct Node **));
    int     BXDeleteNode        _MCPROTO((struct Node **));
    struct  Node * *BXFindNode  _MCPROTO((void *));
    struct  Node *BXGetRootNode _MCPROTO((void));
    /*  struct  Node *BXGetRightHandle _MCPROTO((struct  Node *));  */
    /*  struct  Node *BXGetLeftHandle _MCPROTO((struct  Node *));   */
    void    BXGetData           _MCPROTO((struct Node *,void *));
    int     BXGetMode           _MCPROTO((void));
    char *  BXErrText           _MCPROTO((unsigned));
    void    BXSetNodeCompareFunc _MCPROTO((int (*)(void *,void *)));
    void    BXSetTrackDir       _MCPROTO((char *));
    int     InitBXTree          _MCPROTO((int (*)(void *,void *),unsigned int));
    void    ExitBXTree          _MCPROTO((void));

#   define  BXGetRightHandle(bxptr) ( (bxptr)->RightHandle )
#   define  BXGetLeftHandle(bxptr)  ( (bxptr)->LeftHandle )
#endif

#endif

/* ************* End B-tree decls ************* */


/* Number of Calls Intercepted by MEMCHECK */
#define NUM_CALLS       (sizeof (S_FuncName)/sizeof (char *))

/*
    Function Identification Numbers (MCID's)

    These are the indices used to retrieve information
    about specific runtime library calls.
*/
/* ---  STANDARD C ROUTINES  --- */
#define MCID_CALLOC         0
#define MCID_FREE           1
#define MCID_MALLOC         2
#define MCID_MEMCPY         3
#define MCID_MEMMOVE        4
#define MCID_MEMSET         5
#define MCID_REALLOC        6
#define MCID_SPRINTF        7
#define MCID_STRCAT         8
#define MCID_STRCPY         9
#define MCID_STRNCAT        10
#define MCID_STRNCPY        11
#define MCID_VSPRINTF       12

/* --- MEMCHECK INTERNAL FUNCTIONS --- */
#define MCID_CHECK_BUFFER   13
#define MCID_REGISTER       14
#define MCID_UNREGISTER     15
#define MCID_STRDUP         16
#define MCID_NEW            17
#define MCID_DELETE         18
#define LAST_INTERNAL       18  /* Set = to last internal ID */


/* Memory Checker Internal Message IDs */
#define MCIM_CHKBYT     0
#define MCIM_ALNSIZ     1
#define MCIM_CBSIZE     2
#define MCIM_INTRO      3
#define MCIM_MICBEG     4
#define MCIM_MICEND     5
#define MCIM_MICERR     6
#define MCIM_NOBRND     7
#define MCIM_NLASGN     8

/* For pre-emptive checks */
#define BEFORE_CALL     1
#define AFTER_CALL      2

/* Defines for NULL Pointer alert */
#define OP_ARG          0       /* only arg */
#define OP_SOURCE       1       /* source arg for copy ops */
#define OP_DEST         2       /* destination for copy ops */

/* Define for ops to CheckTransfer */
#define NO_SOURCE   ((MCPTR)0xFFFFABDC)

/* Defines for null source or dest op errors */
#define NULL_SOURCE     -1      /* must be < 0 ! */
#define NULL_DEST       -2      /*   "  "   "   */

#define FAST_MODE   (!MC_ACTIVE || ( MC_Speed == MC_RUN_FAST ))
#define IS_AUTO(s)  ( ((s) != 0) && ((s) != sizeof(char *)) )

typedef unsigned long   ULONG;
typedef unsigned int    UINT;
typedef unsigned char   UCHAR;

typedef MCVOIDP         MCPTR;      /* type of ptr stored in tree */
typedef int             MCID;       /* MEMCHECK function ID */


/* 
    ----- Debugging Definitions ---- 

    Debugging statements won't be included 
    in distribution libraries...
*/

#ifdef MCDEBUG
#   define DEBUG(a)             printf(a)
#   define DEBUG2(a,b)          printf(a,b)
#   define DEBUG3(a,b,c)        printf(a,b,c)
#   define DEBUG4(a,b,c,d)      printf(a,b,c,d)
#   define DEBUG5(a,b,c,d,e)    printf(a,b,c,d,e)
#   define BXApplData(d)        ((S_DataPrint == NULL) ? "NODATA" : (*S_DataPrint)(d))
#else
#   define DEBUG(a)             
#   define DEBUG2(a,b)          
#   define DEBUG3(a,b,c)    
#   define DEBUG4(a,b,c,d)      
#   define DEBUG5(a,b,c,d,e)
#endif

#define MAXMALLOCSIZE   ( ((ULONG)0xFFFFFFFF) - 2 * MAXCHECKBYTECT )

/* Macros must be used with a char cast, i.e.

        void *p = USERPTR ((char *)memptr);
*/
#define USERPTR(p)  ( (p) + (int)MC_CheckByteCt )
#define REALPTR(p)  ( (p) - (int)MC_CheckByteCt )

/* Minimum and Maximum Values */
#define MINALIGNSIZE    2
#define MINCHECKBYTECT  2   /* one is not enough */
#define MAXCHECKBYTECT  64  /* quite enough ? */

/* Memory Macros */
#define AlignedSize(s)      ( (((ULONG)s + (ULONG)MC_AlignSize - 1L) \
                / (ULONG)MC_AlignSize) * (ULONG)MC_AlignSize )
#define CHECKSUM(pos)       ( (UCHAR) ((pos * 23) + (200 * 23) % 256) )

/* General Defines */
#define UNKNOWN_FILE        "?.C"
#define DO_FREE             0
#define DONT_FREE           1
#define FRONT_MAGIC         0       /* front overwritten */
#define BACK_MAGIC          1       /* back  overwritten */

/* C++ defines */
#define CPPFLAG_NEW         1
#define CPPFLAG_DELETE      2

/* User-Definable Defaults */

typedef union {
    long    i;
    double  d;
    int     (*f)();
} align_u;

typedef struct {
    align_u u;
    char    c;
} align_t;

#define D_ALIGNSIZE     (sizeof(align_t) - sizeof(align_u)) 
#define D_CHECKBYTECT   D_ALIGNSIZE
#define D_MFUNC         erf_default /* default error notification */

/* Application Defaults */
#define D_ERRBUFSIZE    ((size_t)256)   /* for reporting errors */

typedef struct MemRecord 
    {
        MCPTR       userptr;        
        MCID        mcid;
        ULONG       allocno;
        ULONG       size;
        MCSF        file;       /* source file (ANSI) */
        MCSL        lineno;     /* source line (ANSI) */

    } MEMRECORD, *MEMRECORDPTR;


/*
    Externally available variable pool.
*/

extern short        MC_ACTIVE;
extern int          MC_Mode;
extern int          MC_Speed;
extern ULONG        MC_AllocCount;
extern int          MC_BreakPoints;
extern UINT *       MC_BreakArray;
extern UINT         MC_CheckByteCt;
extern UINT         MC_AlignSize;           /* alignment boundary size */
extern MFUNC        MC_MFunc;               /* error notification routine */
extern char *       MC_MsgBuf;              /*  */
extern MEMRECORD    MC_MemRecord;           /* work record */
extern short        MC_ErrorFlags;          /* egregious error flag */
extern char *       MC_FuncName[];          /* function name array */
extern int          MC_NullCheck;
extern int          MC_CPPFlag;

#define ENV_MEMCHECK    "MEMCHECK"      /* set if MEMCHECK to be active */
#define ENV_TRACKDIR    "MEMTRACKDIR"   /* sets dir to use under files vers */


#ifndef NOPROTO      /* Skip if creating prototypes */

#ifdef USE_FILES
    void  DoTextMessage         _MCPROTO((int));
    void  DoActiveMessage       _MCPROTO((char *));
    void  DoBufMagicMessage     _MCPROTO((int,struct  MemRecord *));
    void  DoRefBufMagicMessage  _MCPROTO(((int,struct MemRecord *,int,char *,MCSL,int));
    void  DoNotFreedMessage     _MCPROTO((struct MemRecord *));
    void  DoBadPtrMessage       _MCPROTO((int,void *,char *,MCSL));
    void  DoNullPtrMessage      _MCPROTO((int,int,char *,MCSL));
    void  DoOutOfMemMessage     _MCPROTO((int,char *,MCSL,unsigned long,unsigned long));
    void  DoNullAssignMessage   _MCPROTO((char  *,MCSL));
    void  DoDestLenMessage      _MCPROTO((int,char *,MCSL,unsigned long,struct MemRecord *));
    void DoTrashAutoMessage     _MCPROTO((int,MCSIZE,MCSIZE,char *,MCSL));
    unsigned long MEMCHECK_SIZE _MCPROTO((unsigned long));
    void  SetFrontCheckBytes    _MCPROTO((unsigned char *));
    void  NullSegSnapshot       _MCPROTO((void));
    int   NullPtrAssign         _MCPROTO((void));
    void  SetBackCheckBytes     _MCPROTO((unsigned char *,unsigned long));
    int  CheckBufferMagic       _MCPROTO((struct MemRecord *));
    int  CheckRefBufferMagic    _MCPROTO((struct MemRecord *,int,char *,MCSL,int));
    short  MC_TraverseTree      _MCPROTO((fpos_t,short));
    int  MC_TreeComp            _MCPROTO((MCVOIDP,MCVOIDP));
    void  SetMemInfo            _MCPROTO((int,struct MemRecord *,void *,unsigned long,char  *,MCSL));
    fpos_t  FindAllocedPtr      _MCPROTO((struct  MemRecord *,int,void *,char *,MCSL));
    int  CheckTransfer          _MCPROTO((struct MemRecord *,void *,int,unsigned long,char *,MCSL));
    int MC_PtrInTreeBuffer      _MCPROTO((struct MemRecord *));
    
#else
    
    void  DoTextMessage         _MCPROTO((int));
    void  DoActiveMessage       _MCPROTO((char *));
    void  DoBufMagicMessage     _MCPROTO((int, struct MemRecord *));
    void  DoRefBufMagicMessage  _MCPROTO((int, struct MemRecord *, int, char *,MCSL,int));
    void  DoNotFreedMessage     _MCPROTO((struct  MemRecord *));
    void  DoBadPtrMessage       _MCPROTO((int,void *,char *,MCSL));
    void  DoNullPtrMessage      _MCPROTO((int,int, char *,MCSL));
    void  DoOutOfMemMessage     _MCPROTO((int,char *,MCSL,unsigned long,unsigned long));
    void  DoNullAssignMessage   _MCPROTO((char *,MCSL));
    void  DoDestLenMessage      _MCPROTO((int,char *,MCSL,unsigned long,struct MemRecord *));
    void  DoTrashAutoMessage    _MCPROTO((int, MCSIZE, MCSIZE,char *, MCSL));
    unsigned long MEMCHECK_SIZE _MCPROTO((unsigned long));
    void  NullSegSnapshot       _MCPROTO((void));
    int   NullPtrAssign         _MCPROTO((void));
    void  SetFrontCheckBytes    _MCPROTO((unsigned char *));
    void  SetBackCheckBytes     _MCPROTO((unsigned char  *,unsigned long));
    int  CheckBufferMagic       _MCPROTO((struct  MemRecord *));
    int  CheckRefBufferMagic    _MCPROTO((struct MemRecord *,int,char *,MCSL,int));
    short  MC_TraverseTree      _MCPROTO((struct Node *,short));
    int  MC_TreeComp            _MCPROTO((MCVOIDP,MCVOIDP));
    void  SetMemInfo            _MCPROTO((int,struct MemRecord *,MCVOIDP,unsigned long,char *,MCSL));
    struct Node ** FindAllocedPtr _MCPROTO((struct MemRecord *,int,MCVOIDP,char *,MCSL));
    int  CheckTransfer          _MCPROTO((struct  MemRecord *,MCVOIDP,int,unsigned long,char *,MCSL));
    int MC_PtrInTreeBuffer      _MCPROTO((struct MemRecord *));
    
#endif
#endif               /* MEMMGT.P */

/* - End of #include project.h --------------------------------------->  */


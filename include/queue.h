#ifndef	_QUEUE_H
#define	_QUEUE_H

struct	_queue;

typedef	struct	_queue_element {
	struct	_queue_element	*queue_next;
	struct	_queue_element	*queue_prev;
#if	BUFDEBUG
	char			*queue_file;
	unsigned		queue_line;
	struct _queue_element *queue_chain;
	struct _queue		*queue_parent;
	char			*qi_file;
	unsigned		qi_line;
#endif
	long			 queue_key;
	unsigned int		 queue_type;
	unsigned int		 queue_size;	/* size of data object */
	unsigned int		 queue_bufsize;	/* aigh */
	void	       *	 queue_object;
} Queue_Element;

#define	QUEUE_FLAGS_NOTSORTED	0x01

typedef	struct	_queue {
	Queue_Element	*queue_head;
	Queue_Element	*queue_tail;
	int		queue_count;
	unsigned int	queue_flags;
} Queue;

Queue_Element	*QueueAlloc(long key, unsigned type, int size); /* alloc space for queue element of size */

#if	BUFDEBUG
int		QueueInsertX(Queue *Q, Queue_Element *QE, char *file, unsigned lnum);	/* enqueue in correct spot */
#define	QueueInsert(Q,QE)	QueueInsertX(Q,QE,__FILE__,__LINE__)
#else
int		QueueInsert(Queue *Q, Queue_Element *QE);	/* enqueue in correct spot */
#endif
Queue_Element	*QueueExtract(Queue *Q, Queue_Element *QE);	/* remove from queue list
								   return self */

Queue_Element	*QueueFind(Queue *Q, long key); /* get first exact match of key */
Queue_Element	*QueueKey(Queue *Q, long key);	/* get first item of key value or greater */

Queue_Element	*QueueTail(Queue *Q);		/* get last item in list */
Queue_Element	*QueueHead(Queue *Q);		/* get first item in list */

Queue_Element	*QueuePTail(Queue *Q);		/* extract last item */
Queue_Element	*QueuePHead(Queue *Q);		/* extract first item */
Queue_Element	*QueuePFind(Queue *Q, long key);	/* extract first exact match */
Queue_Element	*QueuePKey(Queue *Q, long key);	/* extract first key >= match */

#define	QTYPE_NONE	 0
#define	QTYPE_TASK	 1
#define	QTYPE_MESSAGE	 2
#define	QTYPE_MBUF	 3
#define	QTYPE_SIGNAL	 4		/* proces signal */
#define	QTYPE_PORT	 5		/* port writ signal */
#define	QTYPE_ROUTE	 6
#define	QTYPE_TCPFRAG	 7
#endif

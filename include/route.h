#ifndef	_ROUTE_H
#define	_ROUTE_H

#ifdef	__cplusplus
extern "C" {
#endif

typedef struct route {
	IPaddr		rt_net;
	IPaddr		rt_mask;
	IPaddr		rt_gw;
	unsigned	rt_metric;
	unsigned	rt_ifnum;
	unsigned	rt_key;
	unsigned	rt_ttl;
	struct route *	rt_next;

	long		rt_refcnt;
	long		rt_usecnt;
} ROUTE;

struct rtinfo {
	ROUTE	*ri_default;
	Queue	ri_bpool;
	Bool	ri_valid;
	Semaphore ri_mutex;
};

#define	RT_DEFAULT	ip_anyaddr
#define	RT_LOOPBACK	ip_loopback
#define	RT_TSIZE	256
#define	RT_INF		999     /* no timeout */

#define	RTM_INF		16	/* infinite metric */

#define	RTF_REMOTE	0
#define	RTF_LOCAL	1

#define	RT_BPSIZE	16	/* max number of routes */

#define	RTFREE(prt)	\
	if(--prt->rt_refcnt <= 0) {  \
		BufferFreeData(prt);	\
	}

extern	struct rtinfo	Route;
extern  struct route	*rttable[];


ROUTE	*rtget();
void	rtinit();
int	rtadd(IPaddr net, IPaddr mask, IPaddr gw, int metric, int intf, int ttl);
struct route *rtnew(IPaddr net, IPaddr mask, IPaddr gw, int metric, int intf, int ttl);
int	rtdel(IPaddr net, IPaddr mask);
int	rtfree(struct route *prt);

#ifdef	__cplusplus
};
#endif


#endif
#ifndef	_SEMAPHORE_H
#define	_SEMAPHORE_H
#include "queue.h"


#if	!IPDOOR
// #define	SEMDEBUG	1
#endif

#if	MBBS
#define	SEMDEBUG	1
#endif

typedef	struct	_semaphore {
	Queue	sem_waitQueue;		/* processes blocked in semaphore */
	int	sem_count;		/* current count */
} Semaphore;

#ifdef	__cplusplus
extern "C" {
#endif

Semaphore	*Semaphore_Alloc(int count);			/* allocate semaphore space */
int		Semaphore_Initialize(Semaphore *S, int count);	/* init pre-alloc'd semaphore */

int	Semaphore_Signal(Semaphore *S);
int	Semaphore_ISignal(Semaphore *S);	/* signal from interrupt */
#if	SEMDEBUG
int	Semaphore_XWait(Semaphore *S, char *f, int l);
#define	Semaphore_Wait(_s)	Semaphore_XWait(_s,__FILE__,__LINE__)
#else
int	Semaphore_Wait(Semaphore *S);
#endif
int	Semaphore_Cancel(Semaphore *S);		/* cancel all waiting proc's */
int	Semaphore_Free(Semaphore *S);		/* cancel and free space */

#ifdef	__cplusplus
};
#endif

#define	SEM_RESULT_OK		 0
#define	SEM_RESULT_BADSEM	-1

#endif

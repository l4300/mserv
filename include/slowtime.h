#ifndef	_SLOWTIME_H
#define	_SLOWTIME_H

int	slowtimer(void *parent, char *name, int args, char **argv);
#if	TSDEBUG
#define	SLOWTIME_STACK	1024
#else
#if	MBBS
#define	SLOWTIME_STACK	8192
#else
#define	SLOWTIME_STACK	2048
#endif
#endif
#define	SLOWTIME_PRIORITY	100

#endif

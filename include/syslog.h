#ifndef	_SYSLOG_H
#define	_SYSLOG_H

#ifdef	__cplusplus
extern "C" {
#endif
int	Syslog(int pri, char *format, ...);
void	Syslog_Init();

#ifdef	__cplusplus
};
#endif

#define	syslog	Syslog

#define	LOGFACILITY(n)	(n << 3)
#define	FACMASK		0x3f8
#define	DEFAULT_FACILITY	23		/* log local 7 */

#define	LOG_REBOOT	1
#define	LOG_FATAL	(1 << 1)		/* 02 */
#define	LOG_CRITICAL	(1 << 2)		/* 04 */
#define	LOG_ERROR	(1 << 3)		/* 08 */
#define	LOG_ERR		LOG_ERROR
#define	LOG_WARN	(1 << 4)		/* 10 */
#define	LOG_WARNING	LOG_WARN
#define	LOG_NOTICE	(1 << 5)		/* 20 */
#define	LOG_INFO	(1 << 6)		/* 40 */
#define	LOG_DEBUG	(1 << 7)		/* 80 */

#endif

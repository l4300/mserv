/* task.h -- task information */
#ifndef	_TASK_H
#define	_TASK_H

#include <xsetjmp.h>

#include	"message.h"

enum	_Tsk_State	{TASK_STATE_RUNNING,	/* task is actually running */
			TASK_STATE_READY,	/* task is ready to run	*/
			TASK_STATE_RECV,	/* task waiting for message */
			TASK_STATE_SLEEP,	/* task is sleeping (ALARM Queue) */
			TASK_STATE_SUSP,	/* task is suspended	*/
			TASK_STATE_SEM,		/* task is waiting on semaphore */
			TASK_STATE_SEL,		/* task waiting on select */
			TASK_STATE_DEFUNCT,	/* dead task */
			TASK_MAX_STATES
};

typedef	enum	_Tsk_State Tsk_State;
typedef	int	(*ProcessType)(void *Parent, char *name, int nargs, char *argv[]);

typedef struct _task {
	Queue_Element	QE;			/* my queue self */
	MessageBox	Messages; 		/* messages for myself */
	Tsk_State	tsk_state;
	unsigned	tsk_selector;		/* protected mode stack base selector */
	void	*	tsk_stack_base;		/* bottom of stack */
	unsigned	tsk_stack_size;   	/* total size in bytes */
	void	*	tsk_stack_pointer;	/* starting stack pointer */
	xjmp_buf	tsk_call_jump_buf;
	char		*tsk_name;		/* what its' called */
	int		tsk_retcode;
	ProcessType	tsk_process;
	int		tsk_nargs;
	char		**tsk_args;
	struct _task	*tsk_parent;
	struct	_task	*tsk_next;		/* next task */
	long		tsk_total_ticks;	/* total ticks used by task */
	long		tsk_hold_ticks;
	long		tsk_cycles;
	unsigned	tsk_utilization;	/* running utilization */
	unsigned	tsk_csum;  		/* csum of task stack */
	int		tsk_regbank;		/* register bank selector */
} Task;


#ifdef	__cplusplus
extern 	"C" {
#endif

Task	*TaskAlloc(ProcessType Proc, void *stack, unsigned stacksize, int priority, char *tname, int nargs, char **args);
		/* creates a task and places it on the run queue, its ready to go */


int	TaskReady(Task *T, int retcode, int resched);	/* enable this task, it will return
						   retcode when it returns from
						   taskswitch */
int	TaskInitialize();			/* init task system */
int	TaskKill(Task *T);			/* kill this task, remove from queues */

void	TaskDump();				/* dump diag info to stdout */

#define	TASK_RESCHEDULE		 1
#define	TASK_NORESCHEDULE	 	0

#define	TASK_MAX_MESSAGES	 	50

#define	TASK_RESULT_OK		 0
#define	TASK_RESULT_BADTASK		-1
#define	TASK_RESULT_NOTASKS		-2

int	TaskReschedule();			/* take next runnable process
						    returns result from switch */
extern	Task	*Task_Current_Task;		/* ptr to current running task */
extern	Queue	Task_RunQueue;			/* queue of runable tasks	*/
extern	long	TaskStartTicks;
extern	Task	*Task_List;
extern	char	*Tstates[];

unsigned	int	getpid();

#define	GUARDWORD	0xbcaf

#ifdef	__cplusplus
};
#endif

#endif

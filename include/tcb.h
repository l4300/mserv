#ifndef	_TCB_H
#define	_TCB_H

#if	MBBS
#define	TCPISTACK	8192
#else
#define	TCPISTACK	512
#endif
#define	TCPIPRIO	100

#if	MBBS
#define	TCPOSTACK	8192
#else
#define	TCPOSTACK	512
#endif
#define	TCPOPRIO	100

#if	MBBS || IPDOOR
#define	TCPSBS		4096
#define	TCPRBS		4096
#else
#define	TCPSBS		512	/* send buffer size */
#define	TCPRBS		1460	/* receive buffer size */
#endif

#define	TCPQLEN		20		/* tcp process port queue length */
#define	TCPMAXURG	MAXNETBUF	/* max urget data buffer size */
#define	TCPUQLEN	5	/* tcp urgent queue length */

#define	TCPE_RESET	-1
#define	TCPE_REFUSED	-2
#define	TCPE_TOOBIG	-3
#define	TCPE_TIMEDOUT	-4
#define	TCPE_URGENTMODE	-5
#define	TCPE_NORMALMODE	-6

#define	TCBF_NEEDOUT	0x01	/* need output */
#define	TCBF_FIRSTSEND	0x02	/* no data to ack */
#define	TCBF_GOTFIN	0x04	/* received fin */
#define	TCBF_RDONE	0x08	/* no more receive data */
#define	TCBF_SDONE	0x10	/* send done */
#define	TCBF_DELACK	0x20	/* do delayed acks */
#define	TCBF_BUFFER	0x40	/* do tcp buffering */
#define	TCBF_PUSH	0x80	/* got push */
#define	TCBF_SNDFIN	0x100	/* send a fin */

#define	TCP_BUFFER	TCBF_BUFFER
#define	TCP_DELACK	TCBF_DELACK

#define	NTCPFRAG	10

#define	UQTSIZE	(2*Ntcp)	/* total max # pending urgent segs */

#define	UQS_FREE	0
#define	UQS_ALLOC	1

struct uqe {
	Queue_Element	QE;
	int	uq_state;
	tcpseq	uq_seq;			/* starting sequence of this buffer */
	int	uq_len;			/* length of this buffer */
	char	*uq_data;		/* null if on urgent hole queue */
	Buffer	*uq_B;			/* data buffer if uq_data */
};


#define	SUDK(ptcb, seq)	(ptcb->tcb_sudseq - (seq))
#define	SUHK(ptcb, seq) (ptcb->tcb_suhseq - (seq))
#define	RUDK(ptcb, seq) (ptcb->tcb_rudseq - (seq))
#define	RUHK(ptcb, seq)	(ptcb->tcb_ruhseq - (seq))

extern	Bool	uqidone;
extern	struct	uqe	uqtab[];
extern	Semaphore	uqmutex;

struct tcb {
	short	tcb_state;
	short	tcb_ostate;
	short	tcb_type;
#define	TCPT_SERVER	1
#define	TCPT_CONNECTION	2
#define	TCPT_MASTER	3
	Semaphore	tcb_mutex;
	short	tcb_code;
	short	tcb_flags;
	short	tcb_error;

	IPaddr	tcb_rip;		/* remote ip */
	short	tcb_rport;		/* remote IP port */
	IPaddr	tcb_lip;
	short	tcb_lport;
	struct	netif *tcb_pni;

	tcpseq	tcb_suna;
	tcpseq	tcb_snext;
	tcpseq	tcb_slast;		/* send last fin */
	long	tcb_swindow;
	tcpseq	tcb_lwseq;
	tcpseq	tcb_lwack;
	int	tcb_cwnd;
	int	tcb_ssthresh;
	int	tcb_smss;
	tcpseq	tcb_iss;

	int	tcb_srt;
	int	tcb_rtde;
	int	tcb_persist;
	int	tcb_keep;
	int	tcb_rexmt;
	int	tcb_rexmtcount;

	tcpseq	tcb_rnext;

	tcpseq	tcb_rudseq;
	Queue	tcb_rudq;	/* ### ?  receive urg data queue */
	tcpseq	tcb_ruhseq;
	Queue	tcb_ruhq;
	Queue	tcb_sudq;
	int	tcb_sudseq;
	Queue	tcb_suhq;
	int	tcb_suhseq;

	int	tcb_lqsize;		/* listen queue size */
	Queue	tcb_listenq;		/* listen queue port */
	struct	tcb *tcb_pptcb;		/* parent tcb for accept */
	Semaphore tcb_ocsem;
	int	tcb_dvnum;	/* slave device number ?? */

	Semaphore	tcb_ssema;	/* send sem */
	char	*tcb_sndbuf;
	int	tcb_sbstart;
	int	tcb_sbcount;
	int	tcb_sbsize;

	Semaphore	tcb_rsema;
	char	*tcb_rcvbuf;
	int	tcb_rbstart;
	int	tcb_rbcount;
	int	tcb_rbsize;
	int	tcb_rmss;

	tcpseq	tcb_cwin;
	Queue	tcb_rsegq;	/* seg frag queue */
	tcpseq	tcb_finseq;
	tcpseq	tcb_pushseq;
	void	*tcb_socket;
};


struct	tcpfrag {
	tcpseq	tf_seq;
	int	tf_len;
};

#define	TCPC_LISTENQ	0x01
#define	TCPC_ACCEPT	0x02
#define	TCPC_STATUS	0x03
#define	TCPC_SOPT	0x04
#define	TCPC_COPT	0x05
#define	TCPC_SENDURG	0x06

extern	Task	*tcps_oport;
extern	Task	*tcps_iport;
extern	int	tcps_lqsize;	/* default server queue size */

extern	Semaphore	tcps_tmutex;

#ifdef	DONTDDEF
extern	int    NEAR	(*tcpswitch[])();
extern int     NEAR (*tcposwitch[])();
#endif

#ifdef	Ntcp
extern	struct	tcb	tcbtab[];
#endif


#define	TCPOPT_GETSNDBUF	1			/* opt is pointer to int */
#define	TCPOPT_SETSNDBUF	2
#define	TCPOPT_GETRCVBUF	3
#define	TCPOPT_SETRCVBUF	4


#endif

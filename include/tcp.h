#ifndef	_TCP_H
#define	_TCP_H

typedef	long	tcpseq;

#define	SEQCMP(a,b)	((a) - (b))

struct	tcp {
	unsigned short	tcp_sport;
	unsigned short	tcp_dport;
		tcpseq	tcp_seq;
		tcpseq	tcp_ack;
		char	tcp_offset;
		char	tcp_code;
	unsigned short	tcp_window;
	unsigned short	tcp_cksum;
	unsigned short	tcp_urgptr;
		char	tcp_data[1];
};

#define	TCPF_URG	0x20
#define	TCPF_ACK	0x10
#define	TCPF_PSH	0x08
#define	TCPF_RST	0x04
#define	TCPF_SYN	0x02
#define	TCPF_FIN	0x01

#define	TCPMHLEN	20
#define	TCPHOFFSET	0x50

#define	TCP_HLEN(ptcp)	(((ptcp)->tcp_offset & 0xf0) >> 2)	/* was >> */

#define	TPO_EOOL	0	/* end of option list */
#define	TPO_NOOP	1
#define	TPO_MSS		2



#endif
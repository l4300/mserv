#ifndef	_TCPFSM_H
#define	_TCPFSM_H

#define	READERS		1
#define	WRITERS		2

#define	TCPINCR		924

#define	TCPS_FREE	0
#define	TCPS_CLOSED	1
#define	TCPS_LISTEN	2
#define	TCPS_SYNSENT	3
#define	TCPS_SYNRCVD	4
#define	TCPS_ESTABLISHED	5
#define	TCPS_FINWAIT1	6
#define	TCPS_FINWAIT2	7
#define	TCPS_CLOSEWAIT	8
#define	TCPS_LASTACK	9
#define	TCPS_CLOSING	10
#define	TCPS_TIMEWAIT	11

#define	NTCPISTATES	12

#define	TCPO_IDLE	0
#define	TCPO_PERSIST	1
#define	TCPO_XMIT	2
#define	TCPO_REXMT	3

#define	NTCPOSTATES	4

#define	SEND		0x01
#define	PERSIST		0x02
#define	RETRANSMIT	0x03
#define	DELETE		0x04
#define	TMASK		0x07

#define	EVENT(x)	(x & TMASK)
#define	TCB(x)		(x >> 3)
#define	MKEVENT(timer, tcb)	((tcb << 3) | (timer & TMASK))

#define	TCP_MAXRETRIES	12	/* max retransmits before closing */
#define	TCP_TWOMSL	500	/* 5 seconds, not really up to spec */

#define	TCP_MAXRXT	2000	/* 20 seconds max rexmt time */
#define	TCP_MINRXT	50	/* 1/2 second min time */
#define	TCP_ACKDELAY	20	/* 1/5 sec ack delay if tcbf_delack set */

#define	TCP_MAXPRS	6000	/* 1 min max persist */

#define	TSF_NEWDATA	0	/* send new data */
#define	TSF_REXMT	1	/* send old stuff */

#define	TWF_NORMAL	0
#define	TWF_URGENT	1

#ifdef	__TCP_C
#if	DONEAR
#define	NEAR	near
#else
#define	NEAR
#endif
#else
#define	NEAR
#endif
struct	tcp	*tcpnet2h(struct tcp *ptcp);
struct	tcb	*tcballoc();
int		tcbdealloc(struct tcb *ptcb);
unsigned short	tcpcksum(struct ip *pip);
struct	tcb	*tcpdemux(struct ep *pep);
int		NEAR tcpok(struct tcb *ptcb, struct ep *pep);

int		NEAR tcpclosed(struct tcb *ptcb, struct ep *pep);
int		NEAR tcpwait(struct tcb *ptcb);
int		NEAR tcptimewait(struct tcb *ptcb, struct ep *pep);
int		NEAR tcpclosing(struct tcb *ptcb, struct ep *pep);
int		NEAR tcpfin2(struct tcb *ptcb, struct ep *pep);
int		NEAR tcpfin1(struct tcb *ptcb, struct ep *pep);
int		NEAR tcpclosewait(struct tcb *ptcb, struct ep *pep);
int		NEAR tcplastack(struct tcb *ptcb, struct ep *pep);
int		NEAR tcpestablished(struct tcb *ptcb, struct ep *pep);
int		NEAR tcpdata(struct tcb *ptcb, struct ep *pep);
int		NEAR tcpsynsent(struct tcb *ptcb, struct ep *pep);
int		NEAR tcpsynrcvd(struct tcb *ptcb, struct ep *pep);
int		NEAR tcplisten(struct tcb *ptcb, struct ep *pep);


int		NEAR tcpwinit(struct tcb *ptcb, struct tcb *newptcb, struct ep *pep);
int		NEAR tcpdodat(struct tcb *ptcb, struct tcp *ptcbx, tcpseq first, int datalen);
int		NEAR tfinsert(struct tcb *ptcb, tcpseq seq, int datalen);
int		NEAR tfcoalesce(struct tcb *ptcb, int datalen, struct tcp *ptcp);
int		NEAR tcpabort(struct tcb *ptcb, int error);

int		tcpsync(struct tcb *ptcb);

int		NEAR tcpidle(int tcbnum, int event);
int		NEAR tcppersist(int tcbnum, int event);
int		NEAR tcpxmit(int tcbnum, int event);
int		NEAR tcprexmt(int tcbnum, int event);

int		NEAR tcpsend(int tcbnum, Bool rexmt);
int		NEAR tcpsndlen(struct tcb *ptcb, Bool rexmt, int *poff);
int		NEAR tcphowmuch(struct tcb *ptcb);
int		NEAR tcpreset(struct ep *pepin);

struct		tcp	*tcph2net(struct tcp *ptcp);
int		tcpgetspace(struct tcb *ptcb, int len);
int		tcpwakeup(int type, struct tcb *ptcb);
unsigned int	NEAR tcpiss();

int		tmset(Task *T, int qlen, int value, int delay);


int		NEAR tcprwindow(struct tcb *ptcb);
int		NEAR tcpswindow(struct tcb *ptcb, struct ep *pep);
int		NEAR tcpsmss(struct tcb *ptcb, struct tcp *ptcp, char *popt);
int		NEAR tcpopts(struct tcb *ptcb, struct ep *pep);
int		NEAR tcprmss(struct tcb *ptcb, struct ip *pip);
int		NEAR tcprtt(struct tcb *ptcb);
int		NEAR tcpacked(struct tcb *ptcb, struct ep *pep);
int		NEAR tcpackit(struct tcb *ptcb, struct ep *pep);
int		NEAR tcprcvurg(struct tcb *ptcb, struct ep *pep);
struct	uqe	*uqalloc();
int		uqinit();
int		uqfree(struct uqe *puqe);
int		tcprurg(struct tcb *ptcb, char *pch, int len);
int		NEAR tcpaddhole(struct tcb *ptcb, struct uqe *puqe);
struct	uqe	*tcprhskip(struct tcb *ptcb, struct uqe *puqe, tcpseq seq);
int		tcpgetdata(struct tcb *ptcb, char *pch, int len);
int		tcpwurg(struct tcb *ptcb, int sboff, int len);
int		tcpsndurg(int	tcbnum);
int		NEAR tcpostate(struct tcb *ptcb, int acked);
int		NEAR tcpshskip(struct tcb *ptcb, int datalen, int * poff);

int		tcpcon(struct tcb *ptcb);

int		tcpout(Task *Parent, char *name, int nargs, char **argv);
int		tcpinp(Task *Parent, char *name, int nargs, char *argv[]);
#endif
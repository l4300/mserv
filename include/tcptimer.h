#ifndef	_TCPTIMER_H
#define	_TCPTIMER_H

struct	tqent {
	struct	tqent	*tq_next;	/* next one in list */
	int	tq_timeleft;		/* 1/100 sec time to expire */
	long	tq_time;		/* time when entry was queued */
	Task	*tq_port;		/* port to send signal to */
	int	tq_portlen;		/* length of port queue */
	int	tq_msg;			/* msg to send */
};

#if	TSDEBUG
#define	TMSTACK		1024
#else
#if	MBBS
#define	TMSTACK		8192
#else
#define	TMSTACK		512
#endif
#endif
#define	TMPRIO		100
#define	TMNAME		"tcptimer"

extern	Semaphore	tqmutex;
extern	Task		*tqpid;
extern	struct	tqent	*tqhead;

extern	int	tcptimer(Task *Parent, char *name, int argc, char **argv);

int	tmclear(Task	*port, int msg);
int	tcpkilltimers(struct tcb *ptcb);
int	tmleft(Task	*port, int msg);
int	tmset(Task	*port, int portlen, int msg, int tim);
int	tcpkick(struct tcb *ptcb);
#endif

#ifndef	_UDP_H
#define	_UDP_H

#define	U_HLEN	8
#define	U_DO_CSUM	0

#define	U_MAXLEN	(IP_MAXLEN - (IP_MINHLEN << 2)-U_HLEN)

struct	udp {
	unsigned int	u_src;
	unsigned int	u_dst;
	unsigned int	u_len;
	unsigned int	u_cksum;
	unsigned char	u_data[U_MAXLEN];
};

#define	ULPORT	1025

#define	MAX_UDP_RECV	32		/* max number of receive packets */
#define	MAX_UDP_RBYTES	(16L * 1024L)	/* max number of receive bytes in queues */

struct	upq {
	struct	_socket	*up_socket;	/* socket who owns me, null if free */
	Queue		up_data;
};


void		udpnet2h(struct	udp	*pudp);
void		udph2net(struct udp	*pupd);
int		udp_in(struct netif *pni, struct ep *pep);
unsigned int    udpcksum(struct ip *pip);
int		udpsend(IPaddr sip, IPaddr fip, unsigned int fport, unsigned int lport, struct ep *pep, int datalen, Bool docksum);

#endif

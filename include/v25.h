#ifndef	_V25_H
#define	_V25_H

typedef	void  (interrupt * IntVector)();

typedef	struct	_regbank {
	unsigned int	_r_reserved;
	unsigned int	r_vector_ip;
	unsigned int	r_flags;
	unsigned int	r_ip;
	unsigned int	r_ds;
	unsigned int	r_ss;
	unsigned int	r_cs;
	unsigned int	r_es;
	unsigned int	r_di;
	unsigned int	r_si;
	unsigned int	r_bp;
	unsigned int	r_sp;
	unsigned int	r_bx;
	unsigned int	r_dx;
	unsigned int 	r_cx;
	unsigned int	r_ax;
} RegisterBank;

#define	IDA_REGISTER_COUNT	8
#define	MSF_REGISTER_COUNT	8

typedef	struct	_msf {
	unsigned char	m_bytecount;
	unsigned char	m_sfr_register_pointer;
	unsigned char	m_search_character;
	unsigned char	m_reserved;
	unsigned int	m_offset;
	unsigned int	m_segment;
} MSF;


typedef	union	_ida {		/* internal data area */
	struct {
		MSF		MRegister[MSF_REGISTER_COUNT];
	} msf;
	struct {
		RegisterBank	Register[IDA_REGISTER_COUNT];
	} rb;
} IDA;


/* here are offsets into the SFRs */

#define	SFR_P0		0
#define	SFR_PM0		1
#define	SFR_PMC0      	2

#define	SFR_P1		8
#define	SFR_PM1		9
#define	SFR_PMC1	0xa

#define	SFR_P2		0x10
#define	SFR_PM2		0x11
#define	SFR_PMC2	0x12

#define	SFR_PT		0x38
#define	SFR_PMT		0x3b

#define	SFR_INTM	0x40

#define	SFR_EMS0	0x44
#define	SFR_EMS1	0x45
#define	SFR_EMS2	0x46

#define	SFR_EXIC0	0x4c
#define	SFR_EXIC1	0x4d
#define	SFR_EXIC2	0x4e

#define	SFR_RXB0	0x60
#define	SFR_TXB0	0x62

#define	SFR_SRMS0	0x65
#define	SFR_STMS0	0x66

#define	SFR_SCM0	0x68
#define	SFR_SCC0	0x69
#define	SFR_BRG0	0x6a
#define	SFR_SCE0	0x6b
#define	SFR_SEIC0	0x6c
#define	SFR_SRIC0	0x6d
#define	SFR_STIC0	0x6e

#define	SFR_RXB1	0x70
#define	SFR_TXB1	0x72

#define	SFR_SRMS1	0x75
#define	SFR_STMS1	0x76

#define	SFR_SCM1	0x78
#define	SFR_SCC1	0x79
#define	SFR_BRG1	0x7a
#define	SFR_SCE1	0x7b
#define	SFR_SEIC1	0x7c
#define	SFR_SRIC1	0x7d
#define	SFR_STIC1	0x7e

#define	SFR_TM0L	0x80
#define	SFR_TM0H	0x81
#define	SFR_MD0L	0x82
#define	SFR_MD0H	0x83

#define	SFR_TM1L	0x88
#define	SFR_TM1H	0x89
#define	SFR_MD1L	0x8a
#define	SFR_MD1H	0x8b

#define	SFR_TMC0	0x90
#define	SFR_TMC1	0x91

#define	SFR_TMMS0	0x94
#define	SFR_TMMS1	0x95
#define	SFR_TMMS2	0x96

#define	SFR_TMIC0	0x9c
#define	SFR_TMIC1	0x9d
#define	SFR_TMIC2	0x9e

#define	SFR_DMAC0	0xa0
#define	SFR_DMAM0	0xa1
#define	SFR_DMAC1	0xa2
#define	SFR_DMAM1	0xa3

#define	SFR_DIC0	0xac
#define	SFR_DIC1	0xad

#define	SFR_RFM	0xe1

#define	SFR_TBIC	0xec

#define	SFR_WTCL	0xe8
#define	SFR_WTCH	0xe9

#define	SFR_PSWL 	0xea
#define	SFR_PRC		0xeb

#define	SFR_SB		0xe0
#define	SFR_IDB		0xff

/* Interrupts */

#define	INT_NMI		2
#define	INT_IOTRAP	19

#define	INT_TM0		28
#define	INT_P0		24
#define INT_P1		25
#define INT_P2		26

#define	INT_SE0		12	/* serial error 0 */
#define	INT_SR0		13
#define	INT_ST0		14

/* MS specific definitions */

/* #define	CHANGE_IDBR		0xbf		/* internal data base register */

#define	SEEQ_REG_BASE	0
#define	SEEQ_EMS	SFR_EMS0	/* macro service register */
#define	SEEQ_EXIC	SFR_EXIC0	/* external interrupt control */
#define	SEEQ_INT	INT_P0

#define	PARALLEL_EXIC_BUSY	SFR_EXIC2
#define	PARALLEL_EXIC_ACK	SFR_EXIC1

/* these are Register bank selectors */
#define	EXIC_VECTOR	5
#define	PARALLEL_VECTOR	EXIC_VECTOR
#define	SEEQ_VECTOR	EXIC_VECTOR
#define	SEEQ_BANK	2
#define	TIMER_VECTOR	3
#define	TIMER1_VECTOR	2		/* used by rom */
#define	SERIAL_VECTOR	1


#if	SIM
#define	TIMER_MODULUS		4338	/* 18 ticks per second */
#else
#define	TIMER_MODULUS		781	/* 100 ticks per second */
#endif

#define	PRC_STARTUP_VALUE	0x4c	/* processor control register, RAMEN and TB 2(20)/fclk */

#define	NVRAM_DESELECT		0x8000
#define	NVRAM_SELECT		0xa000
#define	NVRAM_CLOCK		0x40
#define	NVRAM_DATA		0x80

#define	LED_ON_MASK	0x20
#define	LED_OFF_MASK	0xdf


#define	FINT()		__emit__(0xf,0x92)
#define	RETRBI()	__emit__(0xf,0x91)

/* variable definitions */

extern IDA   far		*IDA_PTR;		/* where the IDA is */
extern unsigned char far	*SFR_PTR;	/* and the SFR ptr */

extern	IntVector  far		*INT_PTR;	/* ptr to interrupt table */

extern	void	interrupt	Null_Interrupt();

#endif

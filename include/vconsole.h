#ifndef	_VCONSOLE_H
#define	_VCONSOLE_H

int	vcgets(char *buff, int len, int mode);

#define	GETSMODE_NONE		0	/* if no console, block forever */
#define	GETSMODE_NOBLOCK	1 	/* if no console, return error */

#define	gets(b,l)	vcgets((b),(l),GETSMODE_NONE)

#define	stdout	0
#define	stdin	0
#define	stderr	0

int	vcprintf(char *s, ...);
int	vcsetconsole(int fd);		/* set the console to this fd, returns old one */
int	soprintf(int fd, char *s, ...);

#define	VCNOCONSOLE	-1

#endif
#ifndef	_VSOCK_H
#define	_VSOCK_H

/* virtual loopback socket.. writes go into buffer and become readable */

#define	AF_VSOCK	6

#define	VSOCKET_LOOPBACK	0

int	vsock_proto(struct _socket *so, int pr, void *data);

#define	DEFAULT_VSOCK_SIZE	2048		/* default limit */
#define	CHUNK_SIZE		256		/* alloc'd in hunks this size */

#define	VS_SOPT_READABLE	1
#define	VS_SOPT_RECVQUEUE	2
#define	VS_SOPT_BUFFER		3	/* get ptr to buffer */

typedef	struct _vsock_info {
	int	vs_bufflen;
	char	*vs_buffer;
} VSockInfo;

#endif
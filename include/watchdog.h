#ifndef	_WATCHDOG_H
#define	_WATCHDOG_H

#define	DOMORSECODE	1

#define	WATCHDOG_STACK	512
#define	WATCHDOG_USEC	220

#define	SIG_A	0xd			//  	. _
#define	SIG_B	0xab			//	_ . . . 
#define	SIG_C	0x16b			//	_ . _ .
#define	SIG_D	0x2b			//	_ . .
#define	SIG_E	0x1			//	.
#define	SIG_F	0xb5			//	. . _ .
#define	SIG_G   0x5b			//	_ _ .
#define	SIG_H   0x55			//	. . . . 
#define	SIG_I   0x5			//	. . 
#define	SIG_J   0x36d			//	. _ _ _ 
#define	SIG_K   0x6b			//	_ . _
#define	SIG_L   0xAd			//	. _ . .
#define	SIG_M   0x1b			//	_ _
#define	SIG_N   0xb			//	_ .
#define	SIG_O   0xdb			//	_ _ _ 
#define	SIG_P   0x16d			//	. _ _ .
#define	SIG_Q   0x35b			//	_ _ . _
#define	SIG_R   0x2d			//	. _ .
#define	SIG_S   0x15			//	. . .
#define	SIG_T   0x3			//	_
#define	SIG_U   0x35			//	. . _
#define	SIG_V   0xd5			//	. . . _
#define	SIG_W   0x6d			//	. _ _
#define	SIG_X   0x1Ab			//	_ . . _
#define	SIG_Y   0x36b			//	_ . _ _
#define	SIG_Z   0x15b			//	_ _ . .
#define	SIG_0   0x36db		//	_ _ _ _ _

#define	SIG_SLOWONE     0x1
#define	SIG_SLOWTWO	0x5
#define	SIG_SLOWTHREE	0x15
#define	SIG_SLOWFOUR	0x55

#define	SIG_FASTONE	0x110
#define	SIG_FASTTWO	0x294

#if	DOMORSECODE

#define	WDSIG_BADNVRAMMONITOR	SIG_A	//  	. _
#define	WDSIG_BADNVRAM		SIG_B	//	_ . . . 
#define	WDSIG_MONITOR		 	SIG_C	//	_ . _ .
#define	WDSIG_NICFAILED		SIG_D	//	_ . .
#define	WDSIG_STATUS			SIG_E	//	.
#define	WDSIG_BOOTP			SIG_F	//	. . _ .
#define	WDSIG_RARP			SIG_G	//	_ _ .
#define	WDSIG_TFTP			SIG_H	//	. . . . 
#define	WDSIG_RUN			SIG_J	//	. _ _ _ 
							
#define	WDSIG_GETTIME			SIG_K	//	_ . _
#define	WDSIG_FLASHC			SIG_L	//	. _ . .
#define	WDSIG_RUNNING			SIG_M	//	_ _
#define	WDSIG_CONFIGERR		SIG_N	//	_ .
#define	WDSIG_NOCONFIG		SIG_P	//	. _ _ .
#define	WDSIG_CRASH			SIG_Q	//	_ _ . _
#define	WDSIG_FLASHF			SIG_R	//	. _ .
#define	WDSIG_REPROGRAM		SIG_U	//	. . _
#define 	WDSIG_FLASHR			SIG_V	//	. . . _

#else

#define	WDSIG_BADNVRAMMONITOR	SIG_SLOWFOUR
#define	WDSIG_BADNVRAM		SIG_FASTTWO
#define	WDSIG_MONITOR		 	SIG_SLOWFOUR
#define	WDSIG_NICFAILED		SIG_SLOWTHREE
#define	WDSIG_STATUS			SIG_FASTONE
#define	WDSIG_BOOTP			SIG_SLOWONE
#define	WDSIG_RARP			SIG_SLOWONE
#define	WDSIG_TFTP			SIG_SLOWONE
#define	WDSIG_RUN			SIG_FASTONE

#define	WDSIG_GETTIME			SIG_FASTONE
#define	WDSIG_FLASHC			SIG_FASTONE
#define	WDSIG_RUNNING			SIG_FASTONE
#define	WDSIG_CONFIGERR		SIG_FASTTWO
#define	WDSIG_NOCONFIG		SIG_FASTTWO
#define	WDSIG_CRASH			SIG_SLOWTWO
#define	WDSIG_FLASHF			SIG_FASTONE
#define	WDSIG_REPROGRAM		SIG_FASTONE
#define 	WDSIG_FLASHR			SIG_FASTONE

#endif
int watchdog(void *Parent, char *name, int argc, char **argv);
void WDSetSignal(unsigned int signalword);

#endif

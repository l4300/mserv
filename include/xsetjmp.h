/*  xsetjmp.h

    Defines typedef and functions for setjmp/longjmp.

    Copyright (c) 1987, 1992 by Borland International
    All Rights Reserved.
*/

#ifndef __XSETJMP_H
#define __XSETJMP_H

#if !defined(___DEFS_H)
#include <_defs.h>
#endif

typedef struct __jmp_bufx {
    unsigned    j_sp;
    unsigned    j_ss;
    unsigned    j_flag;
    unsigned    j_cs;
    unsigned    j_ip;
    unsigned    j_bp;
    unsigned    j_di;
    unsigned    j_es;
    unsigned    j_si;
    unsigned    j_ds;
#if !defined(__TINY__)    
    unsigned    j_excep;
    unsigned    j_context;
#endif  /* !__TINY__ */

}   xjmp_buf[1];

#ifdef __cplusplus
extern "C" {
#endif

#if !defined( _Windows ) || defined(NOXX)
void    _CType xlongjmp(xjmp_buf __jmpb, int __retval);
int     _CType xsetjmp(xjmp_buf __jmpb);
#else
void __far __pascal xlongjmp( struct __jmp_bufx __far *__jmpb, int __retval );
int __far __pascal xsetjmp( struct __jmp_bufx __far *__jmpb);
#endif

#ifdef __cplusplus
}
#endif

#endif


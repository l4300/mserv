/* alarm.c */
#define	STATIC
#include	<stdlib.h>
#include	<dos.h>

#if	MBBS
#include	"gcomm.h"
#endif

#include	"nettypes.h"

#include	"task.h"
#include	"alarm.h"

#ifdef	MS
#include	"v25.h"
#endif


#if	MBBS
#undef	__DPMI16__
#endif

#ifdef	__DPMI16__
#define	DPMI	1

typedef	unsigned long	LONG;
typedef unsigned int	WORD;

typedef struct REAL_MODE_REGS{
        LONG        edi;            // Real Mode Registers ...
	LONG        esi;            // used for DPMI call 0x300
	LONG        ebp;
	LONG        reserved;
	LONG        ebx;
	LONG        edx;
	LONG        ecx;
	LONG        eax;
	WORD        flags;
	WORD        es;
	WORD        ds;
	WORD        fs;
	WORD        gs;
	WORD        ip;
	WORD        cs;
	WORD        sp;
	WORD        ss;
} RMREG;

static	RMREG	rmreg;
static void interrupt (*alRealCallBack)();	/* address of real-mode callback */

#endif


#if	DOSX286
#include	<phapi.h>
#endif

#include	<memcheck.h>


#define	ALSTACKSIZE	256

STATIC	struct _alarm _alarms[MAX_ALARMS];
STATIC	void interrupt (*old_timer_int)();
STATIC	char	alarm_stack[ALSTACKSIZE];
STATIC	int	al_recursive;
STATIC	unsigned	atheirsp, atheirss;
STATIC	long            first_alarm;
STATIC	long		tickcount;

static	long lastticks;
static	long	basetime;

void
Settime(long t)
{
	basetime = t - Ticks()/18;
	lastticks = Ticks();
}

unsigned long
Ticks100()
{

	long	ticks = (Ticks() * 100/18);
	return(ticks);
}

unsigned long
Ticks()
{
#if	defined(MS) || MBBS
	return(tickcount);
#else
static	unsigned lasticks;
static	long	 baseticks;
#if	!DOSX286
	unsigned int far *z = MK_FP(__Seg0040,0x6c);
	unsigned value = *z;
#else
	unsigned selector;
	unsigned int far *z;
	unsigned value;

	DosGetBIOSSeg(&selector);
	z = MK_FP(selector, 0x6c);
	value = *z;
#endif
	if(value < lasticks) {
		baseticks += 0x10000;
	}
	lasticks = value;
	return((long) baseticks + value);
#endif
}

unsigned long
Time(long *t)
{
	long	ticks = Ticks();

	if(ticks < lastticks)
		basetime += 86400;
	lastticks = ticks;
	ticks = ticks/18 + basetime;
	if(t)
		*t = ticks;
	return(ticks);

}

#if	!IPDOOR && !MBBS
long
time(long *t)
{
	return(Time(t));
}
#endif



void
alarm_process()
{
	long	now;
	int	x;
	Alarm	*A;

	now = Ticks();
	if(now >= first_alarm) {
		long	 nextfirst = 0x7fffffff;
		for(x=0, A=_alarms; x < MAX_ALARMS; x++, A++) {
			if(A->al_state == ALSTATE_INUSE) {
				if(now >= A->al_ticks) {
					if(now - A->al_ticks > 18) {
						now = Ticks();
					}
					TaskReady(A->al_task, A->al_rcode, TASK_NORESCHEDULE);
					A->al_state = ALSTATE_HOLD;
					now = Ticks();
				}
				else if(A->al_ticks < nextfirst)
					nextfirst = A->al_ticks;
			}
		}
		if(nextfirst != 0x7fffffff)
			first_alarm = nextfirst;
		else
			first_alarm = 0;
	} /* end if now */


}

#ifdef	MS
void
#elif	MBBS
void
#else
void interrupt
#endif
alarm_int()
{
	register unsigned stop;

	long	now;
	int	x;
	Alarm	*A;
	
#if	DPMI
	unsigned int far *iptr;
#endif
#if	defined(MS) || MBBS
	tickcount++;
#else
#if	!DPMI && !MBBS
	(*old_timer_int)();
#endif
#endif
	if(!first_alarm) {
#ifdef	MS
		goto exitme;
#endif

#if	DPMI
		goto done;
#else
		return;
#endif
}
	if(al_recursive++) {
		al_recursive--;
#ifdef	MS
		goto exitme;
#endif
#if	DPMI
		goto done;
#else
		return;
#endif
	}

#if	!MBBS
#ifndef	MS
	atheirss = _SS;
	atheirsp = _SP;
	stop    = FP_OFF(alarm_stack) + ALSTACKSIZE - 2;
	_SS = _DS;
#if	DPMI
	_SP = stop - 16;
#else
	_SP = stop - 12;
#endif
	asm	push	bp
	_BP = stop;
#endif

#endif	/* !MBBS */
	alarm_process();

#if	!MBBS
#ifndef	MS
	asm	pop	bp
	_SS = atheirss;
	_SP = atheirsp;
#endif
	al_recursive--;
#ifdef	MS
exitme:;
	asm	mov	sp, bp
	asm	pop	bp
	FINT();
	RETRBI();
#endif
#else	/* is MBBS */
	al_recursive--;
#endif	/* !MBBS */

#if	DPMI
done:;

//	rmreg.sp += 4;		/* pop calling cs, ip */
	rmreg.cs = FP_SEG(old_timer_int);
	rmreg.ip = FP_OFF(old_timer_int);
#endif
}

void
alarm_init()
{
#ifdef	MS
	RegisterBank   far	*RB = &(IDA_PTR->rb.Register[TIMER_VECTOR]);

	disable();

	RB->r_cs		= _CS;
	RB->r_vector_ip		= FP_OFF(alarm_int);
	RB->r_ds		= _DS;
	RB->r_ss		= _SS;
	RB->r_sp		= FP_OFF(&alarm_stack[ALSTACKSIZE-1]);
	RB->r_es		= _DS;
	RB->r_bp		= RB->r_sp;
	RB->r_si		= 0;
	RB->r_di		= 0;
#ifdef	NOTNEEDED
	INT_PTR[INT_TM0] 	= alarm_int;
#endif
	SFR_PTR[SFR_TMIC0]	= 0x10|TIMER_VECTOR;	/* low priority, vector more */
	SFR_PTR[SFR_TMC0]	|= 0xa0;		/* start timer */
	enable();
#elif	MBBS
	rtihdlr(alarm_int);
#else   /* DPMI or standard dos */
	old_timer_int = getvect(0x1c);

#if	DPMI
	if(!alRealCallBack) {	/* allocate a real-mode callback */
		union	REGS	regs;
		struct SREGS	sregs;

		sregs.ds = FP_SEG(alarm_int);
		regs.w.si = FP_OFF(alarm_int);
		sregs.es = FP_SEG(&rmreg);
		regs.w.di = FP_OFF(&rmreg);

		regs.w.ax = 0x303;

		int86x(0x31, &regs,&regs,&sregs);

		if(regs.x.cflag) {	/* carry set */
			return(-1);
		}

		alRealCallBack = MK_FP(regs.w.cx,regs.w.dx);
		regs.w.ax = 0x201;	/* set real-mode interrupt vector */
		regs.w.bx = 0x1c;
		int86x(0x31,&regs,&regs,&sregs);
//		setvect(0x1c, alRealCallBack);
	}

#else	/* not DPMI */
	setvect(0x1c, alarm_int);
#endif	/* standard dos */
	atexit(alarm_close);
#endif	/* not ms */
}



void
alarm_close()
{
#if	!MBBS
#ifdef	MS
	SFR_PTR[SFR_TMIC0] = 0x47;	/* disable ints */
	SFR_PTR[SFR_TMC0]  = 0x58;

#else
	if(old_timer_int)
		setvect(0x1c, old_timer_int);
#if	DPMI
	if(alRealCallBack) {
		union	REGS	regs;
		struct SREGS	sregs;

		regs.w.cx = FP_SEG(alRealCallBack);
		regs.w.dx = FP_OFF(alRealCallBack);
		regs.w.ax = 0x304;

		int86x(0x31, &regs,&regs,&sregs);
		alRealCallBack = NULL;

	}
#endif
#endif
#endif	/* MBBS */
}


int
alarm_sleep(struct timeval *when, int rc)
{
	unsigned flags = _FLAGS;
	int	x;
	long	now;
	Task	*T;

	disable();
	now = Ticks();
	T = Task_Current_Task;

	for(x=0; x < MAX_ALARMS; x++) {
		if(_alarms[x].al_state == ALSTATE_NONE) {
			long	offset = when->tv_sec * TICKS_PER_SEC;

			offset += when->tv_usec/USEC_PER_TICK;

			if(!offset)
				offset = 1;
			offset += now;
			_alarms[x].al_ticks = offset;
			if(offset < first_alarm || !first_alarm)
				first_alarm =offset;
			_alarms[x].al_rcode = rc;
			_alarms[x].al_task = T;
			_alarms[x].al_state = ALSTATE_INUSE;
			if(T->tsk_state == TASK_STATE_RUNNING)
				T->tsk_state = TASK_STATE_SLEEP;
			rc = TaskReschedule();
			_alarms[x].al_state = ALSTATE_NONE;	/* just in case someone else woke us up */
			/* we just returned from either a wakeup, or a signal */
			if(flags & 512)
				enable();
			return(rc);
		}
	}
	if(flags & 512)
		enable();
	return(SYSERR);
}

int
alarm_reset(int al)
{
	unsigned flags = _FLAGS;

	if(al < 0 || al >= MAX_ALARMS)
		return(SYSERR);
	disable();
	if(_alarms[al].al_state == ALSTATE_INUSE)
		_alarms[al].al_state = ALSTATE_NONE;
	if(flags & 512)
		enable();
	return(OK);
}

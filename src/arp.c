/* arp.c - ethernet arp functions */
#include	<dos.h>
#include	<mem.h>

#define	INTR	512

#include	"network.h"

#include	<memcheck.h>

static	IPaddr	ipnull={0,0,0,0};

Arp_Entry	arptable[ARP_TSIZE];

Arp_Entry	*
arpfind(void  *pra, int prtype, Net_Interface *pni)
{
	Arp_Entry	*pae;
	int		i;

	pae = arptable;
	for(i=0; i < ARP_TSIZE; i++, pae++) {
		if(pae->ae_state == AS_FREE)
			continue;
		if(pae->ae_prtype == prtype &&
		   pae->ae_pni == pni &&
		   !memcmp(pae->ae_pra, pra, pae->ae_prlen))
			return(pae);
	}
	return(NULL);
}


int
arpsend(Arp_Entry	*pae)
{
	Net_Interface	*pni = pae->ae_pni;
	Buffer		*buf;
	EP	*pep;
	ARP_Packet	*parp;
	int		arplen;

	arplen = sizeof(struct arp) + 2 * (pae->ae_hwlen + pae->ae_prlen);
	buf = BufferAlloc(arplen + sizeof(struct ep));
	if(!buf)
		return(SYSERR);
	pep = (EP *) buf->data;
	memcpy(pep->ep_dst, pni->ni_hwb.ha_addr, pae->ae_hwlen);
	pep->ep_type = EPT_ARP;
	parp = (ARP_Packet *) pep->ep_data;
	parp->ar_hwtype = htons(pae->ae_hwtype);
	parp->ar_prtype = htons(pae->ae_prtype);
	parp->ar_hwlen = pae->ae_hwlen;
	parp->ar_prlen = pae->ae_prlen;
	parp->ar_op = htons(AR_REQUEST);

	memcpy(SHA(parp), pni->ni_hwa.ha_addr, pae->ae_hwlen);
	memcpy(SPA(parp), pni->ni_ip, pae->ae_prlen);
	memset(THA(parp), 0, pae->ae_hwlen);
	memcpy(TPA(parp), pae->ae_pra, pae->ae_prlen);
	(*pni->ni_send)(pni,pep, arplen);
	return (OK);
}

Arp_Entry *
arpadd(struct netif *pni, struct arp *parp)
{

	Arp_Entry	*pae;

	pae = arpalloc();
	pae->ae_hwtype = parp->ar_hwtype;
	pae->ae_prtype =parp->ar_prtype;
	pae->ae_hwlen = parp->ar_hwlen;
	pae->ae_prlen = parp->ar_prlen;
	pae->ae_pni = pni;
	memset(&pae->ae_queue, 0, sizeof(Queue));
	memcpy(pae->ae_hwa, SHA(parp), parp->ar_hwlen);
	memcpy(pae->ae_pra, SPA(parp), parp->ar_prlen);
	pae->ae_ttl = ARP_TIMEOUT;
	pae->ae_state = AS_RESOLVED;
	return(pae);
}

int
arpAddProxy(struct netif *pni, IPaddr addr)
{
	int	x, free = -1;

	for(x = 0; x < NI_MAXPROXYARP; x++) {
		if(free == -1 && !memcmp(pni->ni_proxyip[x],ipnull, IP_ALEN))
			free = x;
		if(!memcmp(pni->ni_proxyip[x],addr, IP_ALEN))
			return(0);
	}

	if(free == -1)
		return(-1);

	memcpy(pni->ni_proxyip+free, addr, IP_ALEN);
	return(0);
}

int
arpDeleteProxy(struct netif *pni, IPaddr addr)
{
	int	x, free = -1;

	for(x = 0; x < NI_MAXPROXYARP; x++) {
		if(!memcmp(&pni->ni_proxyip[x],addr, IP_ALEN)) {
			memset(&pni->ni_proxyip[x], 0, sizeof(IP_ALEN));
			return(0);
		}	
	}

	return(-1);
}

void
arpqsend(Arp_Entry *pae)
{
	Buffer		*B;
	struct	ep 	*pep;
	struct	netif	*pni;

	if(!pae->ae_queue.queue_count)
		return;		/* nothing to do */
	pni = pae->ae_pni;
	while((B = (Buffer *) QueuePHead(&pae->ae_queue)) != 0) {
		pep = (struct ep *) B->data;
		netwrite(pni, pep, pep->ep_len);
	}
}

int
arp_in(struct netif *pni, struct ep *pep)
{
	struct	arp	*parp = (struct arp *) pep->ep_data;
	Arp_Entry	*pae;
	int		arplen;
	IPaddr		*localIP = NULL;

	parp->ar_hwtype = ntohs(parp->ar_hwtype);
	parp->ar_prtype = ntohs(parp->ar_prtype);
	parp->ar_op = ntohs(parp->ar_op);

	if(parp->ar_hwtype != pni->ni_hwtype ||
	   parp->ar_prtype != EPT_IP) {
		BufferFreeData(pep);
		return(OK);
	}

	if((pae = arpfind(SPA(parp), parp->ar_prtype, pni)) != 0) {
		memcpy(pae->ae_hwa, SHA(parp), pae->ae_hwlen);
		pae->ae_ttl = ARP_TIMEOUT;
	}

	if(memcmp(TPA(parp), pni->ni_ip, IP_ALEN)) {	// check here for other interfaces
		int  x;

		for(x=0; x < NI_MAXPROXYARP; x++) {
			if(!memcmp(TPA(parp), pni->ni_proxyip[x], IP_ALEN)) {
				localIP = pni->ni_proxyip + x;
				goto foundem;
			}
		}
		BufferFreeData(pep);
		return(OK);
	} else
		localIP = &pni->ni_ip;
foundem:;
	if(pae == 0)
		pae = arpadd(pni, parp);
	if(pae->ae_state == AS_PENDING) {
		pae->ae_state = AS_RESOLVED;
		arpqsend(pae);
	}
	if(parp->ar_op == AR_REQUEST) {
		parp->ar_op = AR_REPLY;
		memcpy(TPA(parp), SPA(parp), parp->ar_prlen);
		memcpy(THA(parp), SHA(parp), parp->ar_hwlen);
		memcpy(pep->ep_dst, THA(parp), EP_ALEN);
		memcpy(SHA(parp), pni->ni_hwa.ha_addr, pni->ni_hwa.ha_len);
		memcpy(SPA(parp), localIP, IP_ALEN);

		parp->ar_hwtype = htons(parp->ar_hwtype);
		parp->ar_prtype = htons(parp->ar_prtype);
		parp->ar_op = htons(parp->ar_op);

		arplen = sizeof(struct arp) + 2 * (parp->ar_prlen + parp->ar_hwlen);

		(*pni->ni_send)(pni,pep, arplen);
	} else
		BufferFreeData(pep);
	return(OK);
}

Arp_Entry *
arpalloc()
{
	static	int	aenext = 0;
	Arp_Entry	*pae;
	int	i;

	for(i=0; i <ARP_TSIZE; ++i) {
		if(arptable[aenext].ae_state == AS_FREE)
			break;
		aenext = (aenext + 1) % ARP_TSIZE;
	}
	pae = &arptable[aenext];
	aenext = (aenext + 1) % ARP_TSIZE;
	if(pae->ae_state == AS_PENDING && pae->ae_queue.queue_count > 0)
		arpdq(pae);
	pae->ae_state = AS_PENDING;
	return(pae);
}

void
arptimer(int gran)
{
	Arp_Entry	*pae;
	int		i;
	unsigned	flags = _FLAGS;


	disable();
	for(i=0; i < ARP_TSIZE; ++i) {
		if((pae = &arptable[i])->ae_state == AS_FREE)
			continue;
		if((pae->ae_ttl -= gran) <= 0) {
			if(pae->ae_state == AS_RESOLVED)
				pae->ae_state = AS_FREE;
			else if(++pae->ae_attempts > ARP_MAXRETRY) {
				pae->ae_state = AS_FREE;
				arpdq(pae);
			} else {
				pae->ae_ttl = ARP_RESEND;
				arpsend(pae);
			}
		}
	}	/* end for */
	if(flags & INTR)
		enable();
}

void
arpdq(Arp_Entry *pae)
{
	struct	ep	*pep;
	struct	ip	*pip;
	Buffer		*B;

	if(pae->ae_queue.queue_count < 0)
		return;

	while((B = (Buffer *) QueuePHead(&pae->ae_queue)) != 0) {
		pep = (struct ep *) B->data;
		if(gateway && pae->ae_prtype == EPT_IP) {
			pip = (struct ip *) pep->ep_data;
			icmp(ICT_DESTUR, ICC_HOSTUR, pip->ip_src, (char *) pep, NULL);
		} else
			BufferFreeData(pep);
	} /* end while */
	memset(&pae->ae_queue, 0, sizeof(Queue));
}


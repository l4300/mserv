#include <alloc.h>
#include <dos.h>

#include	"bufs.h"

#include	<memcheck.h>

#if	BUFDEBUG
Queue_Element *bufChain;
#endif

Queue	_Buffers;
int	minbufs=0x7fff;

long	BufferAllocs, BufferFrees, BufferBogusFrees;
long	BufferNoBufs;

#define	INTR	512
#if	VESTIGIAL
void
BufferDump()
{
	printf("Minimum buffer Count %d Current %d\n",minbufs, _Buffers.queue_count);
}

#endif

int
BufferInitialize(int count, int size)
{
	return(BufferCreate(&_Buffers, count, size, QTYPE_MBUF));

}

int
BufferCreate(Queue *queue, int count, int size, int type)
{
	Buffer	*B;

	while(count--) {
		B = (Buffer *) QueueAlloc(size, type, -size);
		if(!B) {
			return(BUFFER_RESULT_NOMEM);
		}
		B->QE.queue_bufsize = B->QE.queue_key;
#if	BUFDEBUG
		if(queue == &_Buffers) {
			B->QE.queue_chain = bufChain;
			B->QE.queue_file = NULL;
			bufChain = &B->QE;
		}
#endif
		QueueInsert(queue, (Queue_Element *) B);
	}
	return(BUFFER_RESULT_OK);
}

void
BufferFree(Buffer *B)
{
	unsigned int flags = _FLAGS;

	if(!B || B->QE.queue_type != QTYPE_MBUF) {
		BufferBogusFrees++;
		return;
	}
	BufferFrees++;
	B->QE.queue_key = B->QE.queue_bufsize;
#if	BUFDEBUG
	B->QE.queue_file = NULL;
#endif
	QueueInsert(&_Buffers, (Queue_Element *) B);
}

#if	BUFDEBUG
Buffer *
BufferAllocX(int size, char *file, unsigned lnum)
{
	Buffer	*B = (Buffer *) QueuePKey(&_Buffers, size);

	if(!B) {
		BufferNoBufs++;
	}

	if(_Buffers.queue_count < minbufs)
		minbufs = _Buffers.queue_count;
	BufferAllocs++;

	B->QE.queue_file = file;
	B->QE.queue_line = lnum;

	return(B);
}
#else
Buffer *
BufferAlloc(int size)
{
	Buffer	*B = (Buffer *) QueuePKey(&_Buffers, size);

	if(!B) {
		BufferNoBufs++;
	}

	if(_Buffers.queue_count < minbufs)
		minbufs = _Buffers.queue_count;
	BufferAllocs++;
	return(B);
}

#endif


/* #include	<stdlib.h> */
#include	<alloc.h>
#include  	<mem.h>

#include	"network.h"
#include	"sys/socket.h"
#include	"netinet/in.h"
#include	"alarm.h"

#include	"syslog.h"

#include	<memcheck.h>

void
panic(char *c, char *z1, char *z2, char *z3)
{
#if	MBBS
	char	buff[256];

	sprintf(buff,"task %s:",Task_Current_Task ? Task_Current_Task->tsk_name : "none");
	sprintf(buff+strlen(buff),c,z2,z2,z3);
	catastro(c);
#else
#if	SIM
	cprintf("\r\n\r\nPanic: %s\r\n",c);
#elif	IPDOOR
	printf("\r\nPanic %s\r\n",c,z1,z2,z3);
#else
	Syslog(LOG_FATAL|LOG_REBOOT,"panic: %s\n",c, z1, z2, z3);
#endif
	exit(102);
#endif	/* mbbs */	
}

void
bzero(char *s, int len)
{
	memset(s, 0, len);

}

void
bcopy(char *b1, char *b2, int len)
{
	memcpy(b2, b1, len);
}

int
inet_addr_parse(char *a, IPaddr E)
{
	struct in_addr	s;
	int	x;

	x = 0;
	s.s_ip[0] = atoi(a);
	while(*a && x < 3) {
		if(*a == '.')   {
			x++;
			a++;
			s.s_ip[x] = atoi(a);
		}
		else if(*a > '9' || *a < '0')
			return(-1);
		a++;
	}
	if(x != 3)
		return(-1);

	memcpy(E, s.s_ip, IP_ALEN);
	return(0);
}

unsigned long
inet_addr(char *a)
{
	struct in_addr	s;
	int	x;

	x = 0;
	s.s_ip[0] = atoi(a);
	while(*a && x < 3) {
		if(*a == '.')   {
			x++;
			a++;
			s.s_ip[x] = atoi(a);
		}
		else if(*a > '9' || *a < '0')
			return(-1);
		a++;
	}
	if(x != 3)
		return(-1);

	return(s.s_addr);
}

char *
inet_ntoa(struct in_addr in)
{
	static	char	result[18];

	sprintf(result,"%u.%u.%u.%u",in.s_ip[0],in.s_ip[1],in.s_ip[2],in.s_ip[3]);
	return(result);
}


void *
gethostbyname()
{
	return(NULL);
}

void *
gethostbyaddr()
{
	return(NULL);
}

long
getuid()
{
	return(0);
}


#pragma argsused
int
gettimeofday(struct timeval *t, void *d)
{

	t->tv_sec = Time(NULL);
	t->tv_usec = (Ticks() % 18) * 1000/18;
	return(0);
}

int
getdtablesize()
{
	return(FD_SETSIZE);
}

int
getegid()
{
	return(0);
}

int
geteuid()
{
	return(0);
}

int
getgroups()
{
	return(0);
}

#if	VESTIGIAL
int
gethostname(char *n, int len)
{
	strcpy(n,"hammer.aux");
	return(0);

}

#endif

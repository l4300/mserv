#include	"network.h"
#include	"tcp.h"
#include	<dos.h>
#include	<mem.h>

#include	<memcheck.h>

int
cksum(unsigned int *buf, int count)
{
	unsigned xseg, off;

	xseg = FP_SEG(buf);
	off = FP_OFF(buf);
	asm	push	ds
	asm	mov	ax, xseg
	asm	mov	ds, ax
	asm	mov	si, off
	asm	mov	cx, count
	asm	xor	bx, bx
	asm	clc
more:;
	asm	lodsw
	asm	adc	bx, ax
	asm	loop	more
	asm	adc	bx, 0
	asm	not	bx
	asm	pop	ds
	asm	mov	xseg, bx
	return(xseg);
}



unsigned short
tcpcksum(struct ip *pip)
{
	struct	tcp	*ptcp = (struct tcp *) pip->ip_data;
	unsigned	short	*sptr, len;
	unsigned	long	tcksum;
	int		i;
	struct trash {
		IPaddr		src;
		IPaddr		dst;
		unsigned char	zero;
		unsigned char	proto;
		unsigned int	len;
	} X;

	unsigned xseg, off,val;

	X.zero = 0;
	memcpy(&X.src, &pip->ip_src, IP_ALEN);
	memcpy(&X.dst, &pip->ip_dst, IP_ALEN);
	X.proto = IPT_TCP;
	sptr = (unsigned short *) &X;
	len = pip->ip_len - IP_HLEN(pip);
	X.len = htons(len);
	val = 0;
	if(len & 1) {
		((char *) ptcp)[len] = 0;
		len++;
	}

					/* sum IP addresses and len, type */
	xseg = FP_SEG(sptr);
	off = FP_OFF(sptr);
	asm	cld
	asm	push	ds
	asm	mov	ax, xseg
	asm	mov	ds, ax
	asm	mov	si, off
	asm	mov	cx, 6
	asm	mov	bx, val
	asm	clc
more:;
	asm	lodsw
	asm	adc	bx, ax
	asm	loop	more

	asm	adc	bx, 0
	asm	mov	val, bx

	xseg = FP_SEG(ptcp);		/* sum tcp segment */
	off = FP_OFF(ptcp);
	len	>>= 1;
	asm	mov	ax, xseg
	asm	mov	ds, ax
	asm	mov	si, off
	asm	mov	cx, len
	asm	mov	bx, val
	asm	clc
more2:;
	asm	lodsw
	asm	adc	bx, ax
	asm	loop	more2

	asm	adc	bx, 0

	asm	not	bx
	asm	pop	ds
	asm	mov	xseg, bx
	return(xseg);
}

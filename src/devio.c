/* devio.c - device I/O routines -- Copyright (C) MurkWorks 1993 All Rights Reserved

*/
#define	STATIC
#define	DEBUG	1
#define	PAR	1

#define	DEBUGREADB	0

#define	DOINTS	1				// do int's

#define	INTR	512

#include	"network.h"
#include	"sys/errno.h"
#include	"sys/socket.h"
#include	"ioint.h"
#include	"task.h"
#include	"syslog.h"

#include	<dos.h>

#if	DEBUG
#include	"bios.h"
#endif

#if	!SIM
#include	"v25.h"
#endif

int	iofunc_parallel(struct _io_port * port, int mode, void *arg);
int	iofunc_serial(struct _io_port * port, int mode, void *arg);
int	iosocket_proto(struct _socket *so, int pr, void *data);

STATIC	void	*iosocket_task_stack;
STATIC	Task	*iosocket_task_ptr;

STATIC	IO_Port IO_Ports[]={
/* sock_type, unit, base addr, interrupt,
   allowed_modes, allowed_ipost, allowed_opost, dev_flags, io_function */
#if	PAR
#if	SIM
  {IOSOCKET_PARALLEL, 0, 0x378, 7, IOMODE_OUTPUT, 0, 0, 0, iofunc_parallel},
#else
  {IOSOCKET_PARALLEL, 0, SFR_EXIC2, INT_P2, IOMODE_OUTPUT, 0, 0, 0, iofunc_parallel},
#endif
#endif
#if	SIM
  {IOSOCKET_SERIAL, 0, 0x2f8, 3, IOMODE_OUTPUT|IOMODE_RINPUT, 0, 0, IODEVFLAG_NEEDTIMER, iofunc_serial}
#else
  {IOSOCKET_SERIAL, 0, SFR_RXB0, INT_SE0, IOMODE_OUTPUT|IOMODE_RINPUT, 0, 0,IODEVFLAG_NEEDTIMER, iofunc_serial}
#endif
};


#if	DEBUG
int	ptr = 8;
void
Pputchar(unsigned c)
{
	unsigned char far *x = (unsigned char far *) MK_FP(0xb800,0);

	*(x + ptr) = c;
	ptr += 2;
	if(ptr > 76/2)
		ptr = 8;
}
#define	PP(c)	Pputchar(c)
#else
#define	PP(c)
#endif

#pragma argsused
int
iosocket_task(void *Parent, char *tname, int argc, char **argv)
{
	iosocket_task_ptr = Task_Current_Task;

	while(1) {
		IO_Port	*I;
		int	x;
		struct	timeval	tv;

		tv.tv_sec = 0;
		tv.tv_usec = 10;
		alarm_sleep(&tv,0);
		for(x=0, I = IO_Ports; x < sizeof(IO_Ports)/sizeof(IO_Port); x++, I++) {
			if(!(I->io_devflags & IODEVFLAG_NEEDTIMER))
				continue;
			if(I->io_timeout) {
				if(!--I->io_timeout)
					(*I->io_function)(I, IOFUNCTION_TIMER, NULL);
			}
		}
	}
}


int
iosocket_init()	/* initialize all devices */
{
	int	x;
	int	rc;
	IO_Port	*I;
	int	goodcount = 0;

	if(iosocket_task_stack) {
		return(-1);	/* already initialized */
	}
	for(x = 0, I=IO_Ports; x < sizeof(IO_Ports)/sizeof(IO_Port); x++, I++) {
		rc = I->io_function(I, IOFUNCTION_PROBE, NULL);
		if(rc)
			continue;
		rc = (*I->io_function)(I, IOFUNCTION_INIT, NULL);
		if(rc)
			continue;

		rc = soregister(AF_IOSOCKET, I->io_socket_type, I->io_unit_number, iosocket_proto);
		if(rc) {
			Syslog(LOG_ERROR,"iosocket_init got error %d from soregister",rc);
			(*I->io_function)(I, IOFUNCTION_SHUTDOWN, NULL);
			break;
		}
		I->io_devflags |= IODEVFLAG_ACTIVE;
		Semaphore_Initialize(&I->io_owner_semaphore,1);
		Semaphore_Initialize(&I->io_close_semaphore,0);
		I->io_obuffers.queue_flags = QUEUE_FLAGS_NOTSORTED;
		I->io_ibuffers.queue_flags = QUEUE_FLAGS_NOTSORTED;
		I->io_fbuffers.queue_flags = QUEUE_FLAGS_NOTSORTED;
		goodcount++;
	}

	if(goodcount) {
		iosocket_task_stack = malloc(IOTASK_STACKSIZE);
		if(!iosocket_task_stack)
			return(-2);	/* bad news */
		TaskAlloc(iosocket_task, iosocket_task_stack, IOTASK_STACKSIZE,
			IOTASK_PRIORITY, IOTASK_NAME, 0, NULL);

	}
	return(!goodcount);

}

int
iosocket_close()
{
	IO_Port	*I = IO_Ports;
	int	x;

	for(x=0; x < sizeof(IO_Ports)/sizeof(IO_Port); x++, I++) {
		if(!(I->io_devflags & IODEVFLAG_ACTIVE))
			continue;
		(*I->io_function)(I, IOFUNCTION_SHUTDOWN, NULL);
		I->io_devflags &= ~(IODEVFLAG_ACTIVE|IODEVFLAG_NEEDTIMER);
	}
	return(0);
}

STATIC void
iosocket_flushinput(IO_Port *I)
{
	Buffer	*B;
	unsigned flags = _FLAGS;

	disable();
	while(B = (Buffer *) QueuePHead(&I->io_ibuffers))
		BufferFree(B);
	while(B = (Buffer *) QueuePHead(&I->io_fbuffers))
		BufferFree(B);
	I->io_ichar = NULL;
	I->io_iccount = 0;
	I->io_devflags &= ~IODEVFLAG_NEEDRESTART;

	if(flags & INTR)
		enable();
}

STATIC void
iosocket_flushoutput(IO_Port *I)
{
	Buffer	*B;
	unsigned flags = _FLAGS;

	disable();

	while(B = (Buffer *) QueuePHead(&I->io_obuffers))
		BufferFree(B);
	I->io_ochar = NULL;
	I->io_occount = 0;
	if(flags & INTR)
		enable();
}

STATIC int
iosocket_nextibuffer(IO_Port *I)	/* could be called from interrupt */
{
	Buffer	*B;
	unsigned flags = _FLAGS;

//	disable();
	B = (Buffer *) QueuePHead(&I->io_fbuffers);
	if(!B) {
		I->io_ichar = NULL;
		I->io_iccount = 0;
//		if(flags & INTR)
//			enable();
		return(-1);
	}
	I->io_ichar = B->QE.queue_object;
	I->io_iccount = B->QE.queue_bufsize;
	I->io_ibytecount = 0;
	QueueInsert(&I->io_ibuffers, (Queue_Element *) B);
//	if(flags & INTR)
//		enable();
	return(0);
}

STATIC int
iosocket_prepinput(IO_Port *I)		/* alloc an input buffer, or realloc */
{
	Buffer	*B = NULL;
	int	buffcount = I->io_socket->so_recvqueue / I->io_ibufsize;
	int	needrestart = 0;
	int	rc = 0;

	if(buffcount > MAX_INPUT_BUFFERS)
		buffcount = MAX_INPUT_BUFFERS;

	buffcount -= I->io_ibuffers.queue_count;
	if(buffcount < 1)	/* feh! */
		return(-1);
	buffcount -= I->io_fbuffers.queue_count;
	if(buffcount && (I->io_devflags & IODEVFLAG_NEEDRESTART))
		needrestart++;

	while(buffcount > 0) {
		B = NULL;
		while(!(B = BufferAlloc(I->io_ibufsize)))
			xdelay(1);
		B->QE.queue_object = B->data;
		B->QE.queue_size = 0;
		QueueInsert(&I->io_fbuffers, (Queue_Element *) B);
		buffcount--;
	}
	if(!I->io_ibuffers.queue_count)
		rc = iosocket_nextibuffer(I);	/* move over next input buffer */

	if(needrestart)
		(*I->io_function)(I, IOFUNCTION_RESTART, NULL);
	return(rc);
}

int
iosocket_updateInput(IO_Port *I, int count)
{
	struct	_socket *so = I->io_socket;

	if(!I->io_ichar || (I->io_devflags & IODEVFLAG_CLOSED))
		return(0);	/* have to discard, but don't say so */


	I->io_ichar += count;
	I->io_ibytecount += count;
	I->io_iccount -= count;

	so->so_readable += count;

	if(I->io_iccount < 1) {	/* need another buffer */
		Buffer *B = (Buffer *) QueueTail(&I->io_ibuffers);
		B->QE.queue_size = I->io_ibytecount;
		if(iosocket_nextibuffer(I)) {	/* a problem */
			sowakeup(so, SOSEL_READ); /* please read me!*/
			return(-2);	/* better hold off now */
		}
	}
	/* do we need to set timer or post wakeup? */
	if(so->so_readable > I->io_iminbytes && !(so->so_selectflags & SOSEL_READ))
		sowakeup(so, SOSEL_READ);
	else if(I->io_imintimeout && !I->io_timeout) {
		I->io_timeout = I->io_imintimeout;
		I->io_devflags |= IODEVFLAG_NEEDTIMER;
	}
	if(!I->io_fbuffers.queue_count && (I->io_iccount < HOLDOFF_COUNT))
		return(-3);		/* hold off please */
	else
		return(0);


}

STATIC int
iosocket_postinput(IO_Port *I, unsigned char c)	/* save in input buffer, return non-zero
						if we need hold off */
{


	if(I->io_ichar)
		*I->io_ichar = c;
	return(iosocket_updateInput(I,1));
}

STATIC int
iosocket_nextobuffer(IO_Port *I)    /* prep next output buffer, called from int */
{
	Buffer	*B;

nextbuffer:;
	if(I->io_ochar) {		/* if we were outputting, then discard
					   current buffer */

		B = (Buffer *) QueuePHead(&I->io_obuffers);
		if(!B) {
			I->io_ochar = NULL;	/* a mistake */
			return(-1);
		}
		I->io_socket->so_writeable += B->QE.queue_size;
		BufferFree(B);
		if(!(I->io_socket->so_selectflags & SOSEL_WRITE)) { /* was blocked, wakeup */
			if(I->io_socket->so_writeable > 0)
				sowakeup(I->io_socket, SOSEL_WRITE);
		}
	}

	B = (Buffer *) QueueHead(&I->io_obuffers);
	if(!B) {		/* no more to output */
		I->io_ochar = NULL;
		I->io_occount = 0;
		I->io_devflags &= ~IODEVFLAG_IOACTIVE;
		if(I->io_close_semaphore.sem_count) {	/* someone waiting */
			Semaphore_ISignal(&I->io_close_semaphore);
		}
		return(-1);
	}
	I->io_ochar = B->QE.queue_object;
	if(!(I->io_occount = B->QE.queue_size))
		goto nextbuffer;
	return(0);
}

STATIC IO_Port *
find_iosocket(int type, int unit)		/* find device type and unit */
{
	IO_Port	*I = IO_Ports;
	int	x;

	for(x=0; x < sizeof(IO_Ports)/sizeof(IO_Port); x++, I++) {
		if(!(I->io_devflags & IODEVFLAG_ACTIVE))
			continue;
		if(type == I->io_socket_type &&
		   unit == I->io_unit_number)
			return(I);

	}
	return(NULL);
}

int
iosocket_proto(struct _socket *so, int pr, void *data)
{
	int	rc = OK;
	int	len;
	struct	sockaddr_io	*sin = (struct sockaddr_io *) data;
	struct	_sendto		*sendto = (struct _sendto *) data;
	IO_Port			*I = (IO_Port *) so->so_pcb;
	struct	_rwbuffer	*rw = (struct _rwbuffer *) data;
	Buffer			*B;
	int			x;
	unsigned char 		*dest = sendto->s_msg;
	int			*ival = (int *) data;
	unsigned		flags = _FLAGS;
static	int			needkick;

	switch(pr)	{
		case	PR_OPEN :	/* open by socket means a socket call */
			if(so->so_family != AF_IOSOCKET) {
protoerr:;
				SETERROR(EPROTO);
				rc = SYSERR;
				break;
			}
			I = find_iosocket(so->so_protocol, so->so_popt);
			if(!I) {
				SETERROR(EADDRNOTAVAIL);
				rc = SYSERR;
				break;
			}
			so->so_pcb = I;

			break;

		case 	PR_BIND :
			if(sin->sin_family != AF_IOSOCKET)
				goto protoerr;
			sin->sin_port = 0;
			break;

		case	PR_CONNECT :
			if(!sin->sin_family && I->io_socket == so) { /* close */
				goto prclose;
			}
			if(sin->sin_family != AF_IOSOCKET || !I)
				goto protoerr;

			/* wait on owner semaphore to gain access */
			Semaphore_Wait(&I->io_owner_semaphore);
			/* get here, we have control of this device now */
			I->io_devflags &= ~IODEVFLAG_IOACTIVE;
			I->io_socket = so;
			rc = (*I->io_function)(I, IOFUNCTION_OPEN, NULL);
			/* on return from the open call, the device
			   has initialized buffer defaults, etc,
			*/
			if(rc)
				break;
			/* for write only devices, set so_opt */
			if(!(I->io_desired_modes & IOMODES_INPUT))
				so->so_opt |= SOOPT_CANTRECVMORE;
			else
				so->so_opt &= ~SOOPT_CANTRECVMORE;
			if(!(I->io_desired_modes & IOMODES_OUTPUT))
				so->so_opt |= SOOPT_CANTSENDMORE;
			else {
				so->so_opt &= ~SOOPT_CANTSENDMORE;
				so->so_selectflags |= SOSEL_WRITE;
			}
			break;

		case	PR_CLOSE :
			if(so->so_state < SOSTATE_CONNECTED) {
				rc = -1;
				SETERROR(EBADF);
				break;
			}
prclose:;		/* make sure all output is complete, flush all
			   input buffers, then signal the semaphore */

//			disable();
			if(I->io_obuffers.queue_count) {
				/* if the device is off-line, maybe we
				   should flush first? */
				Semaphore_Wait(&I->io_close_semaphore);
			}

//			if(flags & INTR)
//				enable();
			iosocket_flushinput(I);
			iosocket_flushoutput(I);	/* probably not needed */

			(I->io_function)(I, IOFUNCTION_CLOSE, NULL);
			Semaphore_Signal(&I->io_owner_semaphore);
			break;
		case 	PR_SHUTDOWN :
			if(so->so_state < SOSTATE_CONNECTED) {
				rc = -1;
				SETERROR(EBADF);
				break;
			}
			disable();
			if(*ival >= SHUTDOWN_WRITE) {
				if(I->io_obuffers.queue_count) {
					/* if the device is off-line, maybe we
					   should flush first? */
					Semaphore_Wait(&I->io_close_semaphore);
				}
				iosocket_flushoutput(I);	/* probably not needed */
			}
			if(flags & INTR)
				enable();
			if(*ival == SHUTDOWN_READ || *ival == SHUTDOWN_READWRITE)
				iosocket_flushinput(I);
			break;

		case 	PR_READ :		/* read from device if allowed */
			if(so->so_state < SOSTATE_CONNECTED) {
				rc = -1;
				SETERROR(EBADF);
				break;
			}
			/* in theory, you only get here if there's data
				to be read */
			B = (Buffer *) QueueHead(&I->io_ibuffers);
			if(!B || ((((Buffer *) QueueTail(&I->io_ibuffers)) == B &&
					(!I->io_ibytecount ||
					 I->io_devflags & IODEVFLAG_MSRECV)))) { /* read with nothing to read */
				so->so_selectflags &= ~SOSEL_READ;
				rc = 0;
				break;
			}
			len = sendto->s_len;
//			disable();
			while(len > 0) {
				register int xlen = len;

				if(B == (Buffer *) QueueTail(&I->io_ibuffers)) {
					if(I->io_devflags & IODEVFLAG_MSRECV)
						break;
					B->QE.queue_size = I->io_ibytecount;
				}
				if(xlen > B->QE.queue_size)
					xlen = B->QE.queue_size;

				memcpy(dest, B->QE.queue_object, xlen);
				len -= xlen;
				rc  += xlen;
				so->so_readable -= xlen;
				dest += xlen;

				B->QE.queue_size -= xlen;
				((char *) B->QE.queue_object) += xlen;

				if(B->QE.queue_size <= 0 || B == (Buffer *) QueueTail(&I->io_ibuffers)) {
					QueueExtract(&I->io_ibuffers, (Queue_Element *) B);
					BufferFree(B);
					if(!I->io_ibuffers.queue_count) {
						iosocket_prepinput(I);
						so->so_selectflags &= ~SOSEL_READ;
						break;
					}
					iosocket_prepinput(I);
					B = (Buffer *) QueueHead(&I->io_ibuffers);
				}
			}	/* end while len > 0 */
//			if(flags & INTR)
//				enable();
			break;

		case	PR_WRITE :
			if(so->so_state < SOSTATE_CONNECTED) {
				rc = -1;
				SETERROR(EBADF);
				break;
			}
			len = sendto->s_len;

			B = (Buffer *) QueueTail(&I->io_obuffers);
			x  = 0;
			if(B) {
				needkick = 0;
				if(B == (Buffer *) QueueHead(&I->io_obuffers))
					goto allocbuf;
			}
			else {
				needkick = 1;
				goto allocbuf;
			}
			while(len > 0) {
				register int xlen = B->QE.queue_bufsize - B->QE.queue_size;
				char	*xdest = ((char *) B->QE.queue_object) + B->QE.queue_size;

				if(xlen < 1) {
allocbuf:;
					B = NULL;
					while(!B) {
						B = BufferAlloc(len);
						if(!B)
							xdelay(1);	/* len best not be larger than maxbuf */
					}
					B->QE.queue_size = 0;
					B->QE.queue_object = B->data;
					QueueInsert(&I->io_obuffers, (Queue_Element *) B);
					continue;
				}
				/* get here, buf can hold some data */
				if(xlen > len)
					xlen = len;
				memcpy(xdest, dest, xlen);
				len -= xlen;
				dest += xlen;
				B->QE.queue_size += xlen;
				if((so->so_writeable -= xlen) < 1)
					so->so_selectflags &= ~SOSEL_WRITE;

				rc += xlen;
			}	/* end while len > 0 */
/*			if(needkick) */
				(*I->io_function)(I,IOFUNCTION_KICKSTART, NULL);
			break;

		case	PR_READBUF :
			if(so->so_state < SOSTATE_CONNECTED) {
				rc = -1;
				SETERROR(EBADF);
				break;
			}
			/* in theory, you only get here if there's data
				to be read */
			B = (Buffer *) QueueHead(&I->io_ibuffers);
//			disable();
			if(!B || ((B == (Buffer *) QueueTail(&I->io_ibuffers) &&
				(!I->io_ibytecount ||
				  I->io_devflags & IODEVFLAG_MSRECV)))) { /* read with nothing to read */
				so->so_selectflags &= ~SOSEL_READ;
				rw->rw_buffer = NULL;
				rw->rw_data = NULL;
				rw->rw_len = 0;
				rc = 0;
//				if(flags & INTR)
//					enable();
#if	DEBUGREADB
				Syslog(LOG_DEBUG,"dev readbuf returning 0");
#endif
				break;
			}
			rw->rw_buffer = B;
			rw->rw_data   = B->QE.queue_object;
			if(B == (Buffer *) QueueTail(&I->io_ibuffers))
				rc = rw->rw_len = I->io_ibytecount;
			else
				rc = rw->rw_len = B->QE.queue_size;

			QueueExtract(&I->io_ibuffers, (Queue_Element *) B);
			so->so_readable -= rc;

			if(!I->io_ibuffers.queue_count)
				so->so_selectflags &= ~SOSEL_READ;

//			if(flags & INTR)
//				enable();
			iosocket_prepinput(I);
#if	DEBUGREADB
				Syslog(LOG_DEBUG,"dev readbuf returning %d",rc);
#endif
			break;
		case	PR_WRITEBUF :
			if(so->so_state < SOSTATE_CONNECTED) {
				rc = -1;
				SETERROR(EBADF);
				break;
			}
			B = rw->rw_buffer;
			B->QE.queue_object = rw->rw_data;
			rc = B->QE.queue_size = rw->rw_len;
			so->so_writeable -= rw->rw_len;
			if(so->so_writeable < 1)
				so->so_selectflags &= ~SOSEL_WRITE;
			QueueInsert(&I->io_obuffers, (Queue_Element *) B);
			(*I->io_function)(I, IOFUNCTION_KICKSTART, NULL);
			break;

		case	PR_GETSOCKOPT :
			ival = (int *) sendto->s_msg;
			if(sendto->s_len < sizeof(int)) {
				SETERROR(EINVAL);
				rc = SYSERR;
				break;
			}
			switch(sendto->s_flags) {	/* option */
				case	IO_SOPT_IOMODE :	/* lowest level mode (read,write) */
					*ival = I->io_desired_modes;
					break;
				case	IO_SOPT_IPMODE :	/* input post processing mode */
					*ival = I->io_ipost_process;
					break;
				case	IO_SOPT_OPMODE :	/* output post processing mode */
					*ival = I->io_opost_process;
					break;
				case	IO_SOPT_IFMODE :	/* interface modes, flow control, bits etc */
					*ival = I->io_intfmodes;
					break;
				case	IO_SOPT_SPEED  :	/* speed */
					*ival = I->io_speed;
					break;
				case	IO_SOPT_IBSIZE :	/* input buffer size */
					if(I->io_ibuffers.queue_count)
						goto protoerr;
					*ival = so->so_recvqueue;
					break;
				case	IO_SOPT_OBSIZE :	/* output buffer size */
					if(I->io_obuffers.queue_count)
						goto protoerr;
					*ival = so->so_writeable;
					break;
				case	IO_SOPT_IMINCOUNT :	/* min byte count to signal read */
					*ival = I->io_iminbytes;
					break;
				case	IO_SOPT_ITIMEOUT :	/* min time to signal read */
					*ival = I->io_imintimeout;
					break;
				case	IO_SOPT_IBUFSIZE :
					*ival = I->io_ibufsize;
					break;
				case	IO_SOPT_DEVTYPE :
					*ival = I->io_device_type;
					break;
				case	IO_SOPT_AIOMODES :
					*ival = I->io_allowed_modes;
					break;
				case	IO_SOPT_AIPMODES :
					*ival = I->io_allowed_ipost;
					break;
				case	IO_SOPT_AOPMODES :
					*ival = I->io_allowed_opost;
					break;
				case	IO_SOPT_DEVFLAGS :
					*ival = I->io_devflags;
					break;
				case	IO_SOPT_DEVTIMER :
					*ival = I->io_timeout;
					break;
				case	IO_SOPT_DEVSTATUS :
					rc = (*I->io_function)(I,IOFUNCTION_STATUS, sendto);
					break;
				default :
					SETERROR(EINVAL);
					rc = SYSERR;
			}	/* end switch opt */
			break;

		case	PR_SETSOCKOPT :
			if(so->so_state < SOSTATE_CONNECTED) {
				rc = -1;
				SETERROR(EBADF);
				break;
			}
			ival = (int *) sendto->s_msg;
			switch(sendto->s_flags) {	/* option */
				case	IO_SOPT_IOMODE :	/* lowest level mode (read,write) */
					if((*ival & I->io_allowed_modes) != *ival) {
						SETERROR(EINVAL);
						rc = SYSERR;
					}
					else {
						I->io_desired_modes = *ival;
					}
					if(!(I->io_desired_modes & IOMODES_INPUT))
						so->so_opt |= SOOPT_CANTRECVMORE;
					else
						so->so_opt &= ~SOOPT_CANTRECVMORE;
					if(!(I->io_desired_modes & IOMODES_OUTPUT))
						so->so_opt |= SOOPT_CANTSENDMORE;
					else {
						so->so_opt &= ~SOOPT_CANTSENDMORE;
						so->so_selectflags |= SOSEL_WRITE;
					}
					break;
				case	IO_SOPT_IPMODE :	/* input post processing mode */
					I->io_ipost_process = *ival;
					break;
				case	IO_SOPT_OPMODE :	/* output post processing mode */
					I->io_opost_process = *ival;
					break;
				case	IO_SOPT_IFMODE :	/* interface modes, flow control, bits etc */
					I->io_intfmodes = *ival;
					break;
				case	IO_SOPT_SPEED  :	/* speed */
					I->io_speed = *ival;
					break;
				case	IO_SOPT_IBSIZE :	/* input buffer size */
					if(I->io_ibuffers.queue_count)
						goto protoerr;
					so->so_recvqueue = *ival;
					break;
				case	IO_SOPT_OBSIZE :	/* output buffer size */
					if(I->io_obuffers.queue_count)
						goto protoerr;
					so->so_writeable = *ival;
					break;
				case	IO_SOPT_IMINCOUNT :	/* min byte count to signal read */
					I->io_iminbytes = (unsigned int ) *ival;
					break;
				case	IO_SOPT_ITIMEOUT :	/* min time to signal read */
					I->io_imintimeout = (unsigned int) *ival;
					break;
				case	IO_SOPT_IBUFSIZE :
					if(I->io_ibuffers.queue_count)
						goto protoerr;
				default :
					SETERROR(EINVAL);
					rc = SYSERR;
			}	/* end switch opt */
			if(!rc)
				rc = (*I->io_function)(I,IOFUNCTION_SETOPT, sendto);
			break;
	} /* end switch pr */
	return(rc);
}

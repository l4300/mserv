/* dummy.c */
#include	"network.h"

#include	"sys/socket.h"

int
write(int fd, char *ptr, int len)
{
	return(sowrite(fd, ptr, len));
}

int
close(int fd)
{
	return(soclose(fd));
}

int
read(int fd, char *ptr, int len)
{
	return(soread(fd, ptr, len));

}

void
_setenvp()
{

}

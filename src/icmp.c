/* icmp.c */

#include	<mem.h>
#include 	"network.h"
#include	"task.h"

#if	IDEBUG
#include	"syslog.h"
#endif

#include	<memcheck.h>


#if	IDEBUG
char	*icmp_types[]={
	"Echo Reply",
	"Type 1",
	"Type 2",
	"Dest Unreachable",
	"Source Quench",
	"Redirect",
	"Type 6",
	"Type 7",
	"Echo Request",
	"Type 9",
	"Type 10",
	"Timex",
	"Paramp",
	"TimerQ",
	"TimerP",
	"Info Request"
	"Info Reply",
	"Mask Request",
	"Mask Reply"
};

extern char *IP_ntoa();

#endif

long	IcmpInMsgs;
long	IcmpInEchos;
long	IcmpInAddrMasks;
long	IcmpInAddrMaskReps;
long	IcmpInEchoReps;
long	IcmpInRedirects;
long	IcmpInErrors;
long	IcmpOutMsgs;
long	IcmpOutErrors;

int	bsdbrc;		/* if set, use all 0's broadcast */
IPaddr	ip_maskall ={ 255, 255, 255, 255};

#define	ECHOMAX(pip)	(MAXLRGBUF-IC_HLEN-IP_HLEN(pip)-EP_HLEN-EP_CRC)

#pragma argsused

int
icmp_in(struct netif *pni, struct ep *pep)
{
	struct ip	*pip;
	struct icmp	*pic;
	int		i, len;

	pip = (struct ip *) pep->ep_data;
	pic = (struct icmp *) pip->ip_data;

	len = pip->ip_len - IP_HLEN(pip);
	if(cksum((unsigned int *) pic, len >> 1)) {
		IcmpInErrors++;
		BufferFreeData(pep);
		return(SYSERR);
	}

	IcmpInMsgs++;
#if	IDEBUG
	{
		char from[64];

		strcpy(from,IP_ntoa(pip->ip_src));

		Syslog(pic->ic_type == ICT_ECHORP ? LOG_INFO : LOG_DEBUG,"icmp_in %s from %s to %s",
		pic->ic_type < sizeof(icmp_types)/sizeof(char *) ? icmp_types[pic->ic_type] : "unknown",
			from, IP_ntoa(pip->ip_dst));
	}
#endif
	switch(pic->ic_type) {

		case ICT_ECHORQ :
			IcmpInEchos++;
			return icmp(ICT_ECHORP, 0, pip->ip_src, (char *) pep, NULL);
		case ICT_MASKRQ :
			IcmpInAddrMasks++;
#if	!IPDOOR
			if(!gateway) {
#else
			{
#endif
				BufferFreeData(pep);
				return(OK);
			}
			pic->ic_type = (char) ICT_MASKRP;
			netmask(pic->ic_data, pip->ip_dst);
			break;
		case ICT_MASKRP :
			IcmpInAddrMaskReps++;
			for(i=0; i < nifcount; i++)
				if(!memcmp(nif[i].ni_ip, pip->ip_dst, IP_ALEN))
					break;
			if(i != nifcount) {
				setmask(i, pic->ic_data);
#ifdef	BAD
				send(pic->ic_id, ICT_MASKRP);
#endif
			}
			BufferFreeData(pep);
			break;
		case ICT_ECHORP :	/* echo reply */
			IcmpInEchoReps++;
#ifdef	BAD
			if(send(pic->ic_id,pep) != OK)	/* pass echo reply to task */
				BufferFreeData(pep);
#endif

			BufferFreeData(pep);
			return(OK);
		case ICT_REDIRECT :
			IcmpInRedirects++;
			icredirect(pep);
			return OK;

		case ICT_DESTUR :

		default :
			IcmpInErrors++;
			BufferFreeData(pep);
			return(OK);
	} /* end switch */
	icsetsrc(pip);
	len = pip->ip_len - IP_HLEN(pip);
	pic->ic_cksum = 0;
	pic->ic_cksum = cksum((unsigned int *) pic, len >> 1);
	IcmpOutMsgs++;
	ipsend(pip->ip_dst, pep, len);
	return(OK);
}


int
icredirect(struct ep *pep)
{
	struct route	*prt;
	struct ip	*pip,*pip2;
	struct icmp	*pic;
	IPaddr		mask;

	pip = (struct ip *) pep->ep_data;
	pic = (struct icmp *) pip->ip_data;
	pip2 = (struct ip *) pic->ic_data;

	if(pic->ic_code == ICC_HOSTRD)
		memcpy(mask, ip_maskall, IP_ALEN);
	else
		netmask(mask, pip2->ip_dst);
	prt = rtget(pip2->ip_dst, RTF_LOCAL);
	if(!prt) {
		BufferFreeData(pep);
		return (OK);
	}

	if(!memcmp(pip->ip_src, prt->rt_gw, IP_ALEN)) {
		rtdel(pip2->ip_dst, mask);
		rtadd(pip2->ip_dst, mask, pic->ic_gw, prt->rt_metric,
			prt->rt_ifnum, IC_RDTTL);
	}
	rtfree(prt);
	BufferFreeData(pep);
	return(OK);
}

int
setmask(int inum, IPaddr mask)
{
	IPaddr	aobrc;
	IPaddr	defmask;
	int	i;

	if(nif[inum].ni_svalid) {
		rtdel(nif[inum].ni_subnet, nif[inum].ni_mask);
		rtdel(nif[inum].ni_brc, ip_maskall);
		rtdel(nif[inum].ni_subnet, ip_maskall);
	}
	memcpy(nif[inum].ni_mask, mask, IP_ALEN);
	nif[inum].ni_svalid = 1;
	netmask(defmask, nif[inum].ni_ip);

	for(i=0; i < IP_ALEN; i++) {
		nif[inum].ni_subnet[i] = nif[inum].ni_ip[i] & nif[inum].ni_mask[i];
		if(bsdbrc) {
			nif[inum].ni_brc[i] = nif[inum].ni_subnet[i];
			aobrc[i] = nif[inum].ni_subnet[i] | ~nif[inum].ni_mask[i];
		}
		else
			nif[inum].ni_brc[i] = nif[inum].ni_subnet[i] |
						~nif[inum].ni_mask[i];
		/* set network broadcast */
		nif[inum].ni_nbrc[i] = nif[inum].ni_ip[i] | ~defmask[i];
	} /* end for */


	rtadd(nif[inum].ni_subnet, nif[inum].ni_mask,  nif[inum].ni_ip, 0, inum, RT_INF);
	if(bsdbrc)
		rtadd(aobrc, ip_maskall, nif[inum].ni_ip, 0, inum, RT_INF);	// was NI_LOCAL
	else
		rtadd(nif[inum].ni_brc, ip_maskall, nif[inum].ni_ip, 0, inum, RT_INF);	// was NI_LOCAL 9/11/95
	rtadd(nif[inum].ni_subnet, ip_maskall, nif[inum].ni_ip, 0, inum, RT_INF);	// was NI_LOCAL

//	rtadd(nif[inum].ni_ip, ip_maskall, nif[inum].ni_ip, 0, NI_LOCAL, RT_INF); /* ## added bkc 7/22/92 */
	return(OK);
}

void	
icsetsrc(struct ip *pip)
{
	int	i;

	for(i=0; i < nifcount; i++) {
		if(i == NI_LOCAL)
			continue;
		if(netmatch(pip->ip_dst, nif[i].ni_ip, nif[i].ni_mask,0))
			break;
	}

	if(i == nifcount)
		memcpy(pip->ip_src, ip_anyaddr, IP_ALEN);
	else
		memcpy(pip->ip_src, nif[i].ni_ip, IP_ALEN);

}


int
icmp(int type, int code, IPaddr dst, char *pa1, char *pa2)
{
	struct	ep	*pep;
	struct 	ip	*pip;
	struct 	icmp	*pic;
	Bool		isresp, iserr;
	IPaddr		src, tdst;
	int		i, datalen;


	IcmpOutMsgs++;
	memcpy(tdst, dst, IP_ALEN);

	pep = icsetbuf(type, pa1, &isresp, &iserr);
	if(!pep) {
		IcmpOutErrors++;
		return(SYSERR);
	}

	pip = (struct ip *) pep->ep_data;
	pic = (struct icmp *) pip->ip_data;
	datalen = IC_HLEN;
	if(isresp) {
		if(iserr) {
			if(!icerrok(pep)) {
#if	IDEBUG
	Syslog(LOG_DEBUG,"icmp dropped !icerrok type 0x%x code 0x%x dest %s",type,code,IP_ntoa(tdst));
#endif
				BufferFreeData(pep);
				return(OK);
			}
			memcpy(pic->ic_data, pip, IP_HLEN(pip)+8);
			datalen += IP_HLEN(pip) + 8;
		}
		icsetsrc(pip);
	}
	else
		memcpy(pip->ip_src, ip_anyaddr, IP_ALEN);
	memcpy(pip->ip_dst, tdst, IP_ALEN);

	pic->ic_type = (char) type;
	pic->ic_code = (char) code;
	if(!isresp) {
		if(type == ICT_ECHORQ)
			pic->ic_seq = *((int *) pa1);
		else
			pic->ic_seq = 0;

		pic->ic_id = getpid();
	}
	datalen += icsetdata(type, pip, pa2);

#if	IDEBUG
	{
		char from[64];

		strcpy(from,IP_ntoa(pip->ip_src));

		Syslog(LOG_DEBUG,"icmp send type %s (0x%x) code 0x%x len %d from %s to %s",
			type < sizeof(icmp_types)/sizeof(char *) ? icmp_types[type] : "unknown",
			type, code, datalen, from, IP_ntoa(pip->ip_dst));
	}
#endif

	pic->ic_cksum = 0;
	pic->ic_cksum = cksum((unsigned int *) pic, (datalen+1) >> 1);
	pip->ip_proto = IPT_ICMP;
	ipsend(tdst, pep, datalen);
	return(OK);
}

Bool
icerrok(struct ep *pep)
{
	struct	ip	*pip = (struct ip *) pep->ep_data;
	struct	icmp	*pic  = (struct icmp *) pip->ip_data;

	if(pip->ip_proto == IPT_ICMP) {
		switch(pic->ic_type) {
			case ICT_DESTUR :
			case ICT_REDIRECT :
			case ICT_SRCQ :
			case ICT_TIMEX :
			case ICT_PARAMP :
				return(0);
			default :
				break;
		}
	}
	if((pip->ip_fragoff & IP_FRAGOFF) || isbrc(pip->ip_dst))
		return(0);
	return(1);
}

struct ep *
icsetbuf(int type, char *pa1, Bool *pisresp, Bool *piserr)
{
	struct	ep	*pep;
	Buffer		*B;

	*pisresp = *piserr = 0;

	pep = NULL;
	switch(type) {
		case ICT_REDIRECT :
			B =  BufferAlloc(MAXNETBUF);
			if(!B)
				return(NULL);
			pep = (struct ep *) B->data;
			memcpy(pep, pa1, MAXNETBUF);
			pa1 = (char *) pep;
			*piserr = 1;
			break;
		case ICT_DESTUR :
		case ICT_SRCQ :
		case ICT_TIMEX :
		case ICT_PARAMP :
			pep = (struct ep *) pa1;	/* #P */
			*piserr = 1;
			break;
		case ICT_ECHORP :
		case ICT_INFORP :
		case ICT_MASKRP :
			pep = (struct ep *) pa1;
			*pisresp = 1;
			break;
		case ICT_ECHORQ :
		case ICT_TIMERQ :
		case ICT_INFORQ :
		case ICT_MASKRQ :
			B = BufferAlloc(MAXNETBUF);
			if(!B)
				return(NULL);
			pep = (struct ep *) B->data;
			break;
		case ICT_TIMERP :	/* kludge code ? */
			IcmpOutErrors--;
			BufferFreeData((Buffer *) pa1);
			return(NULL);
	} /* end switch */

	/* add MIB settings here */
	switch(type) {


	} /* end switch */
	return(pep);
}

int
icsetdata(int type, struct ip *pip, char *pa2)
{
	struct icmp	*pic = (struct icmp *) pip->ip_data;
	int		i, len, plen;

	switch(type) {
		case ICT_ECHORP :
			len = pip->ip_len - IP_HLEN(pip) - IC_HLEN;
			if(isodd(len))
				pic->ic_data[len] = 0;
			return(len);
		case ICT_DESTUR :
		case ICT_SRCQ :
		case ICT_TIMEX :
			pic->ic_mbz = 0;
			break;
		case ICT_REDIRECT :
			memcpy(pic->ic_gw, pa2, IP_ALEN);
			break;
		case ICT_PARAMP :
			pic->ic_ptr = (char) pa2;	/* ### ?*/
			for(i=0; i < IC_PADLEN; i++)
				pic->ic_pad[i] = 0;
			break;
		case ICT_ECHORQ :
			plen = (int) pa2;
			if(plen > ECHOMAX(pip))
				plen = ECHOMAX(pip);
			for(i = 0; i < plen; i++)
				pic->ic_data[i] = i;
			if(isodd(plen))
				pic->ic_data[plen] = 0;
			return (plen);
		case ICT_MASKRQ :
			memcpy(pic->ic_data, ip_anyaddr, IP_ALEN);
			return(IP_ALEN);
	}	/* end switch */
	return(0);
}

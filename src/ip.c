/* ip.c */

#define	_IP_C		1
#define	DONEAR		0		// this doesn't work
#define	DOINLINE	1		// true to do some functions inline
#if	!IPDOOR
#define	QUICKINPUT	1		// true to short-circuit route testing on incoming packets
#else
#define	QUICKINPUT	0
#endif
					// valid for single interface only
#define	PBURST		1		// process only PACKET_COUNT packets at once

#include	<mem.h>

#include	"network.h"
#include	"ipreass.h"

#include	"task.h"

#if	IPDOOR
#include	"udp.h"
#endif

#include	<memcheck.h>

long	IpInHdrErrors;
long	IpInAddrErrors;
long	IpInDiscards;
long	IpInReceives;
long	IpInUnKnownProtos;
long	IpInDelivers;

long	IpOutNoRoutes;
long	IpForwDatagrams;
long	IpOutDiscards;
long	IpOutRequests;
long 	IpFragFails;
long	IpFragCreates;
long	IpFragOKs;
long	IpReasmReqds;
long	IpReasmDrops;
long	IpReasmFails;
long	IpReasmOKs;

long	IpBootpDropped;

#if	IPDOOR
int	gateway = 1;
#else
int	gateway;
#endif

Semaphore	ipfmutex;
struct	ipfq	ipfqt[IP_FQSIZE];

IPaddr	ip_anyaddr = { 0, 0, 0, 0 };
IPaddr	ip_loopback = { 127, 0, 0, 1};
Task	*iptask;
const	int	PACKET_BURST = 20;

#if	DOINLINE

#define	iph2net(A)	ipnet2h(A)


void near
ipnet2h(struct ip * pip)
{
	asm {
			les	bx, pip
			mov 	ax, es:[bx].ip_len
			xchg 	al, ah
			mov 	es:[bx].ip_len, ax
			mov	ax, es:[bx].ip_id
			xchg	al, ah
			mov	es:[bx].ip_id, ax
			mov	ax, es:[bx].ip_fragoff
			xchg	al, ah
			mov	es:[bx].ip_fragoff, ax
	 }
}

#endif

void
ipfinit()
{
	Semaphore_Initialize(&ipfmutex, 1);
}


#pragma argsused
int
ipproc(void *Parent, char *name, int nargs, char **argv)
{
	struct	ep	*pep;
	IP		*pip;
	ROUTE		*prt;
	Bool		nonlocal;
	int		ifnum, rdtype;
	int		packet_count = PACKET_BURST;

	iptask = Task_Current_Task;
	MessageBoxInitialize(&iptask->Messages, IP_QUEUE_SIZE);
	iptask->Messages.mesg_MQueue.queue_flags |= QUEUE_FLAGS_NOTSORTED;

	while(1) {
#if	PBURST
		if(!iptask->Messages.mesg_MQueue.queue_count)
			packet_count = PACKET_BURST;	// will block on next get
		else  {
			if(!--packet_count) {
				TaskReschedule();
				packet_count = PACKET_BURST;
			}
		}
#endif
		pep = ipgetp(&ifnum);	/* get the next available packet */
		if(!pep)
			continue;
		pip = (struct ip *) pep->ep_data;
		if((pip->ip_verlen >>4) != IP_VERSION) {
			IpInHdrErrors++;
			BufferFreeData(pep);
			continue;
		}
#if	IPDOOR

		if(IP_CLASSE(pip->ip_dst)) {
			IpInAddrErrors++;
			BufferFreeData(pep);
			continue;
		}
#else
		if(IP_CLASSD(pip->ip_dst) || IP_CLASSE(pip->ip_dst)) {
			IpInAddrErrors++;
			BufferFreeData(pep);
			continue;
		}
#endif
		if(ifnum != NI_LOCAL) {
			if(cksum((unsigned *) pip, IP_HLEN(pip) >> 1)) {
				IpInHdrErrors++;
				BufferFreeData(pep);
				continue;
			}
			ipnet2h(pip);
#if	QUICKINPUT
			if(!isbrc(pip->ip_dst)) {
				if(--(pip->ip_ttl) == 0) {
					IpInHdrErrors++;
					iph2net(pip);
					icmp(ICT_TIMEX, ICC_TIMEX, pip->ip_src, (char *) pep, NULL);
					continue;
				}
				iph2net(pip);
				local_out(pep);
				continue;
			}
#endif
		}
#ifdef	NOTRIGHTEITHER
		if(isbrc(pip->ip_dst) && ifnum != NI_LOCAL) {	// don't forward broadcasts
			BufferFreeData(pep);			// 10/6/95
			IpInDiscards++;
			continue;
		}
#endif
		prt = rtget(pip->ip_dst, (ifnum == NI_LOCAL));
		if(!prt) {
			if(gateway && !isbrc(pip->ip_dst)) {	// added isbrc 10/7/95
				iph2net(pip);
				icmp(ICT_DESTUR, ICC_NETUR, pip->ip_src, (char *) pep, NULL);
			} else {
				IpOutNoRoutes++;
				BufferFreeData(pep);
			}
			continue;
		}
		nonlocal = ifnum != NI_LOCAL && prt->rt_ifnum != NI_LOCAL;
		if(!gateway && nonlocal) {
			IpInAddrErrors++;
			BufferFreeData(pep);
			rtfree(prt);
			continue;
		}
		if(nonlocal)
			IpForwDatagrams++;
		/* fill in src IP, if we're the sender */

		if(ifnum == NI_LOCAL) {
			if(!memcmp(pip->ip_src, ip_anyaddr, IP_ALEN) ||
			   !memcmp(pip->ip_src,ip_maskall,IP_ALEN)) {
				if(prt->rt_ifnum == NI_LOCAL)
					memcpy(pip->ip_src, pip->ip_dst, IP_ALEN);
				else
					memcpy(pip->ip_src, nif[prt->rt_ifnum].ni_ip, IP_ALEN);
			}
		}
		if(--(pip->ip_ttl) == 0 && prt->rt_ifnum != NI_LOCAL) {
			IpInHdrErrors++;
			iph2net(pip);
			icmp(ICT_TIMEX, ICC_TIMEX, pip->ip_src, (char *) pep, NULL);
			rtfree(prt);
			continue;
		}
#if	!IPDOOR
		ipdbc(ifnum, pep, prt); /* handle directed broadcast */
		ipredirect(pep, ifnum, prt); /* do redirect if need be */
#else
		// check and trap udp bootp broadcasts on nif 2
		if(ifnum == 2 && prt->rt_ifnum != NI_LOCAL && pip->ip_proto == IPT_UDP) {
			struct	udp	*pudp = (struct udp *) pip->ip_data;

			if(ntohs(pudp->u_dst) == 67) {	// going to bootp port
				local_out(pep);
//				BufferFreeData(pep);		// jammed into us only
				rtfree(prt);
//				IpBootpDropped++;
				continue;
			}
		}
		if(isbrc(pip->ip_dst) && ifnum == 1) {
		 	/* send broadcasts out to ppp/slip port */
			ipputp(2,pip->ip_dst,pep);
			rtfree(prt);
			continue;
		}
#endif
		if(prt->rt_metric != 0)
			ipputp(prt->rt_ifnum, prt->rt_gw, pep);
		else
			ipputp(prt->rt_ifnum, pip->ip_dst, pep);
		rtfree(prt);
	}	/* end while */
}
#ifdef	ELSEWHERE

int
cksum(unsigned int *buf, int nwords)
{

	unsigned long	sum;

	for(sum=0; nwords > 0; nwords--)
		sum += *buf++;
	sum = (sum >> 16) + (sum & 0xffff);
	sum += (sum >> 16);
	return ~sum;
}
#endif

void NEAR
ipdbc(int ifnum, struct ep *pep, struct route *prt)
{
	struct ip	*pip = (struct ip *) pep->ep_data;
	struct ep	*pep2;
	struct route	*prt2;
	int		len;
	int		destif = 0;
	Buffer		*buf;

	if(memcmp(pip->ip_dst,ip_maskall,IP_ALEN)) {
		if(prt->rt_ifnum != NI_LOCAL)
			return;		/* not going to us at all */
		if(!isbrc(pip->ip_dst))
			return;		/* not a broadcast either */
		prt2 = rtget(pip->ip_dst, RTF_LOCAL);
		if(!prt2)
			return;
		if(prt2->rt_ifnum == ifnum) {	/* not a directed brc */
			rtfree(prt2);
			return;
		}
		destif = prt2->rt_ifnum;
		rtfree(prt2);
	}
	/* otherwise, copy this packet */
	len = EP_HLEN + pip->ip_len;
	buf = BufferAlloc(len);
	if(!buf) {
		return;
	}
	pep2 = (struct ep *) buf->data;
	(DataToQE(pep2))->queue_object = (DataToQE(pep))->queue_object;
	memcpy(pep2, pep, pep2->ep_len = len);
	ipputp(destif, pip->ip_dst, pep2);
}

struct ep  * NEAR
ipgetp(int *pifnum)
{

	Buffer	*B;

	while(1) {
		struct ep *pep;

		B = MessageBoxReceiveBuf(&iptask->Messages);	/* this will block */
		if(!B)
			continue;	/* should never happen */
		if(B->QE.queue_type != QTYPE_MBUF) { /* not an mbuf ! */
			continue;	/* ignore for now */
		}

		pep = (struct ep *) B->data;
		if(pep->ep_type == EPT_ARP) {
		 	arp_in(((Net_Interface *) B->QE.queue_object),pep);
			continue;
		}
		if(pifnum) {
			if(B->QE.queue_object)
				*pifnum = ((Net_Interface *) B->QE.queue_object)->ni_ifnumber;
			else
				*pifnum = 0;
		}
		return((struct ep *) B->data);
	}
}

Bool
isbrc(IPaddr dest)
{
	int	inum;

	if(!memcmp(dest, ip_anyaddr, IP_ALEN) ||
	   !memcmp(dest, ip_maskall, IP_ALEN))
		return(1);
#ifdef	SLOW
	for(inum = 0; inum < nifcount; ++inum)
		if(!memcmp(dest, nif[inum].ni_brc, IP_ALEN) ||
		   !memcmp(dest, nif[inum].ni_nbrc, IP_ALEN) ||
		   !memcmp(dest, nif[inum].ni_subnet, IP_ALEN) ||
		   !memcmp(dest, nif[inum].ni_net, IP_ALEN))
			return(1);
#else
	for(inum = 0; inum < nifcount; inum++) {
		register	int	x = IP_ALEN;
		register 	unsigned  char	c;
		Net_Interface 	*pni = &nif[inum];
		register	unsigned	int flag = 0;

		while(x--) {
			c = dest[x];
			if(c != pni->ni_brc[x])
				flag |= 1;
			if(c != pni->ni_nbrc[x])
				flag |= 2;
			if(c != pni->ni_subnet[x])
				flag |= 4;
			if(c != pni->ni_net[x])
				flag |= 8;
			if(flag == 15)
				goto next;
		}
		return(1);	/* at least one was identical */
next:;
	}

#endif
	return(0);
}

#if	!DOINLINE
struct ip *
iph2net(struct ip *pip)
{
	pip->ip_len = htons(pip->ip_len);
	pip->ip_id = htons(pip->ip_id);
	pip->ip_fragoff = htons(pip->ip_fragoff);
	return(pip);
}

struct ip  *
ipnet2h(struct ip *pip)
{
	return(iph2net(pip));	// feeling cheap today
}
#endif

/* complete IP header and send to address */

int
ipsend(IPaddr faddr, struct ep *pep, int datalen)
{
static int ipackid = 1;
	struct ip *pip = (struct ip *) pep->ep_data;

	pep->ep_type = EPT_IP;
	pip->ip_verlen = (IP_VERSION<<4) | IP_MINHLEN;
	pip->ip_tos = 0;
	pip->ip_len = datalen+IP_HLEN(pip);
	pip->ip_id = ipackid++;
	pip->ip_fragoff = 0;
	pip->ip_ttl = IP_TTL;
	memcpy(pip->ip_dst, faddr, IP_ALEN);

	/* for ICMP type, make sure source matches interface addr */
#ifdef	STUPID
	if(pip->ip_proto != IPT_ICMP)
		memcpy(pip->ip_src, ip_anyaddr, IP_ALEN);
#endif
	if(MessageBoxFree(&iptask->Messages) < 1) {
		BufferFreeData(pep);
		IpOutDiscards++;
		return(SYSERR);
	}
	(DataToQE(pep))->queue_object = &nif[NI_LOCAL];
	MessageBoxSendBuf(&iptask->Messages, DataToBuf(pep));
	IpOutRequests++;
	return(OK);
}

#pragma argsused

int
ip_in(struct netif *pni, struct ep *pep)	/* called from INT */
{

	if(!iptask)	{	/* not ready yet */
		BufferFreeData(pep);
		return(SYSERR);
	}
	IpInReceives++;
	if(MessageBoxFree(&iptask->Messages) < 1) {
		IpInDiscards++;
		BufferFreeData(pep);
		return(SYSERR);
	}
	(DataToQE(pep))->queue_object = pni;
	if(MessageBoxISendBuf(&iptask->Messages, DataToBuf(pep)) != MBOX_RESULT_OK) {
		IpInDiscards++;
		BufferFreeData(pep);
	}
	return(OK);
}

#pragma argsused

int NEAR
ipdoopts(struct netif *pni, struct ep *pep)
{
	return (OK);
}

#pragma argsused
int NEAR
ipdstopts(struct netif *pni, struct ep *pep)
{
	struct ip *pip = (struct ip *) pep->ep_data;
#ifdef	JUNK
	char	*popt, *popend;
#endif
	int	len;

	if(IP_HLEN(pip) == IPMHLEN)
		return(OK);		/* no options to process */

#ifdef	JUNK
	popt = pip->ip_data;
	popend = &pep->ep_data[IP_HLEN(pip)];
#endif
	len = pip->ip_len - IP_HLEN(pip);
	if(len)
		memcpy(pip->ip_data, &pep->ep_data[IP_HLEN(pip)], len);
	pip->ip_len = IPMHLEN + len;
	pip->ip_verlen = (pip->ip_verlen & 0xf0) | IP_MINHLEN;
	return(OK);
}

int NEAR
ipputp(int inum, IPaddr nh, struct ep *pep)	/* send packet to nif */
{
	struct netif	*pni = &nif[inum];
	struct	ip	*pip;
	int		hlen, maxdlen, tosend, offset, offindg;

	if(!(pni->ni_state & NIS_UP) || (pni->ni_state & NIS_DOWN)) {
		BufferFreeData(pep);
		return(SYSERR);
	}

	pip = (struct ip *) pep->ep_data;
	if(pip->ip_len <= pni->ni_mtu) {	/* no frag required */
		memcpy(pep->ep_nexthop, nh, IP_ALEN);
		iph2net(pip);
#ifndef	WRONGPLACE
		pip->ip_cksum = 0;
		pip->ip_cksum = cksum((unsigned int *) pip, IP_HLEN(pip) >> 1);
#endif
		return(netwrite(pni, pep, ntohs(pip->ip_len)) );
	}

	/* otherwise, fragment time */
	if(pip->ip_fragoff & IP_DF) {	/* don't fragment */
		IpFragFails++;
		icmp(ICT_DESTUR, ICC_FNADF, pip->ip_src, (char *) pep, NULL);
		return(OK);
	}

	maxdlen = (pni->ni_mtu - IP_HLEN(pip)) & ~7;
	offset =0;
	offindg = (pip->ip_fragoff & IP_FRAGOFF) << 3;
	tosend = pip->ip_len - IP_HLEN(pip);

	while(tosend > maxdlen) {
		if(ipfsend(pni, nh, pep, offset, maxdlen, offindg) != OK) {
			IpOutDiscards++;
			BufferFreeData(pep);
			return(SYSERR);
		}
		IpFragCreates++;
		tosend -= maxdlen;
		offset += maxdlen;
		offindg += maxdlen;
	}	/* end while */
	IpFragOKs++;
	IpFragCreates++;
	hlen = ipfhcopy(pep, pep, offindg);
	pip = (struct ip *) pep->ep_data;
	memcpy(&pep->ep_data[hlen], &pep->ep_data[IP_HLEN(pip)+offset], tosend);
	pip->ip_fragoff = (pip->ip_fragoff & ~IP_MF)  | (offindg >> 3);
	pip->ip_len = tosend + hlen;
	pip->ip_cksum = 0;
	iph2net(pip);
	pip->ip_cksum = cksum((unsigned int *) pip, hlen >> 1);
	memcpy(pep->ep_nexthop, nh, IP_ALEN);
	return(netwrite(pni, pep, ntohs(pip->ip_len)));

}

int NEAR
ipfsend(struct netif *pni, IPaddr nexthop, struct ep *pep, int offset, int maxdlen, int offindg)
{
	struct	ep	*pepnew;
	Buffer		*B;
	struct	ip	*pip, *pipnew;
	int		hlen, len;

	pipnew = (struct ip *) pep->ep_data;
	B = BufferAlloc((IP_MINHLEN << 2)+maxdlen + EP_HLEN);	// 2/17/95 add EP_HLEN
	if(!B)
		return(SYSERR);
	pepnew = (struct ep *) B->data;
	hlen = ipfhcopy(pepnew, pep, offindg);

	pip = (struct ip *) pep->ep_data;
	pipnew = (struct ip *) pepnew->ep_data;
	pipnew->ip_fragoff = IP_MF | (offindg >> 3);
	pipnew->ip_len = len = maxdlen + hlen;
	pipnew->ip_cksum = 0;

	iph2net(pipnew);
	pipnew->ip_cksum = cksum((unsigned int *) pipnew, hlen >> 1);
	memcpy(&pepnew->ep_data[hlen], &pep->ep_data[IP_HLEN(pip)+offset], maxdlen);
	memcpy(pepnew->ep_nexthop, nexthop, IP_ALEN);
	return(netwrite(pni, pepnew, len));
}

int NEAR
ipfhcopy(struct ep *pepto, struct ep *pepfrom, int offindg)
{
	struct ip	*pipto = (struct ip *) pepto->ep_data;
	struct ip	*pipfrom = (struct ip *) pepfrom->ep_data;
	unsigned	i, maxhlen, olen, otype;
	unsigned	hlen = (IP_MINHLEN << 2);

	if(!offindg) {
		memcpy(pepto, pepfrom, EP_HLEN+IP_HLEN(pipfrom));
		return(IP_HLEN(pipfrom));
	}

	memcpy(pepto, pepfrom, EP_HLEN + hlen);

	maxhlen = IP_HLEN(pipfrom);
	i = hlen;
	while(i < maxhlen) {
		otype = pepfrom->ep_data[i];	/* #P */
		olen = pepfrom->ep_data[++i];
		if(otype & IPO_COPY) {
			memcpy(&pepto->ep_data[hlen],&pepfrom->ep_data[i-1], olen);
			hlen += olen;
		} else if (otype == IPO_NOP || otype == IPO_EOOP) {
			pepto->ep_data[hlen++] = otype;
			olen = 1;
		}
		i += olen - 1;

		if(otype == IPO_EOOP)
			break;
	} /* end while */

	/* must be multiple of 4 */
	while(hlen % 4)
		pepto->ep_data[hlen++] = IPO_NOP;
	return(hlen);
}


struct ep * NEAR
ipreass(struct ep *pep)
{
	struct ep	*pep2;
	struct ip	*pip;
	int		firstfree;
	int		i;

	pip = (struct ip *) pep->ep_data;

	if((pip->ip_fragoff & (IP_FRAGOFF|IP_MF)) == 0) {
		return(pep);
	}
	Semaphore_Wait(&ipfmutex);
	IpReasmReqds++;
	firstfree = -1;
	for(i= 0; i < IP_FQSIZE; ++i) {
		struct ipfq	*piq = &ipfqt[i];

		if(piq->ipf_state == IPFF_FREE) {
			if(firstfree == -1)
				firstfree = i;
			continue;
		}
		if(piq->ipf_id != pip->ip_id)
			continue;
		if(memcmp(piq->ipf_src, pip->ip_src, IP_ALEN))
			continue;
		/* otherwise, it matches this queue */
		if(ipfadd(piq, pep) == 0) {
			Semaphore_Signal(&ipfmutex);
			return(NULL);
		}
		pep2 = ipfjoin(piq);
		Semaphore_Signal(&ipfmutex);
		return(pep2);

	} /* end for */

	if(firstfree < 0) {	/* no room */
		BufferFreeData(pep);
		IpReasmDrops++;
		Semaphore_Signal(&ipfmutex);
		return(NULL);
	}
	memset(&ipfqt[firstfree].ipf_q, 0, sizeof(Queue));

	memcpy(ipfqt[firstfree].ipf_src, pip->ip_src, IP_ALEN);
	ipfqt[firstfree].ipf_id = pip->ip_id;
	ipfqt[firstfree].ipf_ttl = IP_FTTL;
	ipfqt[firstfree].ipf_state = IPFF_VALID;
	ipfadd(&ipfqt[firstfree], pep);
	Semaphore_Signal(&ipfmutex);
	return(NULL);


}


Bool NEAR
ipfadd(struct ipfq *iq, struct ep *pep)
{
	struct	ip	*pip;
	int		fragoff;

	if(iq->ipf_state != IPFF_VALID) {
		BufferFreeData(pep);
		return(0);
	}

	pip = (struct ip *) pep->ep_data;
	fragoff = pip->ip_fragoff & IP_FRAGOFF;
	if(iq->ipf_q.queue_count >= IP_MAXNF) {	/* too many in queue already */
		Buffer	*B;

		BufferFreeData(pep);
		IpReasmFails++;
		while((B = (Buffer *) QueuePHead(&iq->ipf_q)) != 0) {
			BufferFree(B);
			IpReasmFails++;
		}
		iq->ipf_state = IPFF_BOGUS;
		return(0);
	}
	DataToQE(pep)->queue_key = fragoff;
	QueueInsert(&iq->ipf_q, DataToQE(pep));
	iq->ipf_ttl = IP_FTTL;
	return(1);
}

struct	ep * NEAR
ipfjoin(struct ipfq *iq)
{
	Buffer		*B;
	struct	ep	*pep;
	struct	ip	*pip;
	int		off, packoff;

	if(iq->ipf_state == IPFF_BOGUS)
		return(NULL);

	off = 0;
	B = (Buffer *) QueueHead(&iq->ipf_q);
	while(B) {
		pep = (struct ep *) B->data;
		pip = (struct ip *) pep->ep_data;
		packoff = (pip->ip_fragoff & IP_FRAGOFF) << 3;

		if(off < packoff)
			return(NULL);
		off  = packoff + pip->ip_len + IP_HLEN(pip);
		B = (Buffer *) B->QE.queue_next;
	} /* end while */
	if(off > MAXLRGBUF) {		/* too large already */
		while((B = (Buffer *) QueuePHead(&iq->ipf_q)) != 0)
			BufferFree(B);
		iq->ipf_state = IPFF_FREE;
		return(NULL);
	}
	if((pip->ip_fragoff & IP_MF) == 0)
		return(ipfcons(iq));
	return(NULL);
}

struct ep * NEAR
ipfcons(struct ipfq *iq)
{
	struct	ep	*pep, *peptmp;
	struct	ip	*pip;
	int		off, seq;
	Buffer		*B,*BT;

	B = BufferAlloc(MAXLRGBUF);
	if(!B) {
		while((B = (Buffer *) QueuePHead(&iq->ipf_q)) != 0) {
			BufferFree(B);
			IpReasmFails++;
		}
		iq->ipf_state = IPFF_FREE;
		return(NULL);
	}

	pep = (struct ep *) B->data;
	BT = (Buffer *) QueuePHead(&iq->ipf_q);
	peptmp = (struct ep *) BT->data;
	pip = (struct ip *) peptmp->ep_data;
	off = IP_HLEN(pip);
	seq = 0;
	memcpy(pep, peptmp, EP_HLEN+off);

	while(BT) {
		int	dlen, doff;

		peptmp = (struct ep *) BT->data;
		pip = (struct ip *) peptmp->ep_data;
		doff = IP_HLEN(pip) + seq - ((pip->ip_fragoff & IP_FRAGOFF) << 3);
		dlen = pip->ip_len - doff;
		memcpy(pep->ep_data + off, peptmp->ep_data+doff, dlen);
		off += dlen;
		seq += dlen;
		BufferFree(BT);
		BT = (Buffer *) QueuePHead(&iq->ipf_q);
	}
	pip = (struct ip *) pep->ep_data;
	pip->ip_len = off;
	pip->ip_fragoff = 0;

	iq->ipf_state = IPFF_FREE;
	IpReasmOKs++;
	return(pep);
}



void
ipftimer(int gran)
{
	struct	ep	*pep;
	struct	ip	*pip;
	int		i;

	Semaphore_Wait(&ipfmutex);
	for(i = 0; i < IP_FQSIZE; ++i) {
		struct ipfq 	*iq = &ipfqt[i];
		Buffer		*B;

		if(iq->ipf_state == IPFF_FREE)
			continue;
		iq->ipf_ttl -= gran;
		if(iq->ipf_ttl <= 0) {
			if(iq->ipf_state == IPFF_BOGUS) {
				iq->ipf_state = IPFF_FREE;
				continue;
			}
			if((B = (Buffer *) QueuePHead(&iq->ipf_q)) != 0) {
				pep = (struct ep *) B->data;
				IpReasmFails++;
				pip = (struct ip *) pep->ep_data;
				icmp(ICT_TIMEX, ICC_FTIMEX, pip->ip_src, (char *) pep, NULL);
			}
			while((B = (Buffer *) QueuePHead(&iq->ipf_q)) != 0) {
				IpReasmFails++;
				BufferFree(B);
			}
			iq->ipf_state = IPFF_FREE;
		} /* end if */
	} /* end for */
	Semaphore_Signal(&ipfmutex);

}


int
local_out(struct ep *pep)
{
	struct	netif	*pni = &nif[NI_LOCAL];
	struct	ip	*pip = (struct ip *) pep->ep_data;
	int		rv;

	ipnet2h(pip);
	pep = ipreass(pep);
	if(pep == 0)
		return(OK);
	pip = (struct ip *) pep->ep_data;
	ipdstopts(pni, pep);
	switch(pip->ip_proto) {
		case IPT_UDP :
			rv = udp_in(pni, pep);
			break;
		case IPT_ICMP :
			rv = icmp_in(pni, pep);
			break;
#if	!IPDOOR
		case IPT_TCP :
			rv = tcp_in(pni, pep);
			break;
#endif
		default :;
			IpInUnKnownProtos++;
			icmp(ICT_DESTUR, ICC_PROTOUR, pip->ip_src, (char *) pep, NULL);
			return(OK);
	}
	IpInDelivers++;
	return(rv);

}


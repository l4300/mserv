/* ipredirect.c */
#include	<mem.h>
#include	"network.h"
#include	<memcheck.h>

#if	IDEBUG
#include	"syslog.h"

extern char *IP_ntoa();
#endif

void
ipredirect(struct ep *pep, int ifnum, struct route *prt)
{
	struct	ip	*pip = (struct ip *) pep->ep_data;
	struct	route	*tprt;
	int		rdtype, isonehop;
	IPaddr		nmask;

	if(ifnum == NI_LOCAL || ifnum != prt->rt_ifnum)
		return;
	tprt = rtget(pip->ip_src, RTF_LOCAL);
	if(!tprt)
		return;
	isonehop = tprt->rt_metric == 0;
	rtfree(tprt);
	if(!isonehop)
		return;

	netmask(nmask, prt->rt_net);
	if(!memcmp(prt->rt_mask, nmask, IP_ALEN))
		rdtype = ICC_NETRD;
	else
		rdtype = ICC_HOSTRD;
#if	IDEBUG
	{
      		char src[32], dst[32],gw[32];

		strcpy(src,IP_ntoa(pip->ip_src));
		strcpy(gw, IP_ntoa(prt->rt_gw));
		strcpy(dst,IP_ntoa(pip->ip_dst));

		Syslog(LOG_DEBUG,"icmpredir type 0x%x %s -> %s to gw %s",
			rdtype, src,dst,gw);
	}
#endif
	icmp(ICT_REDIRECT, rdtype, pip->ip_src, (char *) pep, (char *) prt->rt_gw);
}


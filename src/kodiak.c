#define	STATIC
/* #define	INTRESTART */
#include	"network.h"

#include	"kodiak.h"

#include	"koddefs.h"
#include	<dos.h>

#ifdef	MS
#include	"v25.h"
#endif

#define	DMARETRYCOUNT	100
#define	SETB(reg,mask)	((reg) |= (mask))
#define	CLRB(reg,mask)	((reg) &= ~(mask))

#define	JUNK
#ifdef	JUNK
#define	OUTPW(port, value)	(outportb((port), (value) & 0xff), outportb((port)+1, (value) >> 8))
#define	INPW(port)		((inportb((port)+1) << 8) | inportb(port))
#else
#define	OUTPW(port, value)	(outport(port, value))
#define	INPW(port)		(inport((port)))
#endif

#define	OUTCONFIG2	(OUTPW((CONFIG_2_PORT), config2))
#define	OUTCONFIG1	(OUTPW((CONFIG_1_PORT), config1))

#define	OUTCOMMAND	(OUTPW((COMMAND_PORT), command))
#define	OUTWINDOW(v)	(OUTPW((WINDOW_PORT), v))

#define	ISSUECOMMAND(c)	(SETB(command, (c)), OUTCOMMAND, CLRB(command, (c)))

#define	SDELAY(n)	for(x=0; x <  n; x++)

void	interrupt	(*oldint)();
static	unsigned	reg_base;
static	unsigned	command;
static	unsigned	config2;
static	unsigned	config1;
static	unsigned	intvect;
static	unsigned	current_receive_ptr;
static	int		xmit_is_active;

#define	STACKSIZE	512
STATIC	unsigned	mystack[STACKSIZE/2];
STATIC	unsigned	theirss, theirsp;

STATIC	Net_Interface	*my_pni;

#define	PROMSIZE	32

STATIC	unsigned	char	prombytes[PROMSIZE];

STATIC	void	SetFifoWrite(unsigned xaddr);
STATIC	void	Restart_Receiver(), Full_Restart();

long	KodiakRestarts, KodiakChainedPackets, KodiakWatchdogRestarts, KodiakPicResets;
long	KodiakFullRestarts, KodiakDMAloops, KodiakUnChainedPackets;
long	KodiakEndChains, KodiakNullStatus, KodiakInPackets;
long	KodiakMissedFifo, KodiakInts, KodiakOutDiscards;

static	int	recursive;

unsigned
dummy(unsigned x)
{
	return(x);
}

STATIC	void
SetFifoRead(unsigned xaddr)
{
	unsigned	cx = 0;

	SetFifoWrite(xaddr);

	CLRB(command, FIFO_WRITE);
	SETB(command, FIFO_READ);
	ISSUECOMMAND(0);
#if	NOTNEEDED
	while(--cx) {
#ifdef	OLDWAY
		if(INPW(STATUS_PORT) & WINDOW_INT)
#else
		if(INPW(STATUS_PORT) & FIFO_FULL)
#endif
			break;
		if(!(INPW(STATUS_PORT) & FIFO_DIR)) {	/* still set to write */
/* ###			OUTPW(DMA_ADDR_PORT, xaddr); */
			CLRB(command, FIFO_WRITE);
			SETB(command, FIFO_READ);
			ISSUECOMMAND(0);

			cx = 0;
		}
	}
	if(!cx) {	/* we failed, re-init port ? */
#ifdef	PANIC
		panic("kodiak fifo failed");
#endif
	}
	ISSUECOMMAND(WINDOW_INT_ACK);
#endif
}

STATIC	void
SetFifoWrite(unsigned xaddr)
{
	if((INPW(STATUS_PORT) & FIFO_DIR)) {	/* current set to read */
		CLRB(command, FIFO_READ);
		SETB(command, FIFO_WRITE);
		ISSUECOMMAND(0);
	}
#if	NOTNEEDED
	else {				/* its currently write, wait for fifo */
		unsigned	cx = 0;
		while(--cx) {
			if(INPW(STATUS_PORT) & FIFO_EMPTY)
				break;
		}
#ifdef	PANIC
		if(!cx)
			panic("kodiak fifo write failed");
#endif
	}
#endif
	OUTPW(DMA_ADDR_PORT, xaddr);
}

STATIC int
Receive_Packet()
{
	unsigned	int	next_packet_ptr, packet_length;
	register	unsigned   	int	x;
	unsigned 	int	header_status, packet_status, y;
	unsigned	int	buffersize;
	unsigned	int	rc = 0;

	int		resetme = 0;
				/* avoid having the current packet get overwritten */

	if(!(INPW(STATUS_PORT) & RX_ON))
		goto reset;

readmore:;
	while(!(resetme|rc)) {
/*		CLRB(config2, AUTO_UPDATE_REA);
		OUTCONFIG2;
*/
		SetFifoRead(current_receive_ptr);
		next_packet_ptr = INPW(WINDOW_PORT);
		if(next_packet_ptr & 0xff00) {
			KodiakInPackets++;
			packet_length = next_packet_ptr - current_receive_ptr - 18;
			if(next_packet_ptr < current_receive_ptr)
				packet_length -= RX_BUFFER_START;

			current_receive_ptr = next_packet_ptr;

			header_status = inportb(WINDOW_PORT);
			packet_status = inportb(WINDOW_PORT);
			if((packet_status & 0xff) && (header_status & DATA_FOLLOWS)) {

				if((packet_length >= 46) && (packet_length <= 1500) &&
				   !(packet_status & (SHORT_FRAME|OVERSIZE|CRC_ERROR))) {	/* if not odd, read it */
					unsigned char	dst_addr[6], src_addr[6];
					int	notme = 0;

					for(x=0; x < 6; x++)
						dst_addr[x] = inportb(WINDOW_PORT);

					for(x=0; x < 6; x++) {
						unsigned char c;

						src_addr[x] = c = inportb(WINDOW_PORT);
						if(c != my_pni->ni_hwa.ha_addr[x])
							notme = 1;
					}

					if(notme)	{	/* didn't come from me, ok */
						unsigned	type;
						Buffer		*B;

						type = INPW(WINDOW_PORT);

						buffersize = packet_length + EP_HLEN;
						B = BufferAlloc(buffersize);
						if(B) {

							struct ep *pep = (struct ep *) B->data;
							register unsigned char *p = pep->ep_data;
							memcpy(pep->ep_dst, dst_addr, 6);
							memcpy(pep->ep_src, src_addr, 6);
							pep->ep_type = type;
							pep->ep_len = packet_length;

							_DI = FP_OFF(p);
							_ES = FP_SEG(p);
							_CX = packet_length;
							_DX = WINDOW_PORT;
							asm	cld

more:;
#ifdef	SLOWWAY
							asm	in	al,dx
							asm	stosb
							asm	loop	more
#else
							__emit__(0xf3,0x6c);
#endif
							ni_in(my_pni, pep, packet_length);
						} /* end if B */
						else {
							my_pni->ni_idiscard++;
							rc = 1;
						}
					} /* end if notme */
				} /* end packet length odd */
				else {
					my_pni->ni_ierrors++;
				}
			}	/* end if packet status not zero */
			else
				KodiakNullStatus++;
		} /* end if next packet not zero */
#if	NOTNEEDED
/*		SETB(config2, AUTO_UPDATE_REA);
		OUTCONFIG2; */
#endif
		OUTPW(REA_PTR_PORT, (current_receive_ptr >> 8));
		CLRB(command, FIFO_READ);
		SETB(command, FIFO_WRITE);
		ISSUECOMMAND(0);
		OUTPW(DMA_ADDR_PORT, current_receive_ptr);
		if(!(next_packet_ptr & 0xff))
			break;
	}	/* while !resetme */


	if(resetme) {
reset:;
		Restart_Receiver();
	}
	return(rc);
}

STATIC	void
Restart_Receiver()
{
	ISSUECOMMAND(SET_RX_OFF|RX_INT_ACK);
	current_receive_ptr = RX_BUFFER_START;
	OUTPW(RX_PTR_PORT, current_receive_ptr);
	OUTPW(REA_PTR_PORT, (current_receive_ptr >> 8));
#if	NOTNEEDED
/*	SETB(config2, AUTO_UPDATE_REA);
	OUTCONFIG2; */
#endif
	OUTPW(DMA_ADDR_PORT, current_receive_ptr);
	SETB(command, SET_RX_ON);
	KodiakRestarts++;
	ISSUECOMMAND(0);

	if(!(INPW(STATUS_PORT) & RX_ON))
		Full_Restart();
}

STATIC	void
Full_Restart()
{
	int	x,y;

	KodiakFullRestarts++;
	command = config1 = config2 = 0;

	SETB(config2, RESET);		/* tell the chip to reset itself */
	OUTCONFIG2;
	SDELAY(100);
	CLRB(config2, RESET);
	OUTCONFIG2;

	/* enable all error bits */
	SETB(config2, ALL_ERRORS/*|AUTO_UPDATE_REA*/);
	OUTCONFIG2;


	/* set basic commands */
	SETB(command, TX_INT_EN|RX_INT_EN|FIFO_WRITE);
	OUTCOMMAND;

	/* set the ethernet address */
	OUTPW(CONFIG_1_PORT, STATION_0_SEL);

	/* now write the ethernet address in slot 0 */
	for(x = 0; x < 6; x++)
		outportb(WINDOW_PORT, my_pni->ni_hwa.ha_addr[x]);

	/* set transmit end area */
	OUTPW(CONFIG_1_PORT, TEA_SEL);
	outportb(WINDOW_PORT, ((RX_BUFFER_START -1) >> 8));

	/* enable station address, etc */
	config1 = 0;
	SETB(config1, BUFFER_MEM_SEL|MATCH_ONLY|MATCH_BROAD|STATION_0_EN);
	OUTCONFIG1;


	/* set receive end ptr */
	outportb(REA_PTR_PORT, 0xff);

	/* set xmit ptr reg */
	OUTPW(TX_PTR_PORT, TX_BUFFER_START);

	/* set receive buffer ptr */
	OUTPW(RX_PTR_PORT, RX_BUFFER_START);
	current_receive_ptr = RX_BUFFER_START;

	/* select buffer memory  for window port */
	SETB(command, SET_RX_ON);
	OUTCOMMAND;

#ifndef	MS
	/* enable pic */
#ifndef	XDEBUG
	x = inportb(0x21);	/* get mask reg */
	y = 1 << (intvect - 8);
	x &= ~y;	/* clear my value */
	outportb(0x21,x);
#endif
#endif
}

STATIC void
Transmit_Packet()
{
	Buffer	*B;
	struct	ep	*pep;
	unsigned	packet_address = 0;
	register	int		x, lim;
	unsigned	char		*p;
	unsigned	wport = WINDOW_PORT;

	if(!my_pni->ni_outq.queue_count) {
		xmit_is_active = 0;
		return;
	}

	SetFifoWrite(packet_address);

	while(B = (Buffer *) QueuePHead(&my_pni->ni_outq)) {
		unsigned next_packet_address;
		unsigned int	header_stat, packet_stat;

		pep = (struct ep *) B->data;

		if(pep->ep_len < 46)
			pep->ep_len = 46;

		next_packet_address = pep->ep_len + 14 + 4 + packet_address;
		packet_address = next_packet_address;
		header_stat = 0x80|DATA_FOLLOWS;
		packet_stat = 0;
#ifdef	CHAIN
		if(my_pni->ni_outq.queue_count)
			header_stat |= CHAIN_CONTINUE;
		else  {
#else
		{
#endif
			header_stat |= _16_COLL_INT_EN|XMIT_OK_INT_EN;
		}
			/* int on last one only */

		outportb(WINDOW_PORT, next_packet_address >> 8);
		outportb(WINDOW_PORT, next_packet_address & 0xff);
		outportb(WINDOW_PORT, header_stat);
		outportb(WINDOW_PORT, packet_stat);
		for(x=0; x < 6; x++)
			outportb(WINDOW_PORT,pep->ep_dst[x]);
		for(x=0; x < 6; x++)
			outportb(WINDOW_PORT,my_pni->ni_hwa.ha_addr[x]);
		OUTPW(WINDOW_PORT, ntohs(pep->ep_type));
#ifndef	NOASM
		_CX = pep->ep_len;
		_SI = FP_OFF(pep->ep_data);
		asm	push	ds
		_DX = WINDOW_PORT;
		_DS = FP_SEG(pep->ep_data);
		asm	cld
more:;
#ifdef	SLOWWAY
		asm	lodsb
		asm	out	dx, al
		asm	loop	more
#else
		__emit__(0xf3, 0x6e);
#endif
		asm	pop	ds
#else
		p = pep->ep_data;
		x = pep->ep_len;
		while(x--)
			outportb(wport, *p++);
#endif
		BufferFree(B);
#ifndef	CHAIN
		for(x=0; x < 4; x++)
			outportb(wport, 0);
		break;
#endif
	} /* end while */
	xmit_is_active = 1;
	OUTPW(TX_PTR_PORT, 0);
	ISSUECOMMAND(TX_ON);
}

#ifndef	MS
STATIC void
interrupt
#else
STATIC void
#endif
Kodiak_Int(__CPPARGS)
{

	register	unsigned	stop;

	unsigned	status;
	int		worked;

	KodiakInts++;
	if(recursive++) {
		recursive--;
#ifdef	MS
		goto exitme;
#else
	outportb(0x20, 0x20);		/* clear eoi */
#endif
		return;
	}

#ifndef	MS
	theirss = _SS;
	theirsp = _SP;
	stop    = FP_OFF(mystack) + STACKSIZE - 2;
	_SS = _DS;
	_SP = stop - 8;
	asm	push	bp
	_BP = stop;
#endif

	CLRB(command, TX_INT_EN|RX_INT_EN);
	ISSUECOMMAND(0);

	worked = 1;
	while(worked) {
		worked = 0;
		status = INPW(STATUS_PORT);
		if(status & RX_INT) {	/* receive data */
			int rc = Receive_Packet();
			ISSUECOMMAND(RX_INT_ACK);
/*			if(rc)
				break; */
			worked = 1;
		}
		if(status & TX_INT) {	/* transmit int */
			ISSUECOMMAND(TX_INT_ACK);
			Transmit_Packet();
			worked = 1;
		}
	}	/* end while 1 */
	SETB(command, TX_INT_EN|RX_INT_EN);
	ISSUECOMMAND(0);
#ifndef	MS
	asm	pop	bp
	_SS = theirss;
	_SP = theirsp;
#endif
	recursive--;
#ifdef	MS
exitme:;
	asm	mov	sp, bp
	asm	pop	cx		/* make up for push bp and push si */
	FINT();
	RETRBI();
#else
	outportb(0x20, 0x20);		/* clear eoi */
#endif
	return;

}

int
kodiak_poll()
{
	register	unsigned	stop;

	unsigned	status;
	int		worked;

	KodiakInts++;

	CLRB(command, TX_INT_EN|RX_INT_EN);
	ISSUECOMMAND(0);

	worked = 1;
	while(worked) {
		worked = 0;
		status = INPW(STATUS_PORT);
		if(status & RX_INT) {	/* receive data */
			int rc = Receive_Packet();
			ISSUECOMMAND(RX_INT_ACK);
/*			if(rc)
				break; */
			worked = 1;
		}
		if(status & TX_INT) {	/* transmit int */
			ISSUECOMMAND(TX_INT_ACK);
			Transmit_Packet();
			worked = 1;
		}
	}	/* end while 1 */
	SETB(command, TX_INT_EN|RX_INT_EN);
	ISSUECOMMAND(0);

}

int
kodiak_init(struct netif *pni, unsigned arg1, unsigned arg2, unsigned arg3)
{
	/* arg1 == int, arg2 = base ioaddr */
	int	x, y;
	unsigned	char	csum;

#ifndef	MS
	disable();
	reg_base  = arg2;
	intvect = arg1 + 0x8;	/* convert hardware to software vector */
	oldint = getvect(intvect);

	setvect(intvect, Kodiak_Int);
#else
	RegisterBank   far	*RB = &(IDA_PTR->rb.Register[SEEQ_VECTOR]);

	disable();
	reg_base		= 0;

	RB->r_cs		= _CS;
	RB->r_vector_ip		= FP_OFF(Kodiak_Int);
	RB->r_ds		= _DS;
	RB->r_ss		= _SS;
	RB->r_sp		= FP_OFF(&mystack[STACKSIZE/2-1]);
	RB->r_es		= _DS;
	RB->r_bp		= RB->r_sp;
	RB->r_si		= 0;
	RB->r_di		= 0;
#ifdef	NOTNEEDED
	INT_PTR[SEEQ_INT] 	= Kodiak_Int;	/* not needed */
#endif
	SFR_PTR[SEEQ_EXIC] 	= SEEQ_VECTOR|0x10;
					/* network priority w/ bank switch */
#endif
	SETB(config2, RESET);		/* tell the chip to reset itself */
	OUTCONFIG2;
	SDELAY(100);
	CLRB(config2, RESET);
	OUTCONFIG2;
#ifndef	MS
	ISSUECOMMAND(FIFO_WRITE);
	SETB(config1, PROM_SEL);
	OUTCONFIG1;
	CLRB(config1, PROM_SEL);

	SETB(config2, KILL_WATCHDOG);
	OUTCONFIG2;
	CLRB(config2, KILL_WATCHDOG);

	/* read prom */
	csum = 0;
	for(x = 0; x < PROMSIZE; x++) {
		unsigned char c = inportb(WINDOW_PORT);
		prombytes[x] = c;
		csum += c;
	}

	if(csum || prombytes[0] != 0xaa) {	/* crc error */
		return(-1);
	}
#endif
	my_pni = pni;
	pni->ni_hwtype = 1;		/* my type */
	/* set up  hwa */
#ifndef	MS
	pni->ni_hwa.ha_addr[0] = 0x00;
	pni->ni_hwa.ha_addr[1] = 0x80;
	pni->ni_hwa.ha_addr[2] = 0x1b;
	for(x=0; x < 3; x++)
		pni->ni_hwa.ha_addr[x+3] = prombytes[x+1];
#else
	pni->ni_hwa.ha_addr[0] = 0x00;
	pni->ni_hwa.ha_addr[1] = 0x40;
	pni->ni_hwa.ha_addr[2] = 0xa4;
	pni->ni_hwa.ha_addr[3] = 0x0f;
	pni->ni_hwa.ha_addr[4] = 0x42;
	pni->ni_hwa.ha_addr[5] = 0xa0;
#endif

	pni->ni_hwa.ha_len = 6;
	for(x=0; x < 6; x++)
		pni->ni_hwb.ha_addr[x]  = 0xff;
	pni->ni_hwb.ha_len = 6;

	Full_Restart();
	KodiakFullRestarts--;

	enable();
	return(0);

}

int
kodiak_send(struct netif *pni, struct ep *pep, int len)
{
	unsigned flags = _FLAGS;

	if(!pep)
		return(-1);
	if(my_pni->ni_outq.queue_count > NI_INQSZ) {
		KodiakOutDiscards++;
		BufferFreeData(pep);
		return(-2);
	}
	pep->ep_len = len;
	disable();
	QueueInsert(&my_pni->ni_outq, DataToQE(pep));
	if(!(INPW(STATUS_PORT) & TX_ON))
		Transmit_Packet();	/* feh ? */
	if(flags & 512)
		enable();
	return(0);
}

int
kodiak_close(struct netif *pni)
{
	int	y;

	disable();
#ifdef	MS
	SFR_PTR[SEEQ_EXIC] = 0x46;
#else
	y = 1 << (intvect - 8);
	y |= inportb(0x21);
	outportb(0x21, y);		/* mask my int */

	OUTPW(COMMAND_PORT, (SET_DMA_OFF|SET_RX_OFF|SET_TX_OFF));
	setvect(intvect, oldint);
#endif
	enable();
	return(0);

}


int
kodiak_watchdog(struct netif *pni, int count)
{
	unsigned flags = _FLAGS;
static	int	xmit_count;

	disable();
	if(!recursive) {
		if(xmit_is_active) {
			if(++xmit_count > 3) {
				Transmit_Packet();
				xmit_count = 0;
			}
		}
		if(!(INPW(STATUS_PORT) & RX_ON)) {
			Full_Restart();
			KodiakWatchdogRestarts++;
		}
#if	VESTIGIAL
		if(INPW(STATUS_PORT) & RX_INT) {	/* int but we're here? */
			/* reset pic */
			unsigned x, y;


			x = inportb(0x21);	/* get mask reg */
			y = 1 << (intvect - 8);
			x &= ~y;	/* clear my value */
			outportb(0x21,x);

			KodiakPicResets++;
		}
#endif
	}
	if(flags & 512)
		enable();
	return(0);
}

/* alarm.c */
#define	STATIC
#include	<stdlib.h>
#include	<dos.h>


#include	"nettypes.h"

#include	"task.h"
#include	"alarm.h"

#ifdef	MS
#include	"v25.h"
#include	"ip.h"
#include	"nvram.h"
#endif


#define	ALSTACKSIZE	256

STATIC	struct _alarm _alarms[MAX_ALARMS];
STATIC	void interrupt (*old_timer_int)();
STATIC	char	alarm_stack[ALSTACKSIZE];
STATIC	int	al_recursive;
STATIC	unsigned	atheirsp, atheirss;
STATIC	long            first_alarm;
STATIC	long		tickcount;

static	long lastticks;
static	long	basetime;
int	dostatuspage;
int	rebootcount;

void
Settime(long t)
{
	basetime = t - Ticks()/TICKS_PER_SEC;

}

unsigned long
Ticks100()
{

	long	ticks = (Ticks() * 100/TICKS_PER_SEC);
	return(ticks);
}

unsigned long
Ticks()
{
#ifdef	MS
	return(tickcount);
#else
static	unsigned lasticks;
static	long	 baseticks;
	unsigned value = peek(0x40, 0x6c);

	if(value < lasticks) {
		baseticks += 0x10000;
	}
	lasticks = value;
	return((long) baseticks + value);
#endif
}

unsigned long
Time(long *t)
{
	long	ticks = Ticks();

	if(ticks < lastticks)
		basetime += 86400;
	lastticks = ticks;
	ticks = ticks/TICKS_PER_SEC + basetime;
	if(t)
		*t = ticks;
	return(ticks);

}

long
time(long *t)
{
	return(Time(t));
}

#ifndef	MS
void interrupt
#else
void
#endif
alarm_int()
{
	register unsigned stop;

	long	now;
	int	x;
	Alarm	*A;
#ifdef	MS
	tickcount++;
#else
	(*old_timer_int)();
#endif
	if(!first_alarm) {
#ifdef	MS
		goto exitme;
#endif
		return;
}
	if(al_recursive++) {
		al_recursive--;
#ifdef	MS
		goto exitme;
#endif
		return;
	}
#ifndef	MS
	atheirss = _SS;
	atheirsp = _SP;
	stop    = FP_OFF(alarm_stack) + ALSTACKSIZE - 2;
	_SS = _DS;
	_SP = stop - 12;
	asm	push	bp
	_BP = stop;
#endif
#ifdef	MS
	if(!(SFR_PTR[SFR_PT] & 2)) {		/* switch is down */
		rebootcount++;
		if(rebootcount > 3*TICKS_PER_SEC) {	/* do a reboot */
			disable();
			SFR_PTR[SFR_P2] |= LED_ON_MASK;
			while(!(SFR_PTR[SFR_PT] & 2));
			CallNVRAM(NVF_MONITORCMD << 8, 0, 0, "reboot", NULL, NULL);
		}
		if(rebootcount & 1)
			SFR_PTR[SFR_P2] &= LED_OFF_MASK;
		else
			SFR_PTR[SFR_P2] |= LED_ON_MASK;
	}
	else {
		if(rebootcount) {
			dostatuspage = 1;
			rebootcount = 0;
		}
	}
#endif
	now = Ticks();
	if(now >= first_alarm) {
		long	 nextfirst = 0x7fffffff;
		for(x=0, A=_alarms; x < MAX_ALARMS; x++, A++) {
			if(A->al_state == ALSTATE_INUSE) {
				if(now >= A->al_ticks) {
					if(now - A->al_ticks > TICKS_PER_SEC) {
						now = Ticks();
					}
					TaskReady(A->al_task, A->al_rcode, TASK_NORESCHEDULE);
					A->al_state = ALSTATE_HOLD;
					now = Ticks();
				}
				else if(A->al_ticks < nextfirst)
					nextfirst = A->al_ticks;
			}
		}
		if(nextfirst != 0x7fffffff)
			first_alarm = nextfirst;
		else
			first_alarm = 0;
	} /* end if now */
#ifndef	MS
	asm	pop	bp
	_SS = atheirss;
	_SP = atheirsp;
#endif
	al_recursive--;
#ifdef	MS
exitme:;
	asm	mov	sp, bp
	asm	pop	bp
	FINT();
	RETRBI();
#endif
}

void
alarm_init()
{
#ifdef	MS
	RegisterBank   far	*RB = &(IDA_PTR->rb.Register[TIMER_VECTOR]);

	disable();

	RB->r_cs		= _CS;
	RB->r_vector_ip		= FP_OFF(alarm_int);
	RB->r_ds		= _DS;
	RB->r_ss		= FP_SEG(alarm_stack);
	RB->r_sp		= FP_OFF(&alarm_stack[ALSTACKSIZE-1]);
	RB->r_es		= _DS;
	RB->r_bp		= RB->r_sp;
	RB->r_si		= 0;
	RB->r_di		= 0;
#ifdef	NOTNEEDED
	INT_PTR[INT_TM0] 	= alarm_int;
#endif
	SFR_PTR[SFR_TMIC0]	= 0x10|TIMER_VECTOR;	/* low priority, vector more */
	SFR_PTR[SFR_TMC0]	|= 0xa0;		/* start timer */
	enable();
#else
	old_timer_int = getvect(0x1c);
	setvect(0x1c, alarm_int);
#endif
}



void
alarm_close()
{
#ifdef	MS
	SFR_PTR[SFR_TMIC0] = 0x47;	/* disable ints */
	SFR_PTR[SFR_TMC0]  = 0x58;

#else
	if(old_timer_int)
		setvect(0x1c, old_timer_int);
#endif
}


int
alarm_sleep(struct timeval *when, int rc)
{
	unsigned flags = _FLAGS;
	int	x;
	long	now;
	Task	*T;
      	struct _alarm *A;

	disable();
	now = Ticks();
	T = Task_Current_Task;

	for(x=0, A= _alarms; x < MAX_ALARMS; x++,A++) {
		if(A->al_state == ALSTATE_NONE) {
			int	rc;

			long	offset = ((long) when->tv_sec) * TICKS_PER_SEC;

			offset += when->tv_usec/USEC_PER_TICK;

			if(!offset)
				offset = 1;
			offset += now;
			A->al_ticks = offset;
			if(offset < first_alarm || !first_alarm)
				first_alarm =offset;
			A->al_rcode = rc;
			A->al_task = T;
			A->al_state = ALSTATE_INUSE;
			if(T->tsk_state == TASK_STATE_RUNNING)
				T->tsk_state = TASK_STATE_SLEEP;
			rc = TaskReschedule();
			A->al_state = ALSTATE_NONE;	/* just in case someone else woke us up */
			/* we just returned from either a wakeup, or a signal */
			if(flags & 512)
				enable();
			return(rc);
		}
	}
	if(flags & 512)
		enable();
	return(SYSERR);
}

int
alarm_reset(int al)
{
	unsigned flags = _FLAGS;

	if(al < 0 || al >= MAX_ALARMS)
		return(SYSERR);
	disable();
	if(_alarms[al].al_state == ALSTATE_INUSE)
		_alarms[al].al_state = ALSTATE_NONE;
	if(flags & 512)
		enable();
	return(OK);
}

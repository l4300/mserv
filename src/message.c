#include <alloc.h>
#include <stdlib.h>
#include <mem.h>

#include "message.h"

#include	<memcheck.h>

MessageBox	*
MessageBoxAlloc(int maxcount)
{
	MessageBox	*MB;

	MB = calloc(1, sizeof(MessageBox));
	if(!MB)
		return(NULL);
	MessageBoxInitialize(MB, maxcount);
	return(MB);
}

int
MessageBoxInitialize(MessageBox *MB, int maxcount)
{
	if(!MB)
		return(MBOX_RESULT_BADMBOX);
	memset(MB, 0, sizeof(MessageBox));
	MB->mesg_maxMessages = maxcount;
	MB->mesg_MQueue.queue_flags |= QUEUE_FLAGS_NOTSORTED;
	Semaphore_Initialize(&MB->mesg_readSem, 0);	/* count of messages in queue now */
	Semaphore_Initialize(&MB->mesg_writeSem, maxcount);	/* written messages */
	return(MBOX_RESULT_OK);
}

int
MessageBoxSend(MessageBox *MB, Queue_Element *M)
{
	if(!MB)
		return(MBOX_RESULT_BADMBOX);
	if(Semaphore_Wait(&MB->mesg_writeSem) == SEM_RESULT_BADSEM) {
		return(MBOX_RESULT_BADMBOX);
	}
	/* otherwise, we can write now */
	QueueInsert(&MB->mesg_MQueue, M);
	if(Semaphore_Signal(&MB->mesg_readSem) == SEM_RESULT_BADSEM) {
		return(MBOX_RESULT_BADMBOX);
	}
	return(MBOX_RESULT_OK);
}

int
MessageBoxISend(MessageBox *MB, Queue_Element *M)	/* send from interrupt */
{
	if(!MB)
		return(MBOX_RESULT_BADMBOX);
	if(MB->mesg_writeSem.sem_count < 1)
		return(MBOX_RESULT_EBLOCK);
	if(Semaphore_Wait(&MB->mesg_writeSem) == SEM_RESULT_BADSEM) {
		return(MBOX_RESULT_BADMBOX);
	}
	/* otherwise, we can write now */
	QueueInsert(&MB->mesg_MQueue, M);
	if(Semaphore_ISignal(&MB->mesg_readSem) == SEM_RESULT_BADSEM) {
		return(MBOX_RESULT_OK);	/* oh well */
	}
	return(MBOX_RESULT_OK);
}

Queue_Element	*MessageBoxReceive(MessageBox *MB)
{
	Queue_Element	*QE;

	if(!MB)
		return(NULL);
	if(Semaphore_Wait(&MB->mesg_readSem) == SEM_RESULT_BADSEM) {
		return(NULL);
	}
	QE = QueuePHead(&MB->mesg_MQueue);
	Semaphore_Signal(&MB->mesg_writeSem);
	return(QE);
}

int
MessageBoxCount(MessageBox *MB)
{
	if(!MB)
		return(MBOX_RESULT_BADMBOX);
	return(MB->mesg_MQueue.queue_count);
}

int
MessageBoxFree(MessageBox *MB)
{
	if(!MB)
		return(MBOX_RESULT_BADMBOX);
	return(MB->mesg_maxMessages - MessageBoxCount(MB));
}

int
MessageBoxRelease(MessageBox *MB)
{
	if(!MB)
		return(MBOX_RESULT_BADMBOX);
	return(0);
}

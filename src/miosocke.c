/* iosocket.c -- serial and parallel IO routines for sockets */
#include	"network.h"
#include	"sys/errno.h"
#include	"sys/socket.h"
#include	"iosocket.h"
#include	"task.h"

#include	<dos.h>

#ifdef	MS
#include	"v25.h"
#endif

#define	SERIAL_PORT	0x3f8

struct	_io_socket	IO_Sockets[MAX_IO_SOCKETS];

int	serial_kick(struct _io_socket *s, char *data, int len);
int	parallel_kick(struct _io_socket *s, char *data, int len);

int	iosocket_proto(struct _socket *so, int pr, void *data);
void interrupt	serial_interrupt();
#ifdef	MS
void interrupt	serial_rinterrupt();
#endif

static	Task	*partask;
Task	*Stattsk;

int	iosocket_par_task(void *Parent, char *tname, int argc, char **argv);
extern	int	quit;

int
iosocket_stacksize()
{
	return(IOSOCKET_PAR_STACK);
}

int
iosocket_init(void *stack, int stacksize)
{
	int	x, y;

	for(x=0; x < MAX_IO_SOCKETS; x++) {
		Semaphore_Initialize(&IO_Sockets[x].io_outsemaphore, 1);
		Semaphore_Initialize(&IO_Sockets[x].io_semaphore, 1);
	}
	IO_Sockets[IO_TYPE_SERIAL].io_kickstart = serial_kick;
	IO_Sockets[IO_TYPE_PARR].io_kickstart = parallel_kick;
	IO_Sockets[IO_TYPE_SERIAL].io_postprocess = IO_POST_NLCR;

	soregister(AF_IOSOCKET, IOSOCKET_PARALLEL,  0, iosocket_proto);
	soregister(AF_IOSOCKET, IOSOCKET_SERIAL,  0, iosocket_proto);

#ifndef	MS
	setvect(0xc, serial_interrupt);
	outportb(SERIAL_PORT+3, 3);	/* 8 bits */
	outportb(SERIAL_PORT+1,3);	/* enable all ints */
	outportb(SERIAL_PORT+4,3+8);

	x = inportb(0x21);	/* get mask reg */
	y = 1 << (4);
	x &= ~y;	/* clear my value */
	outportb(0x21,x);
#else
	INT_PTR[INT_SR0] = serial_rinterrupt;
	INT_PTR[INT_ST0] = serial_interrupt;

	SFR_PTR[SFR_SCM0] = 0xc9;	/* 8n1 */
	SFR_PTR[SFR_SCC0] = 2;
	SFR_PTR[SFR_BRG0] = 130;	/* 9600 ? */
	SFR_PTR[SFR_SRIC0] = SERIAL_VECTOR;	/* level 5 */
	SFR_PTR[SFR_STIC0] = SERIAL_VECTOR;

	SFR_PTR[SFR_P2] = 0x13;	/* assert strobe high, auto paper high, DTR high */
	for(x=0; x < 100; x++);
	SFR_PTR[SFR_P2] |= 4;	/* init low */

#endif
	TaskAlloc(iosocket_par_task,stack, stacksize,IOSOCKET_PAR_PRIORITY, IOSOCKET_PAR_TASKNAME, 0, NULL);
	return(0);
}

int
iosocket_proto(struct _socket *so, int pr, void *data)
{
	int	rc = OK;
	int	len;
	struct	sockaddr_io	*sin = (struct sockaddr_io *) data;
	struct	_sendto		*sendto = (struct _sendto *) data;
	struct	_io_socket	*ios =(struct _io_socket *) so->so_pcb;
	struct	_rwbuffer	*rw = (struct _rwbuffer *) data;
	Buffer	*B;

	switch(pr) {
		case	PR_OPEN :
			if(so->so_family != AF_IOSOCKET) {
bomb:;
				SETERROR(EPROTO);
				rc = SYSERR;
				break;
			}
			switch(so->so_protocol) {
				case IOSOCKET_PARALLEL :
					so->so_pcb = &IO_Sockets[0];
					break;

				case IOSOCKET_SERIAL :
					so->so_pcb = &IO_Sockets[1];
					break;
				default:
					goto bomb;
			}	/* end switch protocol */
			break;
	case	PR_BIND :
		if(sin->sin_family != AF_IOSOCKET)
			goto bomb;
		sin->sin_port = FP_OFF(so) & 0xffff;

		break;
	case	PR_CONNECT :
		if(!sin->sin_family && ios->io_socket == so) { /* release */
release:;
			ios->io_socket = NULL;
			Semaphore_Signal(&ios->io_outsemaphore);
			so->so_pcb = NULL;
			break;
		}
		if(sin->sin_family != AF_IOSOCKET || !ios)
			goto bomb;
		/* block and wait for exclusive access */
		Semaphore_Wait(&ios->io_outsemaphore);
		ios->io_socket = so;
		ios->io_mode = sin->sin_mode;
		if(sin->sin_mode & IOSINMODE_READ) {
			ios->io_inptr = &ios->io_outbuffer[IO_BUFFER_SIZE/2];
			ios->io_incount = 0;
		}
		ios->io_outcount = 0;
		so->so_selectflags |= SOSEL_WRITE;
		break;
	case	PR_READ  :
		if(ios->io_socket != so)
			goto bomb;
		if(!(ios->io_mode & IOSINMODE_READ)) {
			SETERROR(EBADF);
			rc = SYSERR;
			break;
		}
		if(ios->io_incount) {
			char	*iptr = &ios->io_outbuffer[IO_BUFFER_SIZE/2];

			int cnt = sendto->s_len;
			if(cnt > ios->io_incount)
				cnt = ios->io_incount;
			memcpy(sendto->s_msg,iptr, cnt);
			ios->io_incount -= cnt;
			if(ios->io_incount)
				memcpy(iptr, iptr+cnt, ios->io_incount);
			ios->io_inptr -= cnt;
			if(!ios->io_incount)
				so->so_selectflags &= ~SOSEL_READ;
			rc = cnt;
		}
		break;
	case	PR_WRITE :
		/* socket controls who can write and when */
		if(!ios->io_kickstart)
			goto bomb;
		rc = ios->io_kickstart(ios, sendto->s_msg, sendto->s_len);
		break;
	case 	PR_WRITEBUF :
		if(ios != &IO_Sockets[IOSOCKET_PARALLEL]) {
			Semaphore_Wait(&ios->io_semaphore);
			ios->io_buffer = rw->rw_buffer;
			ios->io_outptr = rw->rw_data;
			ios->io_outcount = rw->rw_len;
			serial_kick(ios, rw->rw_data, rw->rw_len);
			break;
		}
		if(!partask)
			goto bomb;
		B = (Buffer *) rw->rw_buffer;
		B->QE.queue_object = rw->rw_data;
		B->QE.queue_size = rw->rw_len;
		if(partask->Messages.mesg_MQueue.queue_count < partask->Messages.mesg_maxMessages) {
			if(MessageBoxISendBuf(&partask->Messages, B) == MBOX_RESULT_OK)
				break;
		}
		MessageBoxSendBuf(&partask->Messages, B);
		break;
	case	PR_CLOSE :
		if(ios->io_socket == so)  /* release */
			goto release;
		so->so_pcb = NULL;
		break;
	default :;
		goto bomb;

	}	/* end switch */
	return(rc);
}

int
parallel_kick(struct _io_socket *s, char *data, int len)
{
	Buffer	*B;

	if(!partask)
		return(-1);
	while(!(B = BufferAlloc(len))) {
		xdelay(1);
	}
	memcpy(B->data, data, len);
	B->QE.queue_object = B->data;
	B->QE.queue_size = len;
	MessageBoxSendBuf(&partask->Messages, B);
	return(0);
}

int
serial_kick(struct _io_socket *s, char *data, int len)
{

	int	c;
	int	bufflimit = (s->io_mode & IOSINMODE_READ) ? IO_BUFFER_SIZE/2 : IO_BUFFER_SIZE;

	if(!s->io_buffer) {
		if(len < 1)
			return(0);
		if(len > bufflimit)
			len = bufflimit;
		memcpy(s->io_outbuffer, data, len);
		s->io_outptr = s->io_outbuffer;
		s->io_outcount = len;
	}

	s->io_socket->so_selectflags &= ~SOSEL_WRITE;
	s->io_outcount--;
	c = *s->io_outptr++;
#ifdef	MS
	SFR_PTR[SFR_TXB0] = c;
#else
	outportb(SERIAL_PORT, c);
#endif
	return(len);
}

void interrupt
serial_interrupt()
{
	struct	_io_socket	*s = &IO_Sockets[IO_TYPE_SERIAL];
#ifndef	MS
	int	iir = inportb(SERIAL_PORT+2);
	int	lsr = inportb(SERIAL_PORT+5);

	if(lsr & 1) {
		int c = inportb(SERIAL_PORT);
		if(s->io_mode & IOSINMODE_READ && s->io_socket) {
			if(s->io_incount < IO_BUFFER_SIZE / 2) {
				*s->io_inptr++ = c;
				s->io_incount++;
				if(!(s->io_socket->so_selectflags & SOSEL_READ))
					sowakeup(s->io_socket, SOSEL_READ);
			}
		}
	}

	if(iir & 1 || lsr & 32 && s->io_socket)	{	/* transmitter empty */
		if(s->io_outcount) {
			s->io_outcount--;
			outportb(SERIAL_PORT, *s->io_outptr++);
		}
		else {
			sowakeup(s->io_socket, SOSEL_WRITE);
		}
	}
	outportb(0x20, 0x20);
#else

	if((s->io_postprocess & IO_POST_NLCR) && *(s->io_outptr-1) == '\n') {
		*(s->io_outptr-1) = 0;
		SFR_PTR[SFR_TXB0] = '\r';
		FINT();
		return;
	}
	if(s->io_outcount) {
		s->io_outcount--;
		SFR_PTR[SFR_TXB0] = *s->io_outptr++;
	}
	else {
		if(s->io_buffer) {	/* we were in buffer mode */
			BufferFree(s->io_buffer);
			s->io_buffer = NULL;
			Semaphore_ISignal(&s->io_semaphore);
		}
		if(s->io_socket)
			sowakeup(s->io_socket, SOSEL_WRITE);
	}

	FINT();
#endif
}

#ifdef	MS
void interrupt
serial_rinterrupt()
{
	struct	_io_socket	*s = &IO_Sockets[IO_TYPE_SERIAL];

	char c = SFR_PTR[SFR_RXB0];

	switch (c) {

#ifdef	DONT
		case 1 :
			if(Stattsk)
				TaskReady(Stattsk, 0, TASK_NORESCHEDULE);
			break;
		case 3 :
			quit = 1;
			break;
#endif
		case 17 :	/* ^Q */
			SFR_PTR[SFR_STIC0] &= ~0x40;
			if(s->io_outcount)
				SFR_PTR[SFR_STIC0] |= 0x80;	/* kick */
			break;

		case 19 :	/* ^S */
			SFR_PTR[SFR_STIC0] |= 0x40;	/* disable ints */
			break;
		default :
		if(s->io_mode & IOSINMODE_READ && s->io_socket) {
			if(s->io_incount < IO_BUFFER_SIZE / 2) {
				*s->io_inptr++ = c;
				s->io_incount++;
				if(!(s->io_socket->so_selectflags & SOSEL_READ))
					sowakeup(s->io_socket, SOSEL_READ);
			}
		}
	}	/* end switch */
	FINT();
}
#endif


#pragma argsused
int
iosocket_par_task(void *Parent, char *tname, int argc, char **argv)
{
	unsigned	int	es, outport;

	partask = Task_Current_Task;
	MessageBoxInitialize(&partask->Messages, PAR_QUEUE_SIZE);
	partask->Messages.mesg_MQueue.queue_flags |= QUEUE_FLAGS_NOTSORTED;

	es 		= FP_SEG(&SFR_PTR[SFR_P0]);
	outport 	= FP_OFF(&SFR_PTR[SFR_P0]);

	while(1) {
		Buffer	*B;
		char	*c;
		int 	len;
		int	strobe_on;


		B = (Buffer *) MessageBoxReceive(&partask->Messages);
		if(!B)
			continue;

		c = (char *) B->QE.queue_object;
		len = B->QE.queue_size;
		while(len) {
#ifdef	MS
			strobe_on = SFR_PTR[SFR_P2] & ~1;
			strobe_on |= (strobe_on | 1) << 8;

			_ES	= es;
			_BX 	= outport;
			_SI 	= FP_OFF(c);
			_CX	= len;
			_DX	= strobe_on;
			asm	push	ds
			asm	mov	ax,ds
			asm	push	es
			asm	pop	ds		/* DS now has SFR SEG */
			asm	mov	es, ax		/* ES has DS */
#else
			while(len) {
#endif
#ifdef	MS
top:;
				asm	test 	byte ptr [bx+8], 8
				asm	jnz	busy

				asm	mov	al, es:[si]
				asm	mov	[bx], al
				asm	mov	[bx+16], dl
				asm	inc	si
#else
				c++;
#endif
#ifdef	MS
				asm	mov	[bx+16], dh
				asm	loop	top
busy:;
				asm	pop	ds
				len = _CX;
				c   = (char *) MK_FP(es, _SI);
#else
				len--;
			}	/* end while len #2 */
#endif
			if(len)
				TaskReschedule();	/* let someone else run */
		} /* end while len #1 */
		BufferFree(B);
		TaskReschedule();	/* don't monopolize */
	}
}

#define	STATIC
/* #define	INTRESTART */

#define	_MKODIAK	1

#include	"network.h"
#include	"kodiak.h"

#include	"mserv.h"
#include	"koddefs.h"
#include	<dos.h>

#include	"v25.h"

#define	USEVECTORMODE	0	// true to use a vector
#define	USERBVECTOR	1	// true to use register bank after vector
#define	WAITFIFO	1	// true to wait for fifo to fill
#define	DORESETONERROR	0	// reset seeq if odd packet length
#define	DISCARDOVERFLOW	0	// true to discard overflow packet
#define	CHECKFORLOOPBACK	0	// true if we should check for loopback packets

#define	SETB(reg,mask)	((reg) |= (mask))
#define	CLRB(reg,mask)	((reg) &= ~(mask))

#define	OUTPW(port, value)	(outportb((port), (value) & 0xff), outportb((port)+1, (value) >> 8))
#define	INPW(port)		((inportb((port)+1) << 8) | inportb(port))

#define	FIFOREAD()	(outportb((COMMAND_PORT)+1,(FIFO_READ >> 8)))
#define	FIFOWRITE()	(outportb((COMMAND_PORT)+1,(FIFO_WRITE >> 8)))

#define	OUTCONFIG2	(OUTPW((CONFIG_2_PORT), config2))
#define	OUTCONFIG1	(OUTPW((CONFIG_1_PORT), config1))

#define	OUTCOMMAND	(OUTPW((COMMAND_PORT), command))
#define	OUTWINDOW(v)	(OUTPW((WINDOW_PORT), v))

#define	ISSUECOMMAND(c)	(SETB(command, (c)), OUTCOMMAND, CLRB(command, (c)))
#define	WRITECOMMAND(c)	(OUTPW((COMMAND_PORT), (c)))

#define	SDELAY(n)	for(x=0; x <  n; x++)

void	interrupt	(*oldint)();
static	unsigned	reg_base;
static	unsigned	command;
static	unsigned	config2;
static	unsigned	config1;
static	unsigned	intvect;
static	unsigned	current_receive_ptr;
static	int		xmit_is_active;

#define	STACKSIZE	512
STATIC	unsigned	mystack[STACKSIZE/2];
STATIC	unsigned	theirss, theirsp, stop;

STATIC	Net_Interface	*my_pni;


STATIC	void	SetFifoWrite(unsigned xaddr);
STATIC	void	Restart_Receiver(), Full_Restart();

long	KodiakRestarts, KodiakChainedPackets, KodiakWatchdogRestarts, KodiakPicResets;
long	KodiakFullRestarts, KodiakDMAloops, KodiakUnChainedPackets;
long	KodiakEndChains, KodiakNullStatus, KodiakInPackets;
long	KodiakMissedFifo, KodiakInts, KodiakOutDiscards, KodiakCollisions;

static	int	recursive;

unsigned
dummy(unsigned x)
{
	return(x);
}

STATIC	void
SetFifoRead(unsigned xaddr)
{
	unsigned	cx = 0;

	SetFifoWrite(xaddr);

	if(!(INPW(STATUS_PORT) & FIFO_DIR))	// if not read
		FIFOREAD();
	else
		OUTPW(DMA_ADDR_PORT,xaddr);

	while(--cx) {
		if(INPW(STATUS_PORT) & FIFO_FULL)
			break;
	}
	ISSUECOMMAND(WINDOW_INT_ACK);
}

STATIC	void
SetFifoWrite(unsigned xaddr)
{
	if((INPW(STATUS_PORT) & FIFO_DIR)) 	/* current set to read */
		FIFOWRITE();
	else {				/* its currently write, wait for fifo */
		unsigned	cx = 0;
		while(--cx) {
			if(INPW(STATUS_PORT) & FIFO_EMPTY)
				break;
		}
	}
	OUTPW(DMA_ADDR_PORT, xaddr);
}

unsigned int
KGetStatus()
{
	return(INPW(STATUS_PORT));
}

STATIC int
Receive_Packet()
{
	unsigned	int	next_packet_ptr, packet_length;
	register	unsigned   	int	x;
	unsigned 	int	header_status, packet_status, y;
	unsigned	int	buffersize;
	unsigned	int	rc = 0;
	int		worked = 0;
	int		resetme = 0;
				/* avoid having the current packet get overwritten */
	if(!(INPW(STATUS_PORT) & RX_ON))
		goto reset;

readmore:;
	SetFifoRead(current_receive_ptr);
	while(!rc) {

		next_packet_ptr = INPW(WINDOW_PORT);
		if(next_packet_ptr & 0xff00) {
			KodiakInPackets++;
			packet_length = next_packet_ptr - current_receive_ptr - 18;
			if(next_packet_ptr < current_receive_ptr)
				packet_length -= RX_BUFFER_START;

			current_receive_ptr = next_packet_ptr;

			header_status = inportb(WINDOW_PORT);
			packet_status = inportb(WINDOW_PORT);
			if((packet_status & 0xff) && (header_status & DATA_FOLLOWS)) {

				if((packet_length >= 46) && (packet_length <= 1500) &&
				   !(packet_status & (SHORT_FRAME|OVERSIZE|CRC_ERROR))) {	/* if not odd, read it */
					Buffer		*B;

					buffersize = packet_length + EP_HLEN;
					B = BufferAlloc(buffersize);
					if(B) {

						struct ep *pep = (struct ep *) B->data;
						register unsigned char *p = pep->ep_dst;

						pep->ep_len = packet_length;

						_DI = FP_OFF(p);
						_ES = FP_SEG(p);
						_CX = 6;
						_DX = WINDOW_PORT;
						asm	cld
						__emit__(0xf3,0x6c);	// read dest

						asm	add	di,NI_MAXHWA-6
//						p = pep->ep_src;
//						_DI = FP_OFF(p);
						_CX = 6;
						__emit__(0xf3,0x6c);	// read src

//						p = (unsigned char *) &pep->ep_type;
//						_DI = FP_OFF(p);
						asm	add	di,NI_MAXHWA-6
						_CX = 2;
						__emit__(0xf3,0x6c);	// read type

//						p = pep->ep_data;
//						_DI = FP_OFF(p);
						asm	add	di,6
						_CX = packet_length;
						__emit__(0xf3,0x6c);	// read data

						pep->ep_type = ntohs(pep->ep_type);
						ni_in(my_pni, pep, packet_length);
					} /* end if B */
					else {
						my_pni->ni_idiscard++;
#if	!DISCARDOVERFLOW
						break; 	// don't update current pointer
#else
						rc = 1;
#endif
					}
				} /* end packet length odd */
				else {
					my_pni->ni_ierrors++;
#if	DORESETONERROR
					resetme = 1;
#endif
				}
			}	/* end if packet status not zero */
			else {
				KodiakNullStatus++;
#if	DORESETONERROR
				resetme = 1;
#endif
			}
		} /* end if next packet not zero */
			else if(!worked)
				goto reset;	// should never get a recv int with no packet


		if(resetme)
			break;

		worked++;
		SetFifoRead(current_receive_ptr);
		outportb(REA_PTR_PORT, (current_receive_ptr >> 8));

		if(!(next_packet_ptr & 0xff00))
			break;
		KodiakDMAloops++;
	}	/* while !resetme */

	KodiakDMAloops--;

	if(resetme || !(INPW(STATUS_PORT) & RX_ON)) {
reset:;
		Restart_Receiver();
	} else {
		outportb(REA_PTR_PORT, (current_receive_ptr >> 8));
		FIFOWRITE();
	}
	return(rc);
}

STATIC	void
Restart_Receiver()
{
	unsigned int cx = 0;

	ISSUECOMMAND(SET_RX_OFF);
	while(--cx) {
		if(!(INPW(STATUS_PORT) & RX_ON))
			break;
	}	// make sure receiver off before restarting

//	current_receive_ptr = RX_BUFFER_START;
	OUTPW(RX_PTR_PORT, current_receive_ptr);
	outportb(REA_PTR_PORT, (current_receive_ptr >> 8));
	FIFOWRITE();
//	OUTPW(DMA_ADDR_PORT, current_receive_ptr);
//	SETB(command, SET_RX_ON);
	KodiakRestarts++;

	ISSUECOMMAND(SET_RX_ON);

	if(!(INPW(STATUS_PORT) & RX_ON))
		Full_Restart();
}

STATIC	void
Full_Restart()
{
	int	x,y;

	KodiakFullRestarts++;
	command = config1 = config2 = 0;

	SETB(config2, RESET);		/* tell the chip to reset itself */
	OUTCONFIG2;
	SDELAY(100);
	CLRB(config2, RESET);
	OUTCONFIG2;

	/* enable all error bits */
	SETB(config2, ALL_ERRORS|RECV_WHILE_XMIT_OFF);	// was just all_errors ### 11/29/94
	OUTCONFIG2;


	/* set basic commands */
	SETB(command, TX_INT_EN|RX_INT_EN);
	ISSUECOMMAND(FIFO_WRITE);

	/* set the ethernet address */
	OUTPW(CONFIG_1_PORT, STATION_0_SEL);

	/* now write the ethernet address in slot 0 */
	for(x = 0; x < 6; x++)
		outportb(WINDOW_PORT, my_pni->ni_hwa.ha_addr[x]);

	/* set transmit end area */
	OUTPW(CONFIG_1_PORT, TEA_SEL);
	outportb(WINDOW_PORT, ((RX_BUFFER_START -1) >> 8));

	/* enable station address, etc */
	config1 = 0;
	SETB(config1, BUFFER_MEM_SEL|MATCH_ONLY|MATCH_BROAD|STATION_0_EN);
	OUTCONFIG1;


	/* set receive end ptr */
	outportb(REA_PTR_PORT, 0xff);

	/* set xmit ptr reg */
	OUTPW(TX_PTR_PORT, TX_BUFFER_START);

	/* set receive buffer ptr */
	OUTPW(RX_PTR_PORT, RX_BUFFER_START);
	current_receive_ptr = RX_BUFFER_START;

	/* select buffer memory  for window port */
	ISSUECOMMAND(SET_RX_ON);

}

STATIC void
Transmit_Packet()
{
	Buffer	*B;
	struct	ep	*pep;
	unsigned	packet_address = 0;
	register	int		x, lim;
	unsigned	char		*p;
	unsigned	wport = WINDOW_PORT;

	SetFifoRead(packet_address);
	inportb(WINDOW_PORT);	// size high
	inportb(WINDOW_PORT);	// size low
	inportb(WINDOW_PORT);	// xmit cmd
	x = inportb(WINDOW_PORT);	// status

	if(x & _16_COLL_ERROR) {
		KodiakCollisions += 1;
		my_pni->ni_oerrors++;
	}
	if(!my_pni->ni_outq.queue_count) {
		xmit_is_active = 0;
		return;
	}

	SetFifoWrite(packet_address);

	while(B = (Buffer *) QueuePHead(&my_pni->ni_outq)) {
		unsigned next_packet_address;
		unsigned int	header_stat, packet_stat;
		int		isucast = 0;

		pep = (struct ep *) B->data;

		if(pep->ep_len < 46)
			pep->ep_len = 46;

		my_pni->ni_ooctects += pep->ep_len + 14;
		next_packet_address = pep->ep_len + 14 + 4 + packet_address;
		packet_address = next_packet_address;
		header_stat = 0x80|DATA_FOLLOWS;
		packet_stat = 0;
#ifdef	CHAIN
		if(my_pni->ni_outq.queue_count)
			header_stat |= CHAIN_CONTINUE;
		else  {
#else
		{
#endif
			header_stat |= _16_COLL_INT_EN|XMIT_OK_INT_EN;
		}
			/* int on last one only */

		outportb(WINDOW_PORT, next_packet_address >> 8);
		outportb(WINDOW_PORT, next_packet_address & 0xff);
		outportb(WINDOW_PORT, header_stat);
		outportb(WINDOW_PORT, packet_stat);

		for(x=0; x < 6; x++) {
			outportb(WINDOW_PORT,pep->ep_dst[x]);
			isucast |= (pep->ep_dst[x] != 0xff);
		}
		if(isucast)
			my_pni->ni_oucast++;
		else
			my_pni->ni_obcast++;
		for(x=0; x < 6; x++)
			outportb(WINDOW_PORT,my_pni->ni_hwa.ha_addr[x]);
		OUTPW(WINDOW_PORT, ntohs(pep->ep_type));
#ifndef	NOASM
		_CX = pep->ep_len;
		_SI = FP_OFF(pep->ep_data);
		asm	push	ds
		_DX = WINDOW_PORT;
		_DS = FP_SEG(pep->ep_data);
		asm	cld
		__emit__(0xf3, 0x6e);
		asm	pop	ds
#else
		p = pep->ep_data;
		x = pep->ep_len;
		while(x--)
			outportb(wport, *p++);
#endif
		BufferFree(B);
#ifndef	CHAIN
		for(x=0; x < 4; x++)
			outportb(wport, 0);
		break;
#endif
	} /* end while */
	xmit_is_active = 1;
	OUTPW(TX_PTR_PORT, 0);
	ISSUECOMMAND(TX_ON);
}

#if	USEVECTORMODE
STATIC void
interrupt
#else
STATIC void
#endif
Kodiak_Int(__CPPARGS)
{

	register	unsigned	stop;

	unsigned	status;
	int		worked;

	KodiakInts++;
	if(recursive++) {
		recursive--;
		goto exitme;
		return;
	}

#if	USEVECTORMODE
	theirss = _SS;
	theirsp = _SP;
	stop    = FP_OFF(mystack) + STACKSIZE - 2;
	_SS = _DS;
	_SP = stop - 8;
	asm	push	bp
	_BP = stop;
#endif
	CLRB(command, TX_INT_EN|RX_INT_EN);
	WRITECOMMAND(command);

	worked = 1;
	while(worked) {
		worked = 0;
		status = INPW(STATUS_PORT);
		if(status & RX_INT) {	/* receive data */
			int	rc;

			rc = Receive_Packet();
			ISSUECOMMAND(RX_INT_ACK);
/*			if(rc)
				break; */
//			worked = 1;
		}
		if(status & TX_INT) {	/* transmit int */
			ISSUECOMMAND(TX_INT_ACK);
			Transmit_Packet();
//			worked = 1;
		}
	}	/* end while 1 */
	SETB(command, TX_INT_EN|RX_INT_EN);
	WRITECOMMAND(command);

#if	USEVECTORMODE
	asm	pop	bp
	_SS = theirss;
	_SP = theirsp;
#endif
	recursive--;

exitme:;
#if	!USEVECTORMODE
	asm	mov	sp, bp
	asm	pop	cx		/* make up for push bp and push si */
#endif
	FINT();
#if	!USEVECTORMODE
	RETRBI();
#endif
	return;
}

#if	USERBVECTOR
static void
Kodiak_BankSwitch(void)
{
	asm {
		push	bp
		mov	bp,2
		db	0fh
		db	95h
		db	0fdh	/* movspb, 2 */

		db	0fh
		db	2dh
		db	0c5h	/* brkcs */
		nop
		pop	bp
		iret
	}
}
#endif

int
kodiak_init(struct netif *pni, unsigned arg1, unsigned arg2, unsigned arg3)
{
	/* arg1 == int, arg2 = base ioaddr */
	int	x, y;
	unsigned	char	csum;
#if	USERBVECTOR
	RegisterBank   far	*RB = &(IDA_PTR->rb.Register[SEEQ_BANK]);
#else
	RegisterBank   far	*RB = &(IDA_PTR->rb.Register[SEEQ_VECTOR]);
#endif
	disable();
	reg_base		= 0;
#if	!USEVECTORMODE
	RB->r_cs		= _CS;
	RB->r_vector_ip		= FP_OFF(Kodiak_Int);
	RB->r_ds		= _DS;
	RB->r_ss		= _SS;
	RB->r_sp		= FP_OFF(&mystack[STACKSIZE/2-1]);
	RB->r_es		= _DS;
	RB->r_bp		= RB->r_sp;
	RB->r_si		= 0;
	RB->r_di		= 0;
#if	USERBVECTOR
	INT_PTR[SEEQ_INT]	= Kodiak_BankSwitch;
	SFR_PTR[SEEQ_EXIC] 	= SEEQ_VECTOR;
#else
	SFR_PTR[SEEQ_EXIC] 	= SEEQ_VECTOR|0x10;
#endif
#else
	INT_PTR[SEEQ_INT] 	= Kodiak_Int;
	SFR_PTR[SEEQ_EXIC]	= SEEQ_VECTOR;
#endif
	my_pni = pni;
	pni->ni_hwtype = 1;		/* my type */
	/* set up  hwa */
	memcpy(&pni->ni_hwa.ha_addr[0], &MSI_PTR->ms_etheraddress[0], 6);

	pni->ni_hwa.ha_len = 6;
	for(x=0; x < 6; x++)
		pni->ni_hwb.ha_addr[x]  = 0xff;
	pni->ni_hwb.ha_len = 6;

	Full_Restart();
	KodiakFullRestarts--;

	enable();
	return(0);

}

int
kodiak_send(struct netif *pni, struct ep *pep, int len)
{
	unsigned flags = _FLAGS;

	if(!pep)
		return(-1);
	if(my_pni->ni_outq.queue_count > NI_INQSZ) {
		KodiakOutDiscards++;
		my_pni->ni_odiscard++;
		BufferFreeData(pep);
		return(-2);
	}
	pep->ep_len = len;
	disable();
	QueueInsert(&my_pni->ni_outq, DataToQE(pep));
	if(!(INPW(STATUS_PORT) & TX_ON))
		Transmit_Packet();	/* feh ? */
	if(flags & 512)
		enable();
	return(0);
}

int	kodiak_close(struct netif *pni)
{
	int	y;

	disable();

	SFR_PTR[SEEQ_EXIC] |= 0x40;
	enable();
	return(0);

}


int
kodiak_watchdog(struct netif *pni, int count)
{
	unsigned flags = _FLAGS;
static	int	xmit_count;

	disable();
	if(!recursive) {
		if(xmit_is_active) {
			if(++xmit_count > 3) {
				Transmit_Packet();
				xmit_count = 0;
			}
		}
		if(!(INPW(STATUS_PORT) & RX_ON)) {
			Full_Restart();
			KodiakWatchdogRestarts++;
		}
	}
	if(flags & 512)
		enable();
	return(0);
}

/* net.c -- */
#include 	<mem.h>
#include "network.h"

#include	"task.h"
#include	"slowtime.h"
#include	"tcp.h"
#include	"tcb.h"

#include	"tcpfsm.h"
#include	"tcptimer.h"
#include	"vsock.h"
#include	<memcheck.h>

NetNumbers	Nets[NIFCOUNT];
static	int	running;


unsigned
ntohs(unsigned a)
{
	_BX = a;
	_AH = _BL;
	_AL = _BH;
	return(_AX);

}

unsigned
htons(unsigned a)
{
	_BX = a;
	_AH = _BL;
	_AL = _BH;
	return(_AX);

}

unsigned long
lswap(unsigned long *s)
{
	unsigned int a,b,*z;

	z = (unsigned int *) s;
	a = *s & 0xffff;
	b = *s >> 16;

	a = ntohs(a);
	b = ntohs(b);
	*z++ = b;
	*z = a;
	return(*s);
}

unsigned long
htonl(unsigned long s)
{
	unsigned int a,b;

	a = s & 0xffff;
	b = s >> 16;

	a = ntohs(a);
	b = ntohs(b);
	return((((long)  a) << 16) | b);
}

unsigned long
ntohl(unsigned long s)
{
	return(htonl(s));
}

int
net_setaddr(int ifnum, IPaddr addr, IPaddr mask)
{

	if(ifnum > nifcount)
		return(-1);
	memcpy(Nets[ifnum].net_addr, addr, IP_ALEN);
	memcpy(Nets[ifnum].net_subnetmask, mask, IP_ALEN);

	if(running) {
		ni_ipinit(ifnum, addr, mask);
	}
	return(0);
}

unsigned
netstacksize()
{
	return(IPPROC_STACK + SLOWTIME_STACK + TCPISTACK + TCPOSTACK+TMSTACK);
}

int
netinit(void *stackptr, unsigned ssize)         	/* init all interfaces and return status */
{
	int	x;

#if	!MBBS
	if(IPPROC_STACK + SLOWTIME_STACK + TCPISTACK+TCPOSTACK+TMSTACK > ssize)
		return(-10);			/* not enough stack */
#endif

	ipfinit();
	udp_init();
#if	!IPDOOR
	vsock_init();
#endif
	for(x=0; x < nifcount; x++) {
		int	rc;

		if(!Nifs[x].ni_init)
			continue;

		rc = ni_init(x, Nets[x].net_addr, Nets[x].net_subnetmask);
		if(rc) {
			int	y;

			for(y=0; y <= x; y++)
				(*nif[y].ni_close)(&nif[y]);
			return(rc);
		}
	}
#if	MBBS
	/* now create tasks */
	TaskAlloc(ipproc, NULL, IPPROC_STACK, IPPROC_PRIORITY, "ipproc", 0, NULL);

	TaskAlloc(slowtimer, NULL, SLOWTIME_STACK, SLOWTIME_PRIORITY, "slowtimer", 0, NULL);

	TaskAlloc((ProcessType) tcptimer, NULL, TMSTACK, TMPRIO, "tcptimer",0, NULL);

	TaskAlloc((ProcessType ) tcpinp, NULL, TCPISTACK, TCPIPRIO, "tcpinp",0, NULL);

	TaskAlloc((ProcessType ) tcpout, NULL, TCPOSTACK, TCPOPRIO, "tcpout",0, NULL);
#else	/* not mbbs */
	/* now create tasks */
	TaskAlloc(ipproc, stackptr, IPPROC_STACK, IPPROC_PRIORITY, "ipproc", 0, NULL);
	((char *) stackptr) += IPPROC_STACK;

	TaskAlloc(slowtimer, stackptr, SLOWTIME_STACK, SLOWTIME_PRIORITY, "slowtimer", 0, NULL);
	((char *) stackptr) += SLOWTIME_STACK;

#if	!IPDOOR
	TaskAlloc((ProcessType) tcptimer, stackptr, TMSTACK, TMPRIO, "tcptimer",0, NULL);
	((char *) stackptr) += TMSTACK;

	TaskAlloc((ProcessType ) tcpinp, stackptr, TCPISTACK, TCPIPRIO, "tcpinp",0, NULL);
	((char *) stackptr) += TCPISTACK;

	TaskAlloc((ProcessType ) tcpout, stackptr, TCPOSTACK, TCPOPRIO, "tcpout",0, NULL);
#endif
#endif	/* endif mbbs */
	running = 1;
	return(0);
}

void
netclose()
{
	int	x;

	if(!running)
		return(-1);
	for(x=0; x < nifcount; x++) {
		if(nif[x].ni_close && nif[x].ni_state)
			(*nif[x].ni_close)(&nif[x]);
	}
	running = 0;
}

int
netwatchdog(int delay)
{
	int	x;

	if(!running)
		return(-1);
	for(x=0; x < nifcount; x++)
		if(nif[x].ni_watchdog)
			(*nif[x].ni_watchdog)(&nif[x],delay);
	return(0);
}

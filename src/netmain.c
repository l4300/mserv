#include	"network.h"

#include	"task.h"

#include	<stdio.h>
#include	<dos.h>
#include	<malloc.h>
#include	"alarm.h"

const	int	stacksize = 640;

unsigned int _stklen=8192;

IPaddr	myip = {128,153,43,6};
IPaddr	snetmask = {255,255,255,0};
IPaddr	loopback = { 127,0,0,1};
IPaddr	them = { 128, 153, 43, 1 };

extern	long	IpOutRequests, KodiakRestarts, KodiakFullRestarts;
extern	long	KodiakChainedPackets, KodiakWatchdogRestarts, KodiakPicResets;
extern	long	packet_count;

extern	xmain(int	argc, char *argv[]);

int
maintask(void *Parent, char *name, int argc, char **argv)
{
	exit(xmain(argc, argv));
}


int
dummytask(void *Parent, char *name, int argc, char **argv)
{
	long	now;
static	proccount;
	long	lastout;
	long	nextpoll = Time(NULL);
	proccount = Task_RunQueue.queue_count;

	now = 0;
	lastout = IpOutRequests;
	while(1) {
/*		kodiak_poll(); */
		if(now < Time(NULL)) {
			printf("out %ld Res %ld WD Res %ld FRes %ld PicRs %ld UDP Packets %ld per second %ld free mem %u\n",IpOutRequests,
				KodiakRestarts, KodiakWatchdogRestarts, KodiakFullRestarts, KodiakPicResets, packet_count, (IpOutRequests-lastout)/5,
					coreleft());
/*			BufferDump(); */
			lastout = IpOutRequests;
			now = Time(NULL) + 2;
		}
		if(nextpoll != Time(NULL)) {
			nextpoll = Time(NULL);
			if(kbhit()) {
				int	c = getch();
				Buffer	*B;
				switch (c) {
					case 27 :
						exit(99);
					case 32 :
						printf("icmp send\n");
						if(B = BufferAlloc(512)) {
							icmp(ICT_ECHORQ, 0, them, (char *) B->data, NULL);
						}
				}
				printf("Key %d\n",c);
			}
		}
		TaskReschedule();
	}
}


int
main(int argc, char **argv)
{
	int	rc;
	Task	*P1, *P2;
	void	*stack;
	unsigned	siz;


	net_setaddr(0, loopback, snetmask);
	net_setaddr(1, myip, snetmask);

	BufferInitialize(10,1500+EP_HLEN);
	BufferInitialize(20,96);
/*	BufferInitialize(4,MAXLRGBUF+EP_HLEN); */

	rc = TaskInitialize();
	if(rc) {
		printf("Task Initialize returned %d\n",rc);
		exit(3);
	}

	atexit(TaskDump);
/*	atexit(BufferDump); */
	alloca(50);	/* leave space for task init */

	stack = alloca(stacksize);
	if(!stack) {
		printf("Couldn't alloc stack \n");
		exit(4);
	}

	P2 = TaskAlloc(dummytask,stack, stacksize, 80, "dummy", 0, NULL);
	if(!P2) {
		printf("couldn't get proc2\n");
		exit(6);
	}
	siz = netstacksize();
	stack = alloca(siz);
	if(!stack) {
		printf("couldn't get %u size stack for network\n",siz);
		exit(7);
	}
	if(rc = netinit(stack, siz)) {
		printf("network initialization failed, code %d\n", rc);
		exit(8);
	}
	atexit(netclose);
	alarm_init();
	atexit(alarm_close);
	rtadd(RT_DEFAULT, nif[1].ni_mask, nif[1].ni_ip, 1, 1, RT_INF);

	stack = alloca(stacksize*2);
	if(!stack) {
		printf("couldn't get stack for main\n");
		exit(12);
	}
	P2 = TaskAlloc(maintask, stack, stacksize*2, 100, "main", argc, argv);
	if(!P2) {
		printf("couldn't set up main task \n");
		exit(13);
	}
	if(rc = TaskReschedule()) {
		printf("Task resch returned code %d\n",rc);
	}
	return(0);
}

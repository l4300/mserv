#include	"network.h"

#include	"task.h"

#include	<stdio.h>
#include	<dos.h>
#include	<malloc.h>
#include	"alarm.h"

const	int	stacksize = 512;
IPaddr	myip = {128,153,36,11};
IPaddr	snetmask = {255,255,252,0};
IPaddr	loopback = { 127,0,0,1};
IPaddr	them = { 128, 153, 36, 9 };

extern	long	IpOutRequests, KodiakRestarts, KodiakFullRestarts;
extern	long	KodiakChainedPackets, KodiakWatchdogRestarts, KodiakPicResets;

int
dummytask(void *Parent, char *name, int argc, char **argv)
{
	long	now;
static	proccount;
	long	lastout;

	proccount = Task_RunQueue.queue_count;

	now = 0;
	lastout = IpOutRequests;
	while(1) {
		if(now < Time(NULL)) {
			printf("out %ld Res %ld WD Res %ld FRes %ld PicRs %ld Chained %ld per second %ld free mem %u\n",IpOutRequests,
				KodiakRestarts, KodiakWatchdogRestarts, KodiakFullRestarts, KodiakPicResets, KodiakChainedPackets, (IpOutRequests-lastout)/5,
					coreleft());
			lastout = IpOutRequests;
			now = Time(NULL) + 5;
		}
		if(kbhit()) {
			int	c = getch();
			Buffer	*B;
				switch (c) {
				case 27 :
					exit(99);
				case 32 :
					printf("icmp send\n");
					if(B = BufferAlloc(512)) {
						icmp(ICT_ECHORQ, 0, them, (char *) B->data, NULL);
					}
			}
			printf("Key %d\n",c);
		}
		TaskReschedule();
	}
}

void
panic(char *mesg)
{
	printf("Panic:%s\n",mesg);
	abort(1);
}

int
main()
{
	int	rc;
	Task	*P1, *P2;
	void	*stack;
	unsigned	siz;
/*	extern	int BufferDump(); */
	net_setaddr(0, loopback, snetmask);
	net_setaddr(1, myip, snetmask);

	BufferInitialize(2,MAXLRGBUF);
	BufferInitialize(4,512);
	BufferInitialize(4,128);

	atexit(TaskDump);


	rc = TaskInitialize();
	if(rc) {
		printf("Task Initialize returned %d\n",rc);
		exit(3);
	}

	alloca(50);	/* leave space for task init */

	stack = alloca(stacksize);
	if(!stack) {
		printf("Couldn't alloc stack \n");
		exit(4);
	}

	P2 = TaskAlloc(dummytask,stack, stacksize, 80, "dummy", 0, NULL);
	if(!P2) {
		printf("couldn't get proc2\n");
		exit(6);
	}
	siz = netstacksize();
	stack = alloca(siz);
	if(!stack) {
		printf("couldn't get %u size stack for network\n",siz);
		exit(7);
	}
	if(rc = netinit(stack, siz)) {
		printf("network initialization failed, code %d\n", rc);
		exit(8);
	}
	atexit(netclose);
	rtadd(RT_DEFAULT, nif[1].ni_mask, nif[1].ni_ip, 1, 1, RT_INF);
	if(rc = TaskReschedule()) {
		printf("Task resch returned code %d\n",rc);
	}
	return(0);
}

#include "network.h"

#include	<dos.h>
#include	<mem.h>
#include	<memcheck.h>

#define	INTR	512

int
netwrite(Net_Interface *pni, EP *pep, int len)
{
	Arp_Entry	*pae;
	int		i;
	unsigned	flags;

	if(!(pni->ni_state & NIS_UP)) {
		if(pep)
			BufferFreeData(pep);
		return(SYSERR);
	}
	pep->ep_len = len;
	if(pni->ni_ifnumber == NI_LOCAL)
		return local_out(pep);

	else if (isbrc(pep->ep_nexthop)) {
		memcpy(pep->ep_dst, pni->ni_hwb.ha_addr, pni->ni_hwb.ha_len);
		(*pni->ni_send)(pni, pep, len);
		return OK;
	}
	flags = _FLAGS;
	if(pni->ni_hwtype == HWT_SLIP || pni->ni_hwtype == HWT_PPP) {
		(*pni->ni_send)(pni, pep, len);
		return(OK);
	}
//	disable();
	pae = arpfind(&pep->ep_nexthop, pep->ep_type, pni);
	if(pae && pae->ae_state == AS_RESOLVED) {
		memcpy(pep->ep_dst, pae->ae_hwa, pae->ae_hwlen);
//		if(flags & INTR)
//			enable();
		(*pni->ni_send)(pni, pep, len);
		return(OK);
	}
	if(!pae) {
		pae = arpalloc();
		pae->ae_hwtype = AR_HARDWARE;
		pae->ae_prtype = EPT_IP;
		pae->ae_hwlen = EP_ALEN;
		pae->ae_prlen = sizeof(IPaddr);
		pae->ae_pni = pni;
		memcpy(pae->ae_pra, pep->ep_nexthop, pae->ae_prlen);
		memset(&pae->ae_queue, 0, sizeof(Queue));
		pae->ae_attempts = 0;
		pae->ae_ttl = ARP_RESEND;
		if(arpsend(pae) != OK) {	// not enqueued 
		 	BufferFreeData(pep);
			return(SYSERR);
		}
	}
	QueueInsert(&pae->ae_queue, DataToQE(pep));
//	if(flags & INTR)
//		enable();
	return(OK);
}

/* ni_in.c accept incoming network packets, via INTERRUPT call */
#include	"network.h"

#include	<mem.h>
#include	<memcheck.h>

/*
 * ni_in -- low-level network functions call this routine during interrupt
 *	    processing to pass up a network packet  INTERRUPT!
 *
*/

extern	NetInfo	Nifs[];
Net_Interface	nif[NIFCOUNT];
int	maxnifs = NIFCOUNT;

int
ni_in(Net_Interface * pni, struct ep * pep, int len)
{
	int	rc;

	switch(pep->ep_type) {

#ifdef	old
		case	EPT_ARP :
			rc = arp_in(pni, pep);
			break;
#endif
#ifdef	RARP
		case	EPT_RARP :
			rc = rarp_in(pni, pep);
			break;
#endif
		case 	EPT_ARP :
		case    EPT_IP :
			rc = ip_in(pni, pep);
			break;
		default :
			pni->ni_iunkproto++;
			BufferFreeData(pep);
			return OK;
	}
	pni->ni_ioctects += len;
	if(!memcmp(pni->ni_hwa.ha_addr, pep->ep_dst, pni->ni_hwa.ha_len))
		pni->ni_iucast++;
	else
		pni->ni_ibcast++;
	return rc;
}

int
ni_ipinit(int ifnum, IPaddr addr, IPaddr mask)	/* set up IP addresses for this interface */
{

	memcpy(nif[ifnum].ni_ip, addr, IP_ALEN);
	nif[ifnum].ni_ivalid = 1;
	netnum(nif[ifnum].ni_net, addr);		/* set the network number */
	setmask(ifnum, mask);
	return(OK);
}

int
ni_init(int ifnum, IPaddr addr, IPaddr mask)	/* initialize network interfaces */
{
	int	rc;

	if(ifnum >= nifcount)
		return(-1);

	memset(&nif[ifnum],0, sizeof(Net_Interface));	/* clear setting */
	nif[ifnum].ni_outq.queue_flags |= QUEUE_FLAGS_NOTSORTED;

	strncpy(nif[ifnum].ni_name, Nifs[ifnum].ni_name, NETNLEN);	/* set name */
	nif[ifnum].ni_name[NETNLEN-1] = 0;

	nif[ifnum].ni_ifnumber = ifnum;			/* set ifnumber */
	nif[ifnum].ni_state = NIS_DOWN;			/* we're down */
	nif[ifnum].ni_upcall = ni_in;			/* patch upcall */

	nif[ifnum].ni_init = Nifs[ifnum].ni_init;
	nif[ifnum].ni_send = Nifs[ifnum].ni_send;
	nif[ifnum].ni_close = Nifs[ifnum].ni_close;
	nif[ifnum].ni_watchdog = Nifs[ifnum].ni_watchdog;
	nif[ifnum].ni_mtu = Nifs[ifnum].ni_mtu;
	nif[ifnum].ni_hwtype = Nifs[ifnum].ni_hwtype;
	nif[ifnum].ni_speed = Nifs[ifnum].ni_speed;
	if(!ifnum) {
		nif[ifnum].ni_mtype = 24;
		nif[ifnum].ni_descr = "loopback";
	}
	else {
		nif[ifnum].ni_mtype = 6;
		switch(nif[ifnum].ni_hwtype) {
			default :;
			case HWT_ETHER :
				nif[ifnum].ni_descr = "ethernet";
				break;
			case HWT_SLIP :
				nif[ifnum].ni_descr = "slip";
				break;
			case HWT_PPP :
				nif[ifnum].ni_descr = "ppp";
				break;
		}	// end switch 
	}
	if(nif[ifnum].ni_init)
		if((rc = (*nif[ifnum].ni_init)(&nif[ifnum], Nifs[ifnum].ni_arg1, Nifs[ifnum].ni_arg2, Nifs[ifnum].ni_arg3)) != 0) {
			return(rc);
		}
	nif[ifnum].ni_state = NIS_UP|NIS_PRESENT;

	ni_ipinit(ifnum, addr, mask);		/* set up addresses */
	return(OK);
}


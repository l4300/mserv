/* nif.c -- define all interfaces */

#include	"network.h"

#if	!IPDOOR
#include	"kodiak.h"
#define	KODIAK
#endif

#include	<memcheck.h>

int	local_init(struct netif *pni, unsigned arg1, unsigned arg2, unsigned arg3);
int	local_send(struct ep *packet, int length);
int	local_close();

NetInfo	Nifs[NIFCOUNT+1]={		/* interfaces */
	/* local */
	{local_init, local_send, local_close, NULL,MAXLRGBUF, 100000, "loopback",0, 0,0,0},
#ifdef	KODIAK
	{ kodiak_init,	/* init func */
	  kodiak_send,	/* send func */
	  kodiak_close, /* close func */
	  kodiak_watchdog, /* watchdog */
	  1500,		/* mtu */
	  1000000,	/* speed */
	  "kodiak",	/* name */
	  0,		/* hwtype */
	  5,		/* arg1 */
	  0x210,	/* arg2 */
	  0		/* arg 3 */
	},
#endif
	{NULL, 		NULL, 	NULL, 	0,0,NULL,0, 0,0,0}
};

int	nifcount = (sizeof(Nifs)/sizeof(NetInfo) -1);


#pragma argsused
int	local_init(struct netif *pni, unsigned arg1, unsigned arg2, unsigned arg3)
{

	return(0);
}

#pragma argsused
int	local_send(struct ep *packet, int length)
{
	local_out(packet);
	return(0);

}

int	local_close()
{
	return(0);
}

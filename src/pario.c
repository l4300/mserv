// parrio.c - parallel I/O for MicroServ, Copyright (C) 1993 MurkWorks, Inc.
// All Rights Reserved
// #pragma inline
#define	STATIC

#define	USEPARVECTOR		0	// true to use vector mode instead of rb
#define	USEFASTPAR		1	// true to use fast RB code
#define	COM2DEBUG		0
#define	DONIBBLE		1	// true to do nibble modes

#define	SYSLOG_DEBUG		0	// print out some debugging stuff

#define	INTR	512

#define	PARALLEL_INPUT_TIMEOUT	10	// in 100's of a second

#include	"network.h"
#include	"sys/errno.h"
#include	"sys/socket.h"
#include	"ioint.h"
#include	"task.h"
#include	"syslog.h"

#include	<dos.h>			// get enable()

#if	!SIM
#include	"v25.h"
#endif

#if	DONIBBLE
// define bits in terms of 1284 spec

// on port 1
#define	PtrClk		0x04		// ack
#define	PtrBusy		0x08		// busy line
#define	nDataAvail	0x10		// nFault
#define	XFlag		0x20		// select
#define	AckDataReq	0x40		// PError

// on port 2
#define	HostClk		0x01		// strobe
#define	HostBusy	0x02		// nAutoFd
#define	nReverseRequest	0x04		// nInit
#define	nAStrb		0x08		// nSelectIn

#endif

#define	PARSTACK_SIZE	512			// needs tuning

RegisterBank   far	*Par_RB;
int		Parallel_Ack_Mode;		// bit 1 true for ack else BUSY

void far	parallel_regbank();

STATIC	void interrupt 	parallel0_interrupt();
unsigned char	parstack[PARSTACK_SIZE];



#if	SIM
STATIC void
enable_par_interrupt(IO_Port *I, void interrupt (*isr)(), void far *stack, unsigned int stacklen)
#else
STATIC void
enable_par_interrupt(IO_Port *I, void interrupt (*isr)(), void far *stack, unsigned int stacklen)
#endif
{
#if	SIM
	unsigned int imask;

	disable();
	I->io_p1 = getvect(I->io_interrupt+8);
	setvect(I->io_interrupt+8, isr);
	imask = inportb(0x21);
	imask &= ~(1 << I->io_interrupt);
	outportb(0x21, imask);
#else
	int	x;

	Par_RB = &(IDA_PTR->rb.Register[PARALLEL_VECTOR]);

	disable();

	Par_RB->r_cs		= FP_SEG(parallel_regbank);
	Par_RB->r_vector_ip	= FP_OFF(parallel_regbank);
	Par_RB->r_ds		= _DS;
	Par_RB->r_ss		= FP_SEG(parstack);
	Par_RB->r_sp		= FP_OFF(parstack) + sizeof(parstack) - 2;
	Par_RB->r_es		= FP_SEG(&SFR_PTR[SFR_P0]);
	Par_RB->r_bp		= Par_RB->r_sp;
	Par_RB->r_si		= 0;		// input char location
	Par_RB->r_di		= _DS;
	Par_RB->r_cx		= 0;		// output char count
	Par_RB->r_bx		= FP_OFF(&SFR_PTR[SFR_P0]);
	SFR_PTR[I->io_base_address] = PARALLEL_VECTOR; 	/* vector interrupt */
	INT_PTR[INT_P2] 	= isr;
	INT_PTR[INT_P1] 	= isr;
#endif
	enable();
#if	COM2DEBUG
	Com2Debug('C');
#endif
}

STATIC void
disable_par_interrupt(IO_Port *I)
{
#if	SIM
	unsigned int imask;

	disable();
	setvect(I->io_interrupt+8, (void interrupt (*)()) I->io_p1);
	imask = inportb(0x21);
	imask |= (1 << I->io_interrupt);
	outportb(0x21, imask);
#else
	int	x;

	disable();
	SFR_PTR[I->io_base_address] = 0x40|PARALLEL_VECTOR;	/* regbank interrupt */
	enable();
#endif
}

unsigned int
parallel_saveint()
{
	unsigned int z;
	unsigned flags = _FLAGS;

	disable();
	z = (SFR_PTR[SFR_EXIC1] << 8) | SFR_PTR[SFR_EXIC2];
	SFR_PTR[SFR_EXIC1] 	|= 0x40;
	SFR_PTR[SFR_EXIC2]	|= 0x40;
	if(flags & INTR)
		enable();
	return(z);
}

void
parallel_restoreint(unsigned int z)
{
	unsigned flags = _FLAGS;

	disable();
	SFR_PTR[SFR_EXIC1] = z >> 8;
	SFR_PTR[SFR_EXIC2] = z & 0xff;
	if(flags & INTR)
		enable();
}
#if	DONIBBLE
int
parallel_1284Compliant(int mode, int maxlen, char *data)	// return > -1 if compliant
		// if maxlen is set, tries to read device id
		// returns len
{
	unsigned int saver;
	unsigned char	far	*c = &SFR_PTR[SFR_P0];
				// use c for based register access

	unsigned  int	x;
	int	rc = -1;
	int	charin = 0;
	IO_Port	*I = (IO_Port *) data;

#define	P0	*c
#define	P1	*(c + 0x08)
#define	P2	*(c + 0x10)

	if(mode < 0) {
		charin++;
		mode = 0;
	}
	saver = parallel_saveint();

	P0 = mode;

	P2 |= nAStrb;		// phase 1
	P2 &= ~HostBusy;

	for(x=2000; x > 0; x--) {
		if(P1 & AckDataReq &&
		   P1 & nDataAvail &&
		   P1 & XFlag &&
		   !(P1 & PtrClk))
			break;	// phase 2 complete
	}
	if(x) {
#if	SYSLOG_DEBUG
	if(!charin)
		Syslog(LOG_DEBUG,"pario phase 2 ok P1=0x%x",P1);
#endif
		P2 &= ~HostClk;	// shift in requested value
				// phase 3
#if	SYSLOG_DEBUG
		if(!charin)
			Syslog(LOG_DEBUG,"pario after clock P1=0x%x",P1);
#endif
		asm nop
		asm nop
		P2 |= (HostClk|HostBusy);	// phase 4
		for(x=2000; x > 0; x--) {
			if(P1 & PtrClk &&
//			   !(P1 & AckDataReq))
			    (P1 & PtrClk))		// was above..
				break;
		}
		if(x) {			// phase 6-7
#if	SYSLOG_DEBUG
	if(!charin)
		Syslog(LOG_DEBUG,"pario phase 6 ok");
#endif
			if( (!mode && !(P1 & XFlag)) ||
			     (mode && (P1 & XFlag))) {	// request approved

				rc = 0;		// the request was approved
#if	SYSLOG_DEBUG
	if(!charin)
		Syslog(LOG_DEBUG,"pario ready to read P1=0x%x",P1);
#endif
//				SFR_PTR[SFR_INTM] |= 0x40;
				while(maxlen && !(P1 & nDataAvail)) {
					unsigned char	t, l;

					P2 &= ~HostBusy;
					for(x=2000; x > 0; x--) {
						if(!(P1 & PtrClk))
							break;
					}
					if(!x)
						goto abortread;
					t = P1 & PtrBusy;
					t |= ((P1 & 0x70) >> 4) & 0x7;
//					t = P1 & 0xf;
					P2 |= HostBusy;
					asm	nop
					asm	nop
					for(x=2000; x > 0; x--) {
						if((P1 & PtrClk))
							break;
					}
					if(!x)
						goto abortread;

					P2 &= ~HostBusy;
					for(x=2000; x > 0; x--) {
						if(!(P1 & PtrClk))
							break;
					}
					if(!x)
						goto abortread;
					l = P1 & PtrBusy;
					l |= ((P1 & 0x70) >> 4) & 0x7;

					P2 |= HostBusy;
					asm	nop
					asm	nop
					for(x=2000; x > 0; x--) {
						if((P1 & PtrClk))
							break;
					}
					if(!x)
						goto abortread;
					if(charin && I->io_iccount) {
						if(iosocket_postinput(I,(l << 4) | t)) {
							maxlen--;
							rc++;
							break;
						}
					}
					else
						*data++ = (l << 4) | t;
					maxlen--;
					rc++;
				}	// end while maxlen and data available
//				SFR_PTR[SFR_INTM] &= ~0x40;
abortread:;

			}	// end if request approved
		}	// end if phase 6 passed
#if	SYSLOG_DEBUG
		else if(!charin) {
			Syslog(LOG_DEBUG,"par phase 6 failed P1=0x%x",P1);
		}
#endif
			// go back to compliant mode
		P2 &= ~nAStrb;		// exit 1284 mode
		for(x=2000; x > 0; x--) {
			if(!(P1 & PtrClk))
				break;
		}
#if	SYSLOG_DEBUG
		if(!charin)
			Syslog(LOG_DEBUG,"par shut1 x=%d %x",x, P1);
#endif
		P2 &= ~HostBusy;
		for(x=4000; x > 0; x--) {
			if(P1 & PtrClk)
				break;
		}
#if	SYSLOG_DEBUG
		if(!charin)
			Syslog(LOG_DEBUG,"par shut2 x=%d %x",x, P1);
#endif
		P2 |= HostBusy;
		for(x=4000; x > 0; x--) {
			if(P1 & PtrClk)
				break;
		}
#if	SYSLOG_DEBUG
		if(!charin)
			Syslog(LOG_DEBUG,"par shut3 x=%d %x",x, P1);
#endif
	}	// end if phase 2 ok
	else {			// back to compliant mode
#if	SYSLOG_DEBUG
	if(!charin)
		Syslog(LOG_DEBUG,"pario phase 2 failed");
#endif
		P2 |= HostBusy;
		P2 &= ~nAStrb;
	}
	parallel_restoreint(saver);
	return(rc);
}

#endif

void far
parallel_regbank()
{

reenter:;
	asm {
		reenter:		// each intr, we start here
			jcxz	done
			test	byte ptr es:[bx+0x8], 8
			jz	domore
	}
	// get here, the parallel port is busy, we need to fint it
	FINT();
	RETRBI();
	// don't get here ever.

domore:;
	asm {
		domore:			// ok to output a character
			lodsb
			mov	es:[bx], al
			mov	es:[bx+0x10], dl
			mov	es:[bx+0x10], dh
			loop	reenter
	}
done:;
	asm	and	byte ptr es:[bx+0x4e], not 0x10
	asm	and	byte ptr es:[bx+0x4d], not 0x10
	FINT();
	RETRBI();
}

void
parallel_interrupt(IO_Port *I)	/* general parallel int service */
{
	unsigned char	dl;
	unsigned int	es, ds, bx, si;		// this is a crock

	if(!I->io_occount) {
		iosocket_nextobuffer(I);
		if(!I->io_occount) {
#if	DONIBBLE
			if(I->io_desired_modes & IOMODES_INPUT) {
				I->io_devflags |= IODEVFLAG_NEEDTIMER;
				I->io_timeout = 1;
			}
#endif
			return;
		}
	}
	SFR_PTR[SFR_P0] = *I->io_ochar++;
	SFR_PTR[SFR_P2] &= ~1;
	SFR_PTR[SFR_P2] |= 1;
	if(!--I->io_occount || (Parallel_Ack_Mode & 1)) {	// don't do high speed regbank for one char
		return;
	}
#if	!USEFASTPAR
	return;
#else
	// setup RB registers for maximum mode
	dl = SFR_PTR[SFR_P2];
	Par_RB->r_ds = FP_SEG(I->io_ochar);
	Par_RB->r_si = FP_OFF(I->io_ochar);
	Par_RB->r_cx = I->io_occount;
	I->io_occount = 0;
	Par_RB->r_dx = ((dl | 1) << 8) | (dl & 0xfe);
	SFR_PTR[I->io_base_address]  |= 0x10;
#endif
}


STATIC	IO_Port	*parallel0_interrupt_ptr;

STATIC	void interrupt
parallel0_interrupt()
{
	parallel_interrupt(parallel0_interrupt_ptr);
	FINT();
}


int
iofunc_parallel(struct _io_port *I, int mode, void *arg)
{
	int	rc = 0;
	struct	_sendto	*sendto = (struct _sendto *) arg;
	unsigned flags = _FLAGS;
	void	far	*mystack;
	char	*c;
	int	i,x;
static	char	*truevals[]={"","","Ack, ","Busy, ","NoError, ","Online, ","PaperOut"};
static	char	*falsevals[]={"","","NoAck, ","NotBusy, ","Error, ","Offline, ","PaperOk"};

	switch(mode) {		/* PC parallel port */
		case IOFUNCTION_PROBE :	/* does device exist ? */
			break;
		case IOFUNCTION_INIT :
			I->io_speed 	= 0;
			I->io_intfmodes = IO_IFMODE_DATA8BIT;
			I->io_ipost_process = I->io_opost_process = 0;
			if(I->io_unit_number == 0)
				parallel0_interrupt_ptr = I;

			I->io_devflags |= IODEVFLAG_CLOSED;
			/* physical device initialization */
#if	SIM
			outportb(port,0);
			outportb(port+2,8);
			for(rc=1000; rc > 0; rc--);
			outportb(port+2,0x10|0x8|0x4); /* irq on, init, select */
#else
			SFR_PTR[SFR_P2] = 0x3;
			for(rc=100; rc > 0; rc--)
				;
			SFR_PTR[SFR_P2] |= 4;
//			mystack = malloc(PARSTACK_SIZE);
#endif

			break;
		case IOFUNCTION_OPEN :
			I->io_ibufsize = 0;
			I->io_desired_modes = IOMODE_OUTPUT;	/* default mode */
			I->io_socket->so_readable = 0;
			I->io_socket->so_recvqueue = 0;
			I->io_socket->so_writeable = DEFAULT_OBYTELIMIT;
			I->io_devflags &= ~IODEVFLAG_CLOSED;
			if(Parallel_Ack_Mode & 1)
				I->io_base_address = SFR_EXIC1;
			else
				I->io_base_address = SFR_EXIC2;

			enable_par_interrupt(I, parallel0_interrupt,mystack,PARSTACK_SIZE);
#if	DONIBBLE
#if	SYSLOG_DEBUG
		Syslog(LOG_DEBUG,"pario checking");
#endif
			// see what's plugged in
			if(parallel_1284Compliant(0, 0, NULL) >= 0) {	// bidirectional is possible
#if	SYSLOG_DEBUG
		Syslog(LOG_DEBUG,"pario ok.");
#endif
				I->io_allowed_modes |= IOMODE_RINPUT;
				I->io_ibufsize = DEFAULT_IBUFSIZE;
				I->io_socket->so_recvqueue = DEFAULT_IBYTELIMIT;
			}
			else
				I->io_allowed_modes &= ~IOMODE_RINPUT;
#endif
			break;
		case IOFUNCTION_SETOPT :
			/* the iosocket option handler has already set
			   the IO_Port variables, we need only reconfigure
			   ourselves
			*/
			switch(sendto->s_flags) {
				case	IO_SOPT_IOMODE :
				case	IO_SOPT_IBSIZE :
				case	IO_SOPT_IBUFSIZE :
					if(I->io_desired_modes & IOMODES_INPUT) {
						iosocket_flushinput(I);
						rc = iosocket_prepinput(I);
					}
					else if(I->io_fbuffers.queue_count) {
						iosocket_flushinput(I);
					}
				default :;
			}
#if	DONIBBLE
			if(I->io_desired_modes & IOMODES_INPUT &&
			   !(I->io_devflags & IODEVFLAG_CLOSED)) {
				// check for input from parallel device

				I->io_devflags |= IODEVFLAG_NEEDTIMER;
				I->io_timeout = PARALLEL_INPUT_TIMEOUT;
			}
#endif
			break;
		case IOFUNCTION_CLOSE :
			I->io_devflags |= IODEVFLAG_CLOSED;
			I->io_devflags &= ~IODEVFLAG_NEEDTIMER;
			disable_par_interrupt(I);
			break;
		case IOFUNCTION_SHUTDOWN :	/* opposite of init */

#if	SIM
			outportb(port+2,8|4);
#else
			SFR_PTR[SFR_P2] = 8|4;
#endif
			iosocket_flushinput(I);	/* probably nothing to flush */
			iosocket_flushoutput(I);
			break;
		case IOFUNCTION_STATUS :
			c = sendto->s_msg;
			if(I->io_allowed_modes & IOMODE_RINPUT)
				c += sprintf(c, "nibble mode available, ");
			for(i=0; i < 7; i++) {
				x = (1 << i);
				c += sprintf(c,"%s",(x & SFR_PTR[SFR_P1]) ? truevals[i] : falsevals[i]);
			}
			rc = c - sendto->s_msg;
			break;
		case IOFUNCTION_KICKSTART :
			if(I->io_devflags & IODEVFLAG_IOACTIVE) {	/* already transmitting */
				break;
			}
kickstart:;
			if(iosocket_nextobuffer(I)) {
				break;
			}
			if(!I->io_occount) {
				break;
			}
			I->io_devflags |= IODEVFLAG_IOACTIVE;
			I->io_devflags &= ~IODEVFLAG_NEEDTIMER;
			SFR_PTR[I->io_base_address] |= 0x80;
			break;
		case IOFUNCTION_TIMER :
#if	DONIBBLE
			if(I->io_desired_modes & IOMODES_INPUT &&
			   !(I->io_devflags & IODEVFLAG_CLOSED)) {
				// check for input from parallel device

				disable();
				while(I->io_iccount > 0) {
					if(parallel_1284Compliant(-1, I->io_iccount, (char *) I) < 1)
						break;
				}
				if(flags & INTR)
					enable();
				I->io_devflags |= IODEVFLAG_NEEDTIMER;
				I->io_timeout = PARALLEL_INPUT_TIMEOUT;
			}
#endif
			break;
		case IOFUNCTION_RESTART :
			I->io_devflags &= ~IODEVFLAG_NEEDRESTART;
			break;
		default :;
			rc = -1;
	}

	return(rc);
}

#include <alloc.h>
#include <stdlib.h>
#include <dos.h>

#if	MBBS
#include	"gcomm.h"
#endif

#include "queue.h"
#include	<memcheck.h>

#define	INTR	512

Queue_Element *
QueueAlloc(long key, unsigned typ, int size)
{
	Queue_Element	*QE;

	if(size < sizeof(Queue_Element))
		size = sizeof(Queue_Element) + abs(size);
	QE = (Queue_Element *) calloc(1, size);
	if(!QE)
		return(NULL);
	QE->queue_key = key;
	QE->queue_type = typ;
	QE->queue_size = abs(size);
	return(QE);
}

#if	BUFDEBUG
int
QueueInsertX(Queue *Q, Queue_Element *QE, char *file, unsigned lnum)
{
	Queue_Element	*QP;
	unsigned	flags = _FLAGS;

	if(!Q || !QE)
		return(-1);

	QE->queue_parent = Q;
	QE->qi_file = file;
	QE->qi_line = lnum;
	disable();
	QP = Q->queue_head;
	if(!QP) {	/* only item */
		QE->queue_next = QE->queue_prev = NULL;
		Q->queue_head = Q->queue_tail = QE;
		Q->queue_count = 1;
		if(flags & INTR)
			enable();
		return(0);
	}
	if(!(Q->queue_flags & QUEUE_FLAGS_NOTSORTED))
		while(QP) {
			if(QP->queue_key >= QE->queue_key) 	/* add here */
				break;
			QP = QP->queue_next;
		}
	if(!QP || (Q->queue_flags & QUEUE_FLAGS_NOTSORTED)) {
		Q->queue_tail->queue_next = QE;
		QE->queue_prev = Q->queue_tail;
		Q->queue_tail = QE;
		QE->queue_next = NULL;
		Q->queue_count++;
		if(flags & INTR)
			enable();
		return(0);
	}
	QE->queue_next = QP;
	if(QP->queue_prev)
		QP->queue_prev->queue_next = QE;
	QE->queue_prev = QP->queue_prev;
	QP->queue_prev = QE;
	if(Q->queue_head == QP)
		Q->queue_head = QE;
	Q->queue_count++;
	if(flags & INTR)
		enable();
	return(0);
}
#else
int
QueueInsert(Queue *Q, Queue_Element *QE)
{
	Queue_Element	*QP;
	unsigned	flags = _FLAGS;

	if(!Q || !QE)
		return(-1);
	disable();
	QP = Q->queue_head;
	if(!QP) {	/* only item */
		QE->queue_next = QE->queue_prev = NULL;
		Q->queue_head = Q->queue_tail = QE;
		Q->queue_count = 1;
		if(flags & INTR)
			enable();
		return(0);
	}
	if(!(Q->queue_flags & QUEUE_FLAGS_NOTSORTED))
		while(QP) {
			if(QP->queue_key >= QE->queue_key) 	/* add here */
				break;
			QP = QP->queue_next;
		}
	if(!QP || (Q->queue_flags & QUEUE_FLAGS_NOTSORTED)) {
		Q->queue_tail->queue_next = QE;
		QE->queue_prev = Q->queue_tail;
		Q->queue_tail = QE;
		QE->queue_next = NULL;
		Q->queue_count++;
		if(flags & INTR)
			enable();
		return(0);
	}
	QE->queue_next = QP;
	if(QP->queue_prev)
		QP->queue_prev->queue_next = QE;
	QE->queue_prev = QP->queue_prev;
	QP->queue_prev = QE;
	if(Q->queue_head == QP)
		Q->queue_head = QE;
	Q->queue_count++;
	if(flags & INTR)
		enable();
	return(0);
}
#endif

Queue_Element	*
QueueExtract(Queue *Q, Queue_Element *QE)
{
	register Queue_Element	*QP;
	unsigned flags = _FLAGS;

	if(!Q || !QE)
		return(NULL);
	disable();
	QP = Q->queue_head;
	while(QP) {
		if(QP == QE)
			break;
		QP = QP->queue_next;
	}
	if(QP) {
		if(Q->queue_count == 1) {	/* special case */
			QP->queue_next = QP->queue_prev = NULL;
			Q->queue_head = Q->queue_tail = NULL;
			Q->queue_count = 0;
			if(flags & INTR)
				enable();
			return(QP);
		}
		if(QP->queue_next)
			QP->queue_next->queue_prev = QP->queue_prev;
		if(QP->queue_prev)
			QP->queue_prev->queue_next = QP->queue_next;
		if(Q->queue_head == QP)
			Q->queue_head = QP->queue_next;
		else {
			if(Q->queue_tail == QP)
				Q->queue_tail = QP->queue_prev;
		}
		Q->queue_count--;
		QP->queue_next = QP->queue_prev = NULL;

	}
	if(flags & INTR)
		enable();

	return(QP);
}

Queue_Element *
QueueKey(Queue *Q, long key)
{
	unsigned flags = _FLAGS;
	Queue_Element *QP;

	if(!Q)
		return(NULL);
	disable();
	QP = Q->queue_head;
	while(QP) {
		if(QP->queue_key >= key)
			break;
		QP = QP->queue_next;
	}
	if(flags & INTR)
		enable();
	return(QP);
}
Queue_Element *
QueueFind(Queue *Q, long key)
{
	Queue_Element *QP;
	unsigned flags = _FLAGS;

	if(!Q)
		return(NULL);
	disable();
	QP = Q->queue_head;
	while(QP) {
		if(QP->queue_key == key)
			break;
		QP = QP->queue_next;
	}
	if(flags & INTR)
		enable();
	return(QP);
}

Queue_Element *
QueueHead(Queue *Q)
{
	Queue_Element *QE;
	unsigned flags = _FLAGS;

	if(!Q)
		return(NULL);
	disable();
	
	QE = Q->queue_head;
	if(flags & INTR)
		enable();
	return(QE);
}

Queue_Element *
QueueTail(Queue *Q)
{
	Queue_Element *QE;
	unsigned flags = _FLAGS;

	if(!Q)
		return(NULL);
	disable();
	QE = Q->queue_tail;
	if(flags & INTR)
		enable();

	return(QE);
}

Queue_Element	*
QueuePFind(Queue *Q, long key)
{
	Queue_Element	*QE;
	unsigned 	flags = _FLAGS;

	disable();
	if((QE = QueueFind(Q, key)) != 0)
		QueueExtract(Q, QE);
	if(flags & INTR)
		enable();
	return(QE);
}

Queue_Element	*
QueuePKey(Queue *Q, long key)
{
	Queue_Element	*QE;
	unsigned flags = _FLAGS;

	disable();

	if((QE = QueueKey(Q, key)) != 0)
		QueueExtract(Q, QE);
	if(flags & INTR)
		enable();
	return(QE);
}

Queue_Element	*
QueuePHead(Queue *Q)
{
	Queue_Element	*QE;
	unsigned flags	= _FLAGS;

	disable();
	if((QE = QueueHead(Q)) != 0)
		QueueExtract(Q, QE);
	if(flags & INTR)
		enable();
	return(QE);
}

Queue_Element	*
QueuePTail(Queue *Q)
{
	Queue_Element	*QE;
	unsigned flags = _FLAGS;

	disable();
	if((QE = QueueTail(Q)) != 0)
		QueueExtract(Q, QE);
	if(flags & INTR)
		enable();
	return(QE);
}



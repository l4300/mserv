/* route.c */

#include	<mem.h>
#include	"network.h"
#include	"task.h"
#include	"rip.h"
#include	"netif.h"

#if	IDEBUG
#include	"syslog.h"

extern char *IP_ntoa();
extern int	Debug;
#endif

#include	<memcheck.h>

#if	!IPDOOR
#define	DONEAR	1
#endif

#undef	NEAR
#if	DONEAR
#define	NEAR	static near
#else
#define	NEAR
#endif

struct	route	*rttable[RT_TSIZE];
struct	rtinfo	Route;

int
netnum(IPaddr net, IPaddr ipa)
{
	int	bc;

	memcpy(net, ipa, IP_ALEN);
	if(IP_CLASSA(net)) bc = 1;
	if(IP_CLASSB(net)) bc = 2;
	if(IP_CLASSC(net)) bc = 3;
	if(IP_CLASSD(net)) bc = 4;
	if(IP_CLASSE(net)) bc = 5;
	for(; bc < IP_ALEN; bc++)
		net[bc] = 0;
	return(OK);
}

Bool
netmatch(IPaddr dst, IPaddr net, IPaddr mask, Bool islocal)
{
	int	i;

	for(i=0; i < IP_ALEN; i++)
		if((mask[i] & dst[i]) != net[i])
			return(0);

	if(islocal)
		if(isbrc(dst))
			return(memcmp(mask, ip_maskall, IP_ALEN));
	return(1);
}

int
netmask(IPaddr mask, IPaddr net)
{
	IPaddr	netpart;
	Bool	isdefault = 1;
	int	i, bc;

	for(i=0; i < IP_ALEN; i++) {
		mask[i] = 0;
		isdefault &= net[i] == 0;
	}

	if(isdefault) {
		memcpy(mask, net, IP_ALEN);
		return(OK);
	}

	/* check subnet match */
	netnum(netpart, net);
	for(i=0; i < nifcount; i++) {
		if(nif[i].ni_svalid && nif[i].ni_ivalid &&
			!memcmp(nif[i].ni_net, netpart, IP_ALEN)) {
				memcpy(mask, nif[i].ni_mask, IP_ALEN);
				return(OK);
		}
	}
	if(IP_CLASSA(net)) bc = 1;
	if(IP_CLASSB(net)) bc = 2;
	if(IP_CLASSC(net)) bc = 3;
	if(IP_CLASSD(net)) bc = 4;
	if(IP_CLASSE(net)) bc = 5;
	for(; bc < IP_ALEN; bc++)
		mask[bc] = 0;
	return(OK);
}

Bool NEAR
xnetmatch(IPaddr dst, IPaddr net, IPaddr mask, Bool islocal)
{
	int	i;

	for(i=0; i < IP_ALEN; i++)
		if((mask[i] & dst[i]) != (net[i] & mask[i]))
			return(0);

	if(islocal)
		if(isbrc(dst))
			return(memcmp(mask, ip_maskall, IP_ALEN));
	return(1);
}

int NEAR
rthash(IPaddr net)
{
	int	bc;
	int	hv;	/* hash value */

	hv = 0;
	if(IP_CLASSA(net)) bc = 1;
	if(IP_CLASSB(net)) bc = 2;
	if(IP_CLASSC(net)) bc = 3;
	if(IP_CLASSD(net)) bc = 4;
	if(IP_CLASSE(net)) bc = 4;
	while(bc--)
		hv += net[bc] & 0xff;
	return(hv % RT_TSIZE);
}

ROUTE *
rtget(IPaddr dest, Bool local)
{
	struct	route	*prt;
	int		hv;

	if(!Route.ri_valid)
		rtinit();
	Semaphore_Wait(&Route.ri_mutex);
	hv = rthash(dest);
	for(prt = rttable[hv]; prt; prt=prt->rt_next) {
		if(prt->rt_ttl <=0)
			continue;
		if(xnetmatch(dest,prt->rt_net, prt->rt_mask, local))
			if(prt->rt_metric < RTM_INF)
				break;
	}
	if(!prt)
		prt = Route.ri_default;
	if(prt && prt->rt_metric >= RTM_INF)
		prt = 0;
	if(prt) {
		prt->rt_refcnt++;
		prt->rt_usecnt++;
	}
	Semaphore_Signal(&Route.ri_mutex);
	return(prt);
}

void
rtinit()
{
	memset(&Route,0,sizeof(Route));
	memset(rttable, 0, sizeof(rttable));
	BufferCreate(&Route.ri_bpool, RT_BPSIZE, sizeof(struct route), QTYPE_ROUTE);
	Route.ri_valid = 1;
	Semaphore_Initialize(&Route.ri_mutex, 1);
	Route.ri_default = NULL;
}


#if	IDEBUG
char	*
RouteInfo(char *obuff, ROUTE *prt)
{

	char	network[32],netmask[32],gateway[32];

			//    128.128.128.128
	*obuff = 0;		//      1         2         3         4         5         6         7
	if(!prt) {         // 01234567890123456789012345678901234567890123456789012345678901234567890123456789
		strcpy(obuff,"Network          NetMask          Gateway         Metric Intf     TTL  Rcnt Use");
		return(obuff);
	}
	strcpy(network,IP_ntoa(prt->rt_net));
	strcpy(netmask,IP_ntoa(prt->rt_mask));
	strcpy(gateway,IP_ntoa(prt->rt_gw));

	sprintf(obuff,"%-16.16s %-16.16s %-16.16s % 4d %d %-8.8s %3d %2ld %4ld",
		network,netmask,gateway,prt->rt_metric, prt->rt_ifnum, nif[prt->rt_ifnum].ni_name,
		prt->rt_ttl, prt->rt_refcnt, prt->rt_usecnt);
	return(obuff);
}

void
RouteDump()
{
	struct route *prt, *prev;
	int	i;
	char	buff[128];


	xprintf("%s\n",RouteInfo(buff,NULL));

	if(!Route.ri_valid)
		return;
	Semaphore_Wait(&Route.ri_mutex);

	for(i=0; i < RT_TSIZE; i++) {
		if(!rttable[i])
			continue;
		for(prev = NULL, prt=rttable[i]; prt;) {
			xprintf("%s\n",RouteInfo(buff,prt));
			prev = prt;
			prt = prt->rt_next;
		} /* end for prev */
	} /* end for rt size */
	prt = Route.ri_default;
	xprintf("*Default*\n");
	xprintf("%s\n",RouteInfo(buff,prt));

	Semaphore_Signal(&Route.ri_mutex);
}

#endif

void
rttimer(int delta)
{
	struct route *prt, *prev;
	Bool	ripnotify;
	int	i;


	if(!Route.ri_valid)
		return;
	Semaphore_Wait(&Route.ri_mutex);
	ripnotify = 0;

	for(i=0; i < RT_TSIZE; i++) {
		if(!rttable[i])
			continue;
		for(prev = NULL, prt=rttable[i]; prt;) {
			if(prt->rt_ttl != RT_INF)
				prt->rt_ttl -= delta;
			if(prt->rt_ttl<=0) {
#if	IDEBUG
	{
		char buff[128];

		if(Debug > 1) {
			RouteInfo(buff,prt);
			Syslog(LOG_DEBUG,"rttimer expire %s",buff);
		}
	}
#endif
				if(dorip && prt->rt_metric < RTM_INF) {
					prt->rt_metric = RTM_INF;
					prt->rt_ttl = RIPZTIME;
					ripnotify = 1;
					continue;
				}
				if(prev) {
					prev->rt_next = prt->rt_next;
					if(--prt->rt_refcnt <=0)
						QueueInsert(&Route.ri_bpool, DataToQE(prt));
					prt = prev->rt_next;
				} else {
					rttable[i] = prt->rt_next;
					if(--prt->rt_refcnt <=0)
						QueueInsert(&Route.ri_bpool, DataToQE(prt));
					prt = rttable[i];
				}
				continue;
			} /* end if timed out */
			prev = prt;
			prt = prt->rt_next;
		} /* end for prev */
	} /* end for rt size */
	prt = Route.ri_default;
	if(prt && (prt->rt_ttl <RT_INF) && (prt->rt_ttl -= delta) <=0)
		if(dorip && prt->rt_metric < RTM_INF) {
			prt->rt_metric = RTM_INF;
			prt->rt_ttl = RIPZTIME;
		} else {
			RTFREE(Route.ri_default);
			Route.ri_default = NULL;
		}
	Semaphore_Signal(&Route.ri_mutex);
	if(dorip && ripnotify && riptask) {
		Buffer	*B = BufferAlloc(1);

		memcpy(&B->data, "1", 1);
		MessageBoxSendBuf(&riptask->Messages, B);

	}
}

int
rtadd(IPaddr net, IPaddr mask, IPaddr gw, int metric, int intf, int ttl)
{
	struct route	*prt, *srt, *prev;
	Bool		isdup;
	int		hv, i, j;

	if(!Route.ri_valid)
		rtinit();
	prt = rtnew(net, mask, gw, metric, intf, ttl);
	if(!prt)
		return(SYSERR);
#if	IDEBUG
	{
		char buff[128];

		if(Debug > 1) {
			RouteInfo(buff,prt);
			Syslog(LOG_DEBUG,"route add %s",buff);
		}
	}
#endif

	for(prt->rt_key=0, i=0; i < IP_ALEN; i++)
		for(j=0; j < 8; j++)
			prt->rt_key += (mask[i] >> j) & 1;
	Semaphore_Wait(&Route.ri_mutex);

	if(!memcmp(net, RT_DEFAULT, IP_ALEN)) {
		if(Route.ri_default)
			RTFREE(Route.ri_default);
		Route.ri_default = prt;
		Semaphore_Signal(&Route.ri_mutex);
		return OK;
	}

	prev = NULL;
	hv = rthash(net);
	isdup = 0;
	for(srt = rttable[hv]; srt; srt=srt->rt_next) {
		if(prt->rt_key > srt->rt_key)
			break;
		if(!memcmp(srt->rt_net, prt->rt_net, IP_ALEN) &&
		   !memcmp(srt->rt_mask, prt->rt_mask, IP_ALEN)) {
			isdup = 1;
			break;
		}
		prev = srt;
	} /* end for srt */
	if(isdup) {
		struct route *tmprt;

		if(!memcmp(srt->rt_gw, prt->rt_gw, IP_ALEN)) {
			/* update the existing route */
			if(dorip) {
				srt->rt_ttl = ttl;
				if(srt->rt_metric != metric){
					Buffer *B = BufferAlloc(1);

					memcpy(B->data,"1",1);
					MessageBoxSendBuf(&riptask->Messages, B);
					if(metric == RTM_INF)
						srt->rt_ttl = RIPZTIME;

				}

			} /* end if dorip */
			srt->rt_metric = metric;
			RTFREE(prt);
			Semaphore_Signal(&Route.ri_mutex);
			return(OK);
		} /* end if memcmp */
		/* else there's another route there */
		if(srt->rt_metric <= prt->rt_metric) {
			/* drop new one, not shorter */
			RTFREE(prt);
			Semaphore_Signal(&Route.ri_mutex);
			return(OK);
		}
		else if (dorip) {
			Buffer 	*B = BufferAlloc(1);
			memcpy(B,"1",1);
			MessageBoxSendBuf(&riptask->Messages, B);
		}
		tmprt = srt;
		srt = srt->rt_next;
		RTFREE(tmprt);
	} /* end if isdup */
	else if(dorip) {
		Buffer 	*B = BufferAlloc(1);
		memcpy(B,"1",1);
		MessageBoxSendBuf(&riptask->Messages, B);
	}
	prt->rt_next = srt;
	if(prev)
		prev->rt_next = prt;
	else
		rttable[hv] = prt;
	Semaphore_Signal(&Route.ri_mutex);
	return(OK);
}

struct route *
rtnew(IPaddr net, IPaddr mask, IPaddr gw, int metric, int intf, int ttl)
{
	struct route	*prt;

	prt = (struct route *) QueuePHead(&Route.ri_bpool);
	if(!prt)
		return(NULL);
	memcpy(prt->rt_net, net, IP_ALEN);
	memcpy(prt->rt_mask, mask, IP_ALEN);
	memcpy(prt->rt_gw, gw, IP_ALEN);
	prt->rt_metric = metric;
	prt->rt_ifnum = intf;
	prt->rt_ttl = ttl;
	prt->rt_refcnt = 1;
	prt->rt_usecnt = 0;
	prt->rt_next = NULL;
	return(prt);
}

int
rtdel(IPaddr net, IPaddr mask)
{
	struct route	*prt, *prev;
	int		hv, i;
	int	didsome = 0;

	if(!Route.ri_valid)
		return(SYSERR);
	Semaphore_Wait(&Route.ri_mutex);
	if(Route.ri_default &&
		!memcmp(net, Route.ri_default->rt_net, IP_ALEN)) {
			RTFREE(Route.ri_default);
			Route.ri_default = NULL;
			Semaphore_Signal(&Route.ri_mutex);
			return(OK);
	}
again:;
	hv = rthash(net);
	prev = NULL;

	for(prt = rttable[hv]; prt; prt = prt->rt_next) {
		if(xnetmatch(net,prt->rt_net, prt->rt_mask,0))	// 9/11/95 bkc
			break;

		if(!memcmp(net, prt->rt_net, IP_ALEN) &&
		   !memcmp(mask, prt->rt_mask, IP_ALEN))
			break;
		prev = prt;
	} /* end for prt */
	if(!prt) {
		Semaphore_Signal(&Route.ri_mutex);
#if	IDEBUG
	{
		char xnet[32];

		strcpy(xnet,IP_ntoa(net));


		if(!didsome)
			Syslog(LOG_DEBUG,"route del %s mask %s not found",xnet,IP_ntoa(mask));
	}
#endif
		return(didsome ? OK : SYSERR);
	} else
		didsome++;

	if(prev)
		prev->rt_next = prt->rt_next;
	else
		rttable[hv] = prt->rt_next;
#if	IDEBUG
	{
		char buff[128];

		if(Debug > 1) {
			RouteInfo(buff,prt);
			Syslog(LOG_DEBUG,"route del %s",buff);
		}
	}
#endif

	RTFREE(prt);
	goto again;

	Semaphore_Signal(&Route.ri_mutex);
	return(OK);
}

int
rtfree(struct route *prt)
{
	if(!Route.ri_valid)
		return(SYSERR);
	Semaphore_Wait(&Route.ri_mutex);
	RTFREE(prt);
	Semaphore_Signal(&Route.ri_mutex);
	return(OK);
}


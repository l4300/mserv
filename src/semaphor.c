#include <alloc.h>
#include <stdlib.h>
#include <dos.h>
#include <mem.h>

#include "semaphor.h"
#include "task.h"

#include	<memcheck.h>

#define	INTR	512

Semaphore	*
Semaphore_Alloc(int count)
{
	Semaphore	*S;

	S = calloc(1, sizeof(Semaphore));
	if(S)
		return(NULL);
	Semaphore_Initialize(S, count);
	return(S);
}

int
Semaphore_Initialize(Semaphore *S, int count)
{
	if(!S)
		return(SEM_RESULT_BADSEM);
	memset(S, 0, sizeof(Semaphore));
	S->sem_count = count;
	S->sem_waitQueue.queue_flags |= QUEUE_FLAGS_NOTSORTED;
	return(SEM_RESULT_OK);
}


int
Semaphore_Signal(Semaphore *S)
{
	unsigned	flags = _FLAGS;

	if(!S)
		return(SEM_RESULT_BADSEM);
	disable();
	if(S->sem_count++ < 0) {	/* someone was waiting */
		Task	*T = (Task *) QueuePHead(&S->sem_waitQueue);
		if(!T) {		/* bad news */
			if(flags & INTR)
				enable();
			return(SEM_RESULT_BADSEM);
		}
		TaskReady(T, SEM_RESULT_OK, TASK_RESCHEDULE);
	}
	if(flags & INTR)
		enable();
	return(SEM_RESULT_OK);
}

int
Semaphore_ISignal(Semaphore *S)	/* signal from interrupt */
{
	if(!S)
		return(SEM_RESULT_BADSEM);

	if(S->sem_count++ < 0) {	/* someone was waiting */
		Task	*T = (Task *) QueuePHead(&S->sem_waitQueue);
		if(!T)		/* bad news */
			return(SEM_RESULT_BADSEM);
		TaskReady(T, SEM_RESULT_OK, TASK_NORESCHEDULE);
	}

	return(SEM_RESULT_OK);
}

#if	SEMDEBUG
int
Semaphore_XWait(Semaphore *S, char *f, int l)
#else
int
Semaphore_Wait(Semaphore *S)
#endif
{
	int	rc = SEM_RESULT_OK;
	unsigned	flags	= _FLAGS;
#if	SEMDEBUG
	char	*tokens[5];
	char	lnum[5];
#endif
	if(!S)
		return(SEM_RESULT_BADSEM);
	disable();
	if(--S->sem_count < 0) {	/* we're blocked */
		Task	*T = Task_Current_Task;

		if(!T)
			panic("No current task in sem wait");
		T->tsk_state = TASK_STATE_SEM;
#if	SEMDEBUG
		tokens[0] = f;
		tokens[1] = lnum;
		sprintf(lnum,"%d",l);
		tokens[2] = NULL;
		T->tsk_args = tokens;
#endif
		QueueInsert(&S->sem_waitQueue, (Queue_Element *) T);
		rc = TaskReschedule();
	}
	if(flags & INTR)
		enable();
	return(rc);
}

int
Semaphore_Cancel(Semaphore *S)		/* cancel all jobs waiting */
{
	Task	*T;
	unsigned	flags = _FLAGS;

	if(!S)
		return(SEM_RESULT_BADSEM);
	disable();
	while((T = (Task *) QueuePHead(&S->sem_waitQueue)) != 0) {
		TaskReady(T, SEM_RESULT_BADSEM, TASK_NORESCHEDULE);
		++S->sem_count;
	}
	if(flags & INTR)
		enable();
	return(SEM_RESULT_OK);
}

int
Semaphore_Free(Semaphore *S)
{
	int	rc;

	if(!S)
		return(SEM_RESULT_BADSEM);
	if((rc = Semaphore_Cancel(S)) == SEM_RESULT_OK)
		free(S);
	return(rc);
}

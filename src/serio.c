// serio.c - serial I/O for MicroServ, Copyright (C) 1993 MurkWorks, Inc.
// All Rights Reserved

#define	STATIC
#define	SYSLOG_SERIAL		0	// true to show syslog debug infp

#define	INTR	512

#include	"network.h"
#include	"sys/errno.h"
#include	"sys/socket.h"
#include	"ioint.h"
#include	"task.h"
#include	"syslog.h"

#include	<dos.h>			// get enable()

#if	!SIM
#include	"v25.h"

#define	SRXB(I)		SFR_PTR[(I)->io_base_address + 0x0]
#define	STXB(I)		SFR_PTR[(I)->io_base_address + 0x2]
#define SSRMS(I)	SFR_PTR[(I)->io_base_address + 0x5]
#define	SSCM(I)		SFR_PTR[(I)->io_base_address + 0x8]
#define	SSCC(I)		SFR_PTR[(I)->io_base_address + 0x9]
#define	SBRG(I)		SFR_PTR[(I)->io_base_address + 0xa]
#define	SSCE(I)		SFR_PTR[(I)->io_base_address + 0xb]
#define	SSEIC(I)	SFR_PTR[(I)->io_base_address + 0xc]
#define	SSRIC(I)	SFR_PTR[(I)->io_base_address + 0xd]
#define	SSTIC(I)	SFR_PTR[(I)->io_base_address + 0xe]

#endif

#define	MS_SERIAL_RECV	0
#define	MS_SERIAL_SEND	1

typedef	struct _serial_info {
	unsigned char	si_xchar;	// on next int, xmit this char if non-zero
	unsigned char	si_count;	// last count when we checked ms receive
} SerialInfo;
int	iosocket_updateInput(IO_Port *I, int count);

SerialInfo	ASI;

STATIC void
enable_ser_interrupt(IO_Port *I, void interrupt (*recvisr)(), void interrupt (*sendisr)())
{
#if	SIM
	unsigned int imask;

	disable();
	I->io_p1 = getvect(I->io_interrupt+8);
	setvect(I->io_interrupt+8, isr);
	imask = inportb(0x21);
	imask &= ~(1 << I->io_interrupt);
	outportb(0x21, imask);
#else
	int	x;

	disable();
	for(x=0; x < 2; x++)
		INT_PTR[I->io_interrupt+x] = recvisr;

	INT_PTR[I->io_interrupt+2] = sendisr;

	SSEIC(I) = 0x40|SERIAL_VECTOR;
	SSRIC(I) = SERIAL_VECTOR;
	SSTIC(I) = SERIAL_VECTOR;
#endif
	enable();
}

STATIC void
disable_ser_interrupt(IO_Port *I)
{
#if	SIM
	unsigned int imask;

	disable();
	setvect(I->io_interrupt+8, (void interrupt (*)()) I->io_p1);
	imask = inportb(0x21);
	imask |= (1 << I->io_interrupt);
	outportb(0x21, imask);
#else
	int	x;

	disable();
	SSEIC(I) |= 0x40;
	SSRIC(I) |= 0x40;
	SSTIC(I) |= 0x40;
#endif
	enable();

}

#define	SERFLAG_RECV		1
#define	SERFLAG_SEND		2

void
serial_kickstart(IO_Port *I)
{
	SerialInfo	*SI = (SerialInfo *) I->io_p1;
	unsigned flags = _FLAGS;

	if(I->io_devflags & IODEVFLAG_IOACTIVE) 	/* already transmitting */
		return;

	if(SI->si_xchar) {
		disable();
		STXB(I) = SI->si_xchar;
		SI->si_xchar = 0;
		if(flags & INTR)
			enable();
		return;
	}
	if(!I->io_occount) {
		if(iosocket_nextobuffer(I))
			return;
	}
	if(I->io_intfmodes & IO_IFMODE_FLOW_RTS) { /* held off still */
		if((SFR_PTR[SFR_PT] & 1) /*&& I->io_base_address == SFR_RXB0*/) {
			I->io_devflags |= IODEVFLAG_HELDOFF|IODEVFLAG_NEEDTIMER;
			I->io_timeout = 1;
			return;
		}
	}
	if(I->io_intfmodes & IODEVFLAG_HELDOFF)
		return;	/* xoff was received */
		/* get here, we can xmit a char */

	if(I->io_intfmodes & IO_IFMODE_FLOW_XON) {
		while(((*I->io_ochar == 17) || (*I->io_ochar == 19)) &&
			I->io_occount) {
			I->io_ochar++;
			I->io_occount--;
			if(!I->io_occount) {
				if(iosocket_nextobuffer(I))
					return;
			}
		}
	}
	if(!I->io_occount)
		return;
	disable();
	STXB(I) = *I->io_ochar;
	I->io_ochar++;
	I->io_occount--;
	I->io_devflags |= IODEVFLAG_IOACTIVE;
	if(flags & INTR)
		enable();

}

#if	SIM
void
serial_interrupt(IO_Port *I, int serflags)	/* general serial int service */
{
#if	SIM
	int	iir = inportb(port+2);
	int	lsr = inportb(port+5);
	int	msr = inportb(port+6);
#else
	int	dorms = SSRIC(I) & 0x20;
	int	dotms = SSTIC(I) & 0x20;
#endif
	int	kickstart = 0;

#if	SIM
	if(lsr & 1)	{ 	/* received a character */
		unsigned int c = inportb(port);
#else
	if(SSRIC(I) & 0x80 || serflags & SERFLAG_RECV) {
		unsigned int c = SRXB(I);
#endif
		if(I->io_intfmodes & IO_IFMODE_FLOW_XON) {
			if(c == 17) {	/* ^Q we can restart */
				if(I->io_devflags & IODEVFLAG_HELDOFF) {
					I->io_devflags &= ~IODEVFLAG_HELDOFF;
					kickstart++;
				}
				goto steptwo;
			} else if(c == 19) {	/* ^S stop please */
				I->io_devflags |= IODEVFLAG_HELDOFF;
				goto done;
			}
		}
		if(iosocket_postinput(I,c)) {	/* hold off please */
			if(I->io_intfmodes & IO_IFMODE_FLOW_RTS) {
				/* turn off dtr, etc */
#if	SIM
				outportb(port+4,inportb(port+4) | 1); /* dtr off */
#else
				if(I->io_base_address == SFR_RXB0)
					SFR_PTR[SFR_P2] |= 0x10;
#endif
				I->io_devflags |= IODEVFLAG_NEEDRESTART;
			}
			if(I->io_intfmodes & IO_IFMODE_FLOW_XON) {
				/* must xmit ^S, but not allow transmit part
				   to also send during this interrupt */
#if	SIM
				outportb(port,19);
#else
				STXB(I) = 19;
#endif
				I->io_devflags |= IODEVFLAG_NEEDRESTART;
				goto done;
			}
		}
		if((I->io_intfmodes & IO_IFMODE_FLOW_RTS) &&
			(I->io_devflags & IODEVFLAG_HELDOFF)) { /* check dtr to see if we can send again */
			/* if dsr is now enabled,  we can transmit */
#if	SIM
			if(!(msr & 32))	{	/* dsr is high */
#else
			if((SFR_PTR[SFR_PT] & 1) && I->io_base_address == SFR_RXB0) {
#endif
				I->io_devflags &= ~IODEVFLAG_HELDOFF;
				kickstart = 1;
			}
		}
	}

steptwo:;
#if	SIM
	if(iir & 2 || lsr & 32 || kickstart) {
#else
	if(kickstart || (serflags & SERFLAG_SEND) || SSTIC(I) & 0x80) {
#endif
		if((I->io_intfmodes & IO_IFMODE_FLOW_RTS)) {
#if	SIM
			if(msr & 32) {	/* flow off */
#else
			if((SFR_PTR[SFR_PT] & 1) /* && I->io_base_address == SFR_RXB0 */) {
#endif
				I->io_devflags |= IODEVFLAG_HELDOFF|IODEVFLAG_NEEDTIMER;
				I->io_timeout = 1;
				goto done;
			}
			else {	/* flow on */
				if((I->io_devflags & IODEVFLAG_HELDOFF) &&
				   (I->io_intfmodes & IO_IFMODE_FLOW_XON)) {	/* only set by xoff */
					goto done;
				}
				I->io_devflags &= ~IODEVFLAG_HELDOFF;
			}
		}
		if(I->io_intfmodes & IO_IFMODE_FLOW_XON) {
			if(I->io_devflags & IODEVFLAG_HELDOFF)
				goto done;
			while((*I->io_ochar == 19 ||
				*I->io_ochar == 17) && I->io_occount) {
					I->io_occount--;
					I->io_ochar++;
					if(!I->io_occount)
						iosocket_nextobuffer(I);
			}
		}
		if(!I->io_occount) {
			iosocket_nextobuffer(I);
		}
		if(I->io_occount) {
#if	SIM
			outportb(port, *I->io_ochar++);
#else
			STXB(I) = *I->io_ochar++;
#endif
			I->io_occount--;
		}
	}	/* end if can transmit */
done:;
#if	SIM
	outportb(0x20,0x20);
#endif
}

#endif



void
serial_Recv_interrupt(IO_Port *I)	/* general serial int service */
{
	unsigned int 	c = SRXB(I);
	int		kickstart = 0;
	int		rc = 0;
	SerialInfo	*SI = (SerialInfo *) I->io_p1;
	MSF		*msf = &IDA_PTR->msf.MRegister[MS_SERIAL_RECV];
	int		count = 0;
	int		x;
	char far	*z;

	if(I->io_devflags & IODEVFLAG_MSRECV) {
		count = msf->m_offset - FP_OFF(I->io_ichar);
		SSRIC(I) &= ~0x20;	// clear macro service mode
		I->io_devflags &= ~IODEVFLAG_MSRECV;
	}
	if(I->io_intfmodes & IO_IFMODE_FLOW_XON) {
		switch (c) {
			case 17:
				if(I->io_devflags & IODEVFLAG_HELDOFF) {
					I->io_devflags &= ~IODEVFLAG_HELDOFF;
					kickstart++;
				}
				msf->m_search_character = 19;
				if(count <= 1)
					goto done;
				count--;
				break;
			case 19:
				if(!(I->io_devflags & IODEVFLAG_HELDOFF)) {
					/* disable interrupts, clear xmit ms */
					msf->m_search_character = 17;
					I->io_devflags |= IODEVFLAG_HELDOFF;
				}
				if(count <= 1)
					goto done;
				count--;
				break;
		} // end switch
	}	// end flow method


	if(count) {
		x = I->io_iccount - count;

		if(x < 1) {	// next char will be stored in free buffer
			Buffer	*B = (Buffer *) QueueHead(&I->io_fbuffers);
			if(B) {
				z = B->QE.queue_object;
				x = B->QE.queue_bufsize;
			} else
				x = 0;
		} else
			z = I->io_ichar + count;
		if(x > 0) {

			if(x > 255)
				x = 0;
			msf->m_offset = FP_OFF(z);
			msf->m_segment = FP_SEG(z);
			msf->m_bytecount = x;

			SSRIC(I) |= 0x20;
			if(!I->io_timeout)
				I->io_timeout = 10;
			I->io_devflags |= (IODEVFLAG_NEEDTIMER|IODEVFLAG_MSRECV);
		}
		rc = iosocket_updateInput(I, count);
	} else
		rc = iosocket_postinput(I,c);

done:;
	if(!(I->io_devflags & IODEVFLAG_MSRECV) && I->io_iccount > 1 && I->io_ichar &&
			!(I->io_devflags & IODEVFLAG_CLOSED)) {	// recv mode and have buffers
		int	bcount = I->io_iccount;

		if(bcount > 255)
			bcount = 0;
		msf->m_offset = FP_OFF(I->io_ichar);
		msf->m_segment = FP_SEG(I->io_ichar);
		msf->m_bytecount = bcount;
		SSRIC(I) |= 0x20;
		if(!I->io_timeout)
			I->io_timeout = 10;
		I->io_devflags |= (IODEVFLAG_NEEDTIMER|IODEVFLAG_MSRECV);
	}

	if(rc < 0 || !I->io_fbuffers.queue_count && !(I->io_devflags & IODEVFLAG_CLOSED)) {	/* hold off please */
		if(I->io_intfmodes & IO_IFMODE_FLOW_RTS) {
			/* turn off dtr, etc */
			if(I->io_base_address == SFR_RXB0)
				SFR_PTR[SFR_P2] |= 0x10;
			I->io_devflags |= IODEVFLAG_NEEDRESTART;
		}
		if(I->io_intfmodes & IO_IFMODE_FLOW_XON) {
			/* must xmit ^S, but not allow transmit part
			   to also send during this interrupt */
			SI->si_xchar = 19;
			kickstart++;
			I->io_devflags |= IODEVFLAG_NEEDRESTART;
		}
	}

out:;
	if(kickstart)
		serial_kickstart(I);

}


void
serial_Xmit_interrupt(IO_Port *I)	/* general serial int service */
{
	int	dorms = SSRIC(I) & 0x20;
	SerialInfo	*SI = (SerialInfo *) I->io_p1;


	if(SI->si_xchar) {
		STXB(I) = SI->si_xchar;
		SI->si_xchar = 0;
		return;
	}
	if((I->io_intfmodes & IO_IFMODE_FLOW_RTS)) {
		if((SFR_PTR[SFR_PT] & 1) /* && I->io_base_address == SFR_RXB0 */) {
			I->io_devflags |= (IODEVFLAG_HELDOFF|IODEVFLAG_NEEDTIMER);
			I->io_timeout = 1;
			return;
		}
		else {	/* flow on */
			if((I->io_devflags & IODEVFLAG_HELDOFF) &&
			   (I->io_intfmodes & IO_IFMODE_FLOW_XON)) {	/* only set by xoff */
				return;
			}
			I->io_devflags &= ~IODEVFLAG_HELDOFF;
		}
	}
	if(I->io_intfmodes & IO_IFMODE_FLOW_XON) {
		if(I->io_devflags & IODEVFLAG_HELDOFF)
			return;
		while((*I->io_ochar == 19 ||
			*I->io_ochar == 17) && I->io_occount) {
				I->io_occount--;
				I->io_ochar++;
				if(!I->io_occount)
					iosocket_nextobuffer(I);
		}
	}
	if(!I->io_occount) {
		iosocket_nextobuffer(I);
	}
	if(I->io_occount) {
		STXB(I) = *I->io_ochar++;
		I->io_occount--;
	}
}



STATIC	IO_Port	*serial0_interrupt_ptr;

STATIC	void interrupt
serial0_recv_interrupt()
{
#if	DEBUG && SIM
	unsigned int far *x = (unsigned int far *) MK_FP(0xb800,2);
	*x += 0x0101;
#endif
#if	SIM
	serial_interrupt(serial0_interrupt_ptr, SERFLAG_RECV);
#else
	serial_Recv_interrupt(serial0_interrupt_ptr);
	FINT();
#endif
}

STATIC	void interrupt
serial0_send_interrupt()
{
#if	DEBUG && SIM
	unsigned int far *x = (unsigned int far *) MK_FP(0xb800,2);
	*x += 0x0101;
#endif
#if	SIM
	serial_interrupt(serial0_interrupt_ptr, SERFLAG_SEND);
#else
	serial_Xmit_interrupt(serial0_interrupt_ptr);
	FINT();
#endif
}



static int
iofunc_setserial(struct _io_port *I)
{

static struct	_serialspeed {
	unsigned int	s_speed;
	unsigned char	s_n;
	unsigned char	s_bgn;
} SerialSpeed[]={
	{300,7,130},
	{600,6,130},
	{1200,5,130},
	{2400,4,130},
	{4800,3,130},
	{9600,2,130},
	{19200,1,130},
	{38400,0,130},
	{57600,0,89},
	{115,0,43}
};

	int	x;

	disable();
	SSCM(I) &= ~0xc0;

#if	!SIM
	for(x=0; x < sizeof(SerialSpeed)/sizeof(struct _serialspeed); x++) {
		if(SerialSpeed[x].s_speed == I->io_speed) {
			SSCC(I) = SerialSpeed[x].s_n;
			SBRG(I) = SerialSpeed[x].s_bgn;
#if	SYSLOG_SERIAL
Syslog(LOG_DEBUG,"set sscc %u/%u bgn %u/%u at %x:%x",
		SerialSpeed[x].s_n, SSCC(I), SerialSpeed[x].s_bgn, SBRG(I),
			FP_SEG(&SBRG(I)), FP_OFF(&SBRG(I)));
#endif
			break;
		}
	}
	if(x == sizeof(SerialSpeed)/sizeof(struct _serialspeed)) {
		enable();
		return(-1);
	}
	SSCM(I) = (I->io_intfmodes & 0x3c) | (0x1);
#if	SYSLOG_SERIAL
Syslog(LOG_DEBUG,"setserial iospeed %u mode %x/%x",I->io_speed, I->io_intfmodes,
		SSCM(I));
#endif
#endif

	if(I->io_intfmodes & IO_IFMODE_FLOW_XON) {
		SSRMS(I)  = 0x80|0x10|MS_SERIAL_RECV;
	} else {
		SSRMS(I) = 0x10|MS_SERIAL_RECV;
	}

	SSCM(I) |= 0xc0;
	SRXB(I) = 0;
	enable();
	return(0);
}

int
iofunc_serial(struct _io_port *I, int mode, void *arg)
{
	int	rc = 0;
	struct	_sendto	*sendto = (struct _sendto *) arg;
	unsigned flags = _FLAGS;
	char	*c;
	SerialInfo	*SI = (SerialInfo *) I->io_p1;
	MSF		*msf = &IDA_PTR->msf.MRegister[MS_SERIAL_RECV];

	switch(mode) {		/* PC serial port */
		case IOFUNCTION_PROBE :	/* does device exist ? */
			I->io_p1	= &ASI;
			break;
		case IOFUNCTION_INIT :
			I->io_speed 	= 9600;
			I->io_intfmodes = IO_IFMODE_DATA8BIT|IO_IFMODE_FLOW_XON;
			I->io_ipost_process = I->io_opost_process = 0;
			if(I->io_unit_number == 0)
				serial0_interrupt_ptr = I;

			I->io_devflags |= IODEVFLAG_CLOSED;
			msf->m_sfr_register_pointer = SFR_RXB0;
			disable();
#if	SIM
			outportb(I->io_base_address+3, 3);	/* simulator ignores settings */
			outportb(I->io_base_address+1, 3); /* enable all ints */
			outportb(I->io_base_address+4, 3+8);

			outportb(I->io_base_address+3, 3);	/* simulator ignores settings */
			outportb(I->io_base_address+1, 3); /* enable all ints */
			outportb(I->io_base_address+4, 3+8);

#endif

			enable_ser_interrupt(I, serial0_recv_interrupt, serial0_send_interrupt);
			enable();
			goto setsockopt;
		case IOFUNCTION_OPEN :
			I->io_ibufsize = DEFAULT_IBUFSIZE;
			I->io_desired_modes = IOMODE_OUTPUT;	/* default mode */
			I->io_socket->so_readable = 0;
			I->io_socket->so_recvqueue = DEFAULT_IBYTELIMIT;
			I->io_socket->so_writeable = DEFAULT_OBYTELIMIT;
			I->io_devflags &= ~(IODEVFLAG_CLOSED|IODEVFLAG_HELDOFF|IODEVFLAG_NEEDRESTART);
			break;
		case IOFUNCTION_SETOPT :
			/* the iosocket option handler has already set
			   the IO_Port variables, we need only reconfigure
			   ourselves
			*/
			switch(sendto->s_flags) {
				case	IO_SOPT_IOMODE :
				case	IO_SOPT_IBSIZE :
				case	IO_SOPT_IBUFSIZE :
					if(I->io_desired_modes & IOMODES_INPUT) {
						iosocket_flushinput(I);
						rc = iosocket_prepinput(I);
					}
					else if(I->io_fbuffers.queue_count) {
						iosocket_flushinput(I);
					}
				default :;
			}
setsockopt:;
			rc = iofunc_setserial(I);
			break;
		case IOFUNCTION_CLOSE :
#if	SYSLOG_SERIAL
Syslog(LOG_DEBUG,"serial close");
#endif
			I->io_devflags |= IODEVFLAG_CLOSED;
			SSRIC(I) &= ~0x20;	// turn off macro service
			if(I->io_devflags & IODEVFLAG_NEEDRESTART)
				goto dorestart;
			break;
		case IOFUNCTION_SHUTDOWN :	/* opposite of init */
			disable_ser_interrupt(I);
			iosocket_flushinput(I);
			iosocket_flushoutput(I);
			break;
		case IOFUNCTION_STATUS :
			c = sendto->s_msg;
			c += (int) sprintf(c,"CTS in is %s, ", (SFR_PTR[SFR_PT] & 1) ? "Low" : "High");
			c += (int) sprintf(c,"DTR out is %s", (SFR_PTR[SFR_P2] & 0x10) ? "Low" : "High");
			rc = c - sendto->s_msg;
			break;
		case IOFUNCTION_KICKSTART :
kickstart:;
			serial_kickstart(I);
			break;
		case IOFUNCTION_TIMER :
			if(I->io_devflags & IODEVFLAG_HELDOFF) {
				if(I->io_intfmodes & IO_IFMODE_FLOW_RTS) {
#if	SIM
					if(!(inportb(I->io_base_address+6) & 32)) { /* flow on again */
#else
				if(!(SFR_PTR[SFR_PT] & 1)/* && I->io_base_address == SFR_RXB0*/) {
#endif
						I->io_devflags &= ~IODEVFLAG_HELDOFF;

						if(I->io_socket->so_readable && !(I->io_socket->so_selectflags & SOSEL_READ))
							sowakeup(I->io_socket, SOSEL_READ);
						serial_kickstart(I);
					}
					else {  /* need timer again */
						I->io_devflags |= IODEVFLAG_NEEDTIMER;
						I->io_timeout = 1;
					}
				}
			}
			if(I->io_devflags & IODEVFLAG_MSRECV) {
				int 	count;
				MSF	*msf = &IDA_PTR->msf.MRegister[MS_SERIAL_RECV];

				count = msf->m_offset - FP_OFF(I->io_ichar);

				if(SI->si_count == msf->m_bytecount) {

					disable();
					SSRIC(I) &= ~0x20;
					count = msf->m_offset - FP_OFF(I->io_ichar);
					I->io_devflags &= ~IODEVFLAG_MSRECV;
					if(count > 0)
						iosocket_updateInput(I,count);
					if(flags & INTR)
						enable();
				} else {
					SI->si_count = msf->m_bytecount;
					if(!I->io_timeout) {
						I->io_timeout = 10;
						I->io_devflags |= IODEVFLAG_NEEDTIMER;
					}
				}
#if	SYSLOG_SERIAL
Syslog(LOG_DEBUG,"mserv count %d bcount %d ibytecount %d",count, msf->m_bytecount,I->io_ibytecount);
#endif
			}
			if(I->io_socket->so_readable && !(I->io_socket->so_selectflags & SOSEL_READ))
				sowakeup(I->io_socket, SOSEL_READ);
			break;
		case IOFUNCTION_RESTART :
dorestart:;
			if(!(I->io_devflags & IODEVFLAG_NEEDRESTART))
				break;
			I->io_devflags &= ~IODEVFLAG_NEEDRESTART;
			if(I->io_intfmodes & IO_IFMODE_FLOW_RTS) {
#if	SIM
				outportb(I->io_base_address+4,
					inportb(I->io_base_address+4) &  ~1); /* dtr on */
#else
				if(I->io_base_address == SFR_RXB0)
					SFR_PTR[SFR_P2] &= ~0x10;
#endif
			}
			if(I->io_intfmodes & IO_IFMODE_FLOW_XON) {
				SI->si_xchar = 17;
				serial_kickstart(I);
			}

			break;
		default :;
			rc = -1;
	}
	return(rc);
}


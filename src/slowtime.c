/* slowtimer.c */

#include	"network.h"
#include	"task.h"
#include	"slowtime.h"
#include	"alarm.h"

#include	<time.h>
#include	<dos.h>
#include	<memcheck.h>

#pragma argsused

int
slowtimer(void *Parent, char *name, int nargs, char **argv)
{
	long	lasttime = Time(NULL);
	struct	timeval	timeout;
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;

	while(1) {
		int	delay;

		alarm_sleep(&timeout, 0);
		delay  = Time(NULL) - lasttime;

#if	NETDEBUGX
			printf("slowtime %d\n",delay);
#endif
		lasttime  = Time(NULL);
		arptimer(delay);
		ipftimer(delay);
		rttimer(delay);
		netwatchdog(delay);

	}
}

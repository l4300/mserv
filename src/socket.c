/* socket.c */
#define	STATIC

#define	INTR	512

#include	<dos.h>
#include	<mem.h>

#include 	"network.h"
#include	"alarm.h"
#include	"sys/socket.h"
#include	"netinet/in.h"
#include	"sys/errno.h"

#include	<memcheck.h>

struct	_protosw {
	unsigned int	pr_family;
	unsigned int	pr_protocol;
	unsigned int	pr_option;
	proto_function	pr_proto;
};

extern	int	udp_proto(struct _socket *sock, int pr_proto, void *data);
#if	!IPDOOR
extern	int	tcp_proto(struct _socket *sock, int pr_proto, void *data);
#endif
#define	AF_DUMMY	0xffe0

STATIC	struct _protosw	protos[]={
		{AF_INET, SOCK_DGRAM, IPPROTO_UDP, udp_proto },
#if	!IPDOOR
		{AF_INET, SOCK_STREAM, IPPROTO_TCP, tcp_proto },
#endif
		{AF_INET, SOCK_DGRAM, 0, udp_proto }, /* the default */
#if	!IPDOOR
		{AF_INET, SOCK_STREAM, 0, tcp_proto },
#endif
		{AF_DUMMY,0,0,NULL},
		{AF_DUMMY,0,0,NULL},
		{AF_DUMMY,0,0,NULL},
		{AF_DUMMY,0,0,NULL},
		{AF_DUMMY,0,0,NULL},
		{AF_DUMMY,0,0,NULL},
		{AF_DUMMY,0,0,NULL},
		{AF_DUMMY,0,0,NULL},
		{AF_DUMMY,0,0,NULL}
};

STATIC	struct	_socket	_sockets[MAX_SOCKETS];
STATIC	int	sockets_in_use;
STATIC	struct	sockaddr	sa_null;	/* null sockaddr */

#define	FREESOCKETS	(MAX_SOCKETS - sockets_in_use)

int
soregister(unsigned int family, unsigned int protocol, unsigned int opt, proto_function proto)
{
	int	x;

	for(x=0; x < sizeof(protos)/sizeof(struct _protosw); x++) {
		if(protos[x].pr_family == AF_DUMMY) {
			protos[x].pr_family = family;
			protos[x].pr_protocol = protocol;
			protos[x].pr_option = opt;
			protos[x].pr_proto = proto;
			return(0);
		}
	}
	return(-1);
}


/* find the first socket which matches the local and remote address
*/

int
socktohandle(struct _socket *so)
{
	return((so - _sockets));
}

struct	_socket
*sofind(struct sockaddr *local, struct sockaddr *remote, int len, int protocol)
{
	int	x, y;
	struct	sockaddr	*match;
	struct	_socket		*so,*sm;

	match = local ?local : remote;
	if(!match)
		return(NULL);
	so = _sockets;
	sm = NULL;
	for(x=0; x < MAX_SOCKETS; x++, so++) {
		if((so->so_state < SOSTATE_BOUND) || (so->so_state > SOSTATE_LISTENING) ||
			match->sa_family != so->so_family || protocol != so->so_protocol)
			continue;
		if(local) {
			struct sockaddr_in *xlocal,*xso;

			xlocal = local;
			xso = &so->so_local;
			if(xlocal->sin_port != xso->sin_port)
				continue;	// if ports don't match, skip

			if(memcmp(xso->sin_addr.s_ip,ip_maskall,IP_ALEN) &&
			   memcmp(local, &so->so_local, len)) // if local isn't broadcast and not match, skip
				continue;
		}
		if(remote) {
			if(so->so_state < SOSTATE_CONNECTED)
				sm = so;
			if(memcmp(remote, &so->so_remote, len))
				continue;
		}
		return(so);
	}
	return(sm);
}

struct	_socket
*sohtosock(int sock)
{
	if(sock < 0 || sock >= MAX_SOCKETS)
		return(NULL);
	if(!_sockets[sock].so_state)
		return(NULL);
	return(&_sockets[sock]);
}

STATIC	struct _socket *
soalloc()
{
	int	x;
	struct	_socket	*so = _sockets;

	for(x=0; x < MAX_SOCKETS; x++, so++)
		if(!so->so_state)
			break;
	if(x == MAX_SOCKETS) {
		return(NULL);
	}
	memset(so, 0, sizeof(struct _socket));
	so->so_state = SOSTATE_ALLOCATED;
	sockets_in_use++;
	return(so);
}

void
socketreset(struct _socket *s)
{
	memset(s, 0, sizeof(struct _socket));
}

STATIC	void
sofree(struct _socket *so)
{
	if(so) {
		so->so_state = SOSTATE_NONE;
		while(so->so_listencount)
			sofree(so->so_reserved[--so->so_listencount]);
		while(so->so_readycount)
			sofree(so->so_ready[--so->so_readycount]);
		while(so->so_childcount) {
			so->so_children[--so->so_childcount]->so_parent = NULL;
		}
		if(so->so_parent && so->so_parent->so_state == SOSTATE_LISTENING) {
			struct _socket *s = so->so_parent;

			s->so_reserved[s->so_listencount++] = so;
			s->so_childcount--;
			socketreset(so);
			so->so_parent = s;
			so->so_state = SOSTATE_ALLOCATED;
		}
		else {
			sockets_in_use--;
			socketreset(so);
			so->so_state = SOSTATE_NONE;
		}
	}
}

int
setsockopt(int sock, int level, int opt, char *optval, int optlen)
{
	struct	_socket	*s = sohtosock(sock);
	int	*i	= (int *) optval;
	int	rc = 0;
	int	x;
	struct	_sendto	st;

	st.s_msg = optval;
	st.s_flags = opt;
	st.s_len = optlen;
	st.s_tolen = level;

	if(!s) {
blastoff:;
		SETERROR(EPROTO);
		return(-1);
	}
	switch(level) {
		case SOL_SOCKET :
			switch(opt) {
				case SOOPT_SETOPT :
					if(*i)
						s->so_opt = *i;
					break;
				default :
					SETERROR(EINVAL);
					rc = -1;
					break;
			}
			break;
#ifdef	OLD
		case IPPROTO_TCP :
			rc = tcp_proto(s, PR_SETSOCKOPT, &st);
			break;
#endif
		default :;
			for(x=0; protos[x].pr_family != AF_DUMMY; x++) {
				if(s->so_family == protos[x].pr_family &&
				   s->so_protocol == protos[x].pr_protocol &&
				   s->so_popt == protos[x].pr_option) {
					rc = protos[x].pr_proto(s, PR_SETSOCKOPT, &st);
					return(rc);
				}
			}
			SETERROR(EINVAL);
			rc = -1;
			break;
	}
	return(rc);
}


int
getpeername(int sock, struct sockaddr *sa, int *namelen)
{
	struct _socket *s = sohtosock(sock);

	if(!s) {
bad:;
		SETERROR(EPROTO);
		return(-1);
	}

	if(*namelen < sizeof(s->so_remote))
		goto bad;

	memcpy(sa, &s->so_remote, sizeof(s->so_remote));
	*namelen = sizeof(s->so_remote);
	return(0);
}

int
getsockname(int sock, struct sockaddr *sa, int *namelen)
{
	struct _socket *s = sohtosock(sock);

	if(!s) {
bad:;
		SETERROR(EPROTO);
		return(-1);
	}

	if(*namelen < sizeof(s->so_local))
		goto bad;

	memcpy(sa, &s->so_local, sizeof(s->so_local));
	*namelen = sizeof(s->so_local);
	return(0);
}


int
getsockopt(int sock, int level, int opt, char *optval, int *optlen)
{
	struct	_socket	*s = sohtosock(sock);
	int	rc = 0;
	int	x;
	struct	_sendto	st;

	st.s_msg = optval;
	st.s_flags = opt;
	st.s_len = *optlen;

	if(!s) {
blastoff:;
		SETERROR(EPROTO);
		return(-1);
	}
	switch(level) {
		case SOL_SOCKET :
			switch(opt) {
				case SOOPT_GETOPT :
					if(optval)
						*optval = s->so_opt;
					break;
				case SOOPT_GETPEERNAME :
					if(optval && *optlen >= sizeof(s->so_remote)) {
						memcpy(optval, &s->so_remote, sizeof(s->so_remote));

					}
					else
						goto einval;
					break;
				case SOOPT_GETSOCKNAME :
					if(optval && *optlen >= sizeof(s->so_local)) {
						memcpy(optval, &s->so_local, sizeof(s->so_local));

					}
					else
						goto einval;
					break;
				default :
einval:;
					SETERROR(EINVAL);
					rc = -1;
					break;
			}
			break;
#ifdef	OLD
		case IPPROTO_TCP :
			rc = tcp_proto(s, PR_GETSOCKOPT, &st);
			break;
#endif
		default :;
			for(x=0; protos[x].pr_family != AF_DUMMY; x++) {
				if(s->so_family == protos[x].pr_family &&
				   s->so_protocol == protos[x].pr_protocol &&
				   s->so_popt == protos[x].pr_option) {
					rc = protos[x].pr_proto(s, PR_GETSOCKOPT, &st);
					return(rc);
				}
			}
			SETERROR(EINVAL);
			rc = -1;
			break;
	}
	return(rc);
}

int
socket(unsigned sa_family, unsigned sa_protocol, unsigned sa_opts)
{
	int	x, rc;
	struct	_socket	*so = _sockets;
	proto_function	pr;

	for(x=0; x < sizeof(protos)/sizeof(struct _protosw); x++) {
		if(protos[x].pr_family == sa_family &&
		   protos[x].pr_protocol == sa_protocol &&
		   protos[x].pr_option == sa_opts)
			break;
	}
	if(x == sizeof(protos)/sizeof(struct _protosw)) {
		SETERROR(EADDRNOTAVAIL);
		return(SYSERR);
	}
	pr = protos[x].pr_proto;

	so = soalloc();
	if(!so) {
		SETERROR(ENOSR);
		return(SYSERR);
	}
	so->so_family = sa_family;
	so->so_protocol = sa_protocol;
	so->so_proto = pr;
	so->so_popt = sa_opts;
	rc = (so->so_proto)(so, PR_OPEN, NULL);
	if(rc < 0) {
		sofree(so);
		return(rc);
	}
	return(socktohandle(so));
}

int
bind(int sock, struct sockaddr *sa, int len)
{
	struct	_socket	*s = sohtosock(sock);
	struct	_socket	*sp;
	int	rc;

	if(!s) {
		SETERROR(EBADF);
		return(SYSERR);
	}
	if(s->so_state > SOSTATE_ALLOCATED) {
		SETERROR(EBADF);
		return(SYSERR);	/* already bound */
	}

	sp = sofind(sa, NULL, len, s->so_protocol);
	if(sp) {	/* already bound */
		SETERROR(EADDRINUSE);
		return(SYSERR);
	}
	/* otherwise, try and bind it */
	rc = (s->so_proto)(s, PR_BIND, sa);
	if(rc)
		return(rc);
	/* otherwise, lets finish up */
	memcpy(&s->so_local, sa, len);
	s->so_state = SOSTATE_BOUND;
	return(0);
}

int
listen(int sock, int count)
{
	struct	_socket	*so = sohtosock(sock);
	int	x;
	int	rc;

	if(!so) {
		SETERROR(EBADF);
		return(SYSERR);
	}
	if(so->so_state != SOSTATE_BOUND) {
		SETERROR(EBADF);
		return(SYSERR);
	}

	if(count > FREESOCKETS || count > SOMAXLISTEN) {
		SETERROR(ENOSR);
		return(SYSERR);
	}

	/* otherwise, lets alloc some sockets */
	so->so_listencount = count;
	x = count;
	while(x)
		so->so_reserved[--x] = soalloc();

	rc = (so->so_proto)(so, PR_LISTEN, &x);
	if(rc) {
		/* bummer... should free up all reserved sockets */
		while(count)
			sofree(so->so_reserved[--count]);
		return(rc);
	}
	so->so_state = SOSTATE_LISTENING;
	so->so_opt |= SOOPT_ACCEPTCONN;
	return(0);
}

static
int
soselect(struct _socket *so, int flags, int reschedule)
{
	Task	*T = Task_Current_Task;
	struct _selectblock	*S = so->so_select;
	struct _selectblock	*Z = NULL;
	int		x = 0;

	while(x < MAX_SELECTS) {
		if(!S->sel_T && !Z) {
			Z = S;
		}
		if(S->sel_T == T) {
			S->sel_selectwait |= flags;
			goto out;
		}
		S++;
		x++;
	}
	if(Z) {
		Z->sel_T = T;
		Z->sel_selectwait = flags;
	}
	else
		return(-1);		/* too many selects */
out:;
	so->so_selectwait |= flags;
	if(reschedule) {
		T->tsk_state = TASK_STATE_SEL;
		return(TaskReschedule());
	}
	else
		return(0);
}

int
accept(int sock, struct sockaddr *sa, int *len)
{
	struct	_socket	*so = sohtosock(sock);
	Task	*T =Task_Current_Task;

	if(!so || so->so_state != SOSTATE_LISTENING) {
		SETERROR(EBADF);
		return(SYSERR);
	}

	if(!(so->so_opt & SOOPT_ACCEPTCONN) || *len < sizeof(struct sockaddr)) {
		SETERROR(EINVAL);
		return(SYSERR);
	}
retry:;
	if(so->so_readycount) {
		struct _socket	*sp = so->so_ready[--so->so_readycount];
		so->so_children[so->so_childcount++] = sp;
		sp->so_parent = so;
		memcpy(sa, &sp->so_remote, sizeof(struct sockaddr));
		*len = sizeof(struct sockaddr);
		if(!so->so_readycount)
			so->so_selectflags &= ~(SOSEL_LISTEN|SOSEL_READ);
		return(socktohandle(sp));
	}
	so->so_selectflags &= ~(SOSEL_LISTEN|SOSEL_READ);
	if(so->so_opt & SOOPT_NOBLOCK) {	/* say's don't block */
		SETERROR(EWOULDBLOCK);
		return(SYSERR);
	}
	/* otherwise, select on exception and block */
	/* ##### */
#ifdef	BAD
	if(so->so_select) {	/* yikes, someone already selecting.. */
		SETERROR(EPROTO);
		return(SYSERR);
	}
#endif
	soselect(so, SOSEL_LISTEN, 1);
	soclearsel(T);
	goto retry;
}

int
connect(int sock, struct sockaddr *addr, int len)
{
	struct	_socket	*so = sohtosock(sock);
	int	rc;

	if(!so || so->so_state < SOSTATE_ALLOCATED) {	// was less < bound
		SETERROR(EBADF);
		return(SYSERR);
	}

	if(so->so_state < SOSTATE_BOUND) {	// lets bind it
		struct sockaddr_in sin;

		memset(&sin, 0, sizeof(sin));
		rc = bind(sock, (struct sockaddr *) &sin, sizeof(sin));
		if(rc < 0)
			return(rc);

	}
	if(so->so_opt & SOOPT_CANTCONNECTMORE) {
		SETERROR(EISCONN);
		return(SYSERR);
	}

	if(len < sizeof(struct sockaddr)) {
		SETERROR(EINVAL);
		return(SYSERR);
	}

	/* ### do local bind if not yet bound */
	if(so->so_state < SOSTATE_BOUND) {
		SETERROR(EPROTO);
		return(SYSERR);
	}
	/* otherwise, let lower protocol have a chance first */
	rc = (so->so_proto)(so, PR_CONNECT, addr);
	if(rc) {
		return(rc);
	}
	/* connect was ok, lets set up local address */
	memcpy(&so->so_remote, addr, sizeof(struct sockaddr));
	if(!memcmp(addr, &sa_null, sizeof(struct sockaddr))) {
		if(!memcmp(&so->so_local, &sa_null, sizeof(struct sockaddr)))
			so->so_state = SOSTATE_ALLOCATED;
		else
			so->so_state = SOSTATE_BOUND;
	}
	else
		so->so_state = SOSTATE_CONNECTED;
	return(0);
}


int
shutdown(int sock, int how)
{
	struct	_socket	*so = sohtosock(sock);
	int	rc;

	if(!so) {
		SETERROR(EBADF);
		return(SYSERR);
	}
	if(how > SHUTDOWN_READWRITE || how < SHUTDOWN_READ) {
		SETERROR(EINVAL);
		return(SYSERR);
	}
	rc = (so->so_proto)(so, PR_SHUTDOWN, &how);
	if(rc < 0)
		return(rc);
	switch(how) {
		case 0:
			so->so_opt |= SOOPT_CANTRECVMORE;
			break;
		case 1:
			so->so_opt |= SOOPT_CANTSENDMORE;
			break;
		case 2: so->so_opt |= SOOPT_CANTSENDMORE|SOOPT_CANTRECVMORE;
			break;
	}
	return(rc);
}

int
soclose(int sock)
{
	struct _socket	*so = sohtosock(sock);
	int	rc;

	if(!so) {
		SETERROR(EBADF);
		return(SYSERR);
	}
	rc = (so->so_proto)(so, PR_CLOSE, NULL);
	/* ### do something about reuseaddr */
	sofree(so);
	return(rc);
}


int
sendto(int sock, unsigned char *msg, int len, int flags, struct sockaddr *to, int tolen)
{
	struct	_socket	*so = sohtosock(sock);
	struct	_sendto	sendto;
	int	rc;

	if(!len)
		return(0);
	if(!so || len < 0) {
		SETERROR(EBADF);
		return(SYSERR);
	}
	if(so->so_state < SOSTATE_BOUND || so->so_opt & SOOPT_CANTSENDMORE) {
		SETERROR(EPROTO);
		return(SYSERR);
	}
	sendto.s_msg = msg;
	sendto.s_len = len;
	sendto.s_flags = flags;
	sendto.s_to = to;
	sendto.s_tolen = tolen;
	rc = (so->so_proto)(so, PR_SENDTO, &sendto);
#ifdef	RESCHED
	TaskReschedule();		/* give IP a chance to run */
#endif
	return(rc);
}

int
sowrite(int sock, unsigned char *msg, int len)
{
	struct	_socket	*so = sohtosock(sock);
	struct	_sendto	sendto;
	int	rc;
	unsigned	flags = _FLAGS;

	if(!len)
		return(0);

	if(!so || len < 0) {
		SETERROR(EBADF);
		return(SYSERR);
	}
//	disable();
tryagain:;
	if(so->so_state < SOSTATE_BOUND || so->so_opt & SOOPT_CANTSENDMORE) {
		SETERROR(EPROTO);
bye:;
//		if(flags & INTR)
//			enable();
		return(SYSERR);
	}
	sendto.s_msg = msg;
	sendto.s_len = len;

	if(so->so_selectflags & SOSEL_WRITE) {
		rc = (so->so_proto)(so, PR_WRITE, &sendto);
//		if(flags & INTR)
//			enable();
		return(rc);
	}
	else {
		if(so->so_opt & SOOPT_NOBLOCK) {
			SETERROR(EWOULDBLOCK);
//			if(flags & INTR)
//				enable();
			goto bye;
		}
		/* otherwise, block forever */
		rc = soselect(so, SOSEL_WRITE, 1);
		goto tryagain;
	}
}

int
recvfrom(int sock, unsigned char *buf, int len, int flags, struct sockaddr *from, int *fromlen)
{
	struct	_socket	*so = sohtosock(sock);
	struct	_sendto	sendto;
	int	rc;

	if(!so) {
		SETERROR(EBADF);
		return(SYSERR);
	}
tryagain:;
	if(so->so_state < SOSTATE_BOUND || so->so_opt & SOOPT_CANTRECVMORE) {
		SETERROR(EPROTO);
		return(SYSERR);
	}
	sendto.s_msg = buf;
	sendto.s_len = len;
	sendto.s_flags = flags;
	sendto.s_to = from;
	sendto.s_fromlen = fromlen;

	if(so->so_selectflags & SOSEL_READ) {
		rc = (so->so_proto)(so, PR_RECVFROM, &sendto);
		return(rc);
	}
	else {

		if(so->so_opt & SOOPT_NOBLOCK) {
			SETERROR(EWOULDBLOCK);
			return(SYSERR);
		}
		/* otherwise, block forever */
		rc = soselect(so, SOSEL_READ, 1);
		goto tryagain;
	}
}

int
recv(int sock, unsigned char *buf, int len, int flags)
{
	int	xlen = 0;
	return(soread(sock, buf, len));
}


int
send(int sock, unsigned char *buf, int len, int flags)
{
	int	xlen = 0;
	return(sowrite(sock, buf, len));
}


int
soread(int sock, unsigned char *buf, int len)
{
	struct	_socket	*so = sohtosock(sock);
	struct	_sendto	sendto;
	int	rc;
	unsigned	flags	= _FLAGS;

	if(len < 1)
		return(len);

	if(!so) {
		SETERROR(EBADF);
		return(SYSERR);
	}
//	disable();
tryagain:;
	if(so->so_state < SOSTATE_BOUND || so->so_opt & SOOPT_CANTRECVMORE) {
		SETERROR(EPROTO);
bye:;
//		if(flags & INTR)
//			enable();
		return(SYSERR);
	}
	sendto.s_msg = buf;
	sendto.s_len = len;

	if(so->so_selectflags & SOSEL_READ) {
		rc = (so->so_proto)(so, PR_READ, &sendto);
//		if(flags & INTR)
//			enable();
		return(rc);
	}
	else {

		if(so->so_opt & SOOPT_NOBLOCK) {
			SETERROR(EWOULDBLOCK);
			goto bye;
		}
		/* otherwise, block forever */
		rc = soselect(so, SOSEL_READ, 1);
		goto tryagain;
	}
}

int
select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout)
{
	int	x;
	Task	*T = Task_Current_Task;
	struct	_socket	*so;
	fd_set	rd,wr,ex;
	int	socount = 0;
	int	ncount = nfds;
	int	rc;
	struct	timeval	xt;
	int	rounds = 0;
	unsigned flags = _FLAGS;

//	disable();

lookagain:;

	FD_ZERO(&rd);
	FD_ZERO(&wr);
	FD_ZERO(&ex);

	if(nfds > MAX_SOCKETS) {
		SETERROR(EINVAL);

		return(SYSERR);
	}
	for(x=0, so=_sockets; x < MAX_SOCKETS && ncount; x++, so++) {
		if(so->so_state < SOSTATE_BOUND ||
			(so->so_state < SOSTATE_CONNECTING &&
				so->so_protocol == IPPROTO_TCP))
			continue;
		if(readfds && FD_ISSET(x, readfds)) {	/* waiting to read here */
			if(so->so_selectflags & SOSEL_READ)
				FD_SET(x, &rd), socount++, ncount--;
		}
		if(writefds && FD_ISSET(x, writefds)) {	/* waiting to write */
			if(so->so_selectflags & SOSEL_WRITE)
				FD_SET(x, &wr), socount++, ncount--;
		}
		if(exceptfds && FD_ISSET(x, exceptfds)) {	/* exception condition */
			if(so->so_selectflags & (SOSEL_EXCEPT|SOSEL_LISTEN))
				FD_SET(x, &ex), socount++, ncount--;
		}
	}
	if(socount ||
		(timeout && timeout->tv_sec == 0 && timeout->tv_usec == 0)) {	/* we have something to return now */
		if(readfds)
			*readfds = rd;
		if(writefds)
			*writefds = wr;
		if(exceptfds)
			*exceptfds = ex;

		return(socount);
	}
	if(rounds) {

		return(0);		/* otherwise, we've already blocked ... */
	}
	/* otherwise, we block for timeval count */
	ncount = nfds;
	for(x=0, so=_sockets; x < MAX_SOCKETS && ncount; x++, so++) {
		if(so->so_state < SOSTATE_BOUND)
			continue;
		if(readfds && FD_ISSET(x, readfds)) {	/* waiting to read here */
			soselect(so, SOSEL_READ, 0);
		}
		if(writefds && FD_ISSET(x, writefds)) {	/* waiting to write */
			soselect(so, SOSEL_WRITE, 0);
		}
		if(exceptfds && FD_ISSET(x, exceptfds)) {	/* exception condition */
			soselect(so, SOSEL_EXCEPT|SOSEL_LISTEN, 0);
		}
	}
	/* all sockets are marked, set timer and task reschedule */

	/* DELAY */
	T->tsk_state = TASK_STATE_SEL;
	if(!timeout) {
		xt.tv_sec = 99999999;
		rc = alarm_sleep(&xt, ALRC_WAKEUP);
	} else
		rc = alarm_sleep(timeout, ALRC_WAKEUP);
	soclearsel(T);
	if(rc == ALRC_WAKEUP) {
//		if(flags & INTR)
//			enable();
		return(0);		/* alarm timed out */
	}
	/* otherwise, we have some data */
	rounds = 1;
	goto lookagain;

}


int
sowakeup(struct _socket *so, int whom)
{
	/* wake up this socket */
	int	wakeupmode = whom & SO_RESCHEDULE ? TASK_RESCHEDULE : TASK_NORESCHEDULE;

	if(!so)
		return(SYSERR);

	whom &= ~SO_RESCHEDULE;
	so->so_selectflags |= whom;
	if((so->so_selectwait & so->so_selectflags)) {	/* wake up somebody */
		struct _selectblock	*S = so->so_select;
		int			x = 0;
		Task			*T;

		while(x < MAX_SELECTS) {
			T = S->sel_T;
			if(T && (S->sel_selectwait & so->so_selectflags)) {
				S->sel_T = NULL;
				S->sel_selectwait = 0;
				if(T->tsk_state == TASK_STATE_SEL)
					TaskReady(T, whom, wakeupmode);
			}
			S++;
			x++;
		}
	}
	return(0);
}

int
soclearsel(Task *T)
{
	int	x;

	if(!T)
		return(SYSERR);

	for(x=0; x < MAX_SOCKETS; x++) {
		struct _selectblock  *S;
		int	y;
		int	selflags = 0;

		for(y = 0, S=_sockets[x].so_select; y < MAX_SELECTS; y++, S++) {
			if(S->sel_T == T) {
				S->sel_T = NULL;
				S->sel_selectwait = 0;
			}
			selflags |= S->sel_selectwait;
		}
		_sockets[x].so_selectwait = selflags;
	}
	return(0);
}


int
soreadbuf(int sock, struct _rwbuffer *rwb)	/* returns len of valid data returned */
{
	struct	_socket	*so = sohtosock(sock);
	int	rc;
	unsigned	flags = _FLAGS;

	if(!so) {
		SETERROR(EBADF);
		return(SYSERR);
	}
//	disable();
tryagain:;
	if(so->so_state < SOSTATE_BOUND || so->so_opt & SOOPT_CANTRECVMORE) {
		SETERROR(EPROTO);
//		if(flags & INTR)
//			enable();
		return(SYSERR);
	}
	if(so->so_selectflags & SOSEL_READ) {
		rc = (so->so_proto)(so, PR_READBUF, rwb);
//		if(flags & INTR)
//			enable();
		return(rc);
	}
	else {
		if(so->so_opt & SOOPT_NOBLOCK) {
			SETERROR(EWOULDBLOCK);
//			if(flags & INTR)
//				enable();
			return(SYSERR);
		}
		/* otherwise, block forever */
		rc = soselect(so, SOSEL_READ,1);
		goto tryagain;
	}
}

int
sowritebuf(int sock, struct _rwbuffer *rwb)
{
	struct	_socket	*so = sohtosock(sock);
	int	rc;
	unsigned	flags = _FLAGS;

	if(!rwb->rw_len) {
		BufferFree(rwb->rw_buffer);
		return(0);
	}

	if(!so || rwb->rw_len < 0) {
		SETERROR(EBADF);
		return(SYSERR);
	}
//	disable();
tryagain:;
	if(so->so_state < SOSTATE_BOUND || so->so_opt & SOOPT_CANTSENDMORE) {
		SETERROR(EPROTO);
bye:;
//		if(flags & INTR)
//			enable();
		return(SYSERR);
	}

	if(so->so_selectflags & SOSEL_WRITE) {
		rc = (so->so_proto)(so, PR_WRITEBUF, rwb);
//		if(flags & INTR)
//			enable();
		return(rc);
	}
	else {
		if(so->so_opt & SOOPT_NOBLOCK) {
			SETERROR(EWOULDBLOCK);
			goto bye;
		}
		/* otherwise, block forever */
		rc = soselect(so, SOSEL_WRITE, 1);
		goto tryagain;
	}
}


/* systest.c test system stuff */

#include	<stdio.h>
#include	<time.h>
#include 	<malloc.h>
#include	<dos.h>
#include	"task.h"
#include	"bufs.h"

const	int	stacksize = 512;

Task	*Receiver;
int	Proc3(void *Parent, char *name, int nargs, char **argv);
void interrupt (*oldhandler)(__CPPARGS);

void interrupt Keyboard(__CPPARGS)
{
	Buffer	*B;

	if(Receiver) {
		B = BufferAlloc(20);
		if(B) {
			strcpy(B->data, "keyboard");
			MessageBoxISendBuf(&Receiver->Messages, B);
		}
	}
asm	pushf
	(*oldhandler)();
}

int
panic(char *why)
{
	printf("Panic: %s\n",why);
	abort(0);
}

int
Proc1(void *Parent, char *myname, int nargs, char *argv[])
{
	long	deathtime;
	long	nextwrite;
	int	rc;
	void	*stack;

	printf("Proc1 name %s, parent %p, argcount %d\n",myname, Parent, nargs);
	if(Parent)
		printf("Parent %s\n", ((Task *) Parent)->tsk_name);

	stack = alloca(stacksize);
	Receiver = TaskAlloc(Proc3, stack, stacksize, 50, "p3", 0, NULL);

	deathtime = time(NULL) + 20;
	nextwrite = time(NULL) + 5;
	while(1) {
		if(time(NULL) > deathtime) {
			printf("Proc 1 death\n");
			break;
		}
		if(time(NULL) > nextwrite) {
			nextwrite = time(NULL) + 5;
			printf("%d Proc 1\n", (int) time(NULL) % 100);
			if(Receiver) {
				Buffer	*B = BufferAlloc(10);

				if(!B) {
					printf("Couldn't allocate Qe\n");
				}
				else {
					strcpy(B->data, "proc1");
					if(rc = MessageBoxSendBuf(&Receiver->Messages, B)) {
						printf("proc 1 rc from send = %d\n",rc);
					}
				}
			}
		}
		TaskReschedule();
	}	/* end while */
	if(Receiver) {
		Task *X = Receiver;
		Receiver = NULL;
		TaskKill(X);
	}
	return(0);
}

int
Proc2(void *Parent, char *myname, int nargs, char *argv[])
{
	long	deathtime;
	long	nextwrite;

	printf("Proc2 name %s, parent %p, argcount %d\n",myname, Parent, nargs);
	if(Parent)
		printf("Parent %s\n", ((Task *) Parent)->tsk_name);

	deathtime = time(NULL) + 30;
	nextwrite = time(NULL) + 2;
	while(1) {
		if(time(NULL) > deathtime) {
			printf("Proc 2 death\n");
			break;
		}
		if(time(NULL) > nextwrite) {
			nextwrite = time(NULL) + 2;
			printf("%d Proc 2\n", (int) time(NULL) % 100);
		}
		TaskReschedule();
	}	/* end while */
	return(0);
}

int
Proc3(void *Parent, char *myname, int nargs, char *argv[])
{
	printf("Proc3 name %s, parent %p, argcount %d\n",myname, Parent, nargs);
	if(Parent)
		printf("Parent %s\n", ((Task *) Parent)->tsk_name);

	while(1) {
		Buffer *B = MessageBoxReceiveBuf(&Task_Current_Task->Messages);

		if(!B) {
			printf("Proc 3 unblocked w/o message !\n");
		}
		else {
			printf("Proc 3 message (%s)\n",(char *) B->data);
			BufferFree(B);
		}
	}
}

int
main()
{
	int	rc;
	Task	*P1, *P2;
	void	*stack;

	BufferInitialize(10,50);

	rc = TaskInitialize();
	if(rc) {
		if(oldhandler)
			setvect(9, oldhandler);
		printf("Task Initialize returned %d\n",rc);
		exit(3);
	}


	stack = alloca(stacksize * 3);
	if(!stack) {
		printf("Couldn't alloc stack \n");
		exit(4);
	}
	P1 = TaskAlloc(Proc1,stack, (stacksize * 3) - 50, 50, "p1", 0, NULL);
	if(!P1) {
		printf("couldn't get proc1\n");
		exit(5);
	}
	stack = alloca(stacksize);
	if(!stack) {
		printf("Couldn't alloc stack 2 \n");
		exit(4);
	}
	P2 = TaskAlloc(Proc2,stack, stacksize, 50, "p2", 0, NULL);
	if(!P2) {
		printf("couldn't get proc2\n");
		exit(6);
	}

	oldhandler = getvect(9);
	setvect(9, Keyboard);
	if(rc = TaskReschedule()) {
		printf("Task resch returned code %d\n",rc);
	}
	return(0);
}

#include <alloc.h>
#include <stdlib.h>
#include <dos.h>
#include <asmrules.h>

#if	__DPMI16__
#define GMEM_FIXED          0x0000
#define GMEM_MOVEABLE       0x0002
#define GMEM_NOCOMPACT      0x0010
#define GMEM_NODISCARD      0x0020
#define GMEM_ZEROINIT       0x0040
#define GMEM_MODIFY         0x0080
#define GMEM_DISCARDABLE    0x0100
#define GMEM_NOT_BANKED     0x1000
#define GMEM_SHARE          0x2000
#define GMEM_DDESHARE       0x2000
#define GMEM_NOTIFY         0x4000
#define GMEM_LOWER          GMEM_NOT_BANKED

#define GHND                (GMEM_MOVEABLE | GMEM_ZEROINIT)
#define GPTR                (GMEM_FIXED | GMEM_ZEROINIT)


typedef unsigned int HGLOBAL;
#define WINAPI              _far _pascal
char far * WINAPI GlobalLock(HGLOBAL);
HGLOBAL WINAPI GlobalAlloc(unsigned int, long);
int    WINAPI GlobalUnlock(HGLOBAL);
HGLOBAL WINAPI GlobalFree(HGLOBAL);
#endif

#if	MBBS
#include	"gcomm.h"
#endif

#define	MBBSD		0		/* debug mbbs switching */
#define	MBBSDS		0		/* print new tasks as we run em */

/* #define	TSDEBUG	1*/	     	/* cprintf switch names */
#include	"conf.h"

#include	<memcheck.h>

#undef	NEAR
#if	DONEAR
#define	NEAR	near
#else
#define	NEAR
#endif

/* #define	TDEBUG	1 */
#if	!SIM && !IPDOOR
#define	DOV25SWITCH		0		// true to use V25 task switch
#include	"v25.h"
#endif

#include "task.h"


#if	DOV25SWITCH
typedef	struct	_regbankinfo {
	unsigned int	rb_bank;	// the register bank number
	Task	*	rb_task;	// which task has this register bank
	unsigned long	rb_lru;		// least recently used value
} RegBankInfo;

RegBankInfo	RegisterBanks[]={
	{6},
	{4},
	{2},
	{1}
};
int	RegBankCount = sizeof(RegisterBanks)/sizeof(RegBankInfo);
#endif

int NEAR	TaskSwitch(Task *old, Task *next);

Task	*Task_Current_Task;
Queue	Task_RunQueue;
long	TaskStartTicks;
static	Task	MainTask;
static	Task	InitTask;
Task	*Task_List;
char	*Tstates[]={"Running","Ready","Recv","Sleep","Susp","Sem","Select","Defunct"};

#define	INTR	512

#if     DOV25SWITCH
int
GetRegBank(Task *T)
{
	int	x;
	int	min = -1;
	unsigned long	minlru = 0xffffffff;

	for(x=0; x < RegBankCount; x++) {
		if(RegisterBanks[x].rb_task == T)
			continue;

		if(!RegisterBanks[x].rb_task)
			return(x);

		if(RegisterBanks[x].rb_lru < minlru) {
			minlru = RegisterBanks[x].rb_lru;
			min = x;
		}
	}
	return(min)
}

void
RegisterBankSave(int regindex)	// save the current register bank into the jmpbuf
{
	Task	*T;
	RegisterBank  far	*R;

	if(!(T = RegisterBanks[regindex].rb_task))
		return;		// nothing to save


	R =

}

#endif

static void
TaskStart()		/* this function is called when a task first starts */
{
	int	rc;

	Task	*T = Task_Current_Task;
#if	MBBSD
	shocst("task start","");
#endif
	rc = (*T->tsk_process) (T->tsk_parent, T->tsk_name, T->tsk_nargs, T->tsk_args);
	/* on return, the task is asking for itself to be killed */
	TaskKill(T);
	rc = TaskReschedule();
	/* return here only if an error occured */
	MainTask.tsk_retcode = rc;
	TaskSwitch(&MainTask, &InitTask);
	panic("Fell through task switch in TaskStart");

}

int
TaskInitialize()
{
	int	rc;

	MainTask.tsk_retcode = TASK_RESULT_NOTASKS;
	InitTask.tsk_retcode = TASK_RESULT_NOTASKS;

	rc = xsetjmp(MainTask.tsk_call_jump_buf);
	rc = xsetjmp(InitTask.tsk_call_jump_buf);
	TaskStartTicks = Ticks();
	return(rc);
}

Task	*
TaskAlloc(ProcessType Proc, void *stack, unsigned stacksize, int priority, char *tname, int nargs, char **argv)
{
	int	x;
	unsigned int	*y;
	Task	*T;

	T = calloc(1, sizeof(Task));

	if(!T)
		return(NULL);

#if	MBBS
	if(!stack) {	// allocate a selector based stack
		unsigned selector;

		stacksize = (((stacksize/0x1000)+1) * 0x1000);
		if(DosAllocHuge(0, stacksize, &selector, 0, 0)) {
			free(T);
			return(NULL);
		}

		T->tsk_selector = selector;
		stack = MK_FP(selector,0);
	}
#elif	__DPMI16__
      	if(!stack) {
		HGLOBAL gptr;

		stacksize = (((stacksize/0x1000)+1) * 0x1000);
		gptr = GlobalAlloc(GPTR,stacksize);
		if(!gptr) {
			free(T);	
			return(NULL);
		}

		stack = GlobalLock(gptr);
		T->tsk_selector = gptr;
	}
#else
      	if(!stack) {
		stack = malloc(stacksize);
		if(!stack) {
			free(T);
			return(NULL);
		}
		T->tsk_selector = FP_SEG(stack);
	}
#endif
	T->QE.queue_key = priority;
	T->QE.queue_type = QTYPE_TASK;
	MessageBoxInitialize(&T->Messages, TASK_MAX_MESSAGES);
	T->tsk_state = TASK_STATE_READY;
	T->tsk_stack_base = stack;
	T->tsk_stack_size = stacksize;
	y = (unsigned int *) stack;
	for(x = 0; x < (stacksize >> 1); x++, y++)
		*y = GUARDWORD;
	T->tsk_stack_pointer = (void *) (((char *) stack) + stacksize);
	T->tsk_name = tname;
	T->tsk_process = Proc;
	T->tsk_nargs = nargs;
	T->tsk_args = argv;
	T->tsk_parent = Task_Current_Task;

	xsetjmp(T->tsk_call_jump_buf);	/* we will never return here */
#if	LDATA
	/* assume large DS, SS */
	T->tsk_call_jump_buf[0].j_ss = FP_SEG(stack);
	T->tsk_call_jump_buf[0].j_sp = FP_OFF(stack) + stacksize - sizeof(unsigned);
	T->tsk_call_jump_buf[0].j_ds = _DS;	/* for now .. ? */
#else
	T->tsk_call_jump_buf[0].j_ss = _SS;
	T->tsk_call_jump_buf[0].j_sp = FP_OFF(stack) + stacksize - sizeof(unsigned);
	T->tsk_call_jump_buf[0].j_ds = _DS;
#endif
#if	LPROG
	/* large program */
	T->tsk_call_jump_buf[0].j_cs = FP_SEG(TaskStart);
	T->tsk_call_jump_buf[0].j_ip = FP_OFF(TaskStart);
#else
	T->tsk_call_jump_buf[0].j_cs = _CS;
	T->tsk_call_jump_buf[0].j_ip = FP_OFF(TaskStart);
#endif
	T->tsk_call_jump_buf[0].j_bp = T->tsk_call_jump_buf[0].j_sp;
	T->tsk_call_jump_buf[0].j_es = T->tsk_call_jump_buf[0].j_ds;
	T->tsk_state = TASK_STATE_READY;
	QueueInsert(&Task_RunQueue, (Queue_Element *) T);
	T->tsk_next = Task_List;
	Task_List = T;
	return(T);
}

int NEAR
TaskSwitch(Task *old, Task *next)
{
	int	rc;
	long	now = Ticks();
#ifdef	TDEBUG
	unsigned sp,csum;

	chkheap();
#endif
	if(!old || !next)
		return(TASK_RESULT_BADTASK);
	if(old->tsk_stack_base && *(((unsigned *) old->tsk_stack_base)) != GUARDWORD) {
		char	pstring[64];

		sprintf(pstring,"Task %s Stack Overflow",old->tsk_name);
		panic(pstring);
	}
	old->tsk_total_ticks += (now - old->tsk_hold_ticks);
	next->tsk_hold_ticks = now;
	old->tsk_cycles++;
#if	MBBSD
	shocst("tskswi xset","");
#endif
	rc = xsetjmp(old->tsk_call_jump_buf);
#ifdef	TDEBUG
	sp = old->tsk_call_jump_buf[0].j_bp;	/* use bp */
	csum = cksum(sp, (FP_OFF(old->tsk_stack_pointer) - sp) >> 1);
#endif
	if(rc) {	/* we're being called */
#if	TSDEBUG
		cprintf("setjmp rc %d tsk->%s\n",rc,old->tsk_name);
#endif

#if	MBBSD
	shocst("tskswi xset rc=1","");
#endif

#ifdef	TDEBUG
		if(old->tsk_csum && old->tsk_csum != csum)
			panic("task csum error");
#endif
		if(rc == 1)
			return(0);
		return(rc);
	}
#if	TSDEBUG
	{
		cprintf("longjump to tsk->%s %x:%x\n",next->tsk_name,
				next->tsk_call_jump_buf[0].j_cs,next->tsk_call_jump_buf[0].j_ip);
	}
#endif

#ifdef	TDEBUG
	old->tsk_csum = csum;
#endif
	xlongjmp(next->tsk_call_jump_buf, next->tsk_retcode);
asm { 
	int 3
} 
	return(0);	/* we never get here */
}


int
TaskReady(Task *T, int retcode, int reschedule)
{
	if(!T)
		return(TASK_RESULT_BADTASK);
	QueueExtract(&Task_RunQueue, (Queue_Element *) T);
	T->tsk_state = TASK_STATE_READY;
	QueueInsert(&Task_RunQueue, (Queue_Element *) T);
	T->tsk_retcode = retcode;
	if(reschedule == TASK_RESCHEDULE)
		return(TaskReschedule());
	else
		return(TASK_RESULT_OK);
}

int
TaskReschedule()		/* takes next runable task */
{
	Task	*T;
	unsigned flags = _FLAGS;
	int	rc;

//	disable();
	T = (Task *) QueueTail(&Task_RunQueue);

	if(T == Task_Current_Task) {
		panic("Next task is running task");
	}
	if(Task_Current_Task) {
		Task	*oldT = Task_Current_Task;

		if(!T || ((T->QE.queue_key < Task_Current_Task->QE.queue_key ||
			T->tsk_state != TASK_STATE_READY) &&
			Task_Current_Task->tsk_state == TASK_STATE_RUNNING))  {	/* not higher priority */
				rc = TASK_RESULT_OK;
				goto out;
		}
		QueueExtract(&Task_RunQueue, (Queue_Element *) T);

		if(Task_Current_Task->tsk_state == TASK_STATE_RUNNING) {
			Task_Current_Task->tsk_state = TASK_STATE_READY;
			QueueInsert(&Task_RunQueue, (Queue_Element *) Task_Current_Task);
		}
		Task_Current_Task = T;
		T->tsk_state = TASK_STATE_RUNNING;
#if	TSDEBUG
	cprintf("Tsk switch %s ->%s\n",oldT->tsk_name, T->tsk_name);
#endif
#if	MBBSDS
	{
		char	buff[43];

		sprintf(buff,"%s->%s",oldT->tsk_name, T->tsk_name);

		shocst(buff,"");
	}
#endif
		rc = TaskSwitch(oldT, T);
	}
	else {
		if(!T || T->tsk_state != TASK_STATE_READY) {
			rc = TASK_RESULT_NOTASKS;
			goto out;
		}
		/* otherwise, lets run this one */
		QueueExtract(&Task_RunQueue, (Queue_Element *) T);
		Task_Current_Task = T;
		T->tsk_state = TASK_STATE_RUNNING;
		rc = TaskSwitch(&MainTask, T);
	}
out:;
//	if(flags & INTR)
//		enable();
	return(rc);
}

int
TaskKill(Task *T)
{
	Task	*Prev, *This;

	if(!T)
		return(TASK_RESULT_BADTASK);

	MessageBoxRelease(&T->Messages);	/* this may block us ... */
	QueueExtract(&Task_RunQueue, (Queue_Element *) T);	/* won't be in the run queue */
	if(T->tsk_state == TASK_STATE_SEM) {	/* aught to get out of their sem */

	}
	if(T == Task_Current_Task)
		Task_Current_Task = NULL;
	T->tsk_state = TASK_STATE_DEFUNCT;
	This = Task_List;
	Prev = NULL;

	while(This != T && This) {
		Prev = This;
		This = This->tsk_next;
	}
	if(This) {
		if(!Prev) {
			Task_List = T->tsk_next;
		}
		else {
			Prev->tsk_next = T->tsk_next;
		}
	}
#if	MBBS
	if(T->tsk_selector)
		DosFreeSeg(T->tsk_selector);
	T->tsk_selector = 0;
#elif	__DPMI16__
	if(T->tsk_selector) {
	 	GlobalFree(GlobalUnlock(T->tsk_selector));
	}
#else
	if(T->tsk_selector) {
	 	free(T->tsk_stack_base);
	}
	
#endif
	return(TASK_RESULT_OK);
}

unsigned int
getpid()
{
#if	__LARGE__
	return(FP_SEG(Task_Current_Task));
#else
	return(FP_OFF(Task_Current_Task));
#endif
}

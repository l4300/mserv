/* tcp.c */

#define	__TCP_C
#if	!IPDOOR
#define	DONEAR	1
#endif
#include	<stdlib.h>
#include	<mem.h>

#include	"network.h"

#include	"task.h"

#include	"tcp.h"
#include	"tcb.h"
#include	"tcpfsm.h"
#include	"tcptimer.h"

#include	"sys/socket.h"
#include	"netinet/in.h"
#include	"alarm.h"

#include	<memcheck.h>

#define	LOWBYTE	(0xff)

Bool	uqidone;
Semaphore	uqmutex;
struct	uqe	uqtab[UQTSIZE];

extern	int	tcp_proto(struct _socket *sock, int pr_proto, void *data);

struct	tcb	tcbtab[Ntcp];
Semaphore	tcps_tmutex;
Semaphore	tqmutex;
Task		*tqpid;			/* timer task ptr */
struct		tqent	*tqhead;

Task		*tcps_iport, *tcps_oport;

int	(*AllowConnection)(unsigned port, int mem1, int mem2);	/* returns non zero if connection allowed */

long	TcpInSegs;
long	TcpCkSumErrors;
long	TcpEstabResets;
long	TcpAttemptFails;
long	TcpPassiveOpens;
long	TcpRetransSegs;
long	TcpOutSegs;
long	TcpActiveOpens;

int	TcpCurrEstab;

int	NEAR ioerr(struct tcb *ptcb, struct ep *pep);
int	NEAR tcpidle();
int	NEAR tcppersist();
int	NEAR tcpxmit();
int	NEAR tcprexmt();

int	NEAR (*tcpswitch[])(struct tcb *ptcb, struct ep *pep) = {
	ioerr,
	tcpclosed,
	tcplisten,
	tcpsynsent,
	tcpsynrcvd,
	tcpestablished,
	tcpfin1,
	tcpfin2,
	tcpclosewait,
	tcplastack,
	tcpclosing,
	tcptimewait
};

int     NEAR (*tcposwitch[NTCPOSTATES])() = {
	tcpidle,
	tcppersist,
	tcpxmit,
	tcprexmt
};


struct 	tcb	*
tcballoc()
{
	struct	tcb	*ptcb;
	int	slot;

	Semaphore_Wait(&tcps_tmutex);

	for(ptcb=&tcbtab[0],slot=0; slot < Ntcp; ++slot, ++ptcb)
		if(ptcb->tcb_state == TCPS_FREE)
			break;
	if(slot < Ntcp) {
		ptcb->tcb_state = TCPS_CLOSED;
		memset(ptcb, 0, sizeof(struct tcb));
		Semaphore_Initialize(&ptcb->tcb_mutex,1);
	} else
		ptcb = NULL;
	Semaphore_Signal(&tcps_tmutex);
	return(ptcb);
}

void
tcpSetSocket(struct tcb *ptcb)
{
	struct _socket *so = (struct _socket *) ptcb->tcb_socket;

	if(so) {
		so->so_readable = ptcb->tcb_rbcount;
		so->so_writeable = ptcb->tcb_sbsize - ptcb->tcb_sbcount;
		so->so_recvqueue = ptcb->tcb_rbsize - ptcb->tcb_rbcount;
        }
}


int
tcbdealloc(struct tcb *ptcb)
{
	if(ptcb->tcb_state == TCPS_FREE)
		return (OK);

	switch (ptcb->tcb_type) {
		case TCPT_CONNECTION :

			tcpkilltimers(ptcb);
			Semaphore_Signal(&ptcb->tcb_ocsem);
			Semaphore_Signal(&ptcb->tcb_ssema);
			Semaphore_Signal(&ptcb->tcb_rsema);
			if(ptcb->tcb_sndbuf)
				free(ptcb->tcb_sndbuf);
			if(ptcb->tcb_rcvbuf)
				free(ptcb->tcb_rcvbuf);
			if(ptcb->tcb_rsegq.queue_count > 0) {
				Queue_Element *QE;

				while((QE = QueuePHead(&ptcb->tcb_rsegq)) != 0)
					free(QE);
			}
/* MOVE ME */			if(ptcb->tcb_rudq.queue_count > 0) {
				Queue_Element *QE;

				while((QE = QueuePHead(&ptcb->tcb_rudq)) != 0) {
					Buffer	*B = ((struct uqe *) QE)->uq_B;

					BufferFree(B);
				}
			}
			break;
		case TCPT_SERVER :
/* ###			pdelete(ptcb->tcb_listenq, 0); */
			break;
		default :;
			Semaphore_Signal(&ptcb->tcb_mutex);
			return(SYSERR);
	}	/* end switch */
	ptcb->tcb_state = TCPS_FREE;
	Semaphore_Signal(&ptcb->tcb_mutex);
	Semaphore_Initialize(&ptcb->tcb_mutex,1);	// avoid double signal
	return(OK);
}



#pragma argsused
int
tcp_in(struct netif *pni, struct ep *pep)
{
	TcpInSegs++;
	if(tcps_iport) {
		if(MessageBoxCount(&tcps_iport->Messages) < TCPQLEN) {
			MessageBoxSendBuf(&tcps_iport->Messages,DataToBuf(pep));
			return(OK);
		}
	}
	BufferFreeData(pep);
	return(OK);
}

#pragma argsused
int
tcpinp(Task *Parent, char *name, int nargs, char *argv[])
{
	struct	ep	*pep;
	struct	ip	*pip;
	struct	tcp	*ptcp;
	struct	tcb	*ptcb;
	Buffer		*B;

	tcps_iport = Task_Current_Task;
	MessageBoxInitialize(&tcps_iport->Messages, TCPQLEN);
	tcps_iport->Messages.mesg_MQueue.queue_flags |= QUEUE_FLAGS_NOTSORTED;
	Semaphore_Initialize(&tcps_tmutex,1);

	while(1) {
		B = MessageBoxReceiveBuf(&tcps_iport->Messages);
		if(!B)
			continue;
		if(B->QE.queue_type != QTYPE_MBUF) { /* not an mbuf ! */
			continue;	/* ignore for now */
		}
		pep = (struct ep *) B->data;
		pip = (struct ip *) pep->ep_data;
		if(tcpcksum(pip)) {
			BufferFreeData(pep);
			TcpCkSumErrors++;
			continue;
		}
		if(isbrc(pip->ip_dst)) {
			BufferFreeData(pep);
			continue;
		}
		ptcp = (struct tcp *) pip->ip_data;
		tcpnet2h(ptcp);
		ptcb = tcpdemux(pep);
		if(!ptcb) {
			tcpreset(pep);
			BufferFreeData(pep);
			continue;
		}
		if(!tcpok(ptcb, pep))
			tcpackit(ptcb, pep);
		else {
			tcpopts(ptcb, pep);
			tcpswitch[ptcb->tcb_state](ptcb, pep);
		}
		if(ptcb->tcb_state != TCPS_FREE)
			Semaphore_Signal(&ptcb->tcb_mutex);
		BufferFreeData(pep);
	} /* end while */
}

#pragma argsused
int NEAR
ioerr(struct tcb *ptcb, struct ep *pep)
{
	return(SYSERR);
}


struct	tcp	*
tcpnet2h(struct tcp *ptcp)
{

	ptcp->tcp_sport = ntohs(ptcp->tcp_sport);
	ptcp->tcp_dport = ntohs(ptcp->tcp_dport);
	lswap(&ptcp->tcp_seq);
	lswap(&ptcp->tcp_ack);
	ptcp->tcp_window = ntohs(ptcp->tcp_window);
	ptcp->tcp_urgptr = ntohs(ptcp->tcp_urgptr);
	return(ptcp);
}

#ifdef	ELSEWHERE
unsigned short
tcpcksum(struct ip *pip)
{
	struct	tcp	*ptcp = (struct tcp *) pip->ip_data;
	unsigned	short	*sptr, len;
	unsigned	long	tcksum;
	int		i;

	tcksum = 0;
	sptr = (unsigned short *) pip->ip_src;
	for(i=0; i < IP_ALEN; i++)
		tcksum += *sptr++;
	sptr = (unsigned short *) ptcp;
	len = pip->ip_len - IP_HLEN(pip);
	tcksum += IPT_TCP + ntohs(len);	/* ?? */
	if(len & 1) {
		((char * ) ptcp)[len] = 0;
		len += 1;
	}
	len >>= 1;
	for(i=0; i < len; i++)
		tcksum += *sptr++;

	tcksum = (tcksum >> 16) + (tcksum & 0xffff);
	tcksum += (tcksum >> 16);

	return(ntohs((short) (~tcksum & 0xffff)));
}
#endif

struct	tcb	*
tcpdemux(struct ep *pep)
{
	struct 	ip	*pip = (struct ip *) pep->ep_data;
	struct	tcp	*ptcp = (struct tcp *) pip->ip_data;
	struct	tcb	*ptcb, *lasttcb;

	int	tcbn;

	lasttcb = NULL;
	Semaphore_Wait(&tcps_tmutex);
	for(tcbn=0, ptcb=&tcbtab[0]; tcbn < Ntcp; tcbn++, ptcb++) {
		if(ptcb->tcb_state == TCPS_FREE)
			continue;
		if(ptcp->tcp_dport == ptcb->tcb_lport &&
		   ptcp->tcp_sport == ptcb->tcb_rport &&
		   !memcmp(pip->ip_src, ptcb->tcb_rip, IP_ALEN) &&
		   !memcmp(pip->ip_dst, ptcb->tcb_lip, IP_ALEN)) {
#ifdef	WRONGP1
	       #P	Semaphore_Wait(&ptcb->tcb_mutex);
			if(ptcb->tcb_state == TCPS_FREE)
				continue;
#endif
			break;
		}
		if(ptcb->tcb_state == TCPS_LISTEN &&
			ptcp->tcp_dport == ptcb->tcb_lport) {
				lasttcb = ptcb;
		}
	} /* end for */
	if(tcbn >= Ntcp) {
		if((ptcp->tcp_code & TCPF_SYN) && lasttcb) {
			ptcb = lasttcb;
		}
		else
			ptcb = NULL;
	}
	Semaphore_Signal(&tcps_tmutex);
	if(ptcb) {
		Semaphore_Wait(&ptcb->tcb_mutex);
		if(ptcb->tcb_state == TCPS_FREE)
			ptcb = NULL;
	}
	return(ptcb);
}

int NEAR
tcpok(struct tcb *ptcb, struct ep *pep)
{
	struct	ip	*pip = (struct ip *) pep->ep_data;
	struct	tcp	*ptcp = (struct tcp *) pip->ip_data;
	int	seglen, rwindow;
	tcpseq	wlast, slast;
	Bool	rv;

	if(ptcb->tcb_state < TCPS_SYNRCVD)
		return(1);
	seglen = pip->ip_len - IP_HLEN(pip) - TCP_HLEN(ptcp);

	if(ptcp->tcp_code & TCPF_SYN)
		seglen++;
	if(ptcp->tcp_code & TCPF_FIN)
		seglen++;
	rwindow = ptcb->tcb_rbsize - ptcb->tcb_rbcount;
	if(rwindow == 0 && seglen == 0)
		return(ptcp->tcp_seq == ptcb->tcb_rnext);

	if(ptcp->tcp_code & TCPF_URG)
		tcprcvurg(ptcb, pep);
	wlast = ptcb->tcb_rnext + rwindow - 1;
	rv = (ptcp->tcp_seq - ptcb->tcb_rnext) >=0 &&
		(ptcp->tcp_seq - wlast) <= 0;
	if(seglen == 0)
		return(rv);
	slast  = ptcp->tcp_seq + seglen - 1;
	rv |= (slast - ptcb->tcb_rnext) >= 0 &&
		(slast - wlast) <= 0;
	/* if no window, strip data but keep acks, etc */

	if(rwindow == 0)
		pip->ip_len = IP_HLEN(pip) + TCP_HLEN(ptcp);
	return(rv);
}


#pragma argsused
int NEAR
tcpclosed(struct tcb *ptcb, struct ep *pep)
{

	tcpreset(pep);
	return(SYSERR);
}

#pragma argsused
int NEAR
tcpwait(struct tcb *ptcb)
{
	int	tcbnum = ptcb - &tcbtab[0];

	tcpkilltimers(ptcb);
	tmset(tcps_oport, TCPQLEN, MKEVENT(DELETE, tcbnum), TCP_TWOMSL);
	return(OK);
}

int NEAR
tcptimewait(struct tcb *ptcb, struct ep *pep)
{
	struct	ip	*pip = (struct ip *) pep->ep_data;
	struct	tcp	*ptcp = (struct tcp *) pip->ip_data;

	if(ptcp->tcp_code & TCPF_RST)
		return(tcbdealloc(ptcb));
	if(ptcp->tcp_code & TCPF_SYN) {
		tcpreset(pep);
		return(tcbdealloc(ptcb));
	}
	tcpacked(ptcb, pep);
	tcpdata(ptcb, pep);
	tcpwait(ptcb);
	return(OK);
}

int NEAR
tcpclosing(struct tcb *ptcb, struct ep *pep)
{
	struct	ip	*pip = (struct ip *) pep->ep_data;
	struct	tcp	*ptcp = (struct tcp *) pip->ip_data;

	if(ptcp->tcp_code & TCPF_RST)
		return tcbdealloc(ptcb);
	if(ptcp->tcp_code & TCPF_SYN)   {
		tcpreset(pep);
		return(tcbdealloc(ptcb));
	}
	tcpacked(ptcb, pep);
	if((ptcb->tcb_code & TCPF_FIN) == 0) {
		ptcb->tcb_state = TCPS_TIMEWAIT;
		tcpwait(ptcb);
	}
	return(OK);

}

int NEAR
tcpfin2(struct tcb *ptcb, struct ep *pep)
{
	struct	ip	*pip = (struct ip *) pep->ep_data;
	struct	tcp	*ptcp = (struct tcp *) pip->ip_data;

	if(ptcp->tcp_code & TCPF_RST)
		return tcpabort(ptcb, TCPE_RESET);
	if(ptcp->tcp_code & TCPF_SYN) {
		tcpreset(pep);
		return(tcpabort(ptcb, TCPE_RESET));
	}
	if(tcpacked(ptcb, pep) == SYSERR)
		return(OK);
	tcpdata(ptcb, pep);
	if(ptcb->tcb_flags & TCBF_RDONE) {
		ptcb->tcb_state = TCPS_TIMEWAIT;
		tcpwait(ptcb);
	}
	return(OK);
}


int NEAR
tcpfin1(struct tcb *ptcb, struct ep *pep)
{
	struct	ip	*pip = (struct ip *) pep->ep_data;
	struct	tcp	*ptcp = (struct tcp *) pip->ip_data;

	if(ptcp->tcp_code & TCPF_RST)
		return tcpabort(ptcb, TCPE_RESET);
	if(ptcp->tcp_code & TCPF_SYN) {
		tcpreset(pep);
		return(tcpabort(ptcb, TCPE_RESET));
	}
	if(tcpacked(ptcb, pep) == SYSERR)
		return(OK);
	tcpdata(ptcb, pep);
	tcpswindow(ptcb, pep);

	if(ptcb->tcb_flags & TCBF_RDONE) {
		if(ptcb->tcb_code & TCPF_FIN)
			ptcb->tcb_state = TCPS_CLOSING;
		else {
			ptcb->tcb_state = TCPS_TIMEWAIT;
			Semaphore_Signal(&ptcb->tcb_ocsem);
			tcpwait(ptcb);
		}
	} else if((ptcb->tcb_code & TCPF_FIN) == 0) {
		Semaphore_Signal(&ptcb->tcb_ocsem);
		ptcb->tcb_state = TCPS_FINWAIT2;
		tcpwait(ptcb);		/* #### probably doesn't belong */
	}
	return(OK);
}

int NEAR
tcpclosewait(struct tcb *ptcb, struct ep *pep)
{
	struct	ip	*pip = (struct ip *) pep->ep_data;
	struct	tcp	*ptcp = (struct tcp *) pip->ip_data;

	if(ptcp->tcp_code & TCPF_RST) {
		TcpEstabResets++;
		TcpCurrEstab--;
		return tcpabort(ptcb, TCPE_RESET);
	}
	if(ptcp->tcp_code & TCPF_SYN) {
		TcpEstabResets++;
		TcpCurrEstab--;
		tcpreset(pep);
		return(tcpabort(ptcb, TCPE_RESET));
	}
	tcpacked(ptcb, pep);
	tcpswindow(ptcb, pep);
	return(OK);
}


int NEAR
tcplastack(struct tcb *ptcb, struct ep *pep)
{
	struct	ip	*pip = (struct ip *) pep->ep_data;
	struct	tcp	*ptcp = (struct tcp *) pip->ip_data;

	if(ptcp->tcp_code & TCPF_RST)
		return tcpabort(ptcb, TCPE_RESET);
	if(ptcp->tcp_code & TCPF_SYN) {
		tcpreset(pep);
		return(tcpabort(ptcb, TCPE_RESET));
	}
	tcpacked(ptcb, pep);
	if((ptcb->tcb_code & TCPF_FIN) == 0)
		Semaphore_Signal(&ptcb->tcb_ocsem);
	return(OK);
}


int NEAR
tcpestablished(struct tcb *ptcb, struct ep *pep)
{
	struct	ip	*pip = (struct ip *) pep->ep_data;
	struct	tcp	*ptcp = (struct tcp *) pip->ip_data;

	if(ptcp->tcp_code & TCPF_RST) {
		TcpEstabResets++;
		TcpCurrEstab--;
		return tcpabort(ptcb, TCPE_RESET);
	}
	if(ptcp->tcp_code & TCPF_SYN) {
		TcpEstabResets++;
		TcpCurrEstab--;
		tcpreset(pep);
		return(tcpabort(ptcb, TCPE_RESET));
	}
	if(tcpacked(ptcb, pep) == SYSERR)
		return(OK);
	tcpdata(ptcb, pep);
	tcpswindow(ptcb, pep);
	if(ptcb->tcb_flags & TCBF_RDONE)
		ptcb->tcb_state = TCPS_CLOSEWAIT;
	return(OK);
}


int NEAR
tcpdata(struct tcb *ptcb, struct ep *pep)
{
	struct ip	*pip = (struct ip *) pep->ep_data;
	struct tcp	*ptcp = (struct tcp *) pip->ip_data;
	tcpseq		first, last, wlast;
	int		datalen, rwindow, i, pp, pb;
	int		odata;

	if(ptcp->tcp_code & TCPF_SYN) {
		ptcb->tcb_rnext++;
		ptcb->tcb_flags |= TCBF_NEEDOUT;
		ptcp->tcp_seq++;
	}
	odata = datalen = pip->ip_len - IP_HLEN(pip) - TCP_HLEN(ptcp);
	rwindow = ptcb->tcb_rbsize - ptcb->tcb_rbcount;
	wlast = ptcb->tcb_rnext + rwindow - 1;
	first = ptcp->tcp_seq;
	last = first + datalen - 1;
	if(SEQCMP(ptcb->tcb_rnext, first) > 0) {
		datalen -= ptcb->tcb_rnext - first;
		first = ptcb->tcb_rnext;
	}
	if(SEQCMP(last, wlast) > 0) {
		datalen -= last - wlast;
		ptcp->tcp_code &= ~TCPF_FIN;
	}
	pb = ptcb->tcb_rbstart + ptcb->tcb_rbcount;
	pb += first - ptcb->tcb_rnext;
	pb %= ptcb->tcb_rbsize;
	pp = first - ptcp->tcp_seq;
#ifndef	OLDWAY
	{
		int     xcount = ptcb->tcb_rbsize - pb;

		if(xcount > datalen)
			xcount = datalen;
		if(xcount < 0)
			xcount = 0;
		else {
			if(xcount)
				memcpy(ptcb->tcb_rcvbuf + pb, ptcp->tcp_data + pp, xcount);
		}
		if(xcount < datalen)
			memcpy(ptcb->tcb_rcvbuf, ptcp->tcp_data + pp + xcount, datalen - xcount);
	}
#else
	for(i=0; i < datalen; i++) {
		ptcb->tcb_rcvbuf[pb] = ptcp->tcp_data[pp++];
		if(++pb >= ptcb->tcb_rbsize)
			pb = 0;
	}
#endif
	tcpdodat(ptcb, ptcp, first, datalen);
	if(ptcb->tcb_flags & TCBF_NEEDOUT)
		tcpkick(ptcb);
	return(OK);
}


int  NEAR
tcpdodat(struct tcb *ptcb, struct tcp *ptcp, tcpseq first, int datalen)
{
	int	wakeup = 0;

	if(ptcb->tcb_rnext == first) {
		if(datalen > 0) {
			tfcoalesce(ptcb, datalen, ptcp);
			ptcb->tcb_flags |= TCBF_NEEDOUT;
			wakeup++;
		}
		if(ptcp->tcp_code & TCPF_FIN) {
			ptcb->tcb_flags |= TCBF_RDONE|TCBF_NEEDOUT;
			ptcb->tcb_rnext++;
			wakeup++;
		}
		if(ptcp->tcp_code & TCPF_PSH) {
			ptcb->tcb_flags |= TCBF_PUSH;
			wakeup++;
		}
		if(wakeup)
			tcpwakeup(READERS, ptcb);
	} else {
#if	DOOUTOFSEQUENCE
		if(ptcp->tcp_code & TCPF_FIN)
			ptcb->tcb_finseq = ptcp->tcp_seq + datalen;
		if(ptcp->tcp_code & TCPF_PSH)
			ptcb->tcb_pushseq = ptcp->tcp_seq + datalen;
		ptcp->tcp_code &= ~(TCPF_FIN|TCPF_PSH);
		if(tfinsert(ptcb, first, datalen) != SYSERR)
#endif
			ptcb->tcb_flags |= TCBF_NEEDOUT;	/* 4/2/95 send ack on frag insert */
	}
	return(OK);

}

int NEAR
tfinsert(struct tcb *ptcb, tcpseq seq, int datalen)
{
	struct 	tcpfrag	*tf;
	Queue_Element	*QE;

	if(datalen == 0)
		return(OK);
	if(ptcb->tcb_rsegq.queue_count > NTCPFRAG)
		return(SYSERR);
	QE = QueueAlloc(seq, QTYPE_TCPFRAG, 0);
	if(!QE)
		return(SYSERR);
	QE->queue_size = datalen;
	QueueInsert(&ptcb->tcb_rsegq, QE);
	return(OK);
}


int NEAR
tfcoalesce(struct tcb *ptcb, int datalen, struct tcp *ptcp)
{
	Queue_Element	*QE = NULL;
	int	new;

	ptcb->tcb_rnext += datalen;
	ptcb->tcb_rbcount += datalen;
	tcpSetSocket(ptcb);
	if(ptcb->tcb_rnext == ptcb->tcb_finseq)
		goto alldone;
	if((ptcb->tcb_rnext - ptcb->tcb_pushseq) >= 0) {
		ptcp->tcp_code |= TCPF_PSH;
		ptcb->tcb_pushseq = 0;
	}
	if(ptcb->tcb_rsegq.queue_count == 0)
		return(OK);
	QE = QueuePHead(&ptcb->tcb_rsegq);
	while((QE->queue_key - ptcb->tcb_rnext) <= 0) {
		new = QE->queue_size - (ptcb->tcb_rnext - QE->queue_key);
		if(new > 0) {
			ptcb->tcb_rnext += new;
			ptcb->tcb_rbcount += new;
			tcpSetSocket(ptcb);
		}
		if(ptcb->tcb_rnext == ptcb->tcb_finseq)
			goto alldone;
		if((ptcb->tcb_rnext - ptcb->tcb_pushseq) >= 0) {
			ptcp->tcp_code |= TCPF_PSH;
			ptcb->tcb_pushseq = 0;
		}
		free(QE);
		QE = QueuePHead(&ptcb->tcb_rsegq);
		if(!QE) {
			return(OK);
		}
	}	/* end while */
	QueueInsert(&ptcb->tcb_rsegq, QE);
	return(OK);
alldone:
	if(QE) {
		do {
			free(QE);
		} while ((QE = QueuePHead(&ptcb->tcb_rsegq)) != 0);
	}
	ptcp->tcp_code |= TCPF_FIN;
	return(OK);
}

int NEAR
tcpabort(struct tcb *ptcb, int error)
{
	tcpkilltimers(ptcb);
	ptcb->tcb_flags |= TCBF_RDONE|TCBF_SDONE;
	ptcb->tcb_error = error;
	tcpwakeup(READERS|WRITERS, ptcb);
	return(OK);
}

int
tcpsync(struct tcb *ptcb)
{
/*	memset(ptcb,0, sizeof(struct tcb));	### */
	ptcb->tcb_state = TCPS_CLOSED;
	ptcb->tcb_type = TCPT_CONNECTION;

	ptcb->tcb_iss = ptcb->tcb_suna = ptcb->tcb_snext = tcpiss();
	ptcb->tcb_lwack = ptcb->tcb_iss;

	if(!ptcb->tcb_sbsize)
		ptcb->tcb_sbsize = TCPSBS;
	ptcb->tcb_sndbuf = calloc(ptcb->tcb_sbsize,1);
	if(!ptcb->tcb_sndbuf)
		return(SYSERR);
	ptcb->tcb_sbstart = ptcb->tcb_sbcount = 0;
	Semaphore_Initialize(&ptcb->tcb_ssema,1);

	if(!ptcb->tcb_rbsize)
		ptcb->tcb_rbsize = TCPRBS;
	ptcb->tcb_rcvbuf = calloc(ptcb->tcb_rbsize,1);
	if(!ptcb->tcb_rcvbuf) {
		free(ptcb->tcb_sndbuf);
		ptcb->tcb_sndbuf = NULL;
		return(SYSERR);
	}
	ptcb->tcb_rbstart = ptcb->tcb_rbcount = 0;
	Semaphore_Initialize(&ptcb->tcb_rsema, 0);

#define	CLEAR(X)	memset(&X,0, sizeof(X))

	CLEAR(ptcb->tcb_rsegq);
	CLEAR(ptcb->tcb_rudq);
	CLEAR(ptcb->tcb_ruhq);
	CLEAR(ptcb->tcb_sudq);
	CLEAR(ptcb->tcb_suhq);

	Semaphore_Initialize(&ptcb->tcb_ocsem, 0);
	ptcb->tcb_srt = 0;
	ptcb->tcb_rtde = 0;
	ptcb->tcb_rexmtcount = 0;
	ptcb->tcb_flags = 0;
	ptcb->tcb_rexmt = 50;
	ptcb->tcb_keep = 12000;

	ptcb->tcb_code = TCPF_SYN;
	return(OK);
}

int NEAR
tcpsynsent(struct tcb *ptcb, struct ep *pep)
{
	struct ip	*pip = (struct ip *) pep->ep_data;
	struct tcp	*ptcp = (struct tcp *) pip->ip_data;

	if((ptcp->tcp_code & TCPF_ACK) &&
		((ptcp->tcp_ack - ptcb->tcb_iss <=0) ||
		 (ptcp->tcp_ack - ptcb->tcb_snext) > 0))
			return(tcpreset(pep));
	if(ptcp->tcp_code & TCPF_RST) {
		ptcb->tcb_state = TCPS_CLOSED;
		ptcb->tcb_error = TCPE_RESET;
		TcpAttemptFails++;
		tcpkilltimers(ptcb);
		Semaphore_Signal(&ptcb->tcb_ocsem);
		return(OK);
	}

	if((ptcp->tcp_code & TCPF_SYN) == 0)
		return(OK);
	ptcb->tcb_swindow = ptcp->tcp_window;
	ptcb->tcb_lwseq = ptcp->tcp_seq;
	ptcb->tcb_rnext = ptcp->tcp_seq;
	ptcb->tcb_cwin = ptcb->tcb_rnext + ptcb->tcb_rbsize;
	tcpacked(ptcb, pep);
	tcpdata(ptcb, pep);
	ptcp->tcp_code &= ~TCPF_FIN;
	if(ptcb->tcb_code & TCPF_SYN)
		ptcb->tcb_state = TCPS_SYNRCVD;
	else {
		TcpCurrEstab++;
		ptcb->tcb_state = TCPS_ESTABLISHED;
		Semaphore_Signal(&ptcb->tcb_ocsem);
#ifdef	BAD
		if(so) {
			sowakeup(so, SOSEL_READ|SOSEL_EXCEPT);
		}
#endif
	}
	return(OK);
}

int NEAR
tcpsynrcvd(struct tcb *ptcb, struct ep *pep)
{
	struct ip	*pip = (struct ip *) pep->ep_data;
	struct tcp	*ptcp = (struct tcp *) pip->ip_data;
	struct	tcb	*pptcb = ptcb->tcb_pptcb;
	struct	_socket	*so,*sop;
	struct	sockaddr_in	*sa;


	if(ptcp->tcp_code & TCPF_RST) {
		TcpAttemptFails++;
		if(ptcb->tcb_pptcb)
			return(tcbdealloc(ptcb));
		else
			return(tcpabort(ptcb, TCPE_REFUSED));
	}

	if(ptcp->tcp_code & TCPF_SYN) {
		TcpAttemptFails++;
		tcpreset(pep);
		return(tcpabort(ptcb, TCPE_RESET));
	}

	if(tcpacked(ptcb, pep) == SYSERR)
		return(OK);
	if(ptcb->tcb_pptcb) {
		if(Semaphore_Wait(&pptcb->tcb_mutex) != SEM_RESULT_OK) {
			TcpAttemptFails++;
			tcpreset(pep);
			return(tcbdealloc(ptcb));
		}
		if(pptcb->tcb_state != TCPS_LISTEN) {
			TcpAttemptFails++;
			tcpreset(pep);
			Semaphore_Signal(&pptcb->tcb_mutex);
			return(tcbdealloc(ptcb));
		}
		/* signal parent listen queue with this device number */
		/* then... */
		/* #### */

		sop = (struct _socket *) pptcb->tcb_socket;
		if(!sop || sop->so_listencount < 1) {
bombout:;
			TcpAttemptFails++;
			Semaphore_Signal(&pptcb->tcb_mutex);
			return(tcbdealloc(ptcb));
		}

		so = sop->so_reserved[--sop->so_listencount];
		if(!so)
			goto bombout;
		so->so_family = AF_INET;
		so->so_protocol = SOCK_STREAM;
		so->so_pcb = (void *) ptcb;
		so->so_proto = tcp_proto;
		ptcb->tcb_socket = so;
/*		ptcb->tcb_flags |= TCBF_DELACK;*/ // ### was commented out 8/2/94
		so->so_state = SOSTATE_CONNECTED;
		so->so_selectflags |= SOSEL_WRITE;
		sa = (struct sockaddr_in *) &so->so_local;
		memcpy(sa->sin_addr.s_ip, ptcb->tcb_lip, IP_ALEN);
		sa->sin_port = htons(ptcb->tcb_lport);
		sa->sin_family = AF_INET;
		sa = (struct sockaddr_in *) &so->so_remote;
		memcpy(sa->sin_addr.s_ip, ptcb->tcb_rip, IP_ALEN);
		sa->sin_port = htons(ptcb->tcb_rport);
		sa->sin_family = AF_INET;
		sop->so_ready[sop->so_readycount++] = so;
		Semaphore_Signal(&pptcb->tcb_mutex);
		sowakeup(sop, SOSEL_LISTEN|SOSEL_READ);

	}
	else
		Semaphore_Signal(&ptcb->tcb_ocsem);	/* #P */
	TcpCurrEstab++;
	ptcb->tcb_state = TCPS_ESTABLISHED;
	tcpdata(ptcb, pep);
/*	if(ptcb->tcb_ocsem.sem_count > 0)   #### #P
		Semaphore_Signal(&ptcb->tcb_ocsem); */
	if(ptcb->tcb_flags & TCBF_RDONE)
		ptcb->tcb_state = TCPS_CLOSEWAIT;
	return(OK);
}

int NEAR
tcplisten(struct tcb *ptcb, struct ep *pep)
{
	struct ip	*pip = (struct ip *) pep->ep_data;
	struct tcp	*ptcp = (struct tcp *) pip->ip_data;
	struct	tcb	*newptcb;
	struct	_socket	*so;
	if(ptcp->tcp_code & TCPF_RST)
		return(OK);
	if((ptcp->tcp_code & TCPF_ACK) ||
	   (ptcp->tcp_code & TCPF_SYN) == 0)
		return(tcpreset(pep));

	/* make sure we can accept more */
	so = (struct _socket *) ptcb->tcb_socket;
	if((!so || so->so_listencount < 1)) {	/* can't accept more */
blastoff:;
		tcpreset(pep);
		return(SYSERR);
	}
	newptcb = tcballoc();
	if(!newptcb) {
		return(SYSERR);
	}
	/* this check is after tcballoc because tmutex blocking in tcballoc
	   could cause false reading */
	if(AllowConnection && !(*AllowConnection)(ptcb->tcb_lport, ptcb->tcb_rbsize, ptcb->tcb_sbsize)) {
		tcbdealloc(newptcb);
		goto blastoff;
	}
	newptcb->tcb_rbsize = ptcb->tcb_rbsize;
	newptcb->tcb_sbsize = ptcb->tcb_sbsize;
	if(tcpsync(newptcb) == SYSERR) {
		tcbdealloc(newptcb);
		return(SYSERR);
	}
	newptcb->tcb_state = TCPS_SYNRCVD;
	newptcb->tcb_ostate = TCPO_IDLE;
	newptcb->tcb_error = 0;
	newptcb->tcb_pptcb = ptcb;

	memcpy(newptcb->tcb_rip, pip->ip_src, IP_ALEN);
	newptcb->tcb_rport = ptcp->tcp_sport;
	memcpy(newptcb->tcb_lip, pip->ip_dst, IP_ALEN);
	newptcb->tcb_lport = ptcp->tcp_dport;

	tcpwinit(ptcb, newptcb, pep);

	newptcb->tcb_finseq = newptcb->tcb_pushseq = 0;
	newptcb->tcb_flags = TCBF_NEEDOUT;
	TcpPassiveOpens++;
	ptcp->tcp_code &= ~TCPF_FIN;
	tcpdata(newptcb, pep);
/*	Semaphore_Signal(&newptcb->tcb_mutex);  DONE IN TCBALLOC */
	return(OK);
}


int NEAR
tcpwinit(struct tcb *ptcb, struct tcb *newptcb, struct ep *pep)
{
	struct ip	*pip = (struct ip *) pep->ep_data;
	struct tcp	*ptcp = (struct tcp *) pip->ip_data;
	struct route	*prt;
	Bool		local;
	int		mss;

	newptcb->tcb_swindow = ptcp->tcp_window;
	newptcb->tcb_lwseq = ptcp->tcp_seq;
	newptcb->tcb_lwack = newptcb->tcb_iss;

	prt = rtget(pip->ip_src, RTF_REMOTE);
	local = prt && prt->rt_metric == 0;
	newptcb->tcb_pni = &nif[prt ? prt->rt_ifnum : 1];
	if(prt)
		rtfree(prt);
	if(local)
		mss = newptcb->tcb_pni->ni_mtu - IPMHLEN - TCPMHLEN;
	else
		mss = 536;

	if(ptcb->tcb_smss) {
		newptcb->tcb_smss = min(ptcb->tcb_smss, mss);
		ptcb->tcb_smss = 0;
	}
	else
		newptcb->tcb_smss = mss;
	newptcb->tcb_rmss = mss;
	newptcb->tcb_cwnd = newptcb->tcb_smss;
	newptcb->tcb_ssthresh = 65535;
	newptcb->tcb_rnext = ptcp->tcp_seq;
	newptcb->tcb_cwin = newptcb->tcb_rnext + newptcb->tcb_rbsize;

	return(OK);
}


#pragma argsused
int
tcpout(Task *Parent, char *name, int nargs, char **argv)
{
	struct	tcb	*ptcb;
	int	i;

	tcps_oport = Task_Current_Task;
	MessageBoxInitialize(&tcps_oport->Messages, TCPQLEN);
	tcps_oport->Messages.mesg_MQueue.queue_flags |= QUEUE_FLAGS_NOTSORTED;

	while(1) {
		Queue_Element	*QE;

		QE = MessageBoxReceive(&tcps_oport->Messages);
		if(!QE)
			continue;
		if(QE->queue_type != QTYPE_SIGNAL) { /* not an mbuf ! */
			continue;	/* ignore for now */
		}
		i = QE->queue_key;		/* a stupid place to put it */
		free(QE);
		ptcb = &tcbtab[TCB(i)];
		if(ptcb->tcb_state <= TCPS_CLOSED)
			continue;
		Semaphore_Wait(&ptcb->tcb_mutex);
		if(EVENT(i) == DELETE)
			tcbdealloc(ptcb);
		else
			tcposwitch[ptcb->tcb_ostate](TCB(i), EVENT(i));
		if(ptcb->tcb_state != TCPS_FREE)
			Semaphore_Signal(&ptcb->tcb_mutex);
	}	/* end while */
}

int NEAR
tcpidle(int tcbnum, int event)
{
	if(event == SEND)
		tcpxmit(tcbnum, event);
	return(OK);
}

int NEAR
tcppersist(int tcbnum, int event)
{
	struct	tcb 	*ptcb = &tcbtab[tcbnum];

	if(event != PERSIST)
		return(OK);
	tcpsend(tcbnum, TSF_REXMT);
	ptcb->tcb_persist = min(ptcb->tcb_persist << 1, TCP_MAXPRS);
	tmset(tcps_oport, TCPQLEN, MKEVENT(PERSIST, tcbnum), ptcb->tcb_persist);
	return(OK);
}

int NEAR
tcpxmit(int tcbnum, int event)
{
	struct	tcb 	*ptcb = &tcbtab[tcbnum];
	int	tosend, tv, pending, window;

	if(event == RETRANSMIT) {
		tmclear(tcps_oport, MKEVENT(SEND, tcbnum));
		tcprexmt(tcbnum, event);
		ptcb->tcb_ostate = TCPO_REXMT;
		return(OK);
	}

	tcpsndurg(tcbnum);
	tosend = tcphowmuch(ptcb);
	if(tosend == 0) {
		if(ptcb->tcb_flags & TCBF_NEEDOUT)
			tcpsend(tcbnum, TSF_NEWDATA);
		return(OK);
	}
	else if (ptcb->tcb_swindow == 0) {
		ptcb->tcb_ostate = TCPO_PERSIST;
		ptcb->tcb_persist = ptcb->tcb_rexmt;
		tcpsend(tcbnum, TSF_NEWDATA);
		tmset(tcps_oport, TCPQLEN, MKEVENT(PERSIST, tcbnum), ptcb->tcb_persist);
		return(OK);
	}
	ptcb->tcb_ostate = TCPO_XMIT;
	window = min(ptcb->tcb_swindow, ptcb->tcb_cwnd);
	pending = ptcb->tcb_snext - ptcb->tcb_suna;
	while(tcphowmuch(ptcb) > 0 && pending <= window) {
		tcpsend(tcbnum, TSF_NEWDATA);
		pending = ptcb->tcb_snext - ptcb->tcb_suna;
	}
	tv = MKEVENT(RETRANSMIT, tcbnum);
	if(!tmleft(tcps_oport, tv))
		tmset(tcps_oport, TCPQLEN, tv, ptcb->tcb_rexmt);
	return(OK);
}

int NEAR
tcpsend(int tcbnum, Bool rexmt)
{
	struct	tcb 	*ptcb = &tcbtab[tcbnum];
	struct	ep 	*pep;
	struct	ip	*pip;
	struct	tcp	*ptcp;
	char		*pch;
	int		i, datalen,tocopy, off, newdata;
	Buffer		*B;

	datalen = tcpsndlen(ptcb, rexmt, &off);

	B = BufferAlloc(datalen + TCPMHLEN + 8 + IPMHLEN + EP_HLEN);
	if(!B) {
		return(SYSERR);
	}
	pep = (struct ep *) B->data;
	pip = (struct ip *) pep->ep_data;
	ptcp = (struct tcp *) pip->ip_data;
	memcpy(pip->ip_src, ptcb->tcb_lip, IP_ALEN);
	memcpy(pip->ip_dst, ptcb->tcb_rip, IP_ALEN);
	pip->ip_proto = IPT_TCP;
	ptcp->tcp_sport = ptcb->tcb_lport;
	ptcp->tcp_dport = ptcb->tcb_rport;
	if(!rexmt) {
		if(ptcb->tcb_code & TCPF_URG)
			ptcp->tcp_seq = ptcb->tcb_suna + off;
		else
			ptcp->tcp_seq = ptcb->tcb_snext;
	} else
		ptcp->tcp_seq = ptcb->tcb_suna;
	ptcp->tcp_ack = ptcb->tcb_rnext;
	if((ptcb->tcb_flags & TCBF_SNDFIN) &&	/* #P */
		SEQCMP(ptcp->tcp_seq + datalen, ptcb->tcb_slast) == 0)
			ptcb->tcb_code |= TCPF_FIN;
	ptcp->tcp_code = ptcb->tcb_code;
	ptcp->tcp_offset = TCPHOFFSET;
	if((ptcb->tcb_flags & TCBF_FIRSTSEND) == 0)
		ptcp->tcp_code |= TCPF_ACK;
	if(ptcp->tcp_code & TCPF_SYN)
		tcprmss(ptcb, pip);
	pip->ip_verlen = (IP_VERSION << 4) | IP_MINHLEN;
	pip->ip_len = IP_HLEN(pip)  + TCP_HLEN(ptcp) + datalen;
	if(datalen > 0)
		ptcp->tcp_code |= TCPF_PSH;		/* ### ?? */
	ptcp->tcp_window = tcprwindow(ptcb);
	if(ptcb->tcb_code & TCPF_URG)
#ifdef	BSDURG
		ptcp->tcp_urgptr = datalen;
#else
		ptcp->tcp_urgptr = datalen - 1;
#endif
	else
		ptcp->tcp_urgptr = 0;
	pch = &pip->ip_data[TCP_HLEN(ptcp)];
	i = (ptcb->tcb_sbstart + off) % ptcb->tcb_sbsize;
#ifndef	OLDWAY
	{
		int	xcount = ptcb->tcb_sbsize - i;

		if(xcount > datalen)
			xcount = datalen;
		if(xcount < 0)
			xcount = 0;
		else {
			if(xcount)
				memcpy(pch, ptcb->tcb_sndbuf + i, xcount);
		}
		if(xcount < datalen)
			memcpy(pch+xcount, ptcb->tcb_sndbuf, datalen - xcount);
	}
#else
	for(tocopy = datalen; tocopy > 0; --tocopy) {
		*pch++ = ptcb->tcb_sndbuf[i];
		if(++i >= ptcb->tcb_sbsize)
			i = 0;
	}
#endif
	ptcb->tcb_flags &= ~TCBF_NEEDOUT;
	if(rexmt) {
		newdata = ptcb->tcb_suna + datalen - ptcb->tcb_snext;
		if(newdata < 0)
			newdata = 0;
		TcpRetransSegs++;
	} else {
		newdata = datalen;
		if(ptcb->tcb_code & TCPF_SYN)
			newdata++;
		if(ptcb->tcb_code & TCPF_FIN)
			newdata++;
	}
	ptcb->tcb_snext += newdata;
	if(newdata >= 0)
		TcpOutSegs++;
	if(ptcb->tcb_state == TCPS_TIMEWAIT)
		tcpwait(ptcb);
	datalen += TCP_HLEN(ptcp);
	tcph2net(ptcp);
	ptcp->tcp_cksum = 0;
	ptcp->tcp_cksum = tcpcksum(pip);
	return(ipsend(pip->ip_dst, pep, datalen));

}

int NEAR
tcpsndlen(struct tcb *ptcb, Bool rexmt, int *poff)
{
	struct	uqe	*puqe, *puqe2;
	Buffer		*B;
	int	datalen;

	if(rexmt || ptcb->tcb_sudq.queue_count == 0) {
#ifdef	WRONG
		if(rexmt || (ptcb->tcb_code & TCPF_SYN) || (ptcb->tcb_code & TCPF_FIN)) /* #### May be wrong */
#else
		if(rexmt || (ptcb->tcb_code & TCPF_SYN))
#endif
			*poff = 0;
		else
			*poff = ptcb->tcb_snext - ptcb->tcb_suna;
		datalen = ptcb->tcb_sbcount - *poff;
		if(!rexmt) {	/* remove urgent holes */
			datalen = tcpshskip(ptcb, datalen, poff);
			datalen = min(datalen, ptcb->tcb_swindow);
		}
		return(min(datalen, ptcb->tcb_smss));
	}
	puqe = (struct uqe *) QueuePHead(&ptcb->tcb_sudq);
	*poff = ptcb->tcb_sbstart + puqe->uq_seq - ptcb->tcb_suna;
	if(*poff > ptcb->tcb_sbsize)
		*poff -= ptcb->tcb_sbsize;
	datalen = puqe->uq_len;
	if(datalen > ptcb->tcb_smss) {
		datalen = ptcb->tcb_smss;
		puqe2 = uqalloc();
		if(!puqe2) {
			uqfree(puqe);
			return(ptcb->tcb_smss);
		}
		puqe2->uq_seq = puqe->uq_seq;
		puqe2->uq_len = datalen;

		puqe->uq_seq += datalen;
		puqe->uq_len -= datalen;
	} else {
		puqe2 = puqe;
		puqe = (struct uqe *) QueuePHead(&ptcb->tcb_sudq);
	}
	if(puqe) {
		puqe->QE.queue_key = SUDK(ptcb, puqe->uq_seq);
		QueueInsert(&ptcb->tcb_sudq, (Queue_Element *) puqe);
	}
	if(ptcb->tcb_suhq.queue_count == 0) {
		ptcb->tcb_suhseq = puqe2->uq_seq;
	}
	puqe2->QE.queue_key = SUHK(ptcb, puqe2->uq_seq);
	QueueInsert(&ptcb->tcb_suhq, (Queue_Element *) puqe2);
	return(datalen);
}

int NEAR
tcphowmuch(struct tcb *ptcb)
{
	int	tosend;

	tosend = ptcb->tcb_suna + ptcb->tcb_sbcount - ptcb->tcb_snext;
	if(ptcb->tcb_code & TCPF_SYN)
		tosend++;
	if(ptcb->tcb_flags & TCBF_SNDFIN)	/* P1 */
		tosend++;
	return(tosend);
}

int NEAR
tcpreset(struct ep *pepin)
{
	struct	ep	*pepout;
	struct	ip	*pipin = (struct ip *) pepin->ep_data;
	struct	ip	*pipout;
	struct	tcp	*ptcpin = (struct tcp *) pipin->ip_data;
	struct 	tcp	*ptcpout;
	int		datalen;
	Buffer		*B;

	if(ptcpin->tcp_code & TCPF_RST)
		return(OK);

	B = BufferAlloc(TCPMHLEN + IPMHLEN + EP_HLEN);
	if(!B)
		return(SYSERR);
	pepout = (struct ep *) B->data;
	pipout  = (struct ip *) pepout->ep_data;
	memcpy(pipout->ip_src, pipin->ip_dst, IP_ALEN);
	memcpy(pipout->ip_dst, pipin->ip_src, IP_ALEN);
	pipout->ip_proto = IPT_TCP;
	pipout->ip_verlen = (IP_VERSION << 4) | IP_MINHLEN;
	pipout->ip_len = IPMHLEN + TCPMHLEN;
	ptcpout = (struct tcp *) pipout->ip_data;
	ptcpout->tcp_sport = ptcpin->tcp_dport;
	ptcpout->tcp_dport = ptcpin->tcp_sport;
	if(ptcpin->tcp_code & TCPF_ACK) {
		ptcpout->tcp_seq = ptcpin->tcp_ack;
		ptcpout->tcp_code = TCPF_RST;	/* ### why not ACK here too */
	}
	else {
		ptcpout->tcp_seq = 0;
		ptcpout->tcp_code = TCPF_RST|TCPF_ACK;
	}
	datalen = pipin->ip_len - IP_HLEN(pipin) - TCP_HLEN(ptcpin);
	if(ptcpin->tcp_code & TCPF_FIN)
		datalen++;
	if(ptcpin->tcp_code & TCPF_SYN)
		datalen++;
	ptcpout->tcp_ack = ptcpin->tcp_seq + datalen;
	ptcpout->tcp_offset = TCPHOFFSET;
	ptcpout->tcp_window = ptcpout->tcp_urgptr = 0;
	tcph2net(ptcpout);		/* ### Added 5/15/92 */
	ptcpout->tcp_cksum = 0;
	ptcpout->tcp_cksum = tcpcksum(pipout);
	TcpOutSegs++;
	return(ipsend(pipout->ip_dst, pepout, TCPMHLEN));
}


struct	tcp
*tcph2net(struct tcp *ptcb)
{
	return(tcpnet2h(ptcb));
}

int
tcpgetspace(struct tcb *ptcb, int len)
{
	if(len > ptcb->tcb_sbsize)
		return(TCPE_TOOBIG);
	while(1) {
		Semaphore_Wait(&ptcb->tcb_ssema);
		Semaphore_Wait(&ptcb->tcb_mutex);
		if(ptcb->tcb_state == TCPS_FREE)
			return(SYSERR);
		if(ptcb->tcb_error) {
			tcpwakeup(WRITERS, ptcb);
			Semaphore_Signal(&ptcb->tcb_mutex);
			return(ptcb->tcb_error);
		}
		if(len <= ptcb->tcb_sbsize - ptcb->tcb_sbcount)
			return(len);
		Semaphore_Signal(&ptcb->tcb_mutex);
	}
}

int
tcpwakeup(int type, struct tcb *ptcb)		/* ### needs work */
{
	int	freelen;

	if(type & READERS) {
		if(((ptcb->tcb_flags & TCBF_RDONE) ||
		     ptcb->tcb_rbcount > 0 || ptcb->tcb_rudq.queue_count > 0) &&
		     ptcb->tcb_rsema.sem_count <= 0) {
			Semaphore_Signal(&ptcb->tcb_rsema);

			if(ptcb->tcb_socket) {
				int opt = SOSEL_READ;

				if(ptcb->tcb_flags & TCBF_RDONE)
					opt |= (SOSEL_EXCEPT|SOSEL_READ);

			     sowakeup((struct _socket *) ptcb->tcb_socket, opt);
			}
		    }
	}
	if(type & WRITERS) {
		freelen = ptcb->tcb_sbsize - ptcb->tcb_sbcount;
		if(((ptcb->tcb_flags & TCBF_SDONE) || freelen > 0) &&
		     ptcb->tcb_ssema.sem_count <= 0) {
			Semaphore_Signal(&ptcb->tcb_ssema);
			if(ptcb->tcb_socket)
			     sowakeup((struct _socket *) ptcb->tcb_socket, SOSEL_WRITE|SOSEL_READ); // read is new
		}
		if(ptcb->tcb_error && ptcb->tcb_ocsem.sem_waitQueue.queue_count > 0) {
			Semaphore_Signal(&ptcb->tcb_ocsem);
			if(ptcb->tcb_socket)
			     sowakeup((struct _socket *) ptcb->tcb_socket, SOSEL_EXCEPT|SOSEL_READ); // read is new
		}
	}
	return(OK);
}


unsigned int NEAR
tcpiss()
{
static	int	seq = 0;

	if(!seq) {
		seq = Ticks() & 0xffff;
	}
	seq += TCPINCR;
	return(seq);
}


#pragma argsused
int
tcptimer(Task	*Parent, char *name, int argc, char *argv[])
{
	long	now, lastrun;
	int	delta;
	struct	tqent	*tq;

	Semaphore_Initialize(&tqmutex, 1);
	tqpid = Task_Current_Task;
	lastrun = Ticks100();

	while(1) {
		struct	timeval	tv;

		tv.tv_sec = 0;
		tv.tv_usec = 100;
		alarm_sleep(&tv,0);
		if(!tqhead)
			continue;		/* was suspend */
		Semaphore_Wait(&tqmutex);
		now = Ticks100();
		delta = now - lastrun;

		if(delta < 0 || delta > 100)
			delta = 10;
		lastrun = now;
		while(tqhead && tqhead->tq_timeleft <=delta) {
			delta -= tqhead->tq_timeleft;
			if(tqhead->tq_port->Messages.mesg_MQueue.queue_count <= tqhead->tq_portlen) {
				Queue_Element	*QE = QueueAlloc((long) tqhead->tq_msg, QTYPE_SIGNAL, 0);
				MessageBoxSend(&tqhead->tq_port->Messages, QE);
			}
			else
				break;
			tq = tqhead;
			tqhead = tqhead->tq_next;
			free(tq);
		}
		if(tqhead)
			tqhead->tq_timeleft -= delta;
		Semaphore_Signal(&tqmutex);

	}


}


int
tmclear(Task	*port, int msg)
{
	struct	tqent	*prev, *ptq;
	int	timespent;

	Semaphore_Wait(&tqmutex);
	prev = NULL;
	for(ptq = tqhead; ptq; ptq = ptq->tq_next) {
		if(ptq->tq_port == port && ptq->tq_msg == msg) {
			timespent = Ticks100() - ptq->tq_time;
			if(prev)
				prev->tq_next = ptq->tq_next;
			else
				tqhead = ptq->tq_next;
			if(ptq->tq_next)
				ptq->tq_next->tq_timeleft += ptq->tq_timeleft;
			Semaphore_Signal(&tqmutex);
			free(ptq);
			return(timespent);
		}
		prev = ptq;
	}
	Semaphore_Signal(&tqmutex);
	return(SYSERR);
}


int
tcpkilltimers(struct	tcb	*ptcb)
{
	int	tcbnum = ptcb - &tcbtab[0];

	tmclear(tcps_oport, MKEVENT(SEND, tcbnum));
	tmclear(tcps_oport, MKEVENT(RETRANSMIT, tcbnum));
	tmclear(tcps_oport, MKEVENT(PERSIST, tcbnum));
	return(OK);
}

int
tmleft(Task	*port, int msg)
{
	struct	tqent	*tq;
	int	timeleft = 0;

	if(!tqhead)
		return(0);
	Semaphore_Wait(&tqmutex);
	for(tq = tqhead; tq; tq = tq->tq_next) {
		timeleft += tq->tq_timeleft;	/* #P */
		if(tq->tq_port == port && tq->tq_msg == msg) {

			Semaphore_Signal(&tqmutex);
			return(timeleft);
		}
	}
	Semaphore_Signal(&tqmutex);
	return(0);
}

int
tmset(Task	*port, int portlen, int msg, int tim)
{
	struct	tqent	*ptq, *newtq, *tq;

	newtq = (struct tqent *) calloc(1,sizeof(struct tqent));
	if(!newtq)
		return(SYSERR);
	newtq->tq_timeleft = tim;
	newtq->tq_time = Ticks100();
	newtq->tq_port = port;
	newtq->tq_portlen = portlen;
	newtq->tq_msg = msg;
	newtq->tq_next = NULL;

	tmclear(port, msg);

	Semaphore_Wait(&tqmutex);
	if(!tqhead) {
		tqhead = newtq;
		Semaphore_Signal(&tqmutex);
		return(OK);
	}

	for(ptq=0, tq=tqhead; tq; tq=tq->tq_next) {
		if(newtq->tq_timeleft < tq->tq_timeleft)
			break;
		newtq->tq_timeleft -= tq->tq_timeleft;
		ptq = tq;
	}
	newtq->tq_next = tq;
	if(ptq)
		ptq->tq_next = newtq;
	else
		tqhead = newtq;
	if(tq)
		tq->tq_timeleft -= newtq->tq_timeleft;
	Semaphore_Signal(&tqmutex);
	return(OK);
}

int
tcpkick(struct tcb *ptcb)
{
	int	tcbnum = ptcb - &tcbtab[0];
	int	tv;

	tv = MKEVENT(SEND, tcbnum);
	if(ptcb->tcb_flags & TCBF_DELACK && !tmleft(tcps_oport, tv))
		tmset(tcps_oport, TCPQLEN, tv, TCP_ACKDELAY);
	else if(tcps_oport->Messages.mesg_MQueue.queue_count < TCPQLEN) { /* #P */
		Queue_Element *QE = QueueAlloc((long) tv, QTYPE_SIGNAL, 0);
		if(!QE)
			return(SYSERR);
		MessageBoxSend(&tcps_oport->Messages, QE);
	}
	else
		return(SYSERR);
		// tmset(tcps_oport, TCPQLEN, tv, TCP_ACKDELAY);	// a crock
	return(OK);
}


int NEAR
tcprexmt(int tcbnum, int event)
{
	struct	tcb	*ptcb = &tcbtab[tcbnum];

	if(event != RETRANSMIT)
		return(OK);
	if(++ptcb->tcb_rexmtcount > TCP_MAXRETRIES) {
		tcpabort(ptcb, TCPE_TIMEDOUT);
		return(OK);
	}

	tcpsend(tcbnum, TSF_REXMT);
	tmset(tcps_oport, TCPQLEN, MKEVENT(RETRANSMIT, tcbnum),
		min(ptcb->tcb_rexmt << ptcb->tcb_rexmtcount, TCP_MAXRXT));
	if(ptcb->tcb_ostate != TCPO_REXMT)
		ptcb->tcb_ssthresh = ptcb->tcb_cwnd;
	ptcb->tcb_ssthresh = min(ptcb->tcb_swindow, ptcb->tcb_ssthresh)/2;
	if(ptcb->tcb_ssthresh < ptcb->tcb_smss)
		ptcb->tcb_ssthresh = ptcb->tcb_smss;
	ptcb->tcb_cwnd = ptcb->tcb_smss;
	return(OK);
}

int NEAR
tcprwindow(struct tcb *ptcb)
{
	int	window;

	window = ptcb->tcb_rbsize - ptcb->tcb_rbcount;
	if(ptcb->tcb_state < TCPS_ESTABLISHED)
		return(window);

	if(window * 4 < ptcb->tcb_rbsize || window < ptcb->tcb_rmss)
		window = 0;
	window = max(window, ptcb->tcb_cwin - ptcb->tcb_rnext);
	ptcb->tcb_cwin = ptcb->tcb_rnext + window;
	return(window);
}

int NEAR
tcpswindow(struct tcb *ptcb, struct ep *pep)
{
	struct	ip	*pip = (struct ip *) pep->ep_data;
	struct	tcp	*ptcp = (struct tcp *) pip->ip_data;
	tcpseq		wlast, owlast;

	if(SEQCMP(ptcp->tcp_seq, ptcb->tcb_lwseq) < 0)
		return(OK);
	if(SEQCMP(ptcp->tcp_seq, ptcb->tcb_lwseq) == 0 &&
	   SEQCMP(ptcp->tcp_ack, ptcb->tcb_lwack) < 0)
		return(OK);
	owlast = ptcb->tcb_lwack + ptcb->tcb_swindow;
	wlast = ptcp->tcp_ack + ptcp->tcp_window;

	ptcb->tcb_swindow = ptcp->tcp_window;
	ptcb->tcb_lwseq = ptcp->tcp_seq;
	ptcb->tcb_lwack = ptcp->tcp_ack;
	if(SEQCMP(wlast, owlast) <= 0)
		return(OK);
	if(ptcb->tcb_ostate == TCPO_PERSIST) {
		tmclear(tcps_oport, MKEVENT(PERSIST, ptcb-&tcbtab[0]));
		ptcb->tcb_ostate = TCPO_XMIT;
	}
	tcpkick(ptcb);
	return(OK);
}

int NEAR
tcpsmss(struct tcb *ptcb, struct tcp *ptcp, char *popt)
{
	unsigned	mss, len;

	len = *++popt;
	popt++;
	if((ptcp->tcp_code & TCPF_SYN) == 0)
		return(len);
	switch (len - 2) {
		case 	1 :
			mss = *popt;
			break;
		case	sizeof(short) :
			mss = ntohs(*(unsigned short *) popt);
			break;
		case	sizeof(long) :
			mss = ntohl(*(unsigned long *) popt); /* #### might fail */
			break;
		default :;
			mss = ptcb->tcb_smss;
			break;
	}

	mss -= TCPMHLEN;
	if(ptcb->tcb_smss)
		ptcb->tcb_smss = min(mss, ptcb->tcb_smss);
	else
		ptcb->tcb_smss = mss;
	return(len);
}

int NEAR
tcpopts(struct tcb *ptcb, struct ep *pep)
{
	struct	ip	*pip = (struct ip *) pep->ep_data;
	struct	tcp	*ptcp = (struct tcp *) pip->ip_data;
	char		*popt, *popend;
	int		len;

	if(TCP_HLEN(ptcp) == TCPMHLEN)
		return(OK);
	popt = ptcp->tcp_data;
	popend = &pip->ip_data[TCP_HLEN(ptcp)];
	do {
		switch (*popt) {

			case 	TPO_NOOP :
					popt++;
					break;
			case	TPO_EOOL :
					break;
			case	TPO_MSS :
					popt += tcpsmss(ptcb, ptcp, popt);
					break;
			default :
				if(*(popt+1) < 1)
					goto quitloop;	// 6/27/94 bkc fix unknown opt support
				popt += *(popt+1);
				break;
		}
	} while(*popt != TPO_EOOL && popt < popend);
quitloop:;
	len = pip->ip_len - IP_HLEN(pip) - TCP_HLEN(ptcp);
	if(len)
		memcpy(ptcp->tcp_data, &pip->ip_data[TCP_HLEN(ptcp)], len);
	pip->ip_len = IP_HLEN(pip) + TCPMHLEN + len;
	ptcp->tcp_offset = TCPHOFFSET;
	return(OK);
}

int NEAR
tcprmss(struct tcb *ptcb, struct ip *pip)
{
	struct	tcp	*ptcp = (struct tcp *) pip->ip_data;
	int		mss, hlen, olen, i;

	hlen = TCP_HLEN(ptcp);
	olen = 2 + sizeof(short);
	pip->ip_data[hlen] = TPO_MSS;
	pip->ip_data[hlen+1] = olen;
#ifdef	BADORDER
	mss = htons((short) ptcb->tcb_rmss);
#else
	mss = ptcb->tcb_rmss;
#endif
	for(i=olen-1; i > 1;i--) {
		pip->ip_data[hlen+i] = mss & LOWBYTE;
		mss >>= 8;
	}
	hlen += olen + 3;
	ptcp->tcp_offset = ((hlen << 2) & 0xf0) | ptcp->tcp_offset & 0xf;

	return(OK);
}

int NEAR
tcprtt(struct tcb *ptcb)
{
	int	rtt, delta;

	rtt = tmclear(tcps_oport, MKEVENT(RETRANSMIT, ptcb-&tcbtab[0]));
	if(rtt != SYSERR && ptcb->tcb_ostate != TCPO_REXMT) {
		if(ptcb->tcb_srt == 0)
			ptcb->tcb_srt = rtt << 3;

		delta = rtt - (ptcb->tcb_srt >> 3);
		ptcb->tcb_srt += delta;
		if(delta < 0)
			delta = -delta;

		ptcb->tcb_rtde += delta - (ptcb->tcb_rtde >> 2);

		ptcb->tcb_rexmt = ((ptcb->tcb_srt >> 2) + ptcb->tcb_rtde) >> 1;
		if(ptcb->tcb_rexmt < TCP_MINRXT)
			ptcb->tcb_rexmt = TCP_MINRXT;
	}	/* end if rtt != syserr */
	if(ptcb->tcb_cwnd < ptcb->tcb_ssthresh)
		ptcb->tcb_cwnd += ptcb->tcb_smss;
	else
		ptcb->tcb_cwnd += (ptcb->tcb_smss * ptcb->tcb_smss)/ptcb->tcb_cwnd;
	return(OK);
}


int NEAR
tcpacked(struct tcb *ptcb, struct ep *pep)
{
	struct	ip	*pip = (struct ip *) pep->ep_data;
	struct	tcp	*ptcp = (struct tcp *) pip->ip_data;
	int		acked, tcbnum, cacked;
	tcpseq		wlast;
	int		rwindow;

	if(!(ptcp->tcp_code & TCPF_ACK))
		return(SYSERR);
	acked = ptcp->tcp_ack - ptcb->tcb_suna;
	cacked = 0;

	if(SEQCMP(ptcp->tcp_ack, ptcb->tcb_snext) >0) {
		if(ptcb->tcb_state == TCPS_SYNRCVD)
			return tcpreset(pep);
		else
			return(tcpackit(ptcb, pep));
	}
	// another fix
	rwindow = ptcb->tcb_rbsize - ptcb->tcb_rbcount;
//	wlast = ptcb->tcb_rnext + rwindow - 1;
	if(ptcb->tcb_rnext == ptcp->tcp_seq && !rwindow) {	// window probe ?
		return(tcpackit(ptcb, pep));
	}
	if(acked <=0 )		// was before SEQCMP ## bkc 8/2/94
		return(0);
	tcprtt(ptcb);
	ptcb->tcb_suna = ptcp->tcp_ack;
	if(acked && ptcb->tcb_code & TCPF_SYN) {
		acked--;
		cacked++;
		ptcb->tcb_code &= ~TCPF_SYN;
		ptcb->tcb_flags &= ~TCBF_FIRSTSEND;
	}
	if((ptcb->tcb_code & TCPF_FIN) &&
	    SEQCMP(ptcp->tcp_ack, ptcb->tcb_snext) == 0) {
		acked--;
		cacked++;
		ptcb->tcb_code &= ~TCPF_FIN;
		ptcb->tcb_flags &= ~TCBF_SNDFIN;
	}
	ptcb->tcb_sbstart = (ptcb->tcb_sbstart+acked) % ptcb->tcb_sbsize;
	ptcb->tcb_sbcount -= acked;
	tcpSetSocket(ptcb);
	if(acked && ptcb->tcb_ssema.sem_count <= 0)
		Semaphore_Signal(&ptcb->tcb_ssema);
	tcpostate(ptcb, acked+cacked);
	return(acked);

}

int  NEAR
tcpackit(struct tcb *ptcb, struct ep *pepin)
{
	struct	ep	*pepout;
	struct	ip	*pipin = (struct ip *) pepin->ep_data, *pipout;
	struct	tcp	*ptcpin = (struct tcp *) pipin->ip_data, *ptcpout;
	Buffer	*B;

	if(ptcpin->tcp_code & TCPF_RST)
		return(OK);
#if	GOOFYCODE
	// bkc fix for tcpack of zero window probe ### 8/2/94
	if(pipin->ip_len <= IP_HLEN(pipin) + TCP_HLEN(ptcpin) &&
		!(ptcpin->tcp_code & (TCPF_SYN|TCPF_FIN)))
			return(OK);	/* dup ack #P */
#endif
	B = BufferAlloc(TCPMHLEN + 8 + IPMHLEN + EP_HLEN);
	if(!B) {
		return(SYSERR);
	}
	pepout = (struct ep *) B->data;
	pipout = (struct ip *) pepout->ep_data;
	memcpy(pipout->ip_src, pipin->ip_dst, IP_ALEN);
	memcpy(pipout->ip_dst, pipin->ip_src, IP_ALEN);
	pipout->ip_proto = IPT_TCP;
	pipout->ip_verlen = (IP_VERSION<<4) | IP_MINHLEN;
	pipout->ip_len = IP_HLEN(pipout) + TCPMHLEN;
	ptcpout = (struct tcp *) pipout->ip_data;
	ptcpout->tcp_sport = ptcpin->tcp_dport;
	ptcpout->tcp_dport = ptcpin->tcp_sport;
	ptcpout->tcp_seq = ptcb->tcb_snext;
	ptcpout->tcp_ack = ptcb->tcb_rnext;
	ptcpout->tcp_code = TCPF_ACK;
	ptcpout->tcp_offset = TCPHOFFSET;
	ptcpout->tcp_window = tcprwindow(ptcb);
	ptcpout->tcp_urgptr = 0;
	tcph2net(ptcpout);
	ptcpout->tcp_cksum = 0;
	ptcpout->tcp_cksum = tcpcksum(pipout);
	TcpOutSegs++;
	return(ipsend(pipout->ip_dst, pepout, TCPMHLEN));
}

int NEAR
tcprcvurg(struct tcb *ptcb, struct ep *pep)
{
	struct	ip	*pip = (struct ip *) pep->ep_data;
	struct	tcp	*ptcp = (struct tcp *) pip->ip_data;
	struct	uqe	*puqe;
	int		datalen, offset;

	datalen = pip->ip_len - IP_HLEN(pip) - TCP_HLEN(ptcp);

#ifdef	BSDURG
	ptcp->tcp_urgptr -= 1;		/* shouldn't this be +1 ? */
#endif
	if(ptcp->tcp_urgptr >= datalen || ptcp->tcp_urgptr >= TCPMAXURG)
		ptcp->tcp_urgptr = datalen-1;
	puqe = uqalloc();
	if(!puqe)
		return(SYSERR);
	puqe->uq_seq = ptcp->tcp_seq;
	puqe->uq_len = ptcp->tcp_urgptr + 1;
	puqe->uq_B = BufferAlloc(puqe->uq_len);
	if(!puqe->uq_B) {
		uqfree(puqe);
		return(SYSERR);
	}
	puqe->uq_data = puqe->uq_B->data;
	memcpy(puqe->uq_data, &pip->ip_data[TCP_HLEN(ptcp)],
		puqe->uq_len);

	puqe->QE.queue_key = ptcb->tcb_rudseq - puqe->uq_seq;
	QueueInsert(&ptcb->tcb_rudq, (Queue_Element *) puqe);
	if(datalen > puqe->uq_len) {
		ptcp->tcp_seq += puqe->uq_len;
		offset = TCP_HLEN(ptcp) + puqe->uq_len;
		memcpy(&pip->ip_data[TCP_HLEN(ptcp)],
			&pip->ip_data[offset], datalen - puqe->uq_len);
		ptcp->tcp_urgptr = 0;
		pip->ip_len -= puqe->uq_len;
	}
	return(OK);
}

struct uqe
*uqalloc()
{
	static	int	last = 0;
	struct	uqe	*puqe;
	int		count;

	if(!uqidone)
		uqinit();
	Semaphore_Wait(&uqmutex);
	for(count = 0; count < UQTSIZE; count++) {
		if(++last >= UQTSIZE)
			last = 0;
		puqe = &uqtab[last];
		if(puqe->uq_state == UQS_FREE) {
			memset(puqe, 0, sizeof(struct uqe));
			puqe->uq_state = UQS_ALLOC;
			Semaphore_Signal(&uqmutex);
			return(puqe);
		}
	}
	Semaphore_Signal(&uqmutex);
	return(NULL);
}

int
uqinit()
{
	int	i;

	uqidone = 1;
	Semaphore_Initialize(&uqmutex, 0);
	for(i=0; i < UQTSIZE; i++)
		uqtab[i].uq_state = UQS_FREE;	/* probably not needed */
	Semaphore_Signal(&uqmutex);
	return(OK);
}

int
uqfree(struct uqe *puqe)
{
	Semaphore_Wait(&uqmutex);
	if(puqe->uq_B)
		BufferFree(puqe->uq_B);
	puqe->uq_state = UQS_FREE;
	Semaphore_Signal(&uqmutex);
	return(OK);
}

int
tcprurg(struct tcb *ptcb, char *pch, int len)
{
	struct	uqe	*puqe;
	int		cc, uc;
	tcpseq		cseq, eseq;

	puqe = (struct uqe *) QueuePHead(&ptcb->tcb_rudq);
	if(!puqe)
		return(0);
	cseq = puqe->uq_seq;
	for(cc = uc = 0; cc < len && puqe;) {
		*pch++ = puqe->uq_data[uc++];
		++cc;
		if(uc >= puqe->uq_len) {
			while(puqe) {
				eseq = puqe->uq_seq + puqe->uq_len - 1;
				if(SEQCMP(cseq, eseq) < 0)
					break;
				tcpaddhole(ptcb, puqe);
				puqe = (struct uqe *) QueuePHead(&ptcb->tcb_rudq);
			}
			if(!puqe)
				break;
			if(SEQCMP(cseq, puqe->uq_seq) < 0) {
				cseq = puqe->uq_seq;
				uc = 0;
			}
			else
				uc = cseq - puqe->uq_seq;
		}
		else
			cseq++;
	}	/* end for */
	if(!puqe) {
		return(cc);
	}
	if(uc) {
		puqe->uq_seq = cseq;
		puqe->uq_len -= uc;
		memcpy(puqe->uq_data, &puqe->uq_data[uc], puqe->uq_len);
	}
	puqe->QE.queue_key = RUDK(ptcb, puqe->uq_seq);
	QueueInsert(&ptcb->tcb_rudq, (Queue_Element *) puqe);
	return(cc);
}

int NEAR
tcpaddhole(struct tcb *ptcb, struct uqe *puqe)
{
	if(puqe->uq_B) {
		BufferFree(puqe->uq_B);
		puqe->uq_B =NULL;
		puqe->uq_data = NULL;
	}
	puqe->QE.queue_key = RUHK(ptcb, puqe->uq_seq);
	QueueInsert(&ptcb->tcb_ruhq, (Queue_Element *) puqe);
	return(OK);
}

struct	uqe
*tcprhskip(struct tcb *ptcb, struct uqe *puqe, tcpseq seq)
{
	if(seq < puqe->uq_seq + puqe->uq_len) {
		ptcb->tcb_rbcount -= puqe->uq_len;
		ptcb->tcb_rbstart += puqe->uq_len;
		ptcb->tcb_rbstart %= ptcb->tcb_rbsize;
	}

	uqfree(puqe);
	puqe = (struct uqe *) QueuePHead(&ptcb->tcb_ruhq);
	tcpSetSocket(ptcb);
	return(puqe);
}

int
tcpgetdata(struct tcb *ptcb, char *pch, int len)
{
	struct	uqe	*puqe;
	tcpseq		seq;
	int		cc, win, closewin;

	if(ptcb->tcb_ruhq.queue_count > 0)
		puqe = (struct uqe *) QueuePHead(&ptcb->tcb_ruhq);
	else
		puqe = NULL;
	seq = ptcb->tcb_rnext - ptcb->tcb_rbcount;
#ifdef	NORMAL
	for(cc=0; ptcb->tcb_rbcount && cc < len; ) {
		if(puqe && SEQCMP(puqe->uq_seq, seq) <= 0) {
			puqe = tcprhskip(ptcb, puqe, seq);
			continue;
		}
		*pch++ = ptcb->tcb_rcvbuf[ptcb->tcb_rbstart];
		--ptcb->tcb_rbcount;
		if(++ptcb->tcb_rbstart >= ptcb->tcb_rbsize)
			ptcb->tcb_rbstart = 0;
		cc++;
	}
#else
	if(puqe) {
		for(cc=0; ptcb->tcb_rbcount && cc < len; ) {
			if(puqe && SEQCMP(puqe->uq_seq, seq) <= 0) {
				puqe = tcprhskip(ptcb, puqe, seq);
				continue;
			}
			*pch++ = ptcb->tcb_rcvbuf[ptcb->tcb_rbstart];
			--ptcb->tcb_rbcount;
			if(++ptcb->tcb_rbstart >= ptcb->tcb_rbsize)
				ptcb->tcb_rbstart = 0;
			cc++;
		}
		if(puqe) {
			puqe->QE.queue_key = RUHK(ptcb, puqe->uq_seq);
			QueueInsert(&ptcb->tcb_ruhq, (Queue_Element *) puqe);
		}
	}
	else {		/* no urgent data to receive */
		int	xlen;

		if(len > ptcb->tcb_rbcount)
			len = ptcb->tcb_rbcount;
		xlen = ptcb->tcb_rbsize - ptcb->tcb_rbstart;
		if(xlen > len)
			xlen = len;
		memcpy(pch, ptcb->tcb_rbstart+ptcb->tcb_rcvbuf, xlen);
		if(xlen < len)
			memcpy(pch + xlen, ptcb->tcb_rcvbuf, len - xlen);
		ptcb->tcb_rbstart = (ptcb->tcb_rbstart + len) % ptcb->tcb_rbsize;
		ptcb->tcb_rbcount -= len;
		cc = len;
	}
#endif

	tcpSetSocket(ptcb);

	if(ptcb->tcb_rbcount == 0)
		ptcb->tcb_flags &= ~TCBF_PUSH;

#ifdef	WRONG
	if(SEQCMP(ptcb->tcb_cwin, ptcb->tcb_rnext) <= 0 &&
#else
	win = ptcb->tcb_rbsize - ptcb->tcb_rbcount;
	closewin = max(ptcb->tcb_rbsize/4, ptcb->tcb_rmss);
	if((win*4 >= ptcb->tcb_rbsize || win >= ptcb->tcb_rmss) &&
		(ptcb->tcb_cwin - ptcb->tcb_rnext  < closewin) &&
#endif
		tcprwindow(ptcb)) {
			ptcb->tcb_flags |= TCBF_NEEDOUT;
			tcpkick(ptcb);
	}
	return(cc);
}

int
tcpwurg(struct tcb *ptcb, int sboff, int len)
{
	struct 	uqe	*puqe;

	puqe = uqalloc();
	if(!puqe)
		return(SYSERR);
	puqe->uq_seq = ptcb->tcb_suna + sboff;
	puqe->uq_len = len;
	puqe->QE.queue_key = SUDK(ptcb, puqe->uq_seq);
	QueueInsert(&ptcb->tcb_sudq, (Queue_Element *) puqe);
	return(len);
}

int
tcpsndurg(int	tcbnum)
{
	struct	tcb	*ptcb = &tcbtab[tcbnum];
	int		tv;

	ptcb->tcb_code |= TCPF_URG;
	while(ptcb->tcb_sudq.queue_count > 0)
		tcpsend(tcbnum, TSF_NEWDATA);
	ptcb->tcb_code &= ~TCPF_URG;
	return(OK);
}

int NEAR
tcpostate(struct tcb *ptcb, int acked)
{

	if(acked <= 0)
		return(OK);
	if(ptcb->tcb_ostate == TCPO_REXMT) {
		ptcb->tcb_rexmtcount = 0;
		ptcb->tcb_ostate = TCPO_XMIT;
	}

	if(ptcb->tcb_sbcount == 0) {
		ptcb->tcb_ostate = TCPO_IDLE;
		return(OK);
	}
	tcpkick(ptcb);
	return(OK);
}

int NEAR
tcpshskip(struct tcb *ptcb, int datalen, int * poff)
{
	struct  uqe     *puqe;
	tcpseq          lseq;
	int             resid = 0;

	if (ptcb->tcb_suhq.queue_count == 0)
		return datalen;
	puqe = (struct uqe *)QueuePHead(&ptcb->tcb_suhq);
	while (puqe) {
		lseq = ptcb->tcb_snext + datalen - 1;
		if (SEQCMP(lseq, puqe->uq_seq) > 0)
			resid = lseq - puqe->uq_seq;
		/* chop off at urgent boundary, but save the hole...    */
		if (resid < datalen) {
			datalen -= resid;
			puqe->QE.queue_key = SUHK(ptcb, puqe->uq_seq);
			QueueInsert(&ptcb->tcb_suhq, (Queue_Element *) puqe);
			return datalen;
		}
		/*
		 * else, we're skipping a hole now adjust the beginning
		 * of the segment.
		 */
		lseq = puqe->uq_seq + puqe->uq_len;
		resid = lseq - ptcb->tcb_snext;
		if (resid > 0) {
			/* overlap... */
			*poff += resid;
			if (*poff > ptcb->tcb_sbsize)
				*poff -= ptcb->tcb_sbsize;
			datalen -= resid;
			if (datalen < 0)
				datalen = 0;
		}
		uqfree(puqe);
		puqe = (struct uqe *)QueuePHead(&ptcb->tcb_suhq);
	}
	/* puqe == 0 (No more holes left) */
	return datalen;
}



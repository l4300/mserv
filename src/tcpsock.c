/* tcpsock.c */
#include	<stdlib.h>

#include	"network.h"

#include	"task.h"

#include	"tcp.h"
#include	"tcb.h"
#include	"tcpfsm.h"
#include	"tcptimer.h"

#include	"sys/socket.h"
#include	"sys/neterror.h"
#include	"netinet/in.h"

#include	<memcheck.h>

extern	long	TcpActiveOpens;
extern	long	TcpCurrEstab;


int
tcp_findlport(unsigned int lport)		// return true if local port found
						// in tcb list
{
	struct	tcb	*ptcb;
	int	tcbfound = 0;
	int	tcbn;

	Semaphore_Wait(&tcps_tmutex);
	for(tcbn=0, ptcb=&tcbtab[0]; tcbn < Ntcp; tcbn++, ptcb++) {
		if(ptcb->tcb_state == TCPS_FREE)
			continue;
		if(lport == ptcb->tcb_lport) {
			tcbfound++;
			break;
		}
	}
	Semaphore_Signal(&tcps_tmutex);
	return(tcbfound);
}

#pragma argsused
int
tcp_proto(struct _socket *so, int req, void *data)
{
	struct	tcb	*ptcb = (struct tcb *) so->so_pcb;
	struct	sockaddr_in	*sin = (struct sockaddr_in *) data;
	struct	_sendto	*sendto = (struct _sendto *) data;
	int	val;
	int	rc = OK;
	int	cc;
	int	*i;

	switch (req) {
		case PR_OPEN :		/* attempt to obtain a TCB */
			if(so->so_family != AF_INET || so->so_protocol != SOCK_STREAM) {
protoerr:;
				SETERROR(EPROTO);
				rc = SYSERR;
				break;
			}
			ptcb = tcballoc();
			if(!ptcb) {
				SETERROR(ENOSR);
				rc = SYSERR;
				break;
			}
			so->so_pcb = ptcb;
			ptcb->tcb_error = 0;
			ptcb->tcb_rbsize = TCPRBS;
			ptcb->tcb_sbsize = TCPSBS;
			so->so_recvqueue = TCPRBS;
			ptcb->tcb_socket = so;
			break;
		case PR_BIND :		/* bind local address */
			if((sin->sin_family && sin->sin_family != AF_INET) ||
				(so->so_family != AF_INET) || (so->so_protocol != SOCK_STREAM)) {
					goto protoerr;
			}
			if(sin->sin_addr.s_addr == INADDR_ANY) {
				if(!nif[1].ni_ivalid)
					goto protoerr;
				memcpy(&sin->sin_addr.s_ip, nif[1].ni_ip, IP_ALEN);
				sin->sin_family = AF_INET;
			}
			if(!sin->sin_port || sin->sin_port == 0xffff) {
				int	lim = 0x6000;
				unsigned int x = (Ticks() & 0x0fff) | 0x1024;

				if(sin->sin_port == 0xffff) {
					lim = 1024;
					x = random(lim >> 1) + 512;
				}
				while(x < lim) {
					sin->sin_port = x;
					if(!sofind((struct sockaddr *) sin, NULL, sizeof(struct sockaddr), SOCK_STREAM) &&
						!tcp_findlport(x)) {
						break;
					}
					x++;
				}
				if(x == lim)
					goto protoerr;
				sin->sin_port = htons(x);
			}
			else {
				if(sofind((struct sockaddr *) sin, NULL, sizeof(struct sockaddr), SOCK_STREAM) ||
					tcp_findlport(htons(sin->sin_port))) {
					SETERROR(EADDRINUSE);
					rc = SYSERR;
					break;
				}
			}
			ptcb->tcb_lport = htons(sin->sin_port);
			memcpy(ptcb->tcb_lip, &sin->sin_addr.s_ip,IP_ALEN);
			break;

		case PR_CONNECT :	/* connect to remote port */
			if(sin->sin_family != AF_INET)
				goto protoerr;
			{
				struct route *prt;

				if(0L == sin->sin_addr.s_addr) {
					SETERROR(EADDRNOTAVAIL);
					rc = SYSERR;
					break;
				}
				memcpy(ptcb->tcb_rip, &sin->sin_addr.s_ip, IP_ALEN);
				ptcb->tcb_rport = htons(sin->sin_port);
				prt = rtget(ptcb->tcb_rip, RTF_LOCAL);
				if(!prt) {
					SETERROR(EADDRNOTAVAIL);
					rc = SYSERR;
					break;
				}
				ptcb->tcb_pni = &nif[prt->rt_ifnum];
				rtfree(prt);
				if(tcpsync(ptcb) != OK) {
					SETERROR(ENOSR);
					rc = SYSERR;
					break;
				}
				so->so_opt |= SOOPT_CANTCONNECTMORE;
				rc = tcpcon(ptcb);	/* this blocks */
				if(rc) {	/* an error occured */
					SETERROR(rc);
					rc = SYSERR;
					break;
				}
				else
					so->so_selectflags |= SOSEL_WRITE;
			}
			break;
		case PR_LISTEN :	/* listen,  up to val listen's */
			ptcb->tcb_type = TCPT_SERVER;
			ptcb->tcb_state = TCPS_LISTEN;
			val = *((int *) data);
			ptcb->tcb_lqsize = val;
			memset(&ptcb->tcb_listenq, 0, sizeof(Queue));
			ptcb->tcb_smss = 0;
			Semaphore_Signal(&ptcb->tcb_mutex);
			break;

		case PR_ACCEPT :	/* reap connections */

			break;

		case PR_SHUTDOWN :	/* should half close connection ... */

			break;
		case PR_RECVFROM :
			if(*sendto->s_fromlen < sizeof(struct sockaddr_in))
				goto proerror;

		case PR_READ :
			if(ptcb->tcb_state != TCPS_ESTABLISHED &&
			   ptcb->tcb_state != TCPS_CLOSEWAIT)
				goto protoerr;
retry_read:;
			Semaphore_Wait(&ptcb->tcb_rsema);
			Semaphore_Wait(&ptcb->tcb_mutex);

			if(ptcb->tcb_state == TCPS_FREE)
				goto protoerr;
			if(ptcb->tcb_error) {
				tcpwakeup(READERS, ptcb);
				Semaphore_Signal(&ptcb->tcb_mutex);
				SETERROR(ptcb->tcb_error);
				so->so_opt |= SOOPT_CANTRECVMORE;
				rc = SYSERR;
				break;
			}
#if OLDONE	/* this method won't push up data or update rwin and leads to deadlock
		   when the sender does not use PUSH. 10/15/94 BKC
		*/
			if(ptcb->tcb_rudq.queue_count == 0) {	/* no urgent data */
				if(sendto->s_len > ptcb->tcb_rbcount &&
				   !(so->so_opt & SOOPT_NOBLOCK) &&
				   (ptcb->tcb_flags & (TCBF_PUSH|TCBF_RDONE)) == 0) { // ### 8/5 missing paren around or_bits
					Semaphore_Signal(&ptcb->tcb_mutex);
					goto retry_read;
				   }
				   rc = tcpgetdata(ptcb,sendto->s_msg, sendto->s_len);
			}
#else
	/* this method says that if there is any data in the input queue,
	   satisfy the read request immediately regardless of
	   PUSH */
			if(ptcb->tcb_rudq.queue_count == 0) {	/* no urgent data */
				if(!ptcb->tcb_rbcount &&
				   !(so->so_opt & SOOPT_NOBLOCK) &&
				   (ptcb->tcb_flags & (TCBF_PUSH|TCBF_RDONE)) == 0) { // ### 8/5 missing paren around or_bits
					Semaphore_Signal(&ptcb->tcb_mutex);
					goto retry_read;
				   }
				   rc = tcpgetdata(ptcb,sendto->s_msg, sendto->s_len);
			}
#endif
			else {	/* yes urgent data */
				rc = tcprurg(ptcb, sendto->s_msg, sendto->s_len);
			}
			if(ptcb->tcb_rbcount == 0) {
				if(ptcb->tcb_rsema.sem_count > 0)
					ptcb->tcb_rsema.sem_count = 0;	// ### 8/6/94 zero win fix
				so->so_selectflags &= ~SOSEL_READ;
			}
			if(!rc && (ptcb->tcb_flags & TCBF_RDONE) == 0) {
				Semaphore_Signal(&ptcb->tcb_mutex);
				goto retry_read;
			}
			if(!rc && (ptcb->tcb_flags & TCBF_RDONE)) {	/* no more data to read */
#ifdef	WRONG
				rc = SYSERR;
				SETERROR(EBADF);
#endif
				so->so_opt |= SOOPT_CANTRECVMORE;
			}

			if(req == PR_RECVFROM) {
				struct sockaddr_in *sin = (struct sockaddr_in *) sendto->s_to;
				struct sockaddr_in *remote = (struct sockaddr_in *) &so->so_remote;
				  memcpy(sin->sin_addr.s_ip, remote->sin_addr.s_ip, IP_ALEN);
				  sin->sin_port = remote->sin_port;
				  sin->sin_family = AF_INET;
				  *sendto->s_fromlen = sizeof(struct sockaddr_in);
				  sendto->s_flags = 0;
			}

			tcpwakeup(READERS, ptcb);
			Semaphore_Signal(&ptcb->tcb_mutex);

			break;

		case PR_WRITE :
			if(ptcb->tcb_state != TCPS_ESTABLISHED && ptcb->tcb_state != TCPS_CLOSEWAIT)
				goto protoerr;
			if(ptcb->tcb_error) {
				tcpwakeup(WRITERS, ptcb);
				SETERROR(ptcb->tcb_error);
				so->so_opt |= SOOPT_CANTSENDMORE;
				rc = SYSERR;
				break;
			}
			{
				int	tocopy, sboff,xcount;
				register char *pch = sendto->s_msg;

				tocopy = tcpgetspace(ptcb, sendto->s_len);
				if(tocopy <= 0) {
					rc = tocopy;
					break;
				}
				sboff = (ptcb->tcb_sbstart + ptcb->tcb_sbcount) % ptcb->tcb_sbsize;
#ifndef	OLDWAY
				xcount = ptcb->tcb_sbsize - sboff;	/* the part that fits in the front */
				if(xcount > tocopy)
					xcount = tocopy;
				memcpy(ptcb->tcb_sndbuf + sboff, pch, xcount);
				if(xcount < tocopy)
					memcpy(ptcb->tcb_sndbuf, pch + xcount, tocopy - xcount);
				ptcb->tcb_sbcount += tocopy;
#else
				ptcb->tcb_sbcount += tocopy;
				while(tocopy--) {
					ptcb->tcb_sndbuf[sboff] = *pch++;
					if(++sboff >= ptcb->tcb_sbsize)
						sboff = 0;
				}
#endif
				ptcb->tcb_flags |= TCBF_NEEDOUT;
				tcpwakeup(WRITERS, ptcb);
				Semaphore_Signal(&ptcb->tcb_mutex);
				if(ptcb->tcb_snext == ptcb->tcb_suna)
					tcpkick(ptcb);
				rc = sendto->s_len;
			}
			break;
		case PR_CLOSE :
			Semaphore_Wait(&ptcb->tcb_mutex);
			switch(ptcb->tcb_state) {
				case TCPS_LISTEN :
				case TCPS_ESTABLISHED :
				case TCPS_CLOSEWAIT :
					break;
				case TCPS_CLOSED :	/* recover from timeout in tcpcon */
					tcbdealloc(ptcb);
					goto doquit;
				case TCPS_FREE:
					goto protoerr;
				default :;
					Semaphore_Signal(&ptcb->tcb_mutex);
					goto protoerr;
			}
			if(ptcb->tcb_error || ptcb->tcb_state == TCPS_LISTEN) {
				tcbdealloc(ptcb);
				break;
			}

			TcpCurrEstab--;
			ptcb->tcb_flags |= TCBF_SNDFIN;	/* #P */
			ptcb->tcb_slast = ptcb->tcb_suna + ptcb->tcb_sbcount;	/* #P */
			ptcb->tcb_flags |= TCBF_NEEDOUT;
			if(ptcb->tcb_state == TCPS_ESTABLISHED)
				ptcb->tcb_state = TCPS_FINWAIT1;
			else
				ptcb->tcb_state = TCPS_LASTACK;

			Semaphore_Signal(&ptcb->tcb_mutex);
			while(tcpkick(ptcb) == SYSERR)
				xdelay(1);

			Semaphore_Wait(&ptcb->tcb_ocsem);
			rc = ptcb->tcb_error;
			if(ptcb->tcb_state == TCPS_LASTACK)
				tcbdealloc(ptcb);

			break;
		case PR_SETSOCKOPT :
			i = (int *) sendto->s_msg;
			switch (sendto->s_flags) {

				case TCPOPT_SETRCVBUF :
					if(ptcb->tcb_state  > TCPS_LISTEN)
						goto protoerr;
					ptcb->tcb_rbsize = *i;
					so->so_recvqueue = *i;
					break;
				case TCPOPT_SETSNDBUF :
					if(ptcb->tcb_state > TCPS_LISTEN)
						goto protoerr;
					ptcb->tcb_sbsize = *i;
					break;
				default:;
					goto protoerr;
			}
			break;
		default :
proerror:;
			SETERROR(EPROTO);
			rc = SYSERR;
			break;
	} /* end switch req */
doquit:;
	return(rc);
}


int
tcpcon(struct tcb *ptcb)
{
	struct	netif 	*pni = ptcb->tcb_pni;
	struct 	route	*prt;
	Bool	local;
	int	error, mss;

	prt = rtget(ptcb->tcb_rip, RTF_REMOTE);
	local = prt && prt->rt_metric == 0;
	rtfree(prt);
	if(local) {
		mss = pni->ni_mtu - IPMHLEN - TCPMHLEN;
	}
	else
		mss = 536;
	ptcb->tcb_smss = mss;
	ptcb->tcb_rmss = mss;
	ptcb->tcb_swindow = mss;
	ptcb->tcb_cwnd = mss;
	ptcb->tcb_ssthresh = 65535;
	ptcb->tcb_rnext = 0;
	ptcb->tcb_finseq = 0;
	ptcb->tcb_pushseq = 0;
	ptcb->tcb_flags = TCBF_NEEDOUT|TCBF_FIRSTSEND;
	ptcb->tcb_ostate = TCPO_IDLE;
	ptcb->tcb_state = TCPS_SYNSENT;
	while(tcpkick(ptcb) == SYSERR) {
		xdelay(1);			/* shame on us */
	}
	TcpActiveOpens++;
//	Semaphore_Signal(&ptcb->tcb_mutex);	/* ### NOT NEEDED HERE */
	Semaphore_Wait(&ptcb->tcb_ocsem);
	if(ptcb->tcb_error)
		ptcb->tcb_state = TCPS_CLOSED;
	return(ptcb->tcb_error);
}

.AUTODEPEND

#		*Translator Definitions*
CC = bcc +TTCP.CFG
TASM = TASM
TLIB = tlib
TLINK = tlink
LIBPATH = E:\BORLANDC\LIB
INCLUDEPATH = E:\BORLANDC\INCLUDE;.;C:\ZINC\INCLUDE;D:\PRJ\SRC\TCPSYS\INCLUDE;


#		*Implicit Rules*
.c.obj:
  $(CC) -c {$< }

.cpp.obj:
  $(CC) -c {$< }

#		*List Macros*


EXE_dependencies =  \
 ttcp.obj \
 netmain.obj \
 {$(LIBPATH)}net.lib \
 {$(LIBPATH)}system.lib \
 {$(LIBPATH)}unet.lib

#		*Explicit Rules*
ttcp.exe: ttcp.cfg $(EXE_dependencies)
  $(TLINK) /v/s/c/P-/L$(LIBPATH) @&&|
c0s.obj+
ttcp.obj+
netmain.obj
ttcp,ttcp
net.lib+
system.lib+
unet.lib+
emu.lib+
maths.lib+
cs.lib
|


#		*Individual File Dependencies*
ttcp.obj: ttcp.cfg ttcp.c 

netmain.obj: ttcp.cfg netmain.c 

#		*Compiler Configuration File*
ttcp.cfg: ttcp.mak
  copy &&|
-v
-O
-Oe
-Ob
-Z
-k-
-vi-
-H=TTCP.SYM
-w-ret
-w-nci
-w-inl
-wpin
-wamb
-wamp
-w-par
-wasm
-wcln
-w-cpt
-wdef
-w-dup
-w-pia
-wsig
-wnod
-w-ill
-w-sus
-wstv
-wucp
-wuse
-w-ext
-w-ias
-w-ibc
-w-pre
-w-nst
-I$(INCLUDEPATH)
-L$(LIBPATH)
-DDOS;NETDEBUG
| ttcp.cfg



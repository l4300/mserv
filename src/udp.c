/* udp.c */
#define	STATIC
/* #define	OLDWAY */
#include	<mem.h>
#include	"network.h"
#include	"sys/errno.h"
#include	"sys/socket.h"
#include	"netinet/in.h"
#include	"udp.h"
#include	"task.h"

#include	<memcheck.h>

STATIC	struct	upq	_upqs[MAX_SOCKETS];
STATIC	Semaphore	udpmutex;

#define	UDPBUFF_RETRIES	50

long	UdpCkSumErrors;
long	UdpNoPorts;
long	UdpDiscards;
long	UdpInDatagrams;
long	UdpOutDatagrams;
long	UdpBuffRetries;

int
udp_init()
{
	Semaphore_Initialize(&udpmutex, 1);
	return(0);
}

int
udp_proto(struct _socket *so, int pr, void *data)
{
	int	rc = OK;
	int	len;
	struct	sockaddr_in	*sin = (struct sockaddr_in *) data;
	struct	sockaddr_in	*local = (struct sockaddr_in* ) &so->so_local;
	struct	_sendto		*sendto = (struct _sendto *) data;
	struct	_rwbuffer	*rwb = (struct _rwbuffer *) data;
	struct	ep		*pep;
	struct	ip		*pip;
	struct	udp		*pudp;
	struct	upq		*u = &_upqs[socktohandle(so)];
	Buffer			*B;
	int			retries = 0;

	Semaphore_Wait(&udpmutex);

	switch(pr) {
		case PR_OPEN :
			if(so->so_family != AF_INET || so->so_protocol != SOCK_DGRAM)
				goto proerror;

			so->so_recvqueue = MAX_UDP_RBYTES;
			so->so_pcb = &_upqs[socktohandle(so)];
			memset(&_upqs[socktohandle(so)], 0,sizeof(struct upq));
			_upqs[socktohandle(so)].up_data.queue_flags = QUEUE_FLAGS_NOTSORTED;
			_upqs[socktohandle(so)].up_socket = so;
			break;
		case PR_BIND :	/* bind local address */
			if((sin->sin_family && sin->sin_family != AF_INET) ||
			   (so->so_family != AF_INET) || (so->so_protocol != SOCK_DGRAM)) {
proerror:;
				SETERROR(EPROTO);
				rc = SYSERR;
				break;
			}

			if(!memcmp(&sin->sin_addr.s_addr,ip_anyaddr,IP_ALEN)) {	/* any port, use primary interface */
				if(!nif[1].ni_ivalid)
					goto proerror;
				memcpy(&sin->sin_addr.s_ip, nif[1].ni_ip, IP_ALEN);
				sin->sin_family = AF_INET;
			}

			if(!sin->sin_port) {	/* assign a port */
				unsigned int	x = ULPORT;
				while( x < 0x6000) {
					sin->sin_port = htons(x);
					if(!sofind((struct sockaddr *) sin, NULL, sizeof(struct sockaddr), SOCK_DGRAM)) {
						sin->sin_port = x;
						break;
					}
					x++;
				}
				if(x == 0x6000)
					goto proerror;
			}
			else
				sin->sin_port=sin->sin_port;	/* was htons */
			/* attach upq to so */
			so->so_selectflags |= SOSEL_WRITE;
			break;
	case PR_CONNECT :		/* set remote IP address */
		if(sin->sin_family != AF_INET)
			goto proerror;
		break;
	case PR_WRITE :
		sendto->s_tolen = sizeof(struct sockaddr_in);
		sendto->s_to = &so->so_remote;
	case PR_SENDTO :		/* send a packet */

		len = sendto->s_len;
		if(sendto->s_tolen < sizeof(struct sockaddr_in) ||
		    ((struct sockaddr_in *) sendto->s_to)->sin_family != AF_INET || len > (MAXLRGBUF - EP_HLEN - U_HLEN - IPMHLEN))
			goto proerror;

		B = BufferAlloc(len + U_HLEN + IPMHLEN + EP_HLEN);
		while(!B && ++retries < UDPBUFF_RETRIES) {
			TaskReschedule();
			B = BufferAlloc(len + U_HLEN + IPMHLEN + EP_HLEN);
			UdpBuffRetries++;
		}
		if(!B) {
			SETERROR(ENOSR);
			rc = SYSERR;
			break;
		}
		pep = (struct ep *) B->data;
		pip = (struct ip * ) pep->ep_data;
		pudp = (struct udp *) pip->ip_data;
		memcpy(pudp->u_data, sendto->s_msg, len);
		rc = udpsend(local->sin_addr.s_ip, ((struct sockaddr_in *) sendto->s_to)->sin_addr.s_ip,
			((struct sockaddr_in *) sendto->s_to)->sin_port, local->sin_port, pep, len, sendto->s_flags & MSG_UDPCSUM);
		if(rc >= 0)
			rc = len;
		break;

	case PR_SHUTDOWN :
		break;
	case PR_RECVFROM :		/* read a packet from the queue */
		if(*sendto->s_fromlen < sizeof(struct sockaddr_in))
			goto proerror;
	case PR_READ :
		B = (Buffer *) QueuePHead(&u->up_data);
		if(B) {
			struct	sockaddr_in	*sin = (struct sockaddr_in *) sendto->s_to;

			pep = (struct ep *) B->data;
			pip = (struct ip *) pep->ep_data;
			pudp = (struct udp *) pip->ip_data;
			len = pudp->u_len - U_HLEN;
			so->so_recvqueue += len;
			so->so_readable -= len;
			if(len > sendto->s_len)
				len = sendto->s_len;

			memcpy(sendto->s_msg, pudp->u_data,len);
			if(pr == PR_RECVFROM) {
				  memcpy(sin->sin_addr.s_ip, pip->ip_src, IP_ALEN);
				  sin->sin_port = pudp->u_src;
				  sin->sin_family = AF_INET;
				  sin->sin_nifptr = B->QE.queue_object;
				  *sendto->s_fromlen = sizeof(struct sockaddr_in);
				  sendto->s_flags = 0;
				  sendto->s_len = len;
			}
			if(u->up_data.queue_count < 1)
			   so->so_selectflags &= ~SOSEL_READ;	/* nothing to read */
			BufferFree(B);
			rc = len;
		  }
		  else {
			 goto proerror;
		  }
		  break;
	  case PR_READBUF :

		  B = (Buffer *) QueuePHead(&u->up_data);
		  if(B) {

			  pep = (struct ep *) B->data;
			  pip = (struct ip *) pep->ep_data;
			  pudp = (struct udp *) pip->ip_data;
			  len = pudp->u_len - U_HLEN;
			  rwb->rw_buffer = B;
			  rwb->rw_data = pudp->u_data;
			  rwb->rw_len = len;
			  so->so_recvqueue += len;
			  so->so_readable -= len;
			rc = len;
			if(u->up_data.queue_count < 1)
				so->so_selectflags &= ~SOSEL_READ;	/* nothing to read */
		}
		else {
			goto proerror;
		}
		break;
	case PR_LISTEN :
	case PR_ACCEPT :
		goto proerror;

	case PR_CLOSE :
		while(B = (Buffer *) QueuePHead(&u->up_data))
			BufferFree(B);
		memset(u, 0, sizeof(struct upq));
		break;
	default :
		SETERROR(EPROTO);
		rc = SYSERR;
	}

	Semaphore_Signal(&udpmutex);
	return(rc);

}

#pragma argsused

int
udp_in(struct netif *pni, struct ep *pep)
{
	struct	ip	*pip = (struct ip *) pep->ep_data;
	struct	udp	*pudp = (struct udp *) pip->ip_data;
	struct	_socket	*so;
	struct	upq	*uq;

	struct	sockaddr_in	slocal, sremote;
	unsigned	int	dst;
	int		i;
#if	U_DO_CSUM
	if(pudp->u_cksum && udpcksum(pip)) {
		BufferFreeData(pep);
		UdpCkSumErrors++;
		return(SYSERR);
	}
#endif
	udpnet2h(pudp);
	Semaphore_Wait(&udpmutex);
	memset(&slocal, 0, sizeof(slocal));
	memset(&sremote, 0, sizeof(slocal));
	slocal.sin_family = AF_INET;
	sremote.sin_family = AF_INET;
	slocal.sin_port = pudp->u_dst;
	sremote.sin_port = pudp->u_src;
	memcpy(&sremote.sin_addr.s_ip, pip->ip_src, IP_ALEN);
	memcpy(&slocal.sin_addr.s_ip, pip->ip_dst, IP_ALEN);
	so = sofind((struct sockaddr *) &slocal, (struct sockaddr *) &sremote, sizeof(sremote), SOCK_DGRAM);
	if(!so) {	/* no takers */
		UdpNoPorts++;
		if(!isbrc(pip->ip_dst))
			icmp(ICT_DESTUR, ICC_PORTUR, pip->ip_src, (char *) pep, NULL);
		Semaphore_Signal(&udpmutex);
		return(OK);
	}

	/* otherwise, we have a place to deliver it */
	uq = (struct upq *) so->so_pcb;
	if(so->so_recvqueue < 0) {	/* discard it */
		BufferFreeData(pep);
		UdpDiscards++;
		Semaphore_Signal(&udpmutex);
		return(OK);
	}
	QueueInsert(&uq->up_data, DataToQE(pep));
	so->so_recvqueue -= (pudp->u_len - U_HLEN);
	so->so_readable += (pudp->u_len - U_HLEN);
	UdpInDatagrams++;
	sowakeup(so, SOSEL_READ);
	Semaphore_Signal(&udpmutex);
#ifdef	RESCHED
	if(so->so_recvqueue < (MAX_UDP_RBYTES >> 1))
		TaskReschedule();
#endif
	return(OK);
}

void
udpnet2h(struct udp *u)
{
#ifdef	DUMB
	u->u_src = ntohs(u->u_src);
	u->u_dst = ntohs(u->u_dst);
#endif
	u->u_len = ntohs(u->u_len);
}

void
udph2net(struct udp *u)
{
	udpnet2h(u);
}


unsigned int
udpcksum(struct ip *pip)
{
	struct udp	*pudp = (struct udp *) pip->ip_data;
	unsigned	int	*psh;
	unsigned	long	sum;
	int		len	= ntohs(pudp->u_len);
	int		i;

	sum = 0;
	psh = (unsigned int *) pip->ip_src;
	for(i=0; i < IP_ALEN; i++)
		sum += *psh++;

	psh = (unsigned int *) pudp;
	sum += ntohs(pip->ip_proto + len);
	if(len & 0x1) {
		((char *) pudp)[len] = 0;
		len += 1;
	}

	len /= 2;
	for(i=0; i < len; i++)
		sum += *psh++;

	sum = (sum >> 16)  + (sum & 0xffff);
	if(sum > 65535)
		sum -= 65535;
/*
*	sum += (sum >> 16);
*/
	return (unsigned int)(~sum & 0xffff);
}

int
udpsend(IPaddr sip, IPaddr fip, unsigned int fport, unsigned int lport, struct ep *pep, int datalen, Bool docksum)
{
	struct	ip	*pip = (struct ip *) pep->ep_data;
	struct	udp	*pudp = (struct udp *) pip->ip_data;
 
	pip->ip_proto = IPT_UDP;
	pudp->u_src = lport;
	pudp->u_dst = fport;
	pudp->u_len = U_HLEN + datalen;
	pudp->u_cksum = 0;
	udph2net(pudp);
	memcpy(pip->ip_src, sip, IP_ALEN);
	memcpy(pip->ip_dst, fip, IP_ALEN);


#if	U_DO_CSUM
	if(docksum) {
		pudp->u_cksum = udpcksum(pip);
		if(pudp->u_cksum == 0)
			pudp->u_cksum = ~0;
	} 
#endif
	UdpOutDatagrams++;
	return(ipsend(fip, pep, U_HLEN+datalen));

}

/* v25func.c */

#include	<dos.h>
#include	"v25.h"

#define	COM2DEBUG	1

#ifndef	NVRAM_INT
#define	NVRAM_INT	0x7f
#endif

IDA   far		*IDA_PTR;		/* where the IDA is */
unsigned char far	*SFR_PTR;	/* and the SFR ptr */
IntVector  far		*INT_PTR;	/* ptr to interrupt table */

void interrupt
Null_Interrupt()
{
	FINT();
}

#if	COM2DEBUG
void
Com2Debug(char c)
{
	int	x;

	SFR_PTR[SFR_TXB1] = c;
	for( x = 0; x < 3000; x++)
		;
//		if(SFR_PTR[SFR_STIC1] & 0x80)
//			break;

}
#else
void
Com2Debug(char c)
{

}
#endif

int
Configure_V25()		/* setup most V25 registers */
{

	int	x;
	unsigned char far *vptr = MK_FP(0xffff, 0xf);	/* top of memory is the idb */

	disable();
#ifdef	CHANGE_IDBR
	*vptr = CHANGE_IDBR;
#endif

	INT_PTR = MK_FP(0,0);
	IDA_PTR = MK_FP(*vptr << 8 | 0xe0, 0x00);	/* 256 bytes below SFR */
	SFR_PTR = MK_FP(*vptr << 8 | 0xf0, 0);

	/* configure PRC with RAMEN and Time Base */
	SFR_PTR[SFR_PRC] = PRC_STARTUP_VALUE;

	SFR_PTR[SFR_EXIC2] = 0x47;
	SFR_PTR[SFR_EXIC1] = 0x47;	/* disable ints */

	SFR_PTR[SFR_P0] 	= 0;	/* 0 output value */
	SFR_PTR[SFR_PM0] 	= 0;	/* all output */
	SFR_PTR[SFR_PMC1]	= 0;	/* Port mode */

	SFR_PTR[SFR_P1] 	= 0;	/* 0 input value */
	SFR_PTR[SFR_PM1]	= 0xff;	/* all input */
	SFR_PTR[SFR_PMC1]	= 0x80;	/* P1.7 = Ready */

	SFR_PTR[SFR_P2]		= 0x13;	/* 	strobe high,
						auto paper feed high,
						DTR high
					*/
	SFR_PTR[SFR_PM2]	= 0;	/* all outputs */
	SFR_PTR[SFR_PMC2]	= 0;	/* all IO ports */

	SFR_PTR[SFR_WTCL]	= 0;	/* wait control low */
	SFR_PTR[SFR_WTCH]	= 0xc0;	/* wait control high */

	SFR_PTR[SFR_PMT]	= 0x8;	/* port t mode is vth x 1/2 */
	SFR_PTR[SFR_PT]		= 0x10;	/* unknown.. */

#ifdef	EPROM
	INT_PTR[INT_NMI] 	= Null_Interrupt;
#endif

	for(x = INT_IOTRAP; x < 255; x++) {
		if(x != NVRAM_INT)
			INT_PTR[x]	= Null_Interrupt;
	}

	SFR_PTR[SFR_TMC0]	= 0x50;	/* interval SCLK/128 */
	SFR_PTR[SFR_TM0L]	= TIMER_MODULUS & 0xff;
	SFR_PTR[SFR_TM0H]	= (TIMER_MODULUS >> 8);
	SFR_PTR[SFR_MD0L]	= TIMER_MODULUS & 0xff;
	SFR_PTR[SFR_MD0H]	= (TIMER_MODULUS >> 8);


	SFR_PTR[SFR_TMIC0]	= 0x40;	/* timer 0 interrupt control disable int */

	SFR_PTR[SFR_INTM]	= 0x14;	/* ES0 rising edge triggered */
	SFR_PTR[SFR_TBIC]	= 0xc7;	/* mask time base interrupts */

#if	COM2DEBUG
	SFR_PTR[SFR_SCM1] = 0xc9;	/* 8n1 */
	SFR_PTR[SFR_SCC1] = 2;
	SFR_PTR[SFR_BRG1] = 130;	/* 9600 ? */
	SFR_PTR[SFR_SEIC1] = 0x47;
	SFR_PTR[SFR_SRIC1] = 0x47;
	SFR_PTR[SFR_STIC1] = 0x47;
#endif
	outportb(NVRAM_DESELECT,0);

	enable();
	return(0);
}


/* vconsole.c - virtual console via file descriptor */

#include	"network.h"
#include	"sys/socket.h"
#include	"iosocket.h"
#include	"semaphore.h"

#include	"vconsole.h"


typedef	struct	_ioinfo {
	void	(*Out)(struct _ioinfo *I, char c);
	int	fd;
	char	*buff;
} IOINFO;


#define	XP(a)	(*I->Out)(I,(a))

static	int	console_fd;

int
vcsetconsole(int fd)
{
	int x = console_fd;

	console_fd = fd;
	return(x);
}

/*
 * Printn prints a number n in base b.
 * We don't use recursion to avoid deep kernel stacks.
 */
static
void
VPutchar(IOINFO *I, char c)
{
	if(I->fd == VCNOCONSOLE)
		return;
more:;
	sowrite(I->fd, &c, 1);
	if(c == '\n') {
		c = '\r';
		goto more;
	}

}

static
void
VStuffchar(IOINFO *I, char c)
{
	*I->buff++ = c;
}

static
void
VSocket(IOINFO *I, char c)
{
	sowrite(I->fd, &c, 1);
}

int
printn(IOINFO *I, unsigned n, int b, unsigned long l)
{
static	char	*nn="0123456789abcdef";
	char prbuf[11];
	register char *cp;
	int	width = 0;

	if(l && b == 10 && (long) l < 0) {
		XP('-');
		width++;
		l = (unsigned long) (-(long)l);
	}
	if (b == 10 && (int)n < 0) {
		XP('-');
		width++;
		n = (unsigned)(-(int)n);
	}
	if(b == -10)
		b = 10;
	cp = prbuf;
	if(l) {
		do {
			*cp++ = nn[l%b];
			l /= b;
		} while (l);
	}
	else {
		do {
			*cp++ = nn[n%b];
			n /= b;
		} while (n);
	}
	do {
		XP(*--cp);
		width++;
	}
	while (cp > prbuf);
	return(width);
}

static void
prf(IOINFO *I, char *fmt, unsigned int *adx, ...)
{
	register int b, c, i;
	char *s;
	int any;
	int	width;

look:
	while ((c = *fmt++) != '%') {
		if(c == '\0')
			return;
		XP(c);
	}
again:
	width = 0;
	c = *fmt++;
	/* THIS CODE IS VAX DEPENDENT IN HANDLING %l? AND %c */
	if(c == '-') {		/* left justify this, we do that anyway */
		c = *fmt++;
	}
	if(c == '*')  {
		width = *adx++;
		c = *fmt++;
		if(!c)
			return;
	}
	while(c >= '0' && c <= '9') {	/* a width */
		width *= 10;
		width += c - '0';
		c = *fmt++;
		if(!c)
			return;
	}
	switch (c) {

	case 'I' :
		s = *((char **) adx);
		adx++;
		for(i=0; i < 4; i++, s++) {
			unsigned x = (*s & 0xff);
			printn(I, (unsigned )x, -10, 0);
			if(i < 3)
				XP('.');
		}
		break;
	case 'l':
		goto again;
	case 'x': case 'X':
		b = 16;
		goto number;
	case 'U' :
		b = -10;
		goto dolong;
	case 'D' :
		b = 10;
dolong:;
		width -= printn(I, 0, b, *((long *)adx));
		adx++;
		break;
	case 'u':		/* what a joke */
		b = -10;
		goto number;
	case 'd' :
		b = 10;
		goto number;
	case 'o': case 'O':
		b = 8;
number:
		width -= printn(I, (unsigned )*adx, b, 0);
		if(b == -10)
			b = 10;
		break;
	case 'c':
		b = *adx;
		XP(*(adx) & 0xFF);
		break;
	case 's':
		s = *((char **) adx);
		adx++;
		while (c = *s++) {
			XP(c);
			width--;
		}
		break;

	case '%':
		XP('%');
		goto look;
	}
	adx++;
	while(width > 0) {
		XP(' ');
		width--;
	}
	goto look;
}

int
vcprintf(char *fmt, ...)
{
	IOINFO	I;

	I.Out = VPutchar;
	I.fd = console_fd;
	prf(&I, fmt, (unsigned int *) (&fmt + 1), 0);
	return(0);
}

int
sprintf(char *s, char *fmt, ...)
{
	IOINFO	I;

	I.buff = s;
	I.Out = VStuffchar;
	prf(&I, fmt, (unsigned int *) (&fmt + 1), 0);
	*I.buff++ = 0;
	return(strlen(s));
}

int
soprintf(int fd, char *fmt, ...)
{
	IOINFO	I;

	I.fd = fd;
	I.Out = VSocket;
	prf(&I, fmt, (unsigned int *) (&fmt + 1), 0);
	return(0);
}

int
vvcprintf(char *fmt, unsigned int *addr)
{
	IOINFO	I;

	if(console_fd == VCNOCONSOLE)
		return(-1);
	I.fd = console_fd;
	I.Out = VPutchar;
	prf(&I, fmt, addr, 0);
	return(0);
}

int
vsprintf(char *s, char *fmt, unsigned int *addr)
{
	IOINFO	I;

	I.buff = s;
	I.Out = VStuffchar;

	prf(&I, fmt, addr, 0);
	*I.buff++ = 0;
	return(strlen(s));
}


#pragma argsused
int
fprintf(int handle, char *fmt, ...)
{
	IOINFO	I;

	if(console_fd == VCNOCONSOLE)
		return(-1);
	I.fd = console_fd;
	I.Out = VPutchar;
	prf(&I, fmt, (unsigned int *) (&fmt + 1), 0);
	return(0);
}

int
printf(char *fmt, ...)
{
	IOINFO	I;

	if(console_fd == VCNOCONSOLE)
		return(-1);
	I.fd = console_fd;
	I.Out = VPutchar;
	prf(&I, fmt, (unsigned int *) (&fmt + 1), 0);
	return(0);
}

int
vcgets(char *buff, int len, int mode)
{
	int	offset = 0;

	if(console_fd == VCNOCONSOLE)  {
		if(mode & GETSMODE_NOBLOCK)
			return(-1);
		while(console_fd == VCNOCONSOLE)
			TaskReschedule();
	}

	while(1) {
		char	c;
		int	rc;

		rc = soread(console_fd, &c, 1);
		if(rc < 1)
			break;
		if(!rc)
			continue;
		switch(c) {
			case 8 :
			case 127 :
				if(offset) {
					vcprintf("\010 \010");
					offset--;
				}
				break;
			case 13 :
				buff[offset++] = 0;
				vcprintf("\n");
				goto done;
			default :;
				if(c > 31 && c < 127 && offset < len) {
					buff[offset++] = c;
					printf("%c",c);
				}
				break;
		}	// end switch
	}	// end while
done:;
	buff[offset++] = 0;
	return(offset);
}

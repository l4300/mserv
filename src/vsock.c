/* udp.c */
#define	STATIC
/* #define	OLDWAY */

#include	"network.h"
#include	"sys/errno.h"
#include	"sys/socket.h"
#include	"netinet/in.h"
#include	"task.h"
#include	"vsock.h"

int	vsock_proto(struct _socket *so, int pr, void *data);


int
vsock_init()
{
	soregister(AF_VSOCK, VSOCKET_LOOPBACK, 0, vsock_proto);
	return(0);
}

int
vsock_proto(struct _socket *so, int pr, void *data)
{
	int	rc = OK;
	int	len;
	struct	sockaddr_in	*sin = (struct sockaddr_in *) data;
	struct	sockaddr_in	*local = (struct sockaddr_in* ) &so->so_local;
	struct	_sendto		*sendto = (struct _sendto *) data;
	struct	_rwbuffer	*rwb = (struct _rwbuffer *) data;
	int			xlen;
	int			*ival;
	VSockInfo		*vs = (VSockInfo *) so->so_pcb;

	switch(pr) {
		case PR_OPEN :
			if(so->so_family != AF_VSOCK || so->so_protocol != VSOCKET_LOOPBACK)
				goto proerror;

			so->so_pcb = calloc(1, sizeof(VSockInfo));
			so->so_state = SOSTATE_BOUND;
			so->so_popt = 0;
			so->so_readable = 0;
			so->so_recvqueue = DEFAULT_VSOCK_SIZE;
			so->so_writeable = DEFAULT_VSOCK_SIZE;
			so->so_selectflags |= SOSEL_WRITE;
			so->so_opt |= SOOPT_NOBLOCK;

			break;
		case PR_BIND :	/* bind local address */
proerror:;
				SETERROR(EPROTO);
				rc = SYSERR;
				break;

	case PR_WRITE :
		len = sendto->s_len;
		if(len + so->so_readable > so->so_recvqueue)
			len = so->so_recvqueue - so->so_readable;

		if(!len) {
			SETERROR(ENOSR);
			return(SYSERR);
			break;		/* can't write anything */
		}
		xlen = len + so->so_readable;
		if(!vs->vs_buffer) {
			if(xlen < CHUNK_SIZE)
				xlen = CHUNK_SIZE;
			else
				xlen = ((xlen/CHUNK_SIZE)+1)*CHUNK_SIZE;

			vs->vs_buffer = malloc(xlen);
			if(!vs->vs_buffer) {
nomem:;
				SETERROR(ENOSR);
				rc = SYSERR;
				break;
			}
			vs->vs_bufflen = xlen;
		}
		else {
			if(xlen > vs->vs_bufflen) {
				void	*xy;

				xlen = ((xlen/CHUNK_SIZE)+1)*CHUNK_SIZE;
				xy = realloc(vs->vs_buffer, xlen);
				if(!xy) {
					goto nomem;
				}
				vs->vs_buffer = xy;
			}
		}
		memcpy( ((char *) vs->vs_buffer) + so->so_readable, sendto->s_msg, sendto->s_len);
		so->so_readable += sendto->s_len;
		so->so_writeable -= sendto->s_len;
		if(!so->so_writeable)
			so->so_selectflags &= ~SOSEL_WRITE;
		if(so->so_readable == sendto->s_len)
			sowakeup(so, SOSEL_READ);
		rc = sendto->s_len;
		break;

	case PR_READ :
		if(!vs->vs_buffer || !so->so_readable)
			break;

		len = so->so_readable;
		if(len > sendto->s_len)
			len = sendto->s_len;
		memcpy(sendto->s_msg, vs->vs_buffer, len);
		so->so_readable -= len;
		so->so_writeable += len;
		rc = len;
		if(so->so_readable) {	/* still stuff in the buffer */
			memcpy(vs->vs_buffer, ((char *) vs->vs_buffer) + len, so->so_readable);
		}
		else {
			so->so_selectflags &= ~SOSEL_READ;
		}
		if(so->so_writeable == len)
			sowakeup(so, SOSEL_WRITE);

		if(vs->vs_bufflen - so->so_writeable > CHUNK_SIZE) {
			len = ((so->so_readable/CHUNK_SIZE)+1)*CHUNK_SIZE;
			vs->vs_buffer = realloc(vs->vs_buffer, len);
			vs->vs_bufflen = len;
		}
		break;

	case PR_LISTEN :
	case PR_ACCEPT :
		goto proerror;

	case PR_CLOSE :
		if(vs) {
			if(vs->vs_buffer)
				free(vs->vs_buffer);
			free(vs);
		}
		so->so_pcb = NULL;
		break;
	case	PR_GETSOCKOPT :
			ival = (int *) sendto->s_msg;
			if(sendto->s_len < sizeof(int)) {
				SETERROR(EINVAL);
				rc = SYSERR;
				break;
			}
			switch(sendto->s_flags) {	/* option */
				void	**xx;

				case	VS_SOPT_READABLE :	/* bytes readable */
					*ival = so->so_readable;
					break;

				case	VS_SOPT_BUFFER :	/* get a ptr to the buffer */
					xx = (void **) sendto->s_msg;
					*xx = vs->vs_buffer;
					break;
				default :
					SETERROR(EINVAL);
					rc = SYSERR;
			}	/* end switch opt */
			break;

	case	PR_SETSOCKOPT :
			ival = (int *) sendto->s_msg;
			switch(sendto->s_flags) {	/* option */
				case	VS_SOPT_RECVQUEUE :	/* set recvq size */
					if(vs->vs_buffer) {
						goto proerror;
					}
					else {
						so->so_recvqueue = *ival;
						so->so_writeable = *ival;
					}
					break;
				default :
					SETERROR(EINVAL);
					rc = SYSERR;
			}	/* end switch opt */
			break;
	default :
		SETERROR(EPROTO);
		rc = SYSERR;
	}

	return(rc);

}


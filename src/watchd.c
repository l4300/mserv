/* watchdog.c */
#include	"network.h"

#include	<dos.h>
#include	"v25.h"
#include	"watchdog.h"
#include	"task.h"

#include	"alarm.h"

extern	Task	*Stattsk;

static	unsigned int	watchword;
int	quit;

void
WDSetSignal(unsigned int signalword)
{
	watchword = signalword;
}

#pragma argsused
int
watchdog(void *Parent, char *name, int argc, char **argv)
{
	struct	timeval 	t;
	unsigned int	loadval;
	int	blankcount;
	int	oncount;
	long	startticks;
	long	lastticks;

	watchword = SIG_F;
	loadval = watchword;
	blankcount = 0;
	oncount = 0;

	t.tv_sec = 0;
	t.tv_usec = WATCHDOG_USEC;
	startticks = Ticks(NULL) + (TICKS_PER_SEC * 2);
	lastticks = Ticks(NULL);

	while(1) {
		t.tv_usec = WATCHDOG_USEC;
		if(loadval & 1) {
			SFR_PTR[SFR_P2] &= LED_OFF_MASK;
			blankcount = 0;
			if(++oncount == 2) {
				t.tv_usec = WATCHDOG_USEC * 2;
			}
		}
		else {
			SFR_PTR[SFR_P2] |= LED_ON_MASK;
			blankcount++;
			oncount = 0;	
		}
		loadval >>= 1;
		if(blankcount > 4) {
			loadval = watchword;
			blankcount = 0;
		}

		alarm_sleep(&t, 0);
		if(Ticks(NULL) < startticks) {
			Task	*T = Task_List;
			long	tticks = Ticks() - lastticks;


			while(T) {
				Task	*XT = T->tsk_next;

				T->tsk_utilization = (T->tsk_utilization * 4 + (600 * T->tsk_total_ticks/tticks))/10;
				T->tsk_total_ticks = 0;
				T = XT;
			}
			lastticks = Ticks(NULL);
			startticks = lastticks + (TICKS_PER_SEC * 2);
		}
	}
}
